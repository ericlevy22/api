# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161215163709) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "account_archives", force: :cascade do |t|
    t.string   "email"
    t.string   "encrypted_password"
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count"
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "authentication_token"
    t.datetime "deleted_at"
    t.integer  "user_id"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.datetime "archived_at",            null: false
    t.string   "sanitized_email"
  end

  add_index "account_archives", ["confirmation_token"], name: "index_account_archives_on_confirmation_token", using: :btree
  add_index "account_archives", ["deleted_at"], name: "index_account_archives_on_deleted_at", using: :btree
  add_index "account_archives", ["email"], name: "index_account_archives_on_email", using: :btree
  add_index "account_archives", ["id"], name: "index_account_archives_on_id", using: :btree
  add_index "account_archives", ["reset_password_token"], name: "index_account_archives_on_reset_password_token", using: :btree
  add_index "account_archives", ["sanitized_email"], name: "index_account_archives_on_sanitized_email", unique: true, using: :btree
  add_index "account_archives", ["user_id"], name: "index_account_archives_on_user_id", using: :btree

  create_table "accounts", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "authentication_token"
    t.datetime "deleted_at"
    t.integer  "user_id"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "sanitized_email"
  end

  add_index "accounts", ["confirmation_token"], name: "index_accounts_on_confirmation_token", unique: true, using: :btree
  add_index "accounts", ["deleted_at"], name: "index_accounts_on_deleted_at", using: :btree
  add_index "accounts", ["email"], name: "index_accounts_on_email", unique: true, using: :btree
  add_index "accounts", ["id"], name: "index_accounts_on_id", unique: true, using: :btree
  add_index "accounts", ["reset_password_token"], name: "index_accounts_on_reset_password_token", unique: true, using: :btree
  add_index "accounts", ["sanitized_email"], name: "index_accounts_on_sanitized_email", unique: true, using: :btree
  add_index "accounts", ["user_id"], name: "index_accounts_on_user_id", using: :btree

  create_table "accounts_malls", force: :cascade do |t|
    t.integer  "account_id"
    t.integer  "mall_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "accounts_malls", ["account_id", "mall_id"], name: "index_accounts_malls_on_account_id_and_mall_id", unique: true, using: :btree
  add_index "accounts_malls", ["account_id"], name: "index_accounts_malls_on_account_id", using: :btree
  add_index "accounts_malls", ["mall_id"], name: "index_accounts_malls_on_mall_id", using: :btree

  create_table "accounts_roles", id: false, force: :cascade do |t|
    t.integer "account_id", null: false
    t.integer "role_id",    null: false
  end

  add_index "accounts_roles", ["account_id", "role_id"], name: "index_accounts_roles_on_account_id_and_role_id", using: :btree
  add_index "accounts_roles", ["role_id", "account_id"], name: "index_accounts_roles_on_role_id_and_account_id", using: :btree

  create_table "action_logs", force: :cascade do |t|
    t.integer  "account_id"
    t.string   "action"
    t.string   "model"
    t.integer  "model_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "affinities", force: :cascade do |t|
    t.integer  "heartable_id"
    t.string   "heartable_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
    t.integer  "user_id"
  end

  add_index "affinities", ["deleted_at"], name: "index_affinities_on_deleted_at", using: :btree
  add_index "affinities", ["user_id"], name: "index_affinities_on_user_id", using: :btree

  create_table "affinities_data", id: false, force: :cascade do |t|
    t.integer  "heartable_id"
    t.string   "heartable_type"
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
  end

  create_table "affinity_archives", force: :cascade do |t|
    t.integer  "heartable_id"
    t.string   "heartable_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
    t.integer  "user_id"
    t.datetime "archived_at",    null: false
  end

  add_index "affinity_archives", ["deleted_at"], name: "index_affinity_archives_on_deleted_at", using: :btree
  add_index "affinity_archives", ["user_id"], name: "index_affinity_archives_on_user_id", using: :btree

  create_table "api_execution_deletion", id: false, force: :cascade do |t|
    t.integer  "id",                             null: false
    t.string   "typeis",             limit: 50,  null: false
    t.integer  "typeis_id",                      null: false
    t.datetime "processed_date",                 null: false
    t.datetime "etl_processed_date"
    t.string   "typeis_id_value",    limit: 250
  end

  create_table "api_executions", id: false, force: :cascade do |t|
    t.integer  "execution_id", default: "nextval('api_executions_execution_id_seq'::regclass)", null: false
    t.string   "table_name",                                                                    null: false
    t.datetime "start_time",                                                                    null: false
    t.datetime "end_time"
    t.decimal  "duration"
  end

  create_table "api_executions_restricted", id: false, force: :cascade do |t|
    t.integer "id",     default: "nextval('api_executions_restricted_id_seq'::regclass)", null: false
    t.boolean "active", default: false
  end

  create_table "article_archives", force: :cascade do |t|
    t.string   "author",            limit: 512
    t.string   "title",             limit: 256
    t.text     "subtitle"
    t.string   "short_title"
    t.text     "teaser"
    t.text     "content"
    t.string   "seo_slug",          limit: 256
    t.datetime "display_at"
    t.datetime "end_at"
    t.integer  "display_order"
    t.boolean  "is_active"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
    t.string   "attribution"
    t.integer  "parent_id"
    t.string   "feature_location"
    t.datetime "archived_at",                   null: false
    t.text     "plaintext_content"
  end

  add_index "article_archives", ["deleted_at"], name: "index_article_archives_on_deleted_at", using: :btree
  add_index "article_archives", ["parent_id"], name: "index_article_archives_on_parent_id", using: :btree

  create_table "articles", force: :cascade do |t|
    t.string   "author",            limit: 512
    t.string   "title",             limit: 256
    t.text     "subtitle"
    t.string   "short_title"
    t.text     "teaser"
    t.text     "content"
    t.string   "seo_slug",          limit: 256
    t.datetime "display_at"
    t.datetime "end_at"
    t.integer  "display_order"
    t.boolean  "is_active",                     default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
    t.string   "attribution"
    t.integer  "parent_id"
    t.string   "feature_location"
    t.text     "plaintext_content"
  end

  add_index "articles", ["deleted_at"], name: "index_articles_on_deleted_at", using: :btree
  add_index "articles", ["parent_id"], name: "index_articles_on_parent_id", using: :btree

  create_table "articles_malls", id: false, force: :cascade do |t|
    t.integer "article_id"
    t.integer "mall_id"
  end

  add_index "articles_malls", ["article_id"], name: "index_articles_malls_on_article_id", using: :btree
  add_index "articles_malls", ["mall_id"], name: "index_articles_malls_on_mall_id", using: :btree

  create_table "articles_malls_data", id: false, force: :cascade do |t|
    t.integer "article_id"
    t.integer "malls",      array: true
  end

  create_table "articles_products", id: false, force: :cascade do |t|
    t.integer "article_id", null: false
    t.integer "product_id", null: false
  end

  add_index "articles_products", ["article_id", "product_id"], name: "index_articles_products_on_article_id_and_product_id", unique: true, using: :btree
  add_index "articles_products", ["article_id"], name: "index_articles_products_on_article_id", using: :btree
  add_index "articles_products", ["product_id"], name: "index_articles_products_on_product_id", using: :btree

  create_table "brand_archives", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.string   "seo_slug"
    t.string   "url"
    t.boolean  "is_approved"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
    t.datetime "archived_at", null: false
  end

  add_index "brand_archives", ["deleted_at"], name: "index_brand_archives_on_deleted_at", using: :btree

  create_table "brands", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.string   "seo_slug"
    t.string   "url"
    t.boolean  "is_approved", default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
  end

  add_index "brands", ["deleted_at"], name: "index_brands_on_deleted_at", using: :btree

  create_table "companies", force: :cascade do |t|
    t.string   "name"
    t.string   "nick_name"
    t.string   "seo_slug"
    t.string   "address"
    t.string   "city"
    t.string   "state",                    limit: 2
    t.string   "zip"
    t.string   "phone"
    t.string   "fax"
    t.string   "url"
    t.string   "test_url"
    t.boolean  "is_active",                          default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
    t.boolean  "allow_unconfirmed_access"
  end

  add_index "companies", ["deleted_at"], name: "index_companies_on_deleted_at", using: :btree

  create_table "company_archives", force: :cascade do |t|
    t.string   "name"
    t.string   "nick_name"
    t.string   "seo_slug"
    t.string   "address"
    t.string   "city"
    t.string   "state",                    limit: 2
    t.string   "zip"
    t.string   "phone"
    t.string   "fax"
    t.string   "url"
    t.string   "test_url"
    t.boolean  "is_active"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
    t.boolean  "allow_unconfirmed_access"
    t.datetime "archived_at",                        null: false
  end

  add_index "company_archives", ["deleted_at"], name: "index_company_archives_on_deleted_at", using: :btree

  create_table "contact_archives", force: :cascade do |t|
    t.string   "name"
    t.string   "title"
    t.string   "email"
    t.string   "phone"
    t.integer  "contactable_id"
    t.string   "contactable_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "legacy_id"
    t.string   "legacy_table"
    t.datetime "deleted_at"
    t.datetime "archived_at",      null: false
  end

  add_index "contact_archives", ["contactable_id", "legacy_id"], name: "index_contact_archives_on_contactable_id_and_legacy_id", using: :btree
  add_index "contact_archives", ["deleted_at"], name: "index_contact_archives_on_deleted_at", using: :btree

  create_table "contact_us_emails", force: :cascade do |t|
    t.string   "from"
    t.string   "to"
    t.string   "subject"
    t.text     "body"
    t.datetime "sent_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "contacts", force: :cascade do |t|
    t.string   "name"
    t.string   "title"
    t.string   "email"
    t.string   "phone"
    t.integer  "contactable_id"
    t.string   "contactable_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "legacy_id"
    t.string   "legacy_table"
    t.datetime "deleted_at"
  end

  add_index "contacts", ["contactable_id", "legacy_id"], name: "index_contacts_on_contactable_id_and_legacy_id", using: :btree
  add_index "contacts", ["deleted_at"], name: "index_contacts_on_deleted_at", using: :btree

  create_table "content_locations", force: :cascade do |t|
    t.integer  "store_id"
    t.integer  "locatable_id"
    t.string   "locatable_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "content_locations", ["locatable_id"], name: "index_content_locations_on_locatable_id", using: :btree
  add_index "content_locations", ["locatable_type"], name: "index_content_locations_on_locatable_type", using: :btree
  add_index "content_locations", ["store_id"], name: "index_content_locations_on_store_id", using: :btree

  create_table "content_locations_data", id: false, force: :cascade do |t|
    t.integer  "promo_id"
    t.integer  "retailer_id"
    t.integer  "mall_id"
    t.datetime "updated_at"
  end

  create_table "deal_archives", force: :cascade do |t|
    t.integer  "retailer_id"
    t.integer  "sales_type"
    t.integer  "valid_at_type_id"
    t.string   "title",                  limit: 100
    t.string   "seo_slug",               limit: 250
    t.string   "external_url",           limit: 250
    t.datetime "display_at"
    t.datetime "start_at"
    t.datetime "end_at"
    t.text     "description"
    t.text     "fine_print_description"
    t.boolean  "is_local"
    t.boolean  "is_featured"
    t.string   "cached_tag_list",        limit: 500
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "is_active"
    t.datetime "deleted_at"
    t.datetime "archived_at",                        null: false
  end

  add_index "deal_archives", ["cached_tag_list"], name: "index_deal_archives_on_cached_tag_list", using: :btree
  add_index "deal_archives", ["deleted_at"], name: "index_deal_archives_on_deleted_at", using: :btree
  add_index "deal_archives", ["retailer_id"], name: "index_deal_archives_on_retailer_id", using: :btree

  create_table "deals", force: :cascade do |t|
    t.integer  "retailer_id"
    t.integer  "sales_type"
    t.integer  "valid_at_type_id"
    t.string   "title",                  limit: 100
    t.string   "seo_slug",               limit: 250
    t.string   "external_url",           limit: 250
    t.datetime "display_at"
    t.datetime "start_at"
    t.datetime "end_at"
    t.text     "description"
    t.text     "fine_print_description"
    t.boolean  "is_local",                           default: false
    t.boolean  "is_featured",                        default: false
    t.string   "cached_tag_list",        limit: 500
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "is_active"
    t.datetime "deleted_at"
  end

  add_index "deals", ["cached_tag_list"], name: "index_deals_on_cached_tag_list", using: :btree
  add_index "deals", ["deleted_at"], name: "index_deals_on_deleted_at", using: :btree
  add_index "deals", ["retailer_id"], name: "index_deals_on_retailer_id", using: :btree

  create_table "deals_store_archives", force: :cascade do |t|
    t.integer  "deal_id"
    t.integer  "store_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "archived_at", null: false
  end

  add_index "deals_store_archives", ["deal_id", "store_id"], name: "index_deals_store_archives_on_deal_id_and_store_id", using: :btree
  add_index "deals_store_archives", ["deal_id"], name: "index_deals_store_archives_on_deal_id", using: :btree
  add_index "deals_store_archives", ["store_id"], name: "index_deals_store_archives_on_store_id", using: :btree

  create_table "deals_stores", force: :cascade do |t|
    t.integer  "deal_id"
    t.integer  "store_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "deals_stores", ["deal_id", "store_id"], name: "index_deals_stores_on_deal_id_and_store_id", unique: true, using: :btree
  add_index "deals_stores", ["deal_id"], name: "index_deals_stores_on_deal_id", using: :btree
  add_index "deals_stores", ["store_id"], name: "index_deals_stores_on_store_id", using: :btree

  create_table "epusers_raw", id: false, force: :cascade do |t|
    t.integer  "epid"
    t.string   "email"
    t.datetime "date"
    t.json     "properties"
    t.string   "source"
    t.integer  "identity_id"
    t.integer  "id",          default: "nextval('epusers_raw_id_seq'::regclass)", null: false
  end

  create_table "etl_logs", force: :cascade do |t|
    t.string   "resource"
    t.datetime "extraction_started_at"
    t.datetime "extraction_finished_at"
    t.integer  "extraction_count"
    t.integer  "load_count"
    t.string   "extraction_duration"
    t.datetime "load_started_at"
    t.datetime "load_finished_at"
    t.string   "load_duration"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "event_archives", force: :cascade do |t|
    t.integer  "mall_id"
    t.string   "title",          limit: 100
    t.string   "seo_slug",       limit: 250
    t.text     "description"
    t.datetime "display_at"
    t.datetime "start_at"
    t.datetime "end_at"
    t.boolean  "is_active"
    t.boolean  "is_featured"
    t.boolean  "is_events_page"
    t.integer  "pin_order"
    t.text     "short_text"
    t.text     "date_text"
    t.text     "link_text"
    t.string   "link_url",       limit: 250
    t.boolean  "is_new_window"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
    t.string   "location"
    t.datetime "archived_at",                null: false
  end

  create_table "events", force: :cascade do |t|
    t.integer  "mall_id"
    t.string   "title",          limit: 100
    t.string   "seo_slug",       limit: 250
    t.text     "description"
    t.datetime "display_at"
    t.datetime "start_at"
    t.datetime "end_at"
    t.boolean  "is_active",                  default: false
    t.boolean  "is_featured",                default: false
    t.boolean  "is_events_page",             default: false
    t.integer  "pin_order",                  default: 0
    t.text     "short_text"
    t.text     "date_text"
    t.text     "link_text"
    t.string   "link_url",       limit: 250
    t.boolean  "is_new_window",              default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
    t.string   "location"
  end

  add_index "events", ["deleted_at"], name: "index_events_on_deleted_at", using: :btree
  add_index "events", ["start_at", "end_at", "display_at", "id"], name: "index_events_on_start_at_and_end_at_and_display_at_and_id", unique: true, using: :btree

  create_table "groupings", force: :cascade do |t|
    t.integer  "group_id"
    t.integer  "groupable_id"
    t.string   "groupable_type"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "groupings", ["group_id"], name: "index_groupings_on_group_id", using: :btree
  add_index "groupings", ["groupable_type", "groupable_id"], name: "index_groupings_on_groupable_type_and_groupable_id", using: :btree

  create_table "groups", force: :cascade do |t|
    t.string   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "groups", ["name"], name: "index_groups_on_name", unique: true, using: :btree

  create_table "hours", force: :cascade do |t|
    t.integer  "mall_id"
    t.string   "sunday"
    t.string   "monday"
    t.string   "tuesday"
    t.string   "wednesday"
    t.string   "thursday"
    t.string   "friday"
    t.string   "saturday"
    t.string   "promo_message"
    t.string   "description"
    t.datetime "start_at"
    t.datetime "end_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "is_default"
  end

  add_index "hours", ["mall_id"], name: "index_hours_on_mall_id", using: :btree

  create_table "identities", force: :cascade do |t|
    t.string   "identity_value"
    t.string   "identity_type"
    t.integer  "user_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.datetime "deleted_at"
    t.string   "confirmation_token"
    t.datetime "confirmation_sent_at"
    t.datetime "confirmed_at"
  end

  add_index "identities", ["confirmation_token"], name: "index_identities_on_confirmation_token", unique: true, using: :btree
  add_index "identities", ["deleted_at"], name: "index_identities_on_deleted_at", using: :btree
  add_index "identities", ["identity_type", "identity_value"], name: "index_identities_on_identity_type_and_identity_value", unique: true, using: :btree
  add_index "identities", ["user_id"], name: "index_identities_on_user_id", using: :btree

  create_table "identity_archives", force: :cascade do |t|
    t.string   "identity_value"
    t.string   "identity_type"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
    t.string   "confirmation_token"
    t.datetime "confirmation_sent_at"
    t.datetime "confirmed_at"
    t.datetime "archived_at",          null: false
  end

  add_index "identity_archives", ["identity_type", "identity_value"], name: "index_identity_archives_on_identity_type_and_identity_value", using: :btree
  add_index "identity_archives", ["user_id"], name: "index_identity_archives_on_user_id", using: :btree

  create_table "ifeature_archives", force: :cascade do |t|
    t.integer  "mall_id"
    t.integer  "company_id"
    t.string   "seo_slug"
    t.integer  "pin_order"
    t.string   "headline",      limit: 200
    t.string   "tag_line",      limit: 200
    t.string   "sub_heading",   limit: 50
    t.text     "description"
    t.datetime "start_at"
    t.datetime "end_at"
    t.string   "url"
    t.boolean  "is_active"
    t.boolean  "is_static"
    t.boolean  "is_headline"
    t.boolean  "is_new_window"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
    t.datetime "archived_at",               null: false
  end

  add_index "ifeature_archives", ["deleted_at"], name: "index_ifeature_archives_on_deleted_at", using: :btree

  create_table "ifeatures", force: :cascade do |t|
    t.integer  "mall_id"
    t.integer  "company_id"
    t.string   "seo_slug"
    t.integer  "pin_order",                 default: 0
    t.string   "headline",      limit: 200
    t.string   "tag_line",      limit: 200
    t.string   "sub_heading",   limit: 50
    t.text     "description"
    t.datetime "start_at"
    t.datetime "end_at"
    t.string   "url"
    t.boolean  "is_active",                 default: false
    t.boolean  "is_static",                 default: false
    t.boolean  "is_headline",               default: false
    t.boolean  "is_new_window",             default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
  end

  add_index "ifeatures", ["deleted_at"], name: "index_ifeatures_on_deleted_at", using: :btree

  create_table "image_archives", force: :cascade do |t|
    t.string   "url"
    t.string   "size_parameter"
    t.integer  "imageable_id"
    t.string   "imageable_type"
    t.string   "cached_tag_list", limit: 500
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
    t.datetime "archived_at",                 null: false
  end

  add_index "image_archives", ["deleted_at"], name: "index_image_archives_on_deleted_at", using: :btree
  add_index "image_archives", ["id"], name: "index_image_archives_on_id", using: :btree
  add_index "image_archives", ["imageable_id"], name: "index_image_archives_on_imageable_id", using: :btree
  add_index "image_archives", ["imageable_type"], name: "index_image_archives_on_imageable_type", using: :btree

  create_table "images", force: :cascade do |t|
    t.string   "url"
    t.string   "size_parameter"
    t.integer  "imageable_id"
    t.string   "imageable_type"
    t.string   "cached_tag_list", limit: 500
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
  end

  add_index "images", ["deleted_at"], name: "index_images_on_deleted_at", using: :btree
  add_index "images", ["id"], name: "index_images_on_id", using: :btree
  add_index "images", ["imageable_id"], name: "index_images_on_imageable_id", using: :btree
  add_index "images", ["imageable_type"], name: "index_images_on_imageable_type", using: :btree

  create_table "job_listing_archives", force: :cascade do |t|
    t.string   "title"
    t.string   "start_date_text"
    t.string   "hours_per_week"
    t.string   "number_of_openings"
    t.boolean  "is_active"
    t.boolean  "is_full_time"
    t.boolean  "is_apply_online"
    t.string   "manager_name"
    t.string   "fax_number"
    t.string   "email"
    t.string   "salary"
    t.text     "required_experience"
    t.text     "key_responsibilities"
    t.text     "special_information"
    t.text     "description"
    t.datetime "display_at"
    t.datetime "end_at"
    t.integer  "store_id"
    t.integer  "legacy_requisition_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
    t.string   "seo_slug"
    t.datetime "archived_at",           null: false
  end

  add_index "job_listing_archives", ["deleted_at"], name: "index_job_listing_archives_on_deleted_at", using: :btree

  create_table "job_listings", force: :cascade do |t|
    t.string   "title"
    t.string   "start_date_text"
    t.string   "hours_per_week"
    t.string   "number_of_openings"
    t.boolean  "is_active"
    t.boolean  "is_full_time"
    t.boolean  "is_apply_online"
    t.string   "manager_name"
    t.string   "fax_number"
    t.string   "email"
    t.string   "salary"
    t.text     "required_experience"
    t.text     "key_responsibilities"
    t.text     "special_information"
    t.text     "description"
    t.datetime "display_at"
    t.datetime "end_at"
    t.integer  "store_id"
    t.integer  "legacy_requisition_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
    t.string   "seo_slug"
  end

  add_index "job_listings", ["deleted_at"], name: "index_job_listings_on_deleted_at", using: :btree

  create_table "keywordings", force: :cascade do |t|
    t.integer  "keyword_id"
    t.integer  "keywordable_id"
    t.string   "keywordable_type"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "keywordings", ["keyword_id"], name: "index_keywordings_on_keyword_id", using: :btree
  add_index "keywordings", ["keywordable_type", "keywordable_id"], name: "index_keywordings_on_keywordable_type_and_keywordable_id", using: :btree

  create_table "keywords", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "keywords", ["name"], name: "index_keywords_on_name", using: :btree

  create_table "location_archives", force: :cascade do |t|
    t.float    "latitude"
    t.float    "longitude"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "time_zone"
    t.datetime "deleted_at"
    t.datetime "archived_at", null: false
  end

  add_index "location_archives", ["deleted_at"], name: "index_location_archives_on_deleted_at", using: :btree

  create_table "locations", force: :cascade do |t|
    t.float    "latitude"
    t.float    "longitude"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "time_zone"
    t.datetime "deleted_at"
  end

  add_index "locations", ["deleted_at"], name: "index_locations_on_deleted_at", using: :btree

  create_table "mall_archives", force: :cascade do |t|
    t.integer  "company_id"
    t.integer  "location_id"
    t.string   "name"
    t.string   "sort_name"
    t.string   "nick_name"
    t.string   "seo_slug"
    t.string   "address"
    t.string   "city"
    t.string   "state"
    t.string   "zip"
    t.string   "phone"
    t.string   "url"
    t.string   "email"
    t.boolean  "is_live"
    t.boolean  "is_development"
    t.boolean  "is_archived"
    t.text     "hours_text"
    t.string   "site_type"
    t.string   "lang_code"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "exclude_national_deals"
    t.datetime "deleted_at"
    t.string   "contact_us_email"
    t.string   "description"
    t.datetime "archived_at",                            null: false
    t.string   "guest_services_description", limit: 100
  end

  add_index "mall_archives", ["company_id"], name: "index_mall_archives_on_company_id", using: :btree
  add_index "mall_archives", ["deleted_at"], name: "index_mall_archives_on_deleted_at", using: :btree
  add_index "mall_archives", ["location_id"], name: "index_mall_archives_on_location_id", using: :btree

  create_table "mall_message_archives", force: :cascade do |t|
    t.integer  "mall_id"
    t.text     "description"
    t.datetime "start_at"
    t.datetime "end_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "message_type"
    t.datetime "deleted_at"
    t.datetime "archived_at",  null: false
  end

  add_index "mall_message_archives", ["deleted_at"], name: "index_mall_message_archives_on_deleted_at", using: :btree
  add_index "mall_message_archives", ["mall_id"], name: "index_mall_message_archives_on_mall_id", using: :btree

  create_table "mall_messages", force: :cascade do |t|
    t.integer  "mall_id"
    t.text     "description"
    t.datetime "start_at"
    t.datetime "end_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "message_type"
    t.datetime "deleted_at"
  end

  add_index "mall_messages", ["deleted_at"], name: "index_mall_messages_on_deleted_at", using: :btree
  add_index "mall_messages", ["mall_id"], name: "index_mall_messages_on_mall_id", using: :btree

  create_table "malls", force: :cascade do |t|
    t.integer  "company_id"
    t.integer  "location_id"
    t.string   "name"
    t.string   "sort_name"
    t.string   "nick_name"
    t.string   "seo_slug"
    t.string   "address"
    t.string   "city"
    t.string   "state"
    t.string   "zip"
    t.string   "phone"
    t.string   "url"
    t.string   "email"
    t.boolean  "is_live",                                default: false, null: false
    t.boolean  "is_development",                         default: false, null: false
    t.boolean  "is_archived",                            default: false, null: false
    t.text     "hours_text"
    t.string   "site_type"
    t.string   "lang_code"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "exclude_national_deals",                 default: false
    t.datetime "deleted_at"
    t.string   "contact_us_email"
    t.string   "description"
    t.string   "guest_services_description", limit: 100
  end

  add_index "malls", ["company_id"], name: "index_malls_on_company_id", using: :btree
  add_index "malls", ["deleted_at"], name: "index_malls_on_deleted_at", using: :btree
  add_index "malls", ["location_id"], name: "index_malls_on_location_id", using: :btree

  create_table "membership_archives", force: :cascade do |t|
    t.integer  "program_id"
    t.json     "properties"
    t.integer  "identity_id"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "source_id"
    t.integer  "legacy_id"
    t.string   "email"
    t.datetime "archived_at", null: false
  end

  add_index "membership_archives", ["deleted_at"], name: "index_membership_archives_on_deleted_at", using: :btree
  add_index "membership_archives", ["identity_id"], name: "index_membership_archives_on_identity_id", using: :btree
  add_index "membership_archives", ["program_id"], name: "index_membership_archives_on_program_id", using: :btree

  create_table "memberships", force: :cascade do |t|
    t.integer  "program_id"
    t.json     "properties"
    t.integer  "identity_id"
    t.datetime "deleted_at"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "source_id"
    t.integer  "legacy_id"
    t.string   "email"
  end

  add_index "memberships", ["deleted_at"], name: "index_memberships_on_deleted_at", using: :btree
  add_index "memberships", ["identity_id"], name: "index_memberships_on_identity_id", using: :btree
  add_index "memberships", ["program_id"], name: "index_memberships_on_program_id", using: :btree

  create_table "newsletterusers_raw", id: false, force: :cascade do |t|
    t.string   "email",                                                                            null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "smsphone"
    t.string   "facebook"
    t.string   "json_data"
    t.string   "source"
    t.integer  "slot",                                                                             null: false
    t.integer  "account_id"
    t.integer  "id",                   default: "nextval('newsletterusers_raw_id_seq'::regclass)", null: false
    t.integer  "insertable", limit: 2
    t.integer  "user_id"
  end

  add_index "newsletterusers_raw", ["email", "slot"], name: "a", using: :btree
  add_index "newsletterusers_raw", ["email"], name: "newsletterusers_raw_email", using: :btree
  add_index "newsletterusers_raw", ["slot"], name: "newsletterusers_raw_slot", using: :btree

  create_table "newsletterusers_raw_insertable", id: false, force: :cascade do |t|
    t.integer "slot"
    t.integer "users_id",    default: "nextval('users_id_seq'::regclass)",    null: false
    t.integer "accounts_id", default: "nextval('accounts_id_seq'::regclass)", null: false
  end

  add_index "newsletterusers_raw_insertable", ["slot"], name: "newsletterusers_raw_insertable_slot", using: :btree

  create_table "newsletterusers_raw_merging", id: false, force: :cascade do |t|
    t.integer "slot",    null: false
    t.integer "user_id", null: false
    t.integer "merged",  null: false
  end

  add_index "newsletterusers_raw_merging", ["slot"], name: "newsletterusers_raw_merging_slot", using: :btree
  add_index "newsletterusers_raw_merging", ["user_id"], name: "newsletterusers_raw_merging_user_id", using: :btree

  create_table "press_release_archives", force: :cascade do |t|
    t.string   "name"
    t.string   "phone"
    t.string   "email"
    t.string   "seo_slug"
    t.string   "headline"
    t.string   "tag_line"
    t.text     "content"
    t.integer  "mall_id"
    t.boolean  "is_active"
    t.integer  "pr_type"
    t.datetime "display_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
    t.datetime "archived_at", null: false
  end

  add_index "press_release_archives", ["deleted_at"], name: "index_press_release_archives_on_deleted_at", using: :btree

  create_table "press_releases", force: :cascade do |t|
    t.string   "name"
    t.string   "phone"
    t.string   "email"
    t.string   "seo_slug"
    t.string   "headline"
    t.string   "tag_line"
    t.text     "content"
    t.integer  "mall_id"
    t.boolean  "is_active"
    t.integer  "pr_type"
    t.datetime "display_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
  end

  add_index "press_releases", ["deleted_at"], name: "index_press_releases_on_deleted_at", using: :btree

  create_table "product_archives", force: :cascade do |t|
    t.integer  "brand_id"
    t.string   "name"
    t.string   "seo_slug"
    t.string   "image_file_name"
    t.text     "description"
    t.string   "price"
    t.string   "sale_price"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
    t.datetime "archived_at",     null: false
    t.integer  "retailer_id"
    t.datetime "expired_at"
  end

  add_index "product_archives", ["brand_id"], name: "index_product_archives_on_brand_id", using: :btree
  add_index "product_archives", ["deleted_at"], name: "index_product_archives_on_deleted_at", using: :btree

  create_table "products", force: :cascade do |t|
    t.integer  "brand_id"
    t.string   "name"
    t.string   "seo_slug"
    t.string   "image_file_name"
    t.text     "description"
    t.string   "price"
    t.string   "sale_price"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
    t.integer  "retailer_id"
    t.datetime "expired_at"
  end

  add_index "products", ["brand_id"], name: "index_products_on_brand_id", using: :btree
  add_index "products", ["deleted_at"], name: "index_products_on_deleted_at", using: :btree

  create_table "products_stores", force: :cascade do |t|
    t.integer  "product_id"
    t.integer  "store_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "products_stores", ["product_id", "store_id"], name: "index_products_stores_on_product_id_and_store_id", unique: true, using: :btree
  add_index "products_stores", ["product_id"], name: "index_products_stores_on_product_id", using: :btree
  add_index "products_stores", ["store_id"], name: "index_products_stores_on_store_id", using: :btree

  create_table "program_archives", force: :cascade do |t|
    t.json     "properties"
    t.integer  "owner_id"
    t.string   "owner_type"
    t.boolean  "is_internal_only"
    t.boolean  "is_default_signup_program"
    t.integer  "program_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
    t.boolean  "is_subscribable"
    t.boolean  "is_active"
    t.integer  "legacy_id"
    t.datetime "archived_at",               null: false
  end

  add_index "program_archives", ["deleted_at"], name: "index_program_archives_on_deleted_at", using: :btree
  add_index "program_archives", ["owner_type", "owner_id"], name: "index_program_archives_on_owner_type_and_owner_id", using: :btree

  create_table "programs", force: :cascade do |t|
    t.json     "properties"
    t.integer  "owner_id"
    t.string   "owner_type"
    t.boolean  "is_internal_only"
    t.boolean  "is_default_signup_program"
    t.integer  "program_type"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.datetime "deleted_at"
    t.boolean  "is_subscribable"
    t.boolean  "is_active"
    t.integer  "legacy_id"
  end

  add_index "programs", ["deleted_at"], name: "index_programs_on_deleted_at", using: :btree
  add_index "programs", ["owner_type", "owner_id"], name: "index_programs_on_owner_type_and_owner_id", using: :btree

  create_table "retailer_archives", force: :cascade do |t|
    t.string   "name"
    t.string   "sort_name"
    t.string   "seo_slug"
    t.string   "description", limit: 3000
    t.string   "nick_name"
    t.string   "phone"
    t.string   "email"
    t.string   "url",         limit: 300
    t.string   "store_text",  limit: 2000
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "is_active"
    t.datetime "deleted_at"
    t.string   "url_text",    limit: 300
    t.datetime "archived_at",              null: false
  end

  create_table "retailers", force: :cascade do |t|
    t.string   "name"
    t.string   "sort_name"
    t.string   "seo_slug"
    t.string   "description", limit: 3000
    t.string   "nick_name"
    t.string   "phone"
    t.string   "email"
    t.string   "url",         limit: 300
    t.string   "store_text",  limit: 2000
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "is_active"
    t.datetime "deleted_at"
    t.string   "url_text",    limit: 300
  end

  add_index "retailers", ["deleted_at"], name: "index_retailers_on_deleted_at", using: :btree

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "social_feed_archives", force: :cascade do |t|
    t.integer  "social_network_id"
    t.string   "sociable_type"
    t.integer  "sociable_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "is_active"
    t.datetime "deleted_at"
    t.json     "properties"
    t.datetime "archived_at",       null: false
  end

  add_index "social_feed_archives", ["deleted_at"], name: "index_social_feed_archives_on_deleted_at", using: :btree

  create_table "social_feeds", force: :cascade do |t|
    t.integer  "social_network_id"
    t.string   "sociable_type"
    t.integer  "sociable_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "is_active"
    t.datetime "deleted_at"
    t.json     "properties"
  end

  add_index "social_feeds", ["deleted_at"], name: "index_social_feeds_on_deleted_at", using: :btree

  create_table "social_network_archives", force: :cascade do |t|
    t.string   "name"
    t.string   "url"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "is_active"
    t.datetime "deleted_at"
    t.datetime "archived_at", null: false
  end

  add_index "social_network_archives", ["deleted_at"], name: "index_social_network_archives_on_deleted_at", using: :btree

  create_table "social_networks", force: :cascade do |t|
    t.string   "name"
    t.string   "url"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "is_active"
    t.datetime "deleted_at"
  end

  add_index "social_networks", ["deleted_at"], name: "index_social_networks_on_deleted_at", using: :btree

  create_table "store_archives", force: :cascade do |t|
    t.integer  "mall_id"
    t.integer  "retailer_id"
    t.integer  "location_id"
    t.string   "name"
    t.string   "sort_name"
    t.string   "seo_slug",                limit: 250
    t.string   "address"
    t.string   "city"
    t.string   "state"
    t.string   "zip"
    t.string   "phone"
    t.string   "hours"
    t.string   "directory_tags",          limit: 250
    t.string   "email"
    t.string   "fax"
    t.text     "description"
    t.text     "job_text"
    t.boolean  "is_featured"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "open_at"
    t.datetime "close_at"
    t.string   "tenant_location"
    t.datetime "deleted_at"
    t.string   "url_text",                limit: 300
    t.string   "url",                     limit: 300
    t.string   "tenant_location_map_key"
    t.boolean  "exclude_national_deals"
    t.datetime "opening_date"
    t.string   "opening_text"
    t.datetime "archived_at",                         null: false
  end

  add_index "store_archives", ["location_id"], name: "index_store_archives_on_location_id", using: :btree
  add_index "store_archives", ["mall_id"], name: "index_store_archives_on_mall_id", using: :btree
  add_index "store_archives", ["retailer_id"], name: "index_store_archives_on_retailer_id", using: :btree

  create_table "stores", force: :cascade do |t|
    t.integer  "mall_id"
    t.integer  "retailer_id"
    t.integer  "location_id"
    t.string   "name"
    t.string   "sort_name"
    t.string   "seo_slug",                  limit: 250
    t.string   "address"
    t.string   "city"
    t.string   "state"
    t.string   "zip"
    t.string   "phone"
    t.string   "hours"
    t.string   "directory_tags",            limit: 250
    t.string   "email"
    t.string   "fax"
    t.text     "description"
    t.text     "job_text"
    t.boolean  "is_featured",                           default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "open_at"
    t.datetime "close_at"
    t.string   "tenant_location"
    t.datetime "deleted_at"
    t.string   "url_text",                  limit: 300
    t.string   "url",                       limit: 300
    t.string   "tenant_location_map_key"
    t.boolean  "exclude_national_deals",                default: false
    t.datetime "opening_date"
    t.string   "opening_text"
    t.boolean  "exclude_national_products",             default: false
  end

  add_index "stores", ["deleted_at"], name: "index_stores_on_deleted_at", using: :btree
  add_index "stores", ["location_id"], name: "index_stores_on_location_id", using: :btree
  add_index "stores", ["mall_id"], name: "index_stores_on_mall_id", using: :btree
  add_index "stores", ["retailer_id"], name: "index_stores_on_retailer_id", using: :btree

  create_table "tag_archives", force: :cascade do |t|
    t.string   "name"
    t.integer  "taggings_count"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "archived_at",    null: false
  end

  add_index "tag_archives", ["name"], name: "index_tag_archives_on_name", using: :btree

  create_table "tagging_archives", force: :cascade do |t|
    t.integer  "tag_id"
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.integer  "tagger_id"
    t.string   "tagger_type"
    t.string   "context",       limit: 128
    t.datetime "created_at"
    t.datetime "deleted_at"
    t.datetime "archived_at",               null: false
  end

  add_index "tagging_archives", ["taggable_id", "taggable_type", "context"], name: "index_taggings_archive_on_taggable_and_context", using: :btree
  add_index "tagging_archives", ["taggable_id"], name: "index_tagging_archives_on_taggable_id", using: :btree

  create_table "taggings", force: :cascade do |t|
    t.integer  "tag_id"
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.datetime "created_at"
    t.datetime "deleted_at"
  end

  add_index "taggings", ["deleted_at"], name: "index_taggings_on_deleted_at", using: :btree
  add_index "taggings", ["taggable_id"], name: "index_taggings_on_taggable_id", using: :btree

  create_table "tags", force: :cascade do |t|
    t.string   "name"
    t.datetime "deleted_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "tags", ["deleted_at"], name: "index_tags_on_deleted_at", using: :btree
  add_index "tags", ["name"], name: "index_tags_on_name", unique: true, using: :btree

  create_table "theaters", force: :cascade do |t|
    t.integer  "store_id"
    t.integer  "mall_id"
    t.string   "external_key"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "theaters", ["external_key"], name: "index_theaters_on_external_key", using: :btree
  add_index "theaters", ["mall_id"], name: "index_theaters_on_mall_id", using: :btree
  add_index "theaters", ["store_id"], name: "index_theaters_on_store_id", using: :btree

  create_table "tracking_pixel_archives", force: :cascade do |t|
    t.string   "url"
    t.integer  "trackable_id"
    t.string   "trackable_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
    t.datetime "archived_at",    null: false
  end

  add_index "tracking_pixel_archives", ["deleted_at"], name: "index_tracking_pixel_archives_on_deleted_at", using: :btree

  create_table "tracking_pixels", force: :cascade do |t|
    t.string   "url"
    t.integer  "trackable_id"
    t.string   "trackable_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
  end

  add_index "tracking_pixels", ["deleted_at"], name: "index_tracking_pixels_on_deleted_at", using: :btree

  create_table "user_archives", force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.json     "properties"
    t.datetime "deleted_at"
    t.datetime "archived_at", null: false
  end

  add_index "user_archives", ["deleted_at"], name: "index_user_archives_on_deleted_at", using: :btree

  create_table "users", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.json     "properties"
    t.datetime "deleted_at"
  end

  add_index "users", ["deleted_at"], name: "index_users_on_deleted_at", using: :btree

  add_foreign_key "accounts_malls", "accounts"
  add_foreign_key "accounts_malls", "malls"
  add_foreign_key "affinities", "users"
  add_foreign_key "deals_stores", "deals"
  add_foreign_key "deals_stores", "stores"
  add_foreign_key "groupings", "groups"
  add_foreign_key "keywordings", "keywords"
  add_foreign_key "memberships", "identities"
  add_foreign_key "memberships", "programs"
  add_foreign_key "products", "retailers"
  add_foreign_key "products_stores", "products"
  add_foreign_key "products_stores", "stores"
  add_foreign_key "theaters", "malls"
  add_foreign_key "theaters", "stores"
end
