class CreateHours < ActiveRecord::Migration
  def change
    create_table :hours do |t|
      t.integer  :mall_id
      t.string   :sunday
      t.string   :monday
      t.string   :tuesday
      t.string   :wednesday
      t.string   :thursday
      t.string   :friday
      t.string   :saturday
      t.string   :promo_message
      t.string   :description
      t.datetime :start_at
      t.datetime :end_at
      t.timestamps
    end

    add_index :hours, :mall_id
  end
end
