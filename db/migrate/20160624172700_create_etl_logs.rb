class CreateEtlLogs < ActiveRecord::Migration
  def change
    create_table :etl_logs do |t|
      t.string :resource
      t.timestamp :extraction_started_at
      t.timestamp :extraction_finished_at
      t.string :extraction_duration
      t.integer :extraction_count
      t.timestamp :load_started_at
      t.timestamp :load_finished_at
      t.string :load_duration
      t.integer :load_count

      t.timestamps null: false
    end
  end
end
