class AddSqlColumnNameToTags < ActiveRecord::Migration
  def change
    add_column :tags, :sql_column_name, :string
  end
end
