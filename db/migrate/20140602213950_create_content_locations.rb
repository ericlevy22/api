class CreateContentLocations < ActiveRecord::Migration
  def change
    create_table :content_locations do |t|
      t.integer :store_id
      t.integer :locatable_id
      t.string  :locatable_type
      t.timestamps
    end

    add_index :content_locations, :store_id
    add_index :content_locations, :locatable_id
    add_index :content_locations, :locatable_type
  end
end
