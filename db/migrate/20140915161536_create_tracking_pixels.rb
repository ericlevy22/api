class CreateTrackingPixels < ActiveRecord::Migration
  def change
    create_table :tracking_pixels do |t|
      t.string   :url
      t.integer  :trackable_id
      t.string   :trackable_type
      t.timestamps
    end
  end
end
