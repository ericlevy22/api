class AddDeletedAtToMallMessages < ActiveRecord::Migration
  def change
    add_column :mall_messages, :deleted_at, :datetime
    add_index :mall_messages, :deleted_at
  end
end
