class AddDeletedAtToIfeatures < ActiveRecord::Migration
  def change
    add_column :ifeatures, :deleted_at, :datetime
    add_index :ifeatures, :deleted_at
  end
end
