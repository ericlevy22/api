class CreateContactUsEmails < ActiveRecord::Migration
  def change
    create_table :contact_us_emails do |t|
      t.string :from
      t.string :to
      t.string :subject
      t.text :body
      t.datetime :sent_at
      t.timestamps null: false
    end
  end
end
