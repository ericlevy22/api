class CreateContexts < ActiveRecord::Migration
  def change
    create_table :contexts do |t|
      t.references :user, index: true
      t.string :name
      t.json :properties
      t.references :resource, polymorphic: true, index: true

      t.timestamps
    end
    add_index :contexts, :name
  end
end
