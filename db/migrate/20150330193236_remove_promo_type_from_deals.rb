class RemovePromoTypeFromDeals < ActiveRecord::Migration
  def change
    remove_column :deals, :promo_type
  end
end