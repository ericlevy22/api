class RemoveStoreLogoFromStores < ActiveRecord::Migration
  def change
    remove_column :stores, :store_logo
  end
end
