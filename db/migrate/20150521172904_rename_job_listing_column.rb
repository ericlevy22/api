class RenameJobListingColumn < ActiveRecord::Migration
  def change
    rename_column :job_listings, :start_date, :start_date_text
    rename_column :job_listings, :requisition_number, :legacy_requisition_id
  end
end
