class CreateOperationalMessages < ActiveRecord::Migration
  def change
    create_table :operational_messages do |t|
      t.integer      :mall_id
      t.text         :description
      t.datetime     :start_at, :end_at
      t.timestamps
    end

    add_index :operational_messages, :mall_id
  end
end
