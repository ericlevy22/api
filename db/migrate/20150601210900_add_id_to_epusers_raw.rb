class AddIdToEpusersRaw < ActiveRecord::Migration
  def up
    execute <<-SQL
      ALTER TABLE epusers_raw ADD COLUMN id serial;
    SQL
  end
  
  def down
    drop column :epusers_raw, :id
  end 
end
