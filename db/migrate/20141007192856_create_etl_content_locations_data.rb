class CreateEtlContentLocationsData < ActiveRecord::Migration
  def change
    create_table :content_locations_data, id: false do |t|
      t.integer  :promo_id
      t.integer  :retailer_id
      t.integer  :mall_id
      t.datetime :updated_at
    end
  end
end
