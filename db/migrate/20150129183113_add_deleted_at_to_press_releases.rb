class AddDeletedAtToPressReleases < ActiveRecord::Migration
  def change
    add_column :press_releases, :deleted_at, :datetime
    add_index :press_releases, :deleted_at
  end
end
