class RenameStoresFaxNumberToFax < ActiveRecord::Migration
  def change
    rename_column :stores, :fax_number, :fax
  end
end
