class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string :url
      t.string :size_parameter
      t.integer :imageable_id
      t.string :imageable_type
      t.string :cached_tag_list, limit: 500
      t.timestamps
    end
  end
end
