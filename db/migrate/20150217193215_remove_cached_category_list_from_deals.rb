class RemoveCachedCategoryListFromDeals < ActiveRecord::Migration
  def change
    remove_column :deals, :cached_category_list
  end
end
