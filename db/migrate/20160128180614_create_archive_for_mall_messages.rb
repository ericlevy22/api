class CreateArchiveForMallMessages < ActiveRecord::Migration
  def change
    create_table :mall_message_archives do |t|
      t.integer     :mall_id
      t.text        :description
      t.datetime    :start_at
      t.datetime    :end_at
      t.datetime    :created_at
      t.datetime    :updated_at
      t.string      :message_type
      t.datetime    :deleted_at
      t.datetime    :archived_at,    null: false
    end

    add_index :mall_message_archives, :deleted_at
    add_index :mall_message_archives, :mall_id
  end
end

