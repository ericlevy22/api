class RemoveDeviceIdFromAffinities < ActiveRecord::Migration
  def change
    remove_column :affinities, :device_id
  end
end
