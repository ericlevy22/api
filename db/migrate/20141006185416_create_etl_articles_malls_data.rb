class CreateEtlArticlesMallsData < ActiveRecord::Migration
  def change
    create_table :articles_malls_data, id: false do |t|
      t.integer  :article_id
      t.integer  :malls, array: true
    end
  end
end
