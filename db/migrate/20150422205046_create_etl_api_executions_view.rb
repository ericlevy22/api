class CreateEtlApiExecutionsView < ActiveRecord::Migration
  def self.up
     execute <<-SQL
      CREATE VIEW api_executions_latest
      AS
      WITH last_execution AS (
              SELECT api_executions.table_name,
                 max(api_executions.execution_id) AS latest_execution_id
                FROM api_executions
               GROUP BY api_executions.table_name
             )
      SELECT api.execution_id,
         api.table_name,
         api.start_time,
         api.end_time,
         api.duration
        FROM (api_executions api
          JOIN last_execution last ON ((api.execution_id = last.latest_execution_id)));
     SQL
   end
   def self.down
     execute <<-SQL
       DROP VIEW api_executions_latest;
     SQL
   end
 end
