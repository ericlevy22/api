class RemoveShortTextFromDeals < ActiveRecord::Migration
  def change
    remove_column :deals, :short_text
  end
end
