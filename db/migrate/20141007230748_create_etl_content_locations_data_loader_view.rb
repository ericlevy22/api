class CreateEtlContentLocationsDataLoaderView < ActiveRecord::Migration
  def self.up
   execute <<-SQL
    CREATE VIEW content_locations_loader AS
            (         SELECT cld.promo_id,
                        s.id AS store_id,
                        cld.updated_at
                       FROM content_locations_data cld,
                        malls m,
                        stores s
                      WHERE (((((cld.mall_id = 0) OR (cld.mall_id IS NULL)) AND (m.id = s.mall_id)) AND (NOT m.exclude_global_promos)) AND (cld.retailer_id = s.retailer_id))
            UNION
                     SELECT valid.promo_id,
                        s.id AS store_id,
                        valid.updated_at
                       FROM ((( SELECT DISTINCT content_locations_data.promo_id,
                                content_locations_data.retailer_id,
                                content_locations_data.updated_at
                               FROM content_locations_data
                              WHERE (content_locations_data.mall_id < 0)) valid
                  JOIN stores s ON ((valid.retailer_id = s.retailer_id)))
             LEFT JOIN content_locations_data cld ON ((((cld.promo_id = valid.promo_id) AND (cld.retailer_id = s.retailer_id)) AND ((cld.mall_id * (-1)) = s.mall_id))))
            WHERE (cld.promo_id IS NULL))
    UNION
             SELECT cld.promo_id,
                s.id AS store_id,
                cld.updated_at
               FROM (content_locations_data cld
          JOIN stores s ON (((cld.retailer_id = s.retailer_id) AND (cld.mall_id = s.mall_id))))
         WHERE (cld.mall_id > 0);
   SQL
 end
 def self.down
   execute <<-SQL
     DROP VIEW content_locations_loader;
   SQL
 end
end
