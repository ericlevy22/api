class RenameAffiliateUrlToUrlText < ActiveRecord::Migration
  def change
    rename_column :stores, :affiliate_url, :url_text
    rename_column :retailers, :affiliate_url, :url_text
  end
end
