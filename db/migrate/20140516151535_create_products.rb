class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.integer :brand_id
      t.string :name, :seo_slug, :image_file_name
      t.text :description
      t.string :price, :sale_price
      t.timestamps
    end

    add_index :products, :brand_id
  end
end
