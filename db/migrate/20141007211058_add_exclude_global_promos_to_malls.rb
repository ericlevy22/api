class AddExcludeGlobalPromosToMalls < ActiveRecord::Migration
  def change
    add_column :malls, :exclude_global_promos, :boolean
  end
end
