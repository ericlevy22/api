class CreateMemberships < ActiveRecord::Migration
  def change
    create_table :memberships do |t|
      t.references :program, index: true
      t.json :properties
      t.references :identity, index: true
      t.datetime :deleted_at, index: true
      t.timestamps null: false
    end
    add_foreign_key :memberships, :programs
    add_foreign_key :memberships, :identities
  end
end
