class CreateEtlArticlesMallsLoaderView < ActiveRecord::Migration
 def self.up
    execute <<-SQL
      CREATE VIEW articles_malls_loader
      AS
        SELECT amd.article_id, m.id AS mall_id
        FROM (articles_malls_data amd JOIN malls m ON ((m.id = ANY (amd.malls))));
    SQL
  end
  def self.down
    execute <<-SQL
      DROP VIEW articles_malls_loader;
    SQL
  end
end
