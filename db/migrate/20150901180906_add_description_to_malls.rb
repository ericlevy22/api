class AddDescriptionToMalls < ActiveRecord::Migration
  def change
    add_column :malls, :description, :string
  end
end
