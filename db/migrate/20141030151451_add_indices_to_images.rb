class AddIndicesToImages < ActiveRecord::Migration
  def change
    add_index :images, :id
    add_index :images, :imageable_id
    add_index :images, :imageable_type
  end
end
