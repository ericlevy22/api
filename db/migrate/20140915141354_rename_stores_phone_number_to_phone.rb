class RenameStoresPhoneNumberToPhone < ActiveRecord::Migration
  def change
    rename_column :stores, :phone_number, :phone
  end
end
