class CreateJoinTableArticleProduct < ActiveRecord::Migration
  def change
    create_join_table :articles, :products do |t|
      t.index :article_id
      t.index :product_id
    end
    add_index :articles_products, [:article_id, :product_id], unique: true
  end
end
