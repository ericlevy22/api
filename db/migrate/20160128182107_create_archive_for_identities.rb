class CreateArchiveForIdentities < ActiveRecord::Migration
  def change
    create_table :identity_archives do |t|
      t.string      :identity_value
      t.string      :identity_type
      t.integer     :user_id
      t.datetime    :created_at
      t.datetime    :updated_at
      t.datetime    :deleted_at
      t.string      :confirmation_token
      t.datetime    :confirmation_sent_at
      t.datetime    :confirmed_at
      t.datetime    :archived_at,    null: false
    end

    add_index :identity_archives, [:identity_type, :identity_value]
    add_index :identity_archives, :user_id
  end
end
