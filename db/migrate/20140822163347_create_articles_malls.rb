class CreateArticlesMalls < ActiveRecord::Migration
  def change
    create_table :articles_malls, id: false do |t|
      t.integer :article_id
      t.integer :mall_id
    end

    add_index :articles_malls, :article_id
    add_index :articles_malls, :mall_id
  end
end
