class AddIsActiveToRetailers < ActiveRecord::Migration
  def change
    add_column :retailers, :is_active, :boolean
  end
end
