class AddDeletedAtToRetailers < ActiveRecord::Migration
  def change
    add_column :retailers, :deleted_at, :datetime
    add_index :retailers, :deleted_at
  end
end
