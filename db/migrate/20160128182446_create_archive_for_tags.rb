class CreateArchiveForTags < ActiveRecord::Migration
  def change
    create_table :tag_archives do |t|
      t.string      :name
      t.integer     :taggings_count
      t.datetime    :deleted_at
      t.datetime    :created_at
      t.datetime    :updated_at
      t.datetime    :archived_at,    null: false
    end

    add_index :tag_archives, :name
  end
end
