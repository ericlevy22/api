class CreateDealsStores < ActiveRecord::Migration
  def change
    create_table :deals_stores do |t|
      t.references :deal, index: true, foreign_key: true
      t.references :store, index: true, foreign_key: true
      t.timestamps
    end
    add_index :deals_stores, [:deal_id, :store_id], unique: true
  end
end
