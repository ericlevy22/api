class CreateArchiveForUsers < ActiveRecord::Migration
  def change
    create_table :user_archives do |t|
      t.datetime    :created_at
      t.datetime    :updated_at
      t.json        :properties
      t.datetime    :deleted_at
      t.datetime    :archived_at,    null: false
    end

    add_index :user_archives, :deleted_at
  end
end

