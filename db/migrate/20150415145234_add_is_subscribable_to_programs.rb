class AddIsSubscribableToPrograms < ActiveRecord::Migration
  def change
    add_column :programs, :is_subscribable, :boolean
  end
end
