class AddDeletedAtToSocialNetworks < ActiveRecord::Migration
  def change
    add_column :social_networks, :deleted_at, :datetime
    add_index :social_networks, :deleted_at
  end
end
