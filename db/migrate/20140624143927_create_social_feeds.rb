class CreateSocialFeeds < ActiveRecord::Migration
  def change
    create_table :social_feeds do |t|
      t.integer :social_network_id
      t.string :sociable_type
      t.integer :sociable_id
      t.string :url

      t.timestamps
    end
  end
end
