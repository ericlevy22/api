class RemoveUtcOffsetFromMalls < ActiveRecord::Migration
  def change
    remove_column :malls, :utc_offset
  end
end
