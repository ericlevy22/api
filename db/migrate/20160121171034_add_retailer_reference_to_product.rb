class AddRetailerReferenceToProduct < ActiveRecord::Migration
  def change
    add_foreign_key :products, :retailers
  end
end
