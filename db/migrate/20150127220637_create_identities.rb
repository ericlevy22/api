class CreateIdentities < ActiveRecord::Migration
  def change
    create_table :identities do |t|
      t.string :identity_value
      t.string :identity_type
      t.references :user, index: true

      t.timestamps null: false
    end
    add_index :identities, :identity_value, unique: true
    add_index :identities, :identity_type
  end
end
