class RemoveFeaturedAndExcludedFromArticles < ActiveRecord::Migration
  def change
    remove_column :articles, :featured_to_malls
    remove_column :articles, :excluded_from_malls
  end
end
