class CreateArchiveForEvents < ActiveRecord::Migration
  def change
    create_table :event_archives do |t|
      t.integer     :mall_id
      t.string      :title,           limit: 100
      t.string      :seo_slug,        limit: 250
      t.text        :description
      t.datetime    :display_at
      t.datetime    :start_at
      t.datetime    :end_at
      t.boolean     :is_active
      t.boolean     :is_featured
      t.boolean     :is_events_page
      t.integer     :pin_order
      t.text        :short_text
      t.text        :date_text
      t.text        :link_text
      t.string      :link_url,        limit: 250
      t.boolean     :is_new_window
      t.datetime    :created_at
      t.datetime    :updated_at
      t.datetime    :deleted_at
      t.string      :location
      t.datetime    :archived_at,     null: false
    end
  end
end
