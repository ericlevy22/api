class RemoveIdFromNewsletterusersRawMerging < ActiveRecord::Migration
  def up
    remove_column :newsletterusers_raw_merging, :id
  end

  def down
    add_column :newsletterusers_raw_merging, :id, :integer
  end
end
