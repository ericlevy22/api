class CreateIfeatures < ActiveRecord::Migration
  def change
    create_table :ifeatures do |t|
      t.integer  "mall_id"
      t.integer  "company_id"
      t.string   "seo_slug"
      t.integer  "pin_order",     default: 0
      t.string   "headline",      limit: 200
      t.string   "tag_line",      limit: 200
      t.string   "sub_heading",   limit: 50
      t.text     "description"
      t.datetime "start_at"
      t.datetime "end_at"
      t.datetime "end_at"
      t.string   "url"
      t.boolean  "is_active",      default: false
      t.boolean  "is_static",      default: false
      t.boolean  "is_headline",    default: false
      t.boolean  "is_new_window",  default: false
      t.timestamps
    end
  end
end
