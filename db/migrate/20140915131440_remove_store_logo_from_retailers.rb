class RemoveStoreLogoFromRetailers < ActiveRecord::Migration
  def up
    remove_column :retailers, :store_logo
  end

  def down
    add_column :retailers, :store_logo, :string
  end
end
