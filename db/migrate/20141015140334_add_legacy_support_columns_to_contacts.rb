class AddLegacySupportColumnsToContacts < ActiveRecord::Migration
  def change
    add_column :contacts, :legacy_id, :integer
    add_column :contacts, :legacy_table, :string
  end
end
