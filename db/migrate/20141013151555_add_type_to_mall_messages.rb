class AddTypeToMallMessages < ActiveRecord::Migration
  def change
    add_column :mall_messages, :type, :string
  end
end
