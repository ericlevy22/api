class CreateGroupings < ActiveRecord::Migration
  def change
    create_table :groupings do |t|
      t.references :group, index: true, foreign_key: true
      t.references :groupable, polymorphic: true, index: true

      t.timestamps null: false
    end
  end
end
