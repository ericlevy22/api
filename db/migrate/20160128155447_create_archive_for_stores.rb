class CreateArchiveForStores < ActiveRecord::Migration
  def change
    create_table :store_archives do |t|
      t.integer     :mall_id
      t.integer     :retailer_id
      t.integer     :location_id
      t.string      :name
      t.string      :sort_name
      t.string      :seo_slug,                  limit: 250
      t.string      :address
      t.string      :city
      t.string      :state
      t.string      :zip
      t.string      :phone
      t.string      :hours
      t.string      :directory_tags,            limit: 250
      t.string      :email
      t.string      :fax
      t.text        :description
      t.text        :job_text
      t.boolean     :is_featured
      t.datetime    :created_at
      t.datetime    :updated_at
      t.datetime    :open_at
      t.datetime    :close_at
      t.string      :tenant_location
      t.datetime    :deleted_at
      t.string      :url_text,                  limit: 300
      t.string      :url,                       limit: 300
      t.string      :tenant_location_map_key
      t.boolean     :exclude_national_deals
      t.datetime    :opening_date
      t.string      :opening_text
      t.datetime    :archived_at,               null: false
    end

    add_index :store_archives, :location_id
    add_index :store_archives, :mall_id
    add_index :store_archives, :retailer_id
  end
end
