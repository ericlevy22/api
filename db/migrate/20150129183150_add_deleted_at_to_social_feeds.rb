class AddDeletedAtToSocialFeeds < ActiveRecord::Migration
  def change
    add_column :social_feeds, :deleted_at, :datetime
    add_index :social_feeds, :deleted_at
  end
end
