class RemoveLogoUrlFromBrands < ActiveRecord::Migration
  def change
    remove_column :brands, :logo_url
  end
end
