class CreateArchiveForTrackingPixels < ActiveRecord::Migration
  def change
    create_table :tracking_pixel_archives do |t|
      t.string      :url
      t.integer     :trackable_id
      t.string      :trackable_type
      t.datetime    :created_at
      t.datetime    :updated_at
      t.datetime    :deleted_at
      t.datetime    :archived_at,    null: false
    end

    add_index :tracking_pixel_archives, :deleted_at
  end
end

