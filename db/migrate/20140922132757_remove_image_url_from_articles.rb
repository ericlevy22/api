class RemoveImageUrlFromArticles < ActiveRecord::Migration
  def change
    remove_column :articles, :image_url
  end
end
