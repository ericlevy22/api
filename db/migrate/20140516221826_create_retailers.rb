class CreateRetailers < ActiveRecord::Migration
  def change
    create_table :retailers do |t|
      t.string :name
      t.string :sort_name
      t.string :seo_slug
      t.string :description, limit: 300
      t.string :store_logo
      t.string :store_nick_name
      t.string :phone_number
      t.string :email
      t.string :url, limit: 300
      t.string :store_text, limit: 2000
      t.timestamps
    end
  end
end
