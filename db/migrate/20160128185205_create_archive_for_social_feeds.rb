class CreateArchiveForSocialFeeds < ActiveRecord::Migration
  def change
    create_table :social_feed_archives do |t|
      t.integer     :social_network_id
      t.string      :sociable_type
      t.integer     :sociable_id
      t.datetime    :created_at
      t.datetime    :updated_at
      t.boolean     :is_active
      t.datetime    :deleted_at
      t.json        :properties
      t.datetime    :archived_at,    null: false
    end

    add_index :social_feed_archives, :deleted_at
  end
end

