class AddSanitizedEmailToAccountArchives < ActiveRecord::Migration
  def change
    add_column :account_archives, :sanitized_email, :string
    add_index :account_archives, :sanitized_email, unique: true
  end
end
