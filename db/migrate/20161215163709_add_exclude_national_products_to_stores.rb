class AddExcludeNationalProductsToStores < ActiveRecord::Migration
  def change
    add_column :stores, :exclude_national_products, :boolean, default: false
  end
end
