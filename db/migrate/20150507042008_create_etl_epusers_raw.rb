class CreateEtlEpusersRaw < ActiveRecord::Migration
  def self.up
     execute <<-SQL
      CREATE TABLE epusers_raw (epID int, email varchar, newsletteruserid int, date timestamp, properties json, source varchar);
     SQL
   end
   def self.down
     execute <<-SQL
       DROP TABLE epusers_raw;
     SQL
   end
 end
