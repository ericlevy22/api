class DropActsAsTaggableSpecificColumns < ActiveRecord::Migration
  def change
    remove_column :taggings, :tagger_id
    remove_column :taggings, :tagger_type
    remove_column :taggings, :context
  end
end
