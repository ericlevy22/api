class AddIsActiveToSocialFeeds < ActiveRecord::Migration
  def change
    add_column :social_feeds, :is_active, :boolean
  end
end
