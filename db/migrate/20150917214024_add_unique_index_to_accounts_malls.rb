class AddUniqueIndexToAccountsMalls < ActiveRecord::Migration
  def change
    add_index :accounts_malls, [:account_id, :mall_id], unique: true
  end
end
