class RenameRetailersStoreNickNameToNickName < ActiveRecord::Migration
  def change
    rename_column :retailers, :store_nick_name, :nick_name
  end
end
