class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name
      t.string :nick_name
      t.string :seo_slug
      t.string :address
      t.string :city
      t.string :state, limit: 2
      t.string :zip
      t.string :phone_number, :fax
      t.string :url, :test_url
      t.boolean :is_active, default: false, null: false
      t.timestamps
    end
  end
end
