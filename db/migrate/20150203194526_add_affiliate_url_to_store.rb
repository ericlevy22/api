class AddAffiliateUrlToStore < ActiveRecord::Migration
  def change
    add_column :stores, :affiliate_url, :string, limit: 300
  end
end
