class AddDefaultExcludeNationalDealsToMalls < ActiveRecord::Migration
  def change
    change_column_default :malls, :exclude_national_deals, false
  end
end
