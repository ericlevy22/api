class RenameStoresStoreText < ActiveRecord::Migration
  def self.up
    rename_column :stores, :store_text, :description
  end

  def self.down
    rename_column :stores, :description, :store_text
  end
end
