class CreateEtlApiExecutionsTable < ActiveRecord::Migration
  def self.up
     execute <<-SQL
      CREATE TABLE api_executions  (
        execution_id	serial NOT NULL,
        table_name  	varchar NOT NULL,
        start_time  	timestamp NOT NULL DEFAULT ('now'::text)::timestamp without time zone,
        end_time    	timestamp NULL,
        duration      numeric NULL
      );
     SQL
   end
   def self.down
     execute <<-SQL
       DROP TABLE api_executions;
     SQL
   end
 end
