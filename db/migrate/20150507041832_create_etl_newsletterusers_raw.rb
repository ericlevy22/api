class CreateEtlNewsletterusersRaw < ActiveRecord::Migration
  def self.up
     execute <<-SQL
      CREATE TABLE newsletterusers_raw (
           email character varying NOT NULL,
           created_at timestamp without time zone,
           updated_at timestamp without time zone,
           smsphone character varying,
           facebook character varying,
           json_data character varying,
           source character varying,
           slot integer NOT NULL,
           account_id integer,
           id SERIAL NOT NULL,
           insertable smallint,
           user_id integer
        );

        CREATE INDEX a ON newsletterusers_raw USING btree (email, slot);

        CREATE INDEX newsletterusers_raw_email ON newsletterusers_raw USING btree (email);

        CREATE INDEX newsletterusers_raw_slot ON newsletterusers_raw USING btree (slot);

        CREATE TABLE newsletterusers_raw_insertable (
           slot integer,
           users_id integer DEFAULT nextval('users_id_seq'::regclass) NOT NULL,
           accounts_id integer DEFAULT nextval('accounts_id_seq'::regclass) NOT NULL
        );

        CREATE INDEX newsletterusers_raw_insertable_slot ON newsletterusers_raw_insertable USING btree (slot);
        
        CREATE TABLE newsletterusers_raw_merging (
           slot integer NOT NULL,
           user_id integer NOT NULL,
           id integer NOT NULL,
           merged integer DEFAULT 0::smallint NOT NULL
        );

        CREATE INDEX newsletterusers_raw_merging_slot ON newsletterusers_raw_merging USING btree (slot);

        CREATE INDEX newsletterusers_raw_merging_user_id ON newsletterusers_raw_merging USING btree (user_id);
     SQL
   end
   def self.down
     execute <<-SQL
       DROP TABLE newsletterusers_raw;
       DROP TABLE newsletterusers_raw_insertable;
       DROP TABLE newsletterusers_raw_merging;
     SQL
   end
 end
