class ChangeJobListingsRequiredExperienceFromStringToText < ActiveRecord::Migration
  def up
    change_column :job_listings, :required_experience, :text
  end

  def down
    # warning: this will cause data loss if column contains strings
    # greater than 255 characters in length
    change_column :job_listings, :required_experience, :string
  end
end
