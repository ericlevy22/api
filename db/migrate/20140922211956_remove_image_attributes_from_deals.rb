class RemoveImageAttributesFromDeals < ActiveRecord::Migration
  def change
    remove_column :deals, :image_name
    remove_column :deals, :image_url
  end
end
