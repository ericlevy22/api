class AddLegacyIdToPrograms < ActiveRecord::Migration
  def change
    add_column :programs, :legacy_id, :integer
  end
end
