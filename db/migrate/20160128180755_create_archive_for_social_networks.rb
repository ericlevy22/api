class CreateArchiveForSocialNetworks < ActiveRecord::Migration
  def change
    create_table :social_network_archives do |t|
      t.string      :name
      t.string      :url
      t.datetime    :created_at
      t.datetime    :updated_at
      t.boolean     :is_active
      t.datetime    :deleted_at
      t.datetime    :archived_at,    null: false
    end

    add_index :social_network_archives, :deleted_at
  end
end

