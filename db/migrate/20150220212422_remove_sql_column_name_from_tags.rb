class RemoveSqlColumnNameFromTags < ActiveRecord::Migration
  def change
    remove_column :tags, :sql_column_name
  end
end
