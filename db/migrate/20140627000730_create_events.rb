class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.integer  "mall_id"
      t.string   "title",                limit: 100
      t.string   "seo_slug",             limit: 250
      t.text     "description"
      t.datetime "display_at"
      t.datetime "start_at"
      t.datetime "end_at"
      t.boolean  "is_active",            default: false
      t.boolean  "is_featured",          default: false
      t.boolean  "is_events_page",       default: false
      t.integer  "pin_order",            default: 0
      t.text     "short_text"
      t.text     "date_text"
      t.text     "link_text"
      t.string   "link_url",         limit: 250
      t.boolean  "is_new_window",        default: false
      t.timestamps
    end
  end
end
