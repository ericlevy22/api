class CreateAffinities < ActiveRecord::Migration
  def change
    create_table :affinities do |t|
      t.string :device_id
      t.integer :heartable_id
      t.string :heartable_type
      t.boolean :is_hearted, default: true
      t.timestamps
    end

    add_index :affinities, :device_id
  end
end
