class RenameMallsPhoneNumberToPhone < ActiveRecord::Migration
  def change
    rename_column :malls, :phone_number, :phone
  end
end
