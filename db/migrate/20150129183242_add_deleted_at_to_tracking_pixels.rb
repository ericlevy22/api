class AddDeletedAtToTrackingPixels < ActiveRecord::Migration
  def change
    add_column :tracking_pixels, :deleted_at, :datetime
    add_index :tracking_pixels, :deleted_at
  end
end
