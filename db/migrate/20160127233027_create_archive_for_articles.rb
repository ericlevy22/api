class CreateArchiveForArticles < ActiveRecord::Migration
  def change
    create_table :article_archives do |t|
      t.string      :author,        limit: 512
      t.string      :title,        limit: 256
      t.text        :subtitle
      t.string      :short_title
      t.text        :teaser
      t.text        :content
      t.string      :seo_slug,        limit: 256
      t.datetime    :display_at
      t.datetime    :end_at
      t.integer     :display_order
      t.boolean     :is_active
      t.datetime    :created_at
      t.datetime    :updated_at
      t.datetime    :deleted_at
      t.string      :attribution
      t.integer     :parent_id
      t.string      :feature_location
      t.datetime    :archived_at,    null: false
    end

    add_index :article_archives, :deleted_at
    add_index :article_archives, :parent_id
  end
end

