class AddFeedKeyToSocialFeeds < ActiveRecord::Migration
  def change
    add_column :social_feeds, :feed_key, :string
  end
end
