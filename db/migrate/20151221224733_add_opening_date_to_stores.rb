class AddOpeningDateToStores < ActiveRecord::Migration
  def change
    add_column :stores, :opening_date, :datetime
    add_column :stores, :opening_text, :string
  end
end
