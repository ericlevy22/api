class CreateArchiveForContacts < ActiveRecord::Migration
  def change
    create_table :contact_archives do |t|
      t.string      :name
      t.string      :title
      t.string      :email
      t.string      :phone
      t.integer     :contactable_id
      t.string      :contactable_type
      t.datetime    :created_at
      t.datetime    :updated_at
      t.integer     :legacy_id
      t.string      :legacy_table
      t.datetime    :deleted_at
      t.datetime    :archived_at,    null: false
    end

    add_index :contact_archives, [:contactable_id, :legacy_id]
    add_index :contact_archives, :deleted_at
  end
end

