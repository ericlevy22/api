class CreateArchiveForRetailers < ActiveRecord::Migration
  def change
    create_table :retailer_archives do |t|
      t.string      :name
      t.string      :sort_name
      t.string      :seo_slug
      t.string      :description,       limit: 3000
      t.string      :nick_name
      t.string      :phone
      t.string      :email
      t.string      :url,               limit: 300
      t.string      :store_text,        limit: 2000
      t.datetime    :created_at
      t.datetime    :updated_at
      t.boolean     :is_active
      t.datetime    :deleted_at
      t.string      :url_text,          limit: 300
      t.datetime    :archived_at,       null: false
    end
  end
end
