class CreateArchiveForLocations < ActiveRecord::Migration
  def change
    create_table :location_archives do |t|
      t.float       :latitude
      t.float       :longitude
      t.datetime    :created_at
      t.datetime    :updated_at
      t.string      :time_zone
      t.datetime    :deleted_at
      t.datetime    :archived_at,    null: false
    end

    add_index :location_archives, :deleted_at
  end
end

