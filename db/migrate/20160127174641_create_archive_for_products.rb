class CreateArchiveForProducts < ActiveRecord::Migration
  def change
    create_table :product_archives do |t|
      t.integer     :brand_id
      t.string      :name
      t.string      :seo_slug
      t.string      :image_file_name
      t.text        :description
      t.string      :price
      t.string      :sale_price
      t.datetime    :created_at
      t.datetime    :updated_at
      t.datetime    :deleted_at
      t.datetime    :archived_at,    null: false
    end

    add_index :product_archives, :brand_id
    add_index :product_archives, :deleted_at
  end
end

