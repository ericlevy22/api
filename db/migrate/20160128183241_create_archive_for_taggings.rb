class CreateArchiveForTaggings < ActiveRecord::Migration
  def change
    create_table :tagging_archives do |t|
      t.integer     :tag_id
      t.integer     :taggable_id
      t.string      :taggable_type
      t.integer     :tagger_id
      t.string      :tagger_type
      t.string      :context,        limit: 128
      t.datetime    :created_at
      t.datetime    :deleted_at
      t.datetime    :archived_at,    null: false
    end

    add_index :tagging_archives, :taggable_id
    add_index :tagging_archives, [:taggable_id, :taggable_type, :context], name: 'index_taggings_archive_on_taggable_and_context'
  end
end
