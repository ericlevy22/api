class RenameSalesTypeIdToSalesType < ActiveRecord::Migration
  def change
    rename_column :deals, :sales_type_id, :sales_type
  end
end