class AddSourceIdToMemberships < ActiveRecord::Migration
  def change
    add_column :memberships, :source_id, :integer
  end
end
