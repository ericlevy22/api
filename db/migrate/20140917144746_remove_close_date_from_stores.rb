class RemoveCloseDateFromStores < ActiveRecord::Migration
  def change
    remove_column :stores, :close_date
  end
end
