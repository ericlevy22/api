class AddUserRefToAffinities < ActiveRecord::Migration
  def change
    add_reference :affinities, :user, index: true
    add_foreign_key :affinities, :users
  end
end
