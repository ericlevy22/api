class CreateMalls < ActiveRecord::Migration
  def change
    create_table :malls do |t|
      t.integer  :company_id
      t.integer  :location_id
      t.string   :name
      t.string   :sort_name
      t.string   :nick_name
      t.string   :seo_slug
      t.string   :address
      t.string   :city
      t.string   :state
      t.string   :zip
      t.string   :phone_number
      t.string   :url
      t.string   :email
      t.integer  :time_zone,   default: 0
      t.boolean  :is_live, default: false, null: false
      t.boolean  :is_development, default: false, null: false
      t.boolean  :is_archived, default: false, null: false
      t.text     :hours_text
      t.string   :mall_logo
      t.string   :site_type
      t.string   :lang_code
      t.timestamps
    end

    add_index :malls, :company_id
    add_index :malls, :location_id
  end
end
