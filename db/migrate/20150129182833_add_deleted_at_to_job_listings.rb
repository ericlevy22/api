class AddDeletedAtToJobListings < ActiveRecord::Migration
  def change
    add_column :job_listings, :deleted_at, :datetime
    add_index :job_listings, :deleted_at
  end
end
