class RemoveNewsletterUserIdFromEpusersRaw < ActiveRecord::Migration
  def up
    remove_column :epusers_raw, :newsletteruserid
  end

  def down
    add_column :epusers_raw, :newsletteruserid
  end
end
