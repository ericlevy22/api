class AddGuestServicesDescriptionToMallArchives < ActiveRecord::Migration
  def change
    add_column :mall_archives, :guest_services_description, :string, limit: 100
  end
end
