class CreateDeals < ActiveRecord::Migration
  def change
    create_table :deals do |t|
      t.integer :retailer_id
      t.integer :promo_type
      t.integer :sales_type_id
      t.integer :valid_at_type_id
      t.string :title, limit: 100
      t.string :seo_slug, limit: 250
      t.string :image_name, limit: 200
      t.string :image_url, limit: 500
      t.string :external_url, limit: 250
      t.datetime :display_at
      t.datetime :start_at
      t.datetime :end_at
      t.text :short_text
      t.text :description
      t.text :fine_print_description
      t.boolean :is_local, default: false
      t.boolean :is_featured, default: false
      t.string :cached_tag_list, limit: 500
      t.string :cached_category_list, limit: 500
      t.timestamps
    end

    add_index :deals, :retailer_id
    add_index :deals, :cached_tag_list
    add_index :deals, :cached_category_list
  end
end
