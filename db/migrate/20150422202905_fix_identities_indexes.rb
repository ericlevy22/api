class FixIdentitiesIndexes < ActiveRecord::Migration
  def change
    remove_index :identities, :identity_value
    remove_index :identities, :identity_type
    add_index :identities, [:identity_type, :identity_value], unique: true
  end
end
