class AddSeoSlugColumnToJobListings < ActiveRecord::Migration
  def change
    add_column :job_listings, :seo_slug, :string
  end
end
