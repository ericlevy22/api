class AddUrlToStore < ActiveRecord::Migration
  def change
    add_column :stores, :url, :string, limit: 300
  end
end
