class CreatePrograms < ActiveRecord::Migration
  def change
    create_table :programs do |t|
      t.json :properties
      t.references :owner, polymorphic: true, index: true
      t.boolean :is_internal_only
      t.boolean :is_default_signup_program
      t.integer :program_type

      t.timestamps null: false
    end
  end
end
