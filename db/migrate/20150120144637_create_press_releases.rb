class CreatePressReleases < ActiveRecord::Migration
  def change
    create_table :press_releases do |t|
      t.string :name
      t.string :phone
      t.string :email
      t.string :seo_slug
      t.string :headline
      t.string :tag_line
      t.text :content
      t.references :mall
      t.boolean :is_active
      t.integer :pr_type
      t.datetime :display_at
      t.timestamps null: false
    end
  end
end
