class CreateArchiveForBrands < ActiveRecord::Migration
  def change
    create_table :brand_archives do |t|
      t.string      :name
      t.string      :description
      t.string      :seo_slug
      t.string      :url
      t.boolean     :is_approved
      t.datetime    :created_at
      t.datetime    :updated_at
      t.datetime    :deleted_at
      t.datetime    :archived_at,    null: false
    end

    add_index :brand_archives, :deleted_at
  end
end

