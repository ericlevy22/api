class CreateKeywordings < ActiveRecord::Migration
  def change
    create_table :keywordings do |t|
      t.references :keyword, index: true, foreign_key: true
      t.references :keywordable, polymorphic: true, index: true

      t.timestamps null: false
    end
  end
end
