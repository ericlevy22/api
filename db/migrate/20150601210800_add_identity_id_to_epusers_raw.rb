class AddIdentityIdToEpusersRaw < ActiveRecord::Migration
  def change
    add_column :epusers_raw, :identity_id, :integer
  end
end
