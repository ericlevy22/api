class RenameTimeZoneUtcOffset < ActiveRecord::Migration
  def change
    rename_column :malls, :time_zone, :utc_offset
  end
end
