class AddIndexToEvents < ActiveRecord::Migration
  def change
    add_index(:events, [:start_at, :end_at, :display_at, :id], unique: true)
  end
end
