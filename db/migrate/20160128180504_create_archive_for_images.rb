class CreateArchiveForImages < ActiveRecord::Migration
  def change
    create_table :image_archives do |t|
      t.string      :url
      t.string      :size_parameter
      t.integer     :imageable_id
      t.string      :imageable_type
      t.string      :cached_tag_list,        limit: 500
      t.datetime    :created_at
      t.datetime    :updated_at
      t.datetime    :deleted_at
      t.datetime    :archived_at,    null: false
    end

    add_index :image_archives, :deleted_at
    add_index :image_archives, :id
    add_index :image_archives, :imageable_id
    add_index :image_archives, :imageable_type
  end
end

