class AddTypeisIdValueToApiExecutionDeletion < ActiveRecord::Migration
  def self.up
    execute <<-SQL
      ALTER TABLE api_execution_deletion ADD COLUMN typeis_id_Value character varying(250) NULL;
    SQL
  end

  def self.down
    execute <<-SQL
      ALTER TABLE api_execution_deletion DROP COLUMN typeis_id_value;
    SQL
  end
end
