class CreateArchiveForAffinities < ActiveRecord::Migration
  def change
    create_table :affinity_archives do |t|
      t.integer     :heartable_id
      t.string      :heartable_type
      t.datetime    :created_at
      t.datetime    :updated_at
      t.datetime    :deleted_at
      t.integer     :user_id
      t.datetime    :archived_at,    null: false
    end

    add_index :affinity_archives, :deleted_at
    add_index :affinity_archives, :user_id
  end
end

