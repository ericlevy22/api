class CreateArchiveForDeals < ActiveRecord::Migration
  def change
    create_table :deal_archives do |t|
      t.integer     :retailer_id
      t.integer     :sales_type
      t.integer     :valid_at_type_id
      t.string      :title,        limit: 100
      t.string      :seo_slug,        limit: 250
      t.string      :external_url,        limit: 250
      t.datetime    :display_at
      t.datetime    :start_at
      t.datetime    :end_at
      t.text        :description
      t.text        :fine_print_description
      t.boolean     :is_local
      t.boolean     :is_featured
      t.string      :cached_tag_list,        limit: 500
      t.datetime    :created_at
      t.datetime    :updated_at
      t.boolean     :is_active
      t.datetime    :deleted_at
      t.datetime    :archived_at,    null: false
    end

    add_index :deal_archives, :cached_tag_list
    add_index :deal_archives, :deleted_at
    add_index :deal_archives, :retailer_id
  end
end

