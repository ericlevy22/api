class CreateArchiveForJobListings < ActiveRecord::Migration
  def change
    create_table :job_listing_archives do |t|
      t.string      :title
      t.string      :start_date_text
      t.string      :hours_per_week
      t.string      :number_of_openings
      t.boolean     :is_active
      t.boolean     :is_full_time
      t.boolean     :is_apply_online
      t.string      :manager_name
      t.string      :fax_number
      t.string      :email
      t.string      :salary
      t.text        :required_experience
      t.text        :key_responsibilities
      t.text        :special_information
      t.text        :description
      t.datetime    :display_at
      t.datetime    :end_at
      t.integer     :store_id
      t.integer     :legacy_requisition_id
      t.datetime    :created_at
      t.datetime    :updated_at
      t.datetime    :deleted_at
      t.string      :seo_slug
      t.datetime    :archived_at,    null: false
    end

    add_index :job_listing_archives, :deleted_at
  end
end

