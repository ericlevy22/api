class ReassociateCharacteristicsWithIdentities < ActiveRecord::Migration
  def change
    rename_column :characteristics, :user_id, :identity_id
  end
end
