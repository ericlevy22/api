class AddMapPointsToStores < ActiveRecord::Migration
  def change
    add_column :stores, :tenant_location_map_key, :string
  end
end
