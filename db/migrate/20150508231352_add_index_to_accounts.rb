class AddIndexToAccounts < ActiveRecord::Migration
  def change
    add_index :accounts, :id, unique: true
  end
end
