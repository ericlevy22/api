class AddDeletedAtToIdentities < ActiveRecord::Migration
  def change
    add_column :identities, :deleted_at, :datetime
    add_index :identities, :deleted_at
  end
end
