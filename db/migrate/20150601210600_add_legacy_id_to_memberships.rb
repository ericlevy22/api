class AddLegacyIdToMemberships < ActiveRecord::Migration
  def change
    add_column :memberships, :legacy_id, :integer
  end
end
