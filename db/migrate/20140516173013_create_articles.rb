class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string   :author,              limit: 512
      t.string   :title,               limit: 256
      t.text     :subtitle
      t.string   :short_title
      t.text     :teaser
      t.text     :content
      t.string   :seo_slug,            limit: 256
      t.string   :image_url,           limit: 310
      t.datetime :display_at
      t.datetime :end_at
      t.text     :featured_to_malls
      t.text     :excluded_from_malls
      t.integer  :display_order
      t.boolean  :is_active, default: false, null: false
      t.timestamps
    end
  end
end
