class CreateEtlApiExecutionsRestrictedTable < ActiveRecord::Migration
  def self.up
    execute <<-SQL
      CREATE TABLE api_executions_restricted (
        id      serial NOT NULL,
        active  bool NULL DEFAULT false
      );
      insert into api_executions_restricted(active) values(false);
    SQL
  end
  def self.down
    execute <<-SQL
      DROP TABLE api_executions_restricted;
    SQL
  end
end
