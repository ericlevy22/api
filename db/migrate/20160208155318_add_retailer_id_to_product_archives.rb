class AddRetailerIdToProductArchives < ActiveRecord::Migration
  def change
    add_column :product_archives, :retailer_id, :integer
  end
end
