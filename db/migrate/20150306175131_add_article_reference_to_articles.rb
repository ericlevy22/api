class AddArticleReferenceToArticles < ActiveRecord::Migration
  def change
    add_reference :articles, :article, index: true
  end
end
