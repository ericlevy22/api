class RemoveJobListingColumns < ActiveRecord::Migration
  def change
    remove_column :job_listings, :retailer_id
    remove_column :job_listings, :mall_id
  end
end
