class AddExcludeNationalDealsToStores < ActiveRecord::Migration
  def change
    add_column :stores, :exclude_national_deals, :boolean, default: false
  end
end
