class AddGuestServicesDescriptionToMalls < ActiveRecord::Migration
  def change
    add_column :malls, :guest_services_description, :string, limit: 100
  end
end
