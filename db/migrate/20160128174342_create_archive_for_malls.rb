class CreateArchiveForMalls < ActiveRecord::Migration
  def change
    create_table :mall_archives do |t|
      t.integer     :company_id
      t.integer     :location_id
      t.string      :name
      t.string      :sort_name
      t.string      :nick_name
      t.string      :seo_slug
      t.string      :address
      t.string      :city
      t.string      :state
      t.string      :zip
      t.string      :phone
      t.string      :url
      t.string      :email
      t.boolean     :is_live
      t.boolean     :is_development
      t.boolean     :is_archived
      t.text        :hours_text
      t.string      :site_type
      t.string      :lang_code
      t.datetime    :created_at
      t.datetime    :updated_at
      t.boolean     :exclude_national_deals
      t.datetime    :deleted_at
      t.string      :contact_us_email
      t.string      :description
      t.datetime    :archived_at,    null: false
    end

    add_index :mall_archives, :company_id
    add_index :mall_archives, :deleted_at
    add_index :mall_archives, :location_id
  end
end

