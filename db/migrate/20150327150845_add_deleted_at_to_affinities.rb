class AddDeletedAtToAffinities < ActiveRecord::Migration
  def change
    add_column :affinities, :deleted_at, :datetime
    add_index :affinities, :deleted_at
  end
end
