class RemoveMallLogoFromMalls < ActiveRecord::Migration
  def change
    remove_column :malls, :mall_logo
  end
end
