class AddDeletedAtToMalls < ActiveRecord::Migration
  def change
    add_column :malls, :deleted_at, :datetime
    add_index :malls, :deleted_at
  end
end
