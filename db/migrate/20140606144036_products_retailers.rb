class ProductsRetailers < ActiveRecord::Migration
  def change
    create_table 'products_retailers', id: false do |t|
      t.column :product_id, :integer
      t.column :retailer_id, :integer
    end

    add_index :products_retailers, :retailer_id
    add_index :products_retailers, :product_id
  end
end
