class RenameRetailersPhoneNumberToPhone < ActiveRecord::Migration
  def change
    rename_column :retailers, :phone_number, :phone
  end
end
