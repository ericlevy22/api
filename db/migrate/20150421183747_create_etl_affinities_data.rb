class CreateEtlAffinitiesData < ActiveRecord::Migration
  def change
    create_table :affinities_data, id: false do |t|
      t.integer :heartable_id
      t.string :heartable_type
      t.string :email
      t.datetime "created_at"
      t.datetime "updated_at"
      t.datetime "deleted_at"
    end
  end
end
