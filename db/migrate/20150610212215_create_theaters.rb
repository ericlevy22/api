class CreateTheaters < ActiveRecord::Migration
  def change
    create_table :theaters do |t|
      t.references :store, index: true
      t.references :mall, index: true
      t.string :external_key

      t.timestamps null: false
    end
    add_index :theaters, :external_key
    add_foreign_key :theaters, :stores
    add_foreign_key :theaters, :malls
  end
end
