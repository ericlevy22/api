class RenameContactsPhoneNumberToPhone < ActiveRecord::Migration
  def change
    rename_column :contacts, :phone_number, :phone
  end
end
