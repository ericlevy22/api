require 'multi_json'

class MergeFieldsToPropertiesField < ActiveRecord::Migration
  def change

    add_column :social_feeds, :properties, :json
    remove_column :social_feeds, :url
    remove_column :social_feeds, :feed_key

  end
end
