class RenameCompaniesPhoneNumberToPhone < ActiveRecord::Migration
  def change
    rename_column :companies, :phone_number, :phone
  end
end
