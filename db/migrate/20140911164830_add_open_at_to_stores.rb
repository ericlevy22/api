class AddOpenAtToStores < ActiveRecord::Migration
  def change
    add_column :stores, :open_at, :datetime
  end
end
