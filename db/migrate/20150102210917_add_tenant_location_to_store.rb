class AddTenantLocationToStore < ActiveRecord::Migration
  def change
    add_column :stores, :tenant_location, :string
  end
end
