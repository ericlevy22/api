class RenameArticleIdToParentId < ActiveRecord::Migration
  def change
    rename_column :articles, :article_id, :parent_id
  end
end
