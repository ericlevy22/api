class CreateAccountsMalls < ActiveRecord::Migration
  def change
    create_table :accounts_malls do |t|
      t.references :account, index: true
      t.references :mall, index: true

      t.timestamps null: false
    end
    add_foreign_key :accounts_malls, :accounts
    add_foreign_key :accounts_malls, :malls
  end
end
