class RemoveKeywordsFromStores < ActiveRecord::Migration
  def change
    remove_column :stores, :keywords
  end
end
