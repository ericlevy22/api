class CreateStores < ActiveRecord::Migration
  def change
    create_table :stores do |t|
      t.integer  :mall_id
      t.integer  :retailer_id
      t.integer  :location_id
      t.string   :name, :sort_name
      t.string   :seo_slug,       limit: 250
      t.string   :address, :city, :state
      t.string   :zip, :phone_number
      t.string   :hours
      t.datetime :close_date
      t.string   :directory_tags, limit: 250
      t.text     :keywords
      t.string   :email, :fax_number
      t.text     :store_text
      t.text     :job_text
      t.boolean  :is_featured, default: false
      t.string   :store_logo
      t.timestamps
    end

    add_index :stores, :mall_id
    add_index :stores, :retailer_id
    add_index :stores, :location_id
  end
end
