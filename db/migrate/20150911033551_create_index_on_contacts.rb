class CreateIndexOnContacts < ActiveRecord::Migration
  def change
    add_index :contacts, [:contactable_id, :legacy_id]
  end
end
