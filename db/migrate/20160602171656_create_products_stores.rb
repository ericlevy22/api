class CreateProductsStores < ActiveRecord::Migration
  def change
    create_table :products_stores do |t|
      t.references :product, index: true, foreign_key: true
      t.references :store, index: true, foreign_key: true
      t.timestamps
    end
    add_index :products_stores, [:product_id, :store_id], unique: true
  end
end
