class AddCloseAtToStores < ActiveRecord::Migration
  def change
    add_column :stores, :close_at, :datetime
  end
end
