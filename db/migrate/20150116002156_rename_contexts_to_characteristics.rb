class RenameContextsToCharacteristics < ActiveRecord::Migration
  def change
    rename_table :contexts, :characteristics
  end
end
