class RenameJobListingsTimestamps < ActiveRecord::Migration
  def change
    rename_column :job_listings, :posted_at, :display_at
    rename_column :job_listings, :expired_at, :end_at
  end
end
