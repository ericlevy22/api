class AddAffiliateUrlToRetailer < ActiveRecord::Migration
  def change
    add_column :retailers, :affiliate_url, :string, limit: 300
  end
end
