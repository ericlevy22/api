class AddContactUsEmailToMall < ActiveRecord::Migration
  def change
    add_column :malls, :contact_us_email, :string
  end
end
