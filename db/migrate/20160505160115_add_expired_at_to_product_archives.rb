class AddExpiredAtToProductArchives < ActiveRecord::Migration
  def change
    add_column :product_archives, :expired_at, :timestamp
  end
end
