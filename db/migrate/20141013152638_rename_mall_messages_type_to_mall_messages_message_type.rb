class RenameMallMessagesTypeToMallMessagesMessageType < ActiveRecord::Migration
  def change
    rename_column :mall_messages, :type, :message_type
  end
end
