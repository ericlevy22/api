class AddSanitizedEmailToAccounts < ActiveRecord::Migration
  def change
    add_column :accounts, :sanitized_email, :string
    add_index :accounts, :sanitized_email, unique: true
  end
end
