class CreateArchiveForPrograms < ActiveRecord::Migration
  def change
    create_table :program_archives do |t|
      t.json        :properties
      t.integer     :owner_id
      t.string      :owner_type
      t.boolean     :is_internal_only
      t.boolean     :is_default_signup_program
      t.integer     :program_type
      t.datetime    :created_at
      t.datetime    :updated_at
      t.datetime    :deleted_at
      t.boolean     :is_subscribable
      t.boolean     :is_active
      t.integer     :legacy_id
      t.datetime    :archived_at,    null: false
    end

    add_index :program_archives, :deleted_at
    add_index :program_archives, [:owner_type, :owner_id]
  end
end

