class MakeRetailerFieldLarger < ActiveRecord::Migration
  def change
    change_column :retailers, :description, :string, limit: 3000
  end
end
