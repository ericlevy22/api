class CreateArchiveForMemberships < ActiveRecord::Migration
  def change
    create_table :membership_archives do |t|
      t.integer     :program_id
      t.json        :properties
      t.integer     :identity_id
      t.datetime    :deleted_at
      t.datetime    :created_at
      t.datetime    :updated_at
      t.integer     :source_id
      t.integer     :legacy_id
      t.string      :email
      t.datetime    :archived_at,    null: false
    end

    add_index :membership_archives, :deleted_at
    add_index :membership_archives, :identity_id
    add_index :membership_archives, :program_id
  end
end

