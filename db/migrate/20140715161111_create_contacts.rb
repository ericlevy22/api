class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :name
      t.string :title
      t.string :email
      t.string :phone_number
      t.integer :contactable_id
      t.string :contactable_type

      t.timestamps
    end
  end
end
