class RemoveIsHeartedFromAffinities < ActiveRecord::Migration
  def change
    remove_column :affinities, :is_hearted
  end
end
