class CreateJobListings < ActiveRecord::Migration
  def change
    create_table :job_listings do |t|
      t.string :title
      t.string :start_date
      t.string :hours_per_week
      t.string :number_of_openings
      t.boolean :is_active
      t.boolean :is_full_time
      t.boolean :is_apply_online
      t.string :manager_name
      t.string :fax_number
      t.string :email
      t.string :salary
      t.string :required_experience
      t.text :key_responsibilities
      t.text :special_information
      t.text :description
      t.datetime :posted_at
      t.datetime :expired_at
      t.integer :retailer_id
      t.integer :store_id
      t.integer :mall_id
      t.integer :requisition_number

      t.timestamps
    end
  end
end
