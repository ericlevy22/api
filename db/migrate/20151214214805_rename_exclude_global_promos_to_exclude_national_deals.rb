class RenameExcludeGlobalPromosToExcludeNationalDeals < ActiveRecord::Migration
  def change
    rename_column :malls, :exclude_global_promos, :exclude_national_deals
  end
end
