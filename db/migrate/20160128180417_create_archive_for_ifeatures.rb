class CreateArchiveForIfeatures < ActiveRecord::Migration
  def change
    create_table :ifeature_archives do |t|
      t.integer     :mall_id
      t.integer     :company_id
      t.string      :seo_slug
      t.integer     :pin_order
      t.string      :headline,        limit: 200
      t.string      :tag_line,        limit: 200
      t.string      :sub_heading,        limit: 50
      t.text        :description
      t.datetime    :start_at
      t.datetime    :end_at
      t.string      :url
      t.boolean     :is_active
      t.boolean     :is_static
      t.boolean     :is_headline
      t.boolean     :is_new_window
      t.datetime    :created_at
      t.datetime    :updated_at
      t.datetime    :deleted_at
      t.datetime    :archived_at,    null: false
    end

    add_index :ifeature_archives, :deleted_at
  end
end

