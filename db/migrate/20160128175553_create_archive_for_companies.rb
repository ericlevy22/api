class CreateArchiveForCompanies < ActiveRecord::Migration
  def change
    create_table :company_archives do |t|
      t.string      :name
      t.string      :nick_name
      t.string      :seo_slug
      t.string      :address
      t.string      :city
      t.string      :state,        limit: 2
      t.string      :zip
      t.string      :phone
      t.string      :fax
      t.string      :url
      t.string      :test_url
      t.boolean     :is_active
      t.datetime    :created_at
      t.datetime    :updated_at
      t.datetime    :deleted_at
      t.boolean     :allow_unconfirmed_access
      t.datetime    :archived_at,    null: false
    end

    add_index :company_archives, :deleted_at
  end
end

