class AddDeletedAtToCharacteristics < ActiveRecord::Migration
  def change
    add_column :characteristics, :deleted_at, :datetime
    add_index :characteristics, :deleted_at
  end
end
