class RenameOperationalMessageToMallMessage < ActiveRecord::Migration
  def change
    rename_table :operational_messages, :mall_messages
  end
end
