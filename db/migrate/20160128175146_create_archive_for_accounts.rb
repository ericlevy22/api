class CreateArchiveForAccounts < ActiveRecord::Migration
  def change
    create_table :account_archives do |t|
      t.string      :email
      t.string      :encrypted_password
      t.string      :reset_password_token
      t.datetime    :reset_password_sent_at
      t.datetime    :remember_created_at
      t.integer     :sign_in_count
      t.datetime    :current_sign_in_at
      t.datetime    :last_sign_in_at
      t.string      :current_sign_in_ip
      t.string      :last_sign_in_ip
      t.datetime    :created_at
      t.datetime    :updated_at
      t.string      :authentication_token
      t.datetime    :deleted_at
      t.integer     :user_id
      t.string      :confirmation_token
      t.datetime    :confirmed_at
      t.datetime    :confirmation_sent_at
      t.datetime    :archived_at,    null: false
    end

    add_index :account_archives, :confirmation_token
    add_index :account_archives, :deleted_at
    add_index :account_archives, :email
    add_index :account_archives, :id
    add_index :account_archives, :reset_password_token
    add_index :account_archives, :user_id
  end
end

