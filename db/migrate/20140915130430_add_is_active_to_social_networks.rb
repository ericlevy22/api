class AddIsActiveToSocialNetworks < ActiveRecord::Migration
  def change
    add_column :social_networks, :is_active, :boolean
  end
end
