class DropApiExecutionDeletionTables < ActiveRecord::Migration
  def self.up
    execute <<-SQL
      DROP TABLE api_execution_deletion_tables;
    SQL
  end

  def self.down
    execute <<-SQL
      CREATE TABLE api_execution_deletion_tables (
        id SERIAL NOT NULL,
        sql_table_name character varying(250) NOT NULL,
        pgsql_table_name character varying(250) NOT NULL,
        pgsql_id_column character varying(250) NOT NULL
      );
      INSERT INTO api_execution_deletion_tables VALUES (1, 'StoreInstance', 'stores', 'id');
      INSERT INTO api_execution_deletion_tables VALUES (2, 'Store', 'retailers', 'id');
      INSERT INTO api_execution_deletion_tables VALUES (3, 'FPURL', 'ifeatures', 'id');
      INSERT INTO api_execution_deletion_tables VALUES (4, 'MallEvent', 'events', 'id');
      INSERT INTO api_execution_deletion_tables VALUES (5, 'StorePromos', 'deals', 'id');
      INSERT INTO api_execution_deletion_tables VALUES (6, 'JobReqs', 'job_listings', 'id');
      INSERT INTO api_execution_deletion_tables VALUES (7, 'Mall', 'malls', 'id');
      INSERT INTO api_execution_deletion_tables VALUES (8, 'Companies', 'companies', 'id');
      INSERT INTO api_execution_deletion_tables VALUES (9, 'mf_brandName', 'brands', 'id');
      INSERT INTO api_execution_deletion_tables VALUES (10, 'Administrators', 'administrators', 'id');
      SELECT pg_catalog.setval('api_execution_deletion_tables_id_seq', 10, true);
    SQL
  end
end
