class CreateEtlAffinitiesLoaderLoaderView < ActiveRecord::Migration
  def self.up
     execute <<-SQL
     create view affinities_loader as SELECT ad.heartable_id,
         ad.heartable_type,
         ad.created_at,
         ad.updated_at,
         ad.deleted_at,
         i.user_id
        FROM (affinities_data ad
          JOIN identities i ON (((lower(ad.email)::text = (i.identity_value)::text) AND (lower(i.identity_type)::text = 'email'::text))))
          left join affinities a on ad.heartable_type = a.heartable_type and ad.heartable_id = a.heartable_id and i.user_id = a.user_id WHERE a.user_id IS NULL;
     SQL
   end
   def self.down
     execute <<-SQL
       DROP VIEW affinities_loader;
     SQL
   end
 end
