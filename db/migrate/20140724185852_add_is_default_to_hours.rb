class AddIsDefaultToHours < ActiveRecord::Migration
  def change
    add_column :hours, :is_default, :boolean
  end
end
