class AddAllowUnconfirmedAccessToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :allow_unconfirmed_access, :boolean
  end
end
