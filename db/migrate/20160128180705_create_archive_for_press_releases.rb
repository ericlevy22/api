class CreateArchiveForPressReleases < ActiveRecord::Migration
  def change
    create_table :press_release_archives do |t|
      t.string      :name
      t.string      :phone
      t.string      :email
      t.string      :seo_slug
      t.string      :headline
      t.string      :tag_line
      t.text        :content
      t.integer     :mall_id
      t.boolean     :is_active
      t.integer     :pr_type
      t.datetime    :display_at
      t.datetime    :created_at
      t.datetime    :updated_at
      t.datetime    :deleted_at
      t.datetime    :archived_at,    null: false
    end

    add_index :press_release_archives, :deleted_at
  end
end

