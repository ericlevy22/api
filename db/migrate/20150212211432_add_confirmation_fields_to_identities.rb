class AddConfirmationFieldsToIdentities < ActiveRecord::Migration
  def change
    add_column :identities, :confirmation_token, :string
    add_index :identities, :confirmation_token, unique: true
    add_column :identities, :confirmation_sent_at, :datetime
    add_column :identities, :confirmed_at, :datetime
  end
end
