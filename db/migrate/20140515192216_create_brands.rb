class CreateBrands < ActiveRecord::Migration
  def change
    create_table :brands do |t|
      t.string :name, :description
      t.string :seo_slug, :logo_url, :url
      t.boolean :is_approved, default: false, null: false
      t.timestamps
    end
  end
end
