describe PwLegacy::EmailPrograms, type: :request do
  context 'parses JSON responses' do
    let(:program) { create(:program, is_default_signup_program: true) }
    let(:mall) { create(:mall, programs: [program]) }
    let(:account) { create(:account) }

    it 'successfully with 201' do
      allow(Typhoeus::Request).to receive(:new).and_return(Fyphoeus.new(http_response_code: 200))
      post '/accounts', account: { email: account.email, password: account.password }, mall_id: mall.id
      expect(response.status).to be(201)
    end

    it 'by handling errors with 502' do
      allow(Typhoeus::Request).to receive(:new).and_return(Fyphoeus.new("Invalid Body", 502))
      post '/accounts', account: { email: account.email, password: account.password }, mall_id: mall.id
      expect(response.status).to be(502)
    end
  end
end
