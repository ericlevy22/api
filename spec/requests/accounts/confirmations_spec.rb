describe Accounts::ConfirmationsController, type: :request do
  describe '#confirm' do
    let(:program) { create(:program, is_default_signup_program: true) }
    let(:mall) { create(:mall, programs: [program]) }

    before do
      allow(Account).to receive(:find_or_initialize_with_error_by) { create(:account, :unconfirmed) }
      allow(PwLegacy::SewUsers::Create).to receive(:call)
      allow(PwLegacy::EmailPrograms::Subscribe).to receive(:call)
      allow(AccountsMailer).to receive(:confirmation_instructions).and_return(double('AccountMailer', deliver: true))
      allow(AccountsMailer).to receive(:send_welcome_email).and_return(double('AccountMailer', deliver: true))

      post '/accounts/confirm', confirmation_token: 'token goes here', mall_id: mall.id
    end

    it 'creates legacy account' do
      expect(PwLegacy::SewUsers::Create).to have_received(:call)
    end

    it 'subscribes to email programs' do
      expect(PwLegacy::EmailPrograms::Subscribe).to have_received(:call)
    end

    it 'does not send confirmation email' do
      expect(AccountsMailer).to_not have_received(:confirmation_instructions)
    end

    it 'does not send welcome email' do
      expect(AccountsMailer).to_not have_received(:send_welcome_email)
    end

    it 'is confirmed' do
      expect(JSON.parse(response.body)['account']['confirmed_at']).to_not be_nil
    end

    it 'has status 201 created' do
      expect(response).to have_http_status(:created)
    end
  end
end
