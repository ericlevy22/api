describe 'Account', type: :request do
  context '#update' do
    context 'without account param' do
      it '400 bad request' do
        log_in_as_client!
        patch "/accounts/#{@account.id}", account_id: @account.id, account_token: @token, timestamp: @timestamp
        expect(response).to have_http_status(:bad_request)
      end
    end
  end
end
