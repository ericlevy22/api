describe Accounts::LoginController, type: :request do
  context '#create' do
    before do
      allow(PwLegacy::SewUsers::Create).to receive(:call)
      allow(PwLegacy::EmailPrograms::Subscribe).to receive(:call)
      allow(AccountsMailer).to receive(:confirmation_instructions).and_return(double('AccountMailer', deliver: true))
      allow(AccountsMailer).to receive(:send_welcome_email).and_return(double('AccountMailer', deliver: true))
    end

    context 'without an existing account' do
      it 'has status 404 not found' do
        post '/accounts/login', account: { email: Faker::Internet.email, password: SecureRandom.hex(16) }, mall_id: 1
        expect(response).to have_http_status(:not_found)
      end
    end

    context 'with an existing account' do
      context 'with a mall' do
        let(:program) { create(:program, is_default_signup_program: true) }
        let(:first_mall) { create(:mall) }
        let(:second_mall) { create(:mall, programs: [program]) }
        let(:account) { create(:account, malls: [first_mall]) }

        context 'previous mall' do
          before { post '/accounts/login', account: { email: account.email, password: account.password }, mall_id: first_mall.id }

          it 'does not create legacy account' do
            expect(PwLegacy::SewUsers::Create).to_not have_received(:call)
          end

          it 'does not send a confirmation email' do
            expect(AccountsMailer).to_not have_received(:confirmation_instructions)
          end

          it 'does not send a welcome email' do
            expect(AccountsMailer).to_not have_received(:send_welcome_email)
          end

          it 'does not subscribe to email programs' do
            expect(PwLegacy::EmailPrograms::Subscribe).to_not have_received(:call)
          end

          it 'has the correct mall_ids' do
            expect(JSON.parse(response.body)['account']['mall_ids']).to match_array([first_mall.id])
          end

          it 'returns the account' do
            expect(response.body).to be_json_eql(account.id).at_path('account/id')
          end

          it 'has status 201 created' do
            expect(response).to have_http_status(:created)
          end
        end

        context 'new mall' do
          before { post '/accounts/login', account: { email: account.email, password: account.password }, mall_id: second_mall.id }

          it 'does not create legacy account' do
            expect(PwLegacy::SewUsers::Create).to_not have_received(:call)
          end

          it 'does not send a confirmation email' do
            expect(AccountsMailer).to_not have_received(:confirmation_instructions)
          end

          it 'does not send a welcome email' do
            expect(AccountsMailer).to have_received(:send_welcome_email)
          end

          it 'does not subscribe to email programs' do
            expect(PwLegacy::EmailPrograms::Subscribe).to have_received(:call)
          end

          it 'has the correct mall_ids' do
            expect(JSON.parse(response.body)['account']['mall_ids']).to match_array([first_mall.id, second_mall.id])
          end

          it 'returns the account' do
            expect(response.body).to be_json_eql(account.id).at_path('account/id')
          end

          it 'has status 201 created' do
            expect(response).to have_http_status(:created)
          end
        end
      end

      context 'without a mall' do
        let(:account) { create(:account) }

        before { post '/accounts/login', account: { email: account.email, password: account.password } }

        it 'returns the account' do
          expect(response.body).to be_json_eql(AccountSerializer.new(account).to_json)
        end

        it 'has status 201 created' do
          expect(response).to have_http_status(:created)
        end
      end
    end
  end
end
