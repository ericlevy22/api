describe 'Account', type: :request do
  context '#index' do
    context 'without auth' do
      it '401 Unauthorized' do
        get '/accounts'
        expect(response).to have_http_status(:unauthorized)
      end
    end
  end
end
