describe 'Account', type: :request do
  context '#create' do
    context 'with malformed request' do
      context 'without account param' do
        it 'has status 400 bad request' do
          post '/accounts'
          expect(response).to have_http_status(:bad_request)
        end
      end
    end

    context 'using email' do
      context 'with a new account' do
        context 'with a mall' do
          context 'unconfirmed' do
            context 'new mall' do
              let(:mall) { create(:mall) }
              before do
                allow(PwLegacy::SewUsers::Create).to receive(:call)
                allow(PwLegacy::EmailPrograms::Subscribe).to receive(:call)
                allow(AccountsMailer).to receive(:confirmation_instructions).and_return(double('AccountMailer', deliver: true))
                allow(AccountsMailer).to receive(:send_welcome_email).and_return(double('AccountMailer', deliver: true))

                post '/accounts', account: { email: Faker::Internet.email, password: 'asdfasdf' }, mall_id: mall.id
              end

              it 'creates an account', skip_before: true do
                expect{
                  post '/accounts', account: { email: Faker::Internet.email, password: 'asdfasdf' }, mall_id: mall.id
                }.to change(Account, :count).by(1)
              end

              it 'does not create legacy account' do
                expect(PwLegacy::SewUsers::Create).to_not have_received(:call)
              end

              it 'does not subscribe to email programs' do
                expect(PwLegacy::EmailPrograms::Subscribe).to_not have_received(:call)
              end

              it 'sends confirmation email' do
                expect(AccountsMailer).to have_received(:confirmation_instructions)
              end

              it 'does not send welcome email' do
                expect(AccountsMailer).to_not have_received(:send_welcome_email)
              end

              it 'adds the mall_id' do
                expect(JSON.parse(response.body)['account']['mall_ids']).to match_array([mall.id])
              end

              it 'is not confirmed' do
                expect(JSON.parse(response.body)['account']['confirmed_at']).to be_nil
              end

              it 'has status 201 created' do
                expect(response).to have_http_status(:created)
              end
            end
          end
        end

        context 'with a mall that allows unconfirmed access' do
          context 'unconfirmed' do
            context 'new mall' do
              let(:program) { create(:program, is_default_signup_program: true) }
              let(:company) { create(:company, allow_unconfirmed_access: true) }
              let(:mall) { create(:mall, company: company, programs: [program]) }

              before do
                allow(PwLegacy::SewUsers::Create).to receive(:call)
                allow(PwLegacy::EmailPrograms::Subscribe).to receive(:call)
                allow(AccountsMailer).to receive(:confirmation_instructions).and_return(double('AccountMailer', deliver: true))
                allow(AccountsMailer).to receive(:send_welcome_email).and_return(double('AccountMailer', deliver: true))

                post '/accounts', account: { email: Faker::Internet.email, password: 'asdfasdf' }, mall_id: mall.id
              end

              it 'creates an account', skip_before: true do
                expect{
                  post '/accounts', account: { email: Faker::Internet.email, password: 'asdfasdf' }, mall_id: mall.id
                }.to change(Account, :count).by(1)
              end

              it 'creates legacy account' do
                expect(PwLegacy::SewUsers::Create).to have_received(:call)
              end

              it 'subscribes to email programs' do
                expect(PwLegacy::EmailPrograms::Subscribe).to have_received(:call)
              end

              it 'does not send confirmation email' do
                expect(AccountsMailer).to_not have_received(:confirmation_instructions)
              end

              it 'sends welcome email' do
                expect(AccountsMailer).to have_received(:send_welcome_email)
              end

              it 'adds the mall_id' do
                expect(JSON.parse(response.body)['account']['mall_ids']).to match_array([mall.id])
              end

              it 'is not confirmed' do
                expect(JSON.parse(response.body)['account']['confirmed_at']).to be_nil
              end

              it 'has status 201 created' do
                expect(response).to have_http_status(:created)
              end
            end
          end
        end

        context 'without a mall' do
          context 'unconfirmed' do
            before do
              allow(PwLegacy::SewUsers::Create).to receive(:call)
              allow(PwLegacy::EmailPrograms::Subscribe).to receive(:call)
              allow(AccountsMailer).to receive(:confirmation_instructions).and_return(double('AccountMailer', deliver: true))
              allow(AccountsMailer).to receive(:send_welcome_email).and_return(double('AccountMailer', deliver: true))

              post '/accounts', account: { email: Faker::Internet.email, password: 'asdfasdf' }
            end

            it 'creates an account', skip_before: true do
              expect{
                post '/accounts', account: { email: Faker::Internet.email, password: 'asdfasdf' }
              }.to change(Account, :count).by(1)
            end

            it 'does not create legacy account' do
              expect(PwLegacy::SewUsers::Create).to_not have_received(:call)
            end

            it 'does not subscribe to email programs' do
              expect(PwLegacy::EmailPrograms::Subscribe).to_not have_received(:call)
            end

            it 'sends confirmation email' do
              expect(AccountsMailer).to_not have_received(:confirmation_instructions)
            end

            it 'does not send welcome email' do
              expect(AccountsMailer).to_not have_received(:send_welcome_email)
            end

            it 'does not add a mall_id' do
              expect(JSON.parse(response.body)['account']['mall_ids']).to match_array([])
            end

            it 'is not confirmed' do
              expect(JSON.parse(response.body)['account']['confirmed_at']).to be_nil
            end

            it 'has status 201 created' do
              expect(response).to have_http_status(:created)
            end
          end
        end
      end

      context 'with an existing account' do
        context 'with a mall' do
          context 'confirmed' do
            context 'first new mall' do
              let(:program) { create(:program, is_default_signup_program: true) }
              let(:mall) { create(:mall, programs: [program]) }
              let(:account) { create(:account) }

              before do
                allow(PwLegacy::SewUsers::Create).to receive(:call)
                allow(PwLegacy::EmailPrograms::Subscribe).to receive(:call)
                allow(AccountsMailer).to receive(:confirmation_instructions).and_return(double('AccountMailer', deliver: true))
                allow(AccountsMailer).to receive(:send_welcome_email).and_return(double('AccountMailer', deliver: true))

                post '/accounts', account: { email: account.email, password: 'asdfasdf' }, mall_id: mall.id
              end

              it 'does not create an account', skip_before: true do
                expect{
                  post '/accounts', account: { email: account.email, password: 'asdfasdf' }, mall_id: mall.id
                }.to_not change(Account, :count)
              end

              it 'creates legacy account' do
                expect(PwLegacy::SewUsers::Create).to have_received(:call)
              end

              it 'subscribes to email programs' do
                expect(PwLegacy::EmailPrograms::Subscribe).to have_received(:call)
              end

              it 'does not send confirmation email' do
                expect(AccountsMailer).to_not have_received(:confirmation_instructions)
              end

              it 'sends welcome email' do
                expect(AccountsMailer).to have_received(:send_welcome_email)
              end

              it 'adds the mall_id' do
                expect(JSON.parse(response.body)['account']['mall_ids']).to match_array([mall.id])
              end

              it 'is confirmed' do
                expect(JSON.parse(response.body)['account']['confirmed_at']).to_not be_nil
              end

              it 'has status 201 created' do
                expect(response).to have_http_status(:created)
              end
            end

            context 'second new mall' do
              let(:program) { create(:program, is_default_signup_program: true) }
              let(:first_mall) { create(:mall) }
              let(:second_mall) { create(:mall, programs: [program]) }
              let(:account) { create(:account, malls: [first_mall]) }

              before do
                allow(PwLegacy::SewUsers::Create).to receive(:call)
                allow(PwLegacy::EmailPrograms::Subscribe).to receive(:call)
                allow(AccountsMailer).to receive(:confirmation_instructions).and_return(double('AccountMailer', deliver: true))
                allow(AccountsMailer).to receive(:send_welcome_email).and_return(double('AccountMailer', deliver: true))

                post '/accounts', account: { email: account.email, password: 'asdfasdf' }, mall_id: second_mall.id
              end

              it 'does not create an account', skip_before: true do
                expect{
                  post '/accounts', account: { email: account.email, password: 'asdfasdf' }, mall_id: second_mall.id
                }.to_not change(Account, :count)
              end

              it 'does not create legacy account' do
                expect(PwLegacy::SewUsers::Create).to_not have_received(:call)
              end

              it 'subscribes to email programs' do
                expect(PwLegacy::EmailPrograms::Subscribe).to have_received(:call)
              end

              it 'does not send confirmation email' do
                expect(AccountsMailer).to_not have_received(:confirmation_instructions)
              end

              it 'sends welcome email' do
                expect(AccountsMailer).to have_received(:send_welcome_email)
              end

              it 'adds the mall_id' do
                expect(JSON.parse(response.body)['account']['mall_ids']).to match_array([first_mall.id, second_mall.id])
              end

              it 'is confirmed' do
                expect(JSON.parse(response.body)['account']['confirmed_at']).to_not be_nil
              end

              it 'has status 201 created' do
                expect(response).to have_http_status(:created)
              end
            end

            context 'previous mall' do
              let(:first_mall) { create(:mall) }
              let(:account) { create(:account, malls: [first_mall]) }

              before do
                allow(PwLegacy::SewUsers::Create).to receive(:call)
                allow(PwLegacy::EmailPrograms::Subscribe).to receive(:call)
                allow(AccountsMailer).to receive(:confirmation_instructions).and_return(double('AccountMailer', deliver: true))
                allow(AccountsMailer).to receive(:send_welcome_email).and_return(double('AccountMailer', deliver: true))

                post '/accounts', account: { email: account.email, password: 'asdfasdf' }, mall_id: first_mall.id
              end

              it 'does not create an account', skip_before: true do
                expect{
                  post '/accounts', account: { email: account.email, password: 'asdfasdf' }, mall_id: first_mall.id
                }.to_not change(Account, :count)
              end

              it 'does not create legacy account' do
                expect(PwLegacy::SewUsers::Create).to_not have_received(:call)
              end

              it 'does not subscribe to email programs' do
                expect(PwLegacy::EmailPrograms::Subscribe).to_not have_received(:call)
              end

              it 'does not send confirmation email' do
                expect(AccountsMailer).to_not have_received(:confirmation_instructions)
              end

              it 'does not send welcome email' do
                expect(AccountsMailer).to_not have_received(:send_welcome_email)
              end

              it 'has the correct mall_id' do
                expect(JSON.parse(response.body)['account']['mall_ids']).to match_array([first_mall.id])
              end

              it 'is confirmed' do
                expect(JSON.parse(response.body)['account']['confirmed_at']).to_not be_nil
              end

              it 'has status 201 created' do
                expect(response).to have_http_status(:created)
              end
            end
          end

          context 'unconfirmed' do
            context 'new mall' do
              let(:first_mall) { create(:mall) }
              let(:second_mall) { create(:mall) }
              let(:account) { create(:unconfirmed, malls: [first_mall]) }

              before do
                allow(PwLegacy::SewUsers::Create).to receive(:call)
                allow(PwLegacy::EmailPrograms::Subscribe).to receive(:call)
                allow(AccountsMailer).to receive(:confirmation_instructions).and_return(double('AccountMailer', deliver: true))
                allow(AccountsMailer).to receive(:send_welcome_email).and_return(double('AccountMailer', deliver: true))

                post '/accounts', account: { email: account.email, password: 'asdfasdf' }, mall_id: second_mall.id
              end

              it 'does not create an account', skip_before: true do
                expect{
                  post '/accounts', account: { email: account.email, password: 'asdfasdf' }, mall_id: second_mall.id
                }.to_not change(Account, :count)
              end

              it 'does not create legacy account' do
                expect(PwLegacy::SewUsers::Create).to_not have_received(:call)
              end

              it 'does not subscribe to email programs' do
                expect(PwLegacy::EmailPrograms::Subscribe).to_not have_received(:call)
              end

              it 'does not send confirmation email' do
                expect(AccountsMailer).to_not have_received(:confirmation_instructions)
              end

              it 'does not send welcome email' do
                expect(AccountsMailer).to_not have_received(:send_welcome_email)
              end

              it 'adds the mall_id' do
                expect(JSON.parse(response.body)['account']['mall_ids']).to match_array([first_mall.id, second_mall.id])
              end

              it 'is not confirmed' do
                expect(JSON.parse(response.body)['account']['confirmed_at']).to be_nil
              end

              it 'has status 201 created' do
                expect(response).to have_http_status(:created)
              end
            end

            context 'new mall that allows unconfirmed access' do
              let(:first_mall) { create(:mall) }
              let(:company) { create(:company, allow_unconfirmed_access: true) }
              let(:program) { create(:program, is_default_signup_program: true) }
              let(:second_mall) { create(:mall, company: company, programs: [program]) }
              let(:account) { create(:unconfirmed, malls: [first_mall]) }

              before do
                allow(PwLegacy::SewUsers::Create).to receive(:call)
                allow(PwLegacy::EmailPrograms::Subscribe).to receive(:call)
                allow(AccountsMailer).to receive(:confirmation_instructions).and_return(double('AccountMailer', deliver: true))
                allow(AccountsMailer).to receive(:send_welcome_email).and_return(double('AccountMailer', deliver: true))

                post '/accounts', account: { email: account.email, password: 'asdfasdf' }, mall_id: second_mall.id
              end

              it 'does not create an account', skip_before: true do
                expect{
                  post '/accounts', account: { email: account.email, password: 'asdfasdf' }, mall_id: second_mall.id
                }.to_not change(Account, :count)
              end

              it 'creates legacy account' do
                expect(PwLegacy::SewUsers::Create).to have_received(:call)
              end

              it 'subscribes to email programs' do
                expect(PwLegacy::EmailPrograms::Subscribe).to have_received(:call)
              end

              it 'does not send confirmation email' do
                expect(AccountsMailer).to_not have_received(:confirmation_instructions)
              end

              it 'sends a welcome email' do
                expect(AccountsMailer).to have_received(:send_welcome_email)
              end

              it 'adds the mall_id' do
                expect(JSON.parse(response.body)['account']['mall_ids']).to match_array([first_mall.id, second_mall.id])
              end

              it 'is not confirmed' do
                expect(JSON.parse(response.body)['account']['confirmed_at']).to be_nil
              end

              it 'has status 201 created' do
                expect(response).to have_http_status(:created)
              end
            end

            context 'previous mall' do
              let(:first_mall) { create(:mall) }
              let(:account) { create(:unconfirmed, malls: [first_mall]) }

              before do
                allow(PwLegacy::SewUsers::Create).to receive(:call)
                allow(PwLegacy::EmailPrograms::Subscribe).to receive(:call)
                allow(AccountsMailer).to receive(:confirmation_instructions).and_return(double('AccountMailer', deliver: true))
                allow(AccountsMailer).to receive(:send_welcome_email).and_return(double('AccountMailer', deliver: true))

                post '/accounts', account: { email: account.email, password: 'asdfasdf' }, mall_id: first_mall.id
              end

              it 'does not create an account', skip_before: true do
                expect{
                  post '/accounts', account: { email: account.email, password: 'asdfasdf' }, mall_id: first_mall.id
                }.to_not change(Account, :count)
              end

              it 'does not create legacy account' do
                expect(PwLegacy::SewUsers::Create).to_not have_received(:call)
              end

              it 'does not subscribe to email programs' do
                expect(PwLegacy::EmailPrograms::Subscribe).to_not have_received(:call)
              end

              it 'does not send confirmation email' do
                expect(AccountsMailer).to_not have_received(:confirmation_instructions)
              end

              it 'does not send welcome email' do
                expect(AccountsMailer).to_not have_received(:send_welcome_email)
              end

              it 'has the correct mall_id' do
                expect(JSON.parse(response.body)['account']['mall_ids']).to match_array([first_mall.id])
              end

              it 'is not confirmed' do
                expect(JSON.parse(response.body)['account']['confirmed_at']).to be_nil
              end

              it 'has status 201 created' do
                expect(response).to have_http_status(:created)
              end
            end
          end
        end

        context 'without a mall' do
          context 'confirmed' do
            let(:account) { create(:account) }

              before do
              allow(PwLegacy::SewUsers::Create).to receive(:call)
              allow(PwLegacy::EmailPrograms::Subscribe).to receive(:call)
              allow(AccountsMailer).to receive(:confirmation_instructions).and_return(double('AccountMailer', deliver: true))
              allow(AccountsMailer).to receive(:send_welcome_email).and_return(double('AccountMailer', deliver: true))

              post '/accounts', account: { email: account.email, password: 'asdfasdf' }
            end

            it 'does not create an account', skip_before: true do
              expect{
                post '/accounts', account: { email: account.email, password: 'asdfasdf' }
              }.to_not change(Account, :count)
            end

            it 'does not create legacy account' do
              expect(PwLegacy::SewUsers::Create).to_not have_received(:call)
            end

            it 'does not subscribe to email programs' do
              expect(PwLegacy::EmailPrograms::Subscribe).to_not have_received(:call)
            end

            it 'does not send confirmation email' do
              expect(AccountsMailer).to_not have_received(:confirmation_instructions)
            end

            it 'does not send welcome email' do
              expect(AccountsMailer).to_not have_received(:send_welcome_email)
            end

            it 'has the correct mall_id' do
              expect(JSON.parse(response.body)['account']['mall_ids']).to match_array([])
            end

            it 'is confirmed' do
              expect(JSON.parse(response.body)['account']['confirmed_at']).to_not be_nil
            end

            it 'has status 201 created' do
              expect(response).to have_http_status(:created)
            end
          end

          context 'unconfirmed' do
            let(:account) { create(:unconfirmed) }

            before do
              allow(PwLegacy::SewUsers::Create).to receive(:call)
              allow(PwLegacy::EmailPrograms::Subscribe).to receive(:call)
              allow(AccountsMailer).to receive(:confirmation_instructions).and_return(double('AccountMailer', deliver: true))
              allow(AccountsMailer).to receive(:send_welcome_email).and_return(double('AccountMailer', deliver: true))

              post '/accounts', account: { email: account.email, password: 'asdfasdf' }
            end

            it 'does not create an account', skip_before: true do
              expect{
                post '/accounts', account: { email: account.email, password: 'asdfasdf' }
              }.to_not change(Account, :count)
            end

            it 'does not create legacy account' do
              expect(PwLegacy::SewUsers::Create).to_not have_received(:call)
            end

            it 'does not subscribe to email programs' do
              expect(PwLegacy::EmailPrograms::Subscribe).to_not have_received(:call)
            end

            it 'does not send confirmation email' do
              expect(AccountsMailer).to_not have_received(:confirmation_instructions)
            end

            it 'does not send welcome email' do
              expect(AccountsMailer).to_not have_received(:send_welcome_email)
            end

            it 'has the correct mall_id' do
              expect(JSON.parse(response.body)['account']['mall_ids']).to match_array([])
            end

            it 'is not confirmed' do
              expect(JSON.parse(response.body)['account']['confirmed_at']).to be_nil
            end

            it 'has status 201 created' do
              expect(response).to have_http_status(:created)
            end
          end
        end
      end
    end

    context 'using facebook' do
      context 'without authorizing email' do
        let(:fakebook) { Fakebook.new(email: false) }

        before do
          allow(Koala::Facebook::API).to receive(:new) { fakebook }
          post '/accounts', account: { facebook_access_token: 'zQ3r5g3iowZnv0s9Zz92q' }
        end

        it 'does not create a new account', skip_before: true do
          allow(Koala::Facebook::API).to receive(:new) { fakebook }
          expect {
            post '/accounts', account: { facebook_access_token: 'zQ3r5g3iowZnv0s9Zz92q' }
          }.to_not change(Account, :count)
        end

        it '400 bad request' do
          expect(response).to have_http_status(:bad_request)
        end
      end

      context 'with new account' do
        context 'with mall' do
          context 'confirmed' do
            context 'new mall' do
              let(:fakebook) { Fakebook.new }
              let(:program) { create(:program, is_default_signup_program: true) }
              let(:mall) { create(:mall, programs: [program]) }

              before do
                allow(Koala::Facebook::API).to receive(:new) { fakebook }
                allow(PwLegacy::SewUsers::Create).to receive(:call)
                allow(PwLegacy::EmailPrograms::Subscribe).to receive(:call)
                allow(AccountsMailer).to receive(:confirmation_instructions).and_return(double('AccountMailer', deliver: true))
                allow(AccountsMailer).to receive(:send_welcome_email).and_return(double('AccountMailer', deliver: true))

                post '/accounts', account: { facebook_access_token: 'zQ3r5g3iowZnv0s9Zz92q' }, mall_id: mall.id
              end

              it 'creates an account', skip_before: true do
                allow(Koala::Facebook::API).to receive(:new) { Fakebook.new }
                expect {
                  post '/accounts', account: { facebook_access_token: 'zQ3r5g3iowZnv0s9Zz92q' }, mall_id: mall.id
                }.to change(Account, :count).by(1)
              end

              it 'creates legacy account' do
                expect(PwLegacy::SewUsers::Create).to have_received(:call)
              end

              it 'subscribes to email programs' do
                expect(PwLegacy::EmailPrograms::Subscribe).to have_received(:call)
              end

              it 'does not send confirmation email' do
                expect(AccountsMailer).to_not have_received(:confirmation_instructions)
              end

              it 'sends welcome email' do
                expect(AccountsMailer).to have_received(:send_welcome_email)
              end

              it 'adds the mall_id' do
                expect(JSON.parse(response.body)['account']['mall_ids']).to match_array([mall.id])
              end

              it 'is confirmed' do
                expect(JSON.parse(response.body)['account']['confirmed_at']).to_not be_nil
              end

              it 'has status 201 created' do
                expect(response).to have_http_status(:created)
              end
            end
          end

          context 'unconfirmed' do
            context 'new mall' do
              let(:fakebook) { Fakebook.new(verified: false) }
              let(:mall) { create(:mall) }

              before do
                allow(Koala::Facebook::API).to receive(:new) { fakebook }
                allow(PwLegacy::SewUsers::Create).to receive(:call)
                allow(PwLegacy::EmailPrograms::Subscribe).to receive(:call)
                allow(AccountsMailer).to receive(:confirmation_instructions).and_return(double('AccountMailer', deliver: true))
                allow(AccountsMailer).to receive(:send_welcome_email).and_return(double('AccountMailer', deliver: true))

                post '/accounts', account: { facebook_access_token: 'zQ3r5g3iowZnv0s9Zz92q' }, mall_id: mall.id
              end

              it 'creates an account', skip_before: true do
                allow(Koala::Facebook::API).to receive(:new) { Fakebook.new }
                expect {
                  post '/accounts', account: { facebook_access_token: 'zQ3r5g3iowZnv0s9Zz92q' }, mall_id: mall.id
                }.to change(Account, :count).by(1)
              end

              it 'does not create legacy account' do
                expect(PwLegacy::SewUsers::Create).to_not have_received(:call)
              end

              it 'does not subscribe to email programs' do
                expect(PwLegacy::EmailPrograms::Subscribe).to_not have_received(:call)
              end

              it 'sends confirmation email' do
                expect(AccountsMailer).to have_received(:confirmation_instructions)
              end

              it 'does not send welcome email' do
                expect(AccountsMailer).to_not have_received(:send_welcome_email)
              end

              it 'adds the mall_id' do
                expect(JSON.parse(response.body)['account']['mall_ids']).to match_array([mall.id])
              end

              it 'is not confirmed' do
                expect(JSON.parse(response.body)['account']['confirmed_at']).to be_nil
              end

              it 'has status 201 created' do
                expect(response).to have_http_status(:created)
              end
            end
          end
        end

        context 'without mall' do
          context 'confirmed' do
            let(:fakebook) { Fakebook.new }

            before do
              allow(Koala::Facebook::API).to receive(:new) { fakebook }
              allow(PwLegacy::SewUsers::Create).to receive(:call)
              allow(PwLegacy::EmailPrograms::Subscribe).to receive(:call)
              allow(AccountsMailer).to receive(:confirmation_instructions).and_return(double('AccountMailer', deliver: true))
              allow(AccountsMailer).to receive(:send_welcome_email).and_return(double('AccountMailer', deliver: true))

              post '/accounts', account: { facebook_access_token: 'zQ3r5g3iowZnv0s9Zz92q' }
            end

            it 'creates an account', skip_before: true do
              allow(Koala::Facebook::API).to receive(:new) { Fakebook.new }
              expect {
                post '/accounts', account: { facebook_access_token: 'zQ3r5g3iowZnv0s9Zz92q' }
              }.to change(Account, :count).by(1)
            end

            it 'does not create legacy account' do
              expect(PwLegacy::SewUsers::Create).to_not have_received(:call)
            end

            it 'does not subscribe to email programs' do
              expect(PwLegacy::EmailPrograms::Subscribe).to_not have_received(:call)
            end

            it 'does not send confirmation email' do
              expect(AccountsMailer).to_not have_received(:confirmation_instructions)
            end

            it 'does not send welcome email' do
              expect(AccountsMailer).to_not have_received(:send_welcome_email)
            end

            it 'does not add a mall_id' do
              expect(JSON.parse(response.body)['account']['mall_ids']).to match_array([])
            end

            it 'is confirmed' do
              expect(JSON.parse(response.body)['account']['confirmed_at']).to_not be_nil
            end

            it 'has status 201 created' do
              expect(response).to have_http_status(:created)
            end
          end

          context 'unconfirmed' do
            let(:fakebook) { Fakebook.new(verified: false) }

            before do
              allow(Koala::Facebook::API).to receive(:new) { fakebook }
              allow(PwLegacy::SewUsers::Create).to receive(:call)
              allow(PwLegacy::EmailPrograms::Subscribe).to receive(:call)
              allow(AccountsMailer).to receive(:confirmation_instructions).and_return(double('AccountMailer', deliver: true))
              allow(AccountsMailer).to receive(:send_welcome_email).and_return(double('AccountMailer', deliver: true))

              post '/accounts', account: { facebook_access_token: 'zQ3r5g3iowZnv0s9Zz92q' }
            end

            it 'creates an account', skip_before: true do
              allow(Koala::Facebook::API).to receive(:new) { Fakebook.new }
              expect {
                post '/accounts', account: { facebook_access_token: 'zQ3r5g3iowZnv0s9Zz92q' }
              }.to change(Account, :count).by(1)
            end

            it 'does not create legacy account' do
              expect(PwLegacy::SewUsers::Create).to_not have_received(:call)
            end

            it 'does not subscribe to email programs' do
              expect(PwLegacy::EmailPrograms::Subscribe).to_not have_received(:call)
            end

            it 'does not send confirmation email' do
              expect(AccountsMailer).to_not have_received(:confirmation_instructions)
            end

            it 'does not send welcome email' do
              expect(AccountsMailer).to_not have_received(:send_welcome_email)
            end

            it 'does not add a mall_id' do
              expect(JSON.parse(response.body)['account']['mall_ids']).to match_array([])
            end

            it 'is not confirmed' do
              expect(JSON.parse(response.body)['account']['confirmed_at']).to be_nil
            end

            it 'has status 201 created' do
              expect(response).to have_http_status(:created)
            end
          end
        end
      end

      context 'with existing account' do
        context 'with mall' do
          context 'confirmed' do
            context 'first new mall' do
              let(:account) { create(:account) }
              let(:fakebook) { Fakebook.new(email: account.email) }
              let(:program) { create(:program, is_default_signup_program: true) }
              let(:mall) { create(:mall, programs: [program]) }

              before do
                allow(Koala::Facebook::API).to receive(:new) { fakebook }
                allow(PwLegacy::SewUsers::Create).to receive(:call)
                allow(PwLegacy::EmailPrograms::Subscribe).to receive(:call)
                allow(AccountsMailer).to receive(:confirmation_instructions).and_return(double('AccountMailer', deliver: true))
                allow(AccountsMailer).to receive(:send_welcome_email).and_return(double('AccountMailer', deliver: true))

                post '/accounts', account: { facebook_access_token: 'zQ3r5g3iowZnv0s9Zz92q' }, mall_id: mall.id
              end

              it 'does not create an account', skip_before: true do
                expect {
                  post '/accounts', account: { facebook_access_token: 'zQ3r5g3iowZnv0s9Zz92q' }, mall_id: mall.id
                }.to_not change(Account, :count)
              end

              it 'creates legacy account' do
                expect(PwLegacy::SewUsers::Create).to have_received(:call)
              end

              it 'subscribes to email programs' do
                expect(PwLegacy::EmailPrograms::Subscribe).to have_received(:call)
              end

              it 'does not send confirmation email' do
                expect(AccountsMailer).to_not have_received(:confirmation_instructions)
              end

              it 'sends welcome email' do
                expect(AccountsMailer).to have_received(:send_welcome_email)
              end

              it 'adds the mall_id' do
                expect(JSON.parse(response.body)['account']['mall_ids']).to match_array([mall.id])
              end

              it 'is confirmed' do
                expect(JSON.parse(response.body)['account']['confirmed_at']).to_not be_nil
              end

              it 'has status 201 created' do
                expect(response).to have_http_status(:created)
              end
            end

            context 'second new mall' do
              let(:program) { create(:program, is_default_signup_program: true) }
              let(:first_mall) { create(:mall) }
              let(:second_mall) { create(:mall, programs: [program]) }
              let(:account) { create(:account, malls: [first_mall]) }
              let(:fakebook) { Fakebook.new(email: account.email) }

              before do
                allow(Koala::Facebook::API).to receive(:new) { fakebook }
                allow(PwLegacy::SewUsers::Create).to receive(:call)
                allow(PwLegacy::EmailPrograms::Subscribe).to receive(:call)
                allow(AccountsMailer).to receive(:confirmation_instructions).and_return(double('AccountMailer', deliver: true))
                allow(AccountsMailer).to receive(:send_welcome_email).and_return(double('AccountMailer', deliver: true))

                post '/accounts', account: { facebook_access_token: 'zQ3r5g3iowZnv0s9Zz92q' }, mall_id: second_mall.id
              end

              it 'does not create an account', skip_before: true do
                expect {
                  post '/accounts', account: { facebook_access_token: 'zQ3r5g3iowZnv0s9Zz92q' }, mall_id: second_mall.id
                }.to_not change(Account, :count)
              end

              it 'does not create legacy account' do
                expect(PwLegacy::SewUsers::Create).to_not have_received(:call)
              end

              it 'subscribes to email programs' do
                expect(PwLegacy::EmailPrograms::Subscribe).to have_received(:call)
              end

              it 'does not send confirmation email' do
                expect(AccountsMailer).to_not have_received(:confirmation_instructions)
              end

              it 'sends welcome email' do
                expect(AccountsMailer).to have_received(:send_welcome_email)
              end

              it 'adds the mall_id' do
                expect(JSON.parse(response.body)['account']['mall_ids']).to match_array([first_mall.id, second_mall.id])
              end

              it 'is confirmed' do
                expect(JSON.parse(response.body)['account']['confirmed_at']).to_not be_nil
              end

              it 'has status 201 created' do
                expect(response).to have_http_status(:created)
              end
            end

            context 'previous mall' do
              let(:mall) { create(:mall) }
              let(:account) { create(:account, malls: [mall]) }
              let(:fakebook) { Fakebook.new(email: account.email) }

              before do
                allow(Koala::Facebook::API).to receive(:new) { fakebook }
                allow(PwLegacy::SewUsers::Create).to receive(:call)
                allow(PwLegacy::EmailPrograms::Subscribe).to receive(:call)
                allow(AccountsMailer).to receive(:confirmation_instructions).and_return(double('AccountMailer', deliver: true))
                allow(AccountsMailer).to receive(:send_welcome_email).and_return(double('AccountMailer', deliver: true))

                post '/accounts', account: { facebook_access_token: 'zQ3r5g3iowZnv0s9Zz92q' }, mall_id: mall.id
              end

              it 'does not create an account', skip_before: true do
                expect {
                  post '/accounts', account: { facebook_access_token: 'zQ3r5g3iowZnv0s9Zz92q' }, mall_id: mall.id
                }.to_not change(Account, :count)
              end

              it 'does not create legacy account' do
                expect(PwLegacy::SewUsers::Create).to_not have_received(:call)
              end

              it 'does not subscribe to email programs' do
                expect(PwLegacy::EmailPrograms::Subscribe).to_not have_received(:call)
              end

              it 'does not send confirmation email' do
                expect(AccountsMailer).to_not have_received(:confirmation_instructions)
              end

              it 'does not send welcome email' do
                expect(AccountsMailer).to_not have_received(:send_welcome_email)
              end

              it 'has the correct mall_id' do
                expect(JSON.parse(response.body)['account']['mall_ids']).to match_array([mall.id])
              end

              it 'is confirmed' do
                expect(JSON.parse(response.body)['account']['confirmed_at']).to_not be_nil
              end

              it 'has status 201 created' do
                expect(response).to have_http_status(:created)
              end
            end
          end

          context 'unconfirmed' do
            context 'new mall' do
              let(:account) { create(:unconfirmed) }
              let(:fakebook) { Fakebook.new(email: account.email, verified: false) }
              let(:mall) { create(:mall) }

              before do
                allow(Koala::Facebook::API).to receive(:new) { fakebook }
                allow(PwLegacy::SewUsers::Create).to receive(:call)
                allow(PwLegacy::EmailPrograms::Subscribe).to receive(:call)
                allow(AccountsMailer).to receive(:confirmation_instructions).and_return(double('AccountMailer', deliver: true))
                allow(AccountsMailer).to receive(:send_welcome_email).and_return(double('AccountMailer', deliver: true))

                post '/accounts', account: { facebook_access_token: 'zQ3r5g3iowZnv0s9Zz92q' }, mall_id: mall.id
              end

              it 'does not create an account', skip_before: true do
                expect {
                  post '/accounts', account: { facebook_access_token: 'zQ3r5g3iowZnv0s9Zz92q' }, mall_id: mall.id
                }.to_not change(Account, :count)
              end

              it 'does not create legacy account' do
                expect(PwLegacy::SewUsers::Create).to_not have_received(:call)
              end

              it 'does not subscribe to email programs' do
                expect(PwLegacy::EmailPrograms::Subscribe).to_not have_received(:call)
              end

              it 'does not send confirmation email' do
                expect(AccountsMailer).to_not have_received(:confirmation_instructions)
              end

              it 'does not send welcome email' do
                expect(AccountsMailer).to_not have_received(:send_welcome_email)
              end

              it 'adds the mall_id' do
                expect(JSON.parse(response.body)['account']['mall_ids']).to match_array([mall.id])
              end

              it 'is not confirmed' do
                expect(JSON.parse(response.body)['account']['confirmed_at']).to be_nil
              end

              it 'has status 201 created' do
                expect(response).to have_http_status(:created)
              end
            end

            context 'previous mall' do
              let(:mall) { create(:mall) }
              let(:account) { create(:unconfirmed, malls: [mall]) }
              let(:fakebook) { Fakebook.new(email: account.email, verified: false) }

              before do
                allow(Koala::Facebook::API).to receive(:new) { fakebook }
                allow(PwLegacy::SewUsers::Create).to receive(:call)
                allow(PwLegacy::EmailPrograms::Subscribe).to receive(:call)
                allow(AccountsMailer).to receive(:confirmation_instructions).and_return(double('AccountMailer', deliver: true))
                allow(AccountsMailer).to receive(:send_welcome_email).and_return(double('AccountMailer', deliver: true))

                post '/accounts', account: { facebook_access_token: 'zQ3r5g3iowZnv0s9Zz92q' }, mall_id: mall.id
              end

              it 'does not create an account', skip_before: true do
                expect {
                  post '/accounts', account: { facebook_access_token: 'zQ3r5g3iowZnv0s9Zz92q' }, mall_id: mall.id
                }.to_not change(Account, :count)
              end

              it 'does not create legacy account' do
                expect(PwLegacy::SewUsers::Create).to_not have_received(:call)
              end

              it 'does not subscribe to email programs' do
                expect(PwLegacy::EmailPrograms::Subscribe).to_not have_received(:call)
              end

              it 'does not send confirmation email' do
                expect(AccountsMailer).to_not have_received(:confirmation_instructions)
              end

              it 'does not send welcome email' do
                expect(AccountsMailer).to_not have_received(:send_welcome_email)
              end

              it 'has the correct mall_id' do
                expect(JSON.parse(response.body)['account']['mall_ids']).to match_array([mall.id])
              end

              it 'is not confirmed' do
                expect(JSON.parse(response.body)['account']['confirmed_at']).to be_nil
              end

              it 'has status 201 created' do
                expect(response).to have_http_status(:created)
              end
            end
          end
        end

        context 'without mall' do
          context 'confirmed' do
            let(:account) { create(:account) }
            let(:fakebook) { Fakebook.new(email: account.email) }

            before do
              allow(Koala::Facebook::API).to receive(:new) { fakebook }
              allow(PwLegacy::SewUsers::Create).to receive(:call)
              allow(PwLegacy::EmailPrograms::Subscribe).to receive(:call)
              allow(AccountsMailer).to receive(:confirmation_instructions).and_return(double('AccountMailer', deliver: true))
              allow(AccountsMailer).to receive(:send_welcome_email).and_return(double('AccountMailer', deliver: true))

              post '/accounts', account: { facebook_access_token: 'zQ3r5g3iowZnv0s9Zz92q' }
            end

            it 'does not create an account', skip_before: true do
              expect {
                post '/accounts', account: { facebook_access_token: 'zQ3r5g3iowZnv0s9Zz92q' }
              }.to_not change(Account, :count)
            end

            it 'does not create legacy account' do
              expect(PwLegacy::SewUsers::Create).to_not have_received(:call)
            end

            it 'does not subscribe to email programs' do
              expect(PwLegacy::EmailPrograms::Subscribe).to_not have_received(:call)
            end

            it 'does not send confirmation email' do
              expect(AccountsMailer).to_not have_received(:confirmation_instructions)
            end

            it 'does not send welcome email' do
              expect(AccountsMailer).to_not have_received(:send_welcome_email)
            end

            it 'has the correct mall_id' do
              expect(JSON.parse(response.body)['account']['mall_ids']).to match_array([])
            end

            it 'is confirmed' do
              expect(JSON.parse(response.body)['account']['confirmed_at']).to_not be_nil
            end

            it 'has status 201 created' do
              expect(response).to have_http_status(:created)
            end
          end

          context 'unconfirmed' do
            let(:account) { create(:unconfirmed) }
            let(:fakebook) { Fakebook.new(email: account.email, verified: false) }

            before do
              allow(Koala::Facebook::API).to receive(:new) { fakebook }
              allow(PwLegacy::SewUsers::Create).to receive(:call)
              allow(PwLegacy::EmailPrograms::Subscribe).to receive(:call)
              allow(AccountsMailer).to receive(:confirmation_instructions).and_return(double('AccountMailer', deliver: true))
              allow(AccountsMailer).to receive(:send_welcome_email).and_return(double('AccountMailer', deliver: true))

              post '/accounts', account: { facebook_access_token: 'zQ3r5g3iowZnv0s9Zz92q' }
            end

            it 'does not create an account', skip_before: true do
              expect {
                post '/accounts', account: { facebook_access_token: 'zQ3r5g3iowZnv0s9Zz92q' }
              }.to_not change(Account, :count)
            end

            it 'does not create legacy account' do
              expect(PwLegacy::SewUsers::Create).to_not have_received(:call)
            end

            it 'does not subscribe to email programs' do
              expect(PwLegacy::EmailPrograms::Subscribe).to_not have_received(:call)
            end

            it 'does not send confirmation email' do
              expect(AccountsMailer).to_not have_received(:confirmation_instructions)
            end

            it 'does not send welcome email' do
              expect(AccountsMailer).to_not have_received(:send_welcome_email)
            end

            it 'does not add a mall_id' do
              expect(JSON.parse(response.body)['account']['mall_ids']).to match_array([])
            end

            it 'is not confirmed' do
              expect(JSON.parse(response.body)['account']['confirmed_at']).to be_nil
            end

            it 'has status 201 created' do
              expect(response).to have_http_status(:created)
            end
          end
        end
      end
    end
  end
end
