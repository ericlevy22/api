describe MallsController, type: :controller do
  before do
    @mall_1 = create :mall
    @mall_2 = create :mall

    @retailer = create :retailer

    @store_1 = create :store, mall: @mall_1, retailer: @retailer
    @store_2 = create :store, mall: @mall_2, retailer: @retailer

    @number_of_articles = 10
    @number_of_products = 10

    @brand = create :brand

    @number_of_articles.times do
      article_1 = create :article
      article_2 = create :article
      @number_of_products.times do
        product_1 = create :product, brand: @brand
        product_2 = create :product, brand: @brand

        @retailer.products << product_1
        @retailer.products << product_2

        @store_1.products << product_1
        @store_2.products << product_2

        article_1.products << product_1
        article_2.products << product_2

        article_1.products << product_2 if rand > 0.5
        article_2.products << product_1 if rand > 0.5
      end
      @mall_1.articles << article_1
      @mall_2.articles << article_2
    end
    log_in_as_admin!
  end

  describe 'getting articles for different malls' do
    it 'returns the correct articles for Mall 1' do
      get :articles, id: @mall_1.id, account_id: @account.id,
                     account_token: @token, timestamp: @timestamp, limit: 1
      @articles = JSON.parse(response.body)
      expect(@articles['products'].count).to eql(@number_of_products)
      expect(@articles['articles'][0]['id']).to eql(@mall_1.articles.first.id)
    end

    it 'returns the correct articles for Mall 2' do
      get :articles, id: @mall_2.id, account_id: @account.id,
                     account_token: @token, timestamp: @timestamp, limit: 1
      @articles = JSON.parse(response.body)
      expect(@articles['products'].count).to eql(@number_of_products)
      expect(@articles['articles'][0]['id']).to eql(@mall_2.articles.first.id)
    end
  end
end
