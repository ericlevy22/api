describe ProductsController, type: :controller do
  describe 'posting images' do
    let(:product)  { create(:product) }
    let(:image_1)  { create(:image) }
    let(:image_2)  { create(:image) }
    let(:image_3)  { create(:image) }

    before do
      log_in_as_admin!
    end

    it 'successfully saves a product with multiple images' do
      post :create, account_email: @account.email, account_token: @token, timestamp: @timestamp,
                    product: { name: product.name,
                               retailer_id: product.retailer.id,
                               store_ids: product.stores.map(&:id),
                               seo_slug: product.seo_slug,
                               image_ids: [image_1.id, image_2.id] }

      @product = JSON.parse(response.body)['product']

      expect(@product['name']).to eql(product.name)
      expect(@product['image_ids']).to include(image_1.id)
      expect(@product['image_ids']).to include(image_2.id)
      expect(response.status).to eql(201)
    end

    it 'successfully updates a product with multiple images' do
      product.images << image_1
      patch :update, id: product.id, account_email: @account.email, account_token: @token, timestamp: @timestamp,
                    product: { image_ids: [image_2.id, image_3.id] }

      @product = JSON.parse(response.body)['product']

      expect(@product['id']).to eql(product.id)
      expect(@product['image_ids']).to_not include(image_1.id)
      expect(@product['image_ids']).to include(image_2.id)
      expect(@product['image_ids']).to include(image_3.id)
      expect(response.status).to eql(200)
    end
  end
end
