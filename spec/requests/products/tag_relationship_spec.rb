describe ProductsController, type: :controller do
  describe 'posting products' do
    let(:product)      { build(:product) }
    let(:tag)          { create(:tag) }
    let(:second_tag)   { create(:tag) }
    let(:third_tag)    { create(:tag) }
    let(:fourth_tag)   { create(:tag) }

    before do
      log_in_as_admin!
    end

    it 'successfully saves a product with a tag' do
      post :create, account_email: @account.email, account_token: @token, timestamp: @timestamp,
                    product: { name: product.name,
                               retailer_id: product.retailer.id,
                               store_ids: product.stores.map(&:id),
                               image_ids: product.images.map(&:id),
                               seo_slug: product.seo_slug,
                               tag_ids: [tag.id, second_tag.id] }

      @product = JSON.parse(response.body)['product']

      expect(@product['tag_ids']).to include(tag.id, second_tag.id)
      expect(response.status). to eql(201)
    end

    it 'successfully updates a product with new tags' do
      product.save

      post :update, id: product.id, account_email: @account.email, account_token: @token, timestamp: @timestamp,
                    product: { tag_ids: [third_tag.id, fourth_tag.id] }

      @product = JSON.parse(response.body)['product']

      expect(@product['tag_ids']).to include(third_tag.id, fourth_tag.id)
      expect(response.status).to eql(200)
    end
  end
end
