describe ProductsController, type: :controller do
  describe 'posting products' do
    let(:retailer)   { create(:retailer) }
    let(:store_1)    { create(:store, retailer: retailer) }
    let(:store_2)    { create(:store, retailer: retailer) }
    let(:store_3)    { create(:store, retailer: retailer) }
    let(:image)      { create(:image) }
    let(:name)       { Faker::Lorem.sentence(3) }
    let(:seo_slug)   { name.slugify }
    let(:product)    { create(:product) }

    before do
      log_in_as_admin!
    end

    it 'successfully saves a product with one store' do
      post :create, account_email: @account.email, account_token: @token, timestamp: @timestamp,
                    product: { name: name,
                               store_ids: [store_1.id],
                               retailer_id: retailer.id,
                               seo_slug: seo_slug,
                               image_ids: [image.id] }

      @product = JSON.parse(response.body)['product']

      expect(@product['name']).to eql(name)
      expect(@product['store_ids']).to include(store_1.id)
      expect(response.status).to eql(201)
    end

    it 'successfully saves a product with multiple stores' do
      post :create, account_email: @account.email, account_token: @token, timestamp: @timestamp,
                    product: { name: name,
                              store_ids: [store_1.id, store_2.id],
                              retailer_id: retailer.id,
                              seo_slug: seo_slug,
                              image_ids: [image.id] }

      @product = JSON.parse(response.body)['product']

      expect(@product['name']).to eql(name)
      expect(@product['store_ids']).to include(store_1.id)
      expect(@product['store_ids']).to include(store_2.id)
      expect(response.status).to eql(201)
    end

    it 'returns an error if the required fields are missing' do
      post :create, account_email: @account.email, account_token: @token, timestamp: @timestamp,
                    product: { store_ids: store_1.id, seo_slug: seo_slug }

      expect(response.status).to eql(422)
    end

    it 'successfully updates a product with new stores' do
      product.stores << store_1
      patch :update, id: product.id, account_email: @account.email, account_token: @token, timestamp: @timestamp,
                     product: { store_ids: [store_2.id, store_3.id],
                                retailer_id: retailer.id }

      @product = JSON.parse(response.body)['product']

      expect(@product['id']).to eql(product.id)
      expect(@product['name']).to eql(product.name)
      expect(@product['store_ids']).to_not include(store_1.id)
      expect(@product['store_ids']).to include(store_2.id)
      expect(@product['store_ids']).to include(store_3.id)
      expect(response.status).to eql(200)
    end
  end
end
