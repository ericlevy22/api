describe ImagesController, type: :controller do
  describe 'uploading images' do
    context 'as a Base64 file' do
      before do
        allow(Images::Upload).to receive(:image_from_base64)
        allow(Images::Upload).to receive(:upload)
        allow(Base64).to receive(:decode64)
        log_in_as_admin!
      end

      it 'decodes file and uploads to AWS' do
        post :create, account_email: @account.email, account_token: @token, timestamp: @timestamp,
                      image: { file_data: "data:image/png;base64," + Base64.encode64('image file here') }

        expect(Images::Upload).to have_received(:image_from_base64)
        expect(Images::Upload).to have_received(:upload)
      end
    end

    context 'with an image URL' do
      before do
        allow(Images::Upload).to receive(:image_from_url)
        allow(Images::Upload).to receive(:image_from_base64)
        allow(Images::Upload).to receive(:upload)
        log_in_as_admin!
      end

      it 'decodes file and uploads to AWS' do
        post :create, account_email: @account.email, account_token: @token, timestamp: @timestamp,
                      image: { url: Faker::Internet.url }

        expect(Images::Upload).to_not have_received(:image_from_base64)
        expect(Images::Upload).to have_received(:image_from_url)
        expect(Images::Upload).to have_received(:upload)
      end
    end
  end
end
