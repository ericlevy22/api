describe MallsController, type: :controller do
  before do
    log_in_as_admin!
  end

  describe 'searching for a mall by name' do
    let!(:disallowed_mall) { create :mall }
    let!(:mall) { create :mall, name: 'ZZZZZZUnique' }

    it 'returns the matching record' do
      queryTerm = mall.name[0..2].downcase
      get :index, account_email: @account.email, account_token: @token,
                      timestamp: @timestamp, q: queryTerm, limit: 2
      malls = JSON.parse(response.body)['malls'].map{ |mall| mall['id'] }

      expect(malls).to include(mall.id)
      expect(malls).to_not include(disallowed_mall.id)
    end
  end

  describe 'searching for a event by title' do
    let!(:disallowed_event) { create :event }
    let!(:event) { create :event, title: 'ZZZZZZUnique' }
    let!(:mall) { create :mall, events: [event, disallowed_event] }

    it 'returns the matching record' do
      queryTerm = event.title[0..2].downcase
      get :events, id: mall.id, account_email: @account.email, account_token: @token,
                   timestamp: @timestamp, q: queryTerm, limit: 2
      events = JSON.parse(response.body)['events'].map{ |event| event['id'] }

      expect(events).to include(event.id)
      expect(events).to_not include(disallowed_event.id)
    end
  end

  describe 'searching for an article by title' do
    let!(:article) { create :article }
    let!(:mall) { create :mall, articles: [article] }

    it 'returns the matching record' do
      queryTerm = article.title[0..2].downcase
      get :articles, id: mall.id, account_email: @account.email, account_token: @token,
                   timestamp: @timestamp, q: queryTerm, limit: 1
      expect(Sunspot.session).to be_a_search_for(Article)
    end
  end

  describe 'searching for a contact by name' do
    let!(:disallowed_contact) { create :contact }
    let!(:contact) { create :contact, name: 'ZZZZZZUnique' }
    let!(:mall) { create :mall, contacts: [contact, disallowed_contact] }

    it 'returns the matching record' do
      queryTerm = contact.name[0..2].downcase
      get :contacts, id: mall.id, account_email: @account.email, account_token: @token,
      timestamp: @timestamp, q: queryTerm, limit: 2
      contacts = JSON.parse(response.body)['contacts'].map{ |contact| contact['id'] }

      expect(contacts).to include(contact.id)
      expect(contacts).to_not include(disallowed_contact.id)
    end
  end

  describe 'searching for a press_release by name' do
    let!(:disallowed_press_release) { create :press_release }
    let!(:press_release) { create :press_release, name: 'ZZZZZZUnique' }
    let!(:mall) { create :mall, press_releases: [press_release, disallowed_press_release] }

    it 'returns the matching record' do
      queryTerm = press_release.name[0..2].downcase
      get :press_releases, id: mall.id, account_email: @account.email, account_token: @token,
      timestamp: @timestamp, q: queryTerm, limit: 2
      press_releases = JSON.parse(response.body)['press_releases'].map{ |press_release| press_release['id'] }

      expect(press_releases).to include(press_release.id)
      expect(press_releases).to_not include(disallowed_press_release.id)
    end
  end

  describe 'searching for a store by name' do
    let!(:store) { create :store }
    let!(:mall) { create :mall, stores: [store] }

    it 'returns the matching record' do
      queryTerm = store.name[0..2].downcase
      get :stores, id: mall.id, account_email: @account.email, account_token: @token,
                      timestamp: @timestamp, q: queryTerm, limit: 1
      expect(Sunspot.session).to be_a_search_for(Store)
    end
  end

  describe 'searching for a deal by name' do
    let!(:deal) { create :deal }
    let!(:store) { create :store, deals: [deal] }
    let!(:mall) { create :mall, stores: [store] }

    it 'returns the matching record' do
      queryTerm = deal.title[0..2].downcase
      get :deals, id: mall.id, account_email: @account.email, account_token: @token,
                      timestamp: @timestamp, q: queryTerm, limit: 1
      expect(Sunspot.session).to be_a_search_for(Deal)
    end
  end

  describe 'searching for an ifeature by name' do
    let!(:disallowed_ifeature) { create :ifeature }
    let!(:ifeature) { create :ifeature, headline: 'ZZZZZZUnique' }
    let!(:mall) { create :mall, ifeatures: [ifeature, disallowed_ifeature] }

    it 'returns the matching record' do
      queryTerm = ifeature.headline[0..2].downcase
      get :ifeatures, id: mall.id, account_email: @account.email, account_token: @token,
      timestamp: @timestamp, q: queryTerm, limit: 2
      ifeatures = JSON.parse(response.body)['i_features'].map{ |ifeature| ifeature['id'] }

      expect(ifeatures).to include(ifeature.id)
      expect(ifeatures).to_not include(disallowed_ifeature.id)
    end
  end

  describe 'searching for a job_listing by name' do
    let!(:disallowed_job_listing) { create :job_listing }
    let!(:job_listing) { create :job_listing, title: 'ZZZZZZUnique' }
    let!(:store) { create :store, job_listings: [job_listing, disallowed_job_listing] }
    let!(:mall) { create :mall, stores: [store] }

    it 'returns the matching record' do
      queryTerm = job_listing.title[0..2].downcase
      get :job_listings, id: mall.id, account_email: @account.email, account_token: @token,
      timestamp: @timestamp, q: queryTerm, limit: 2
      job_listings = JSON.parse(response.body)['job_listings'].map{ |job_listing| job_listing['id'] }

      expect(job_listings).to include(job_listing.id)
      expect(job_listings).to_not include(disallowed_job_listing.id)
    end
  end

  describe 'searching for a mall_message by name' do
    let!(:disallowed_mall_message) { create :mall_message }
    let!(:mall_message) { create :mall_message, description: 'ZZZZZZUnique' }
    let!(:mall) { create :mall, mall_messages: [mall_message, disallowed_mall_message] }

    it 'returns the matching record' do
      queryTerm = mall_message.description[0..2].downcase
      get :mall_messages, id: mall.id, account_email: @account.email, account_token: @token,
      timestamp: @timestamp, q: queryTerm, limit: 2
      mall_messages = JSON.parse(response.body)['mall_messages'].map{ |mall_message| mall_message['id'] }

      expect(mall_messages).to include(mall_message.id)
      expect(mall_messages).to_not include(disallowed_mall_message.id)
    end
  end
end
