describe ArticlesController, type: :controller do
  before do
    create :article

    log_in_as_admin!
  end

  describe 'searching for a article by name' do
    let!(:article) { create :article }

    it 'returns the matching record' do
      queryTerm = article.title[0..2].downcase
      get :index, account_email: @account.email, account_token: @token,
                      timestamp: @timestamp, q: queryTerm, limit: 1
      expect(Sunspot.session).to be_a_search_for(Article)
    end
  end
end
