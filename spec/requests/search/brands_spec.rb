describe BrandsController, type: :controller do
  before do
    log_in_as_admin!
  end

  describe 'searching for a brand by name' do
    let!(:disallowed_brand) { create :brand }
    let!(:brand) { create :brand, name: 'ZZZUnique' }

    it 'returns the matching record' do
      queryTerm = 'zzz'
      get :index, account_email: @account.email, account_token: @token,
                      timestamp: @timestamp, q: queryTerm, limit: 1
      brands = JSON.parse(response.body)['brands'].map{ |brand| brand['id'] }

      expect(brands).to include(brand.id)
      expect(brands).to_not include(disallowed_brand.id)
    end
  end
end
