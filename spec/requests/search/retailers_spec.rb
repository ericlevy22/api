describe RetailersController, type: :controller do
  before do
    log_in_as_admin!
  end

  describe 'searching for a retailer by name' do
    let!(:disallowed_retailer) { create :retailer }
    let!(:retailer) { create :retailer, name: 'ZZZUnique' }

    it 'returns the matching record' do
      queryTerm = 'zzz'
      get :index, account_email: @account.email, account_token: @token,
                      timestamp: @timestamp, q: queryTerm, limit: 1
      retailers = JSON.parse(response.body)['retailers'].map{ |retailer| retailer['id'] }

      expect(retailers).to include(retailer.id)
      expect(retailers).to_not include(disallowed_retailer.id)
    end
  end
end
