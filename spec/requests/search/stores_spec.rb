describe StoresController, type: :controller do
  before do
    create :store

    log_in_as_admin!
  end

  describe 'searching for a store by name' do
    let!(:store) { create :store }
    it 'returns the matching record' do
      queryTerm = store.name[0..2].downcase
      get :index, account_email: @account.email, account_token: @token,
                      timestamp: @timestamp, q: queryTerm, limit: 1
      expect(Sunspot.session).to be_a_search_for(Store)
    end
  end
end
