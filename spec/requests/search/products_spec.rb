describe ProductsController, type: :controller do
  before do
    log_in_as_admin!
  end

  describe 'searching for a product by name' do
    let!(:disallowed_product) { create :product }
    let!(:product) { create :product, name: 'ZZZUnique' }

    it 'returns the matching record' do
      queryTerm = 'zzz'
      get :index, account_email: @account.email, account_token: @token,
                  timestamp: @timestamp, q: queryTerm, limit: 1
      products = JSON.parse(response.body)['products'].map{ |product| product['id'] }

      expect(products).to include(product.id)
      expect(products).to_not include(disallowed_product.id)
    end
  end
end
