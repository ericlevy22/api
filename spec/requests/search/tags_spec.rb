describe TagsController, type: :controller do
  before do
    log_in_as_admin!
  end

  describe 'searching for a tag by name' do
    let!(:disallowed_tag) { create :tag }
    let!(:tag) { create :tag, name: 'ZZZUnique' }

    it 'returns the matching record' do
      queryTerm = 'zzz'
      get :index, account_email: @account.email, account_token: @token,
                  timestamp: @timestamp, q: queryTerm, limit: 1
      tags = JSON.parse(response.body)['tags'].map{ |tag| tag['id'] }

      expect(tags).to include(tag.id)
      expect(tags).to_not include(disallowed_tag.id)
    end
  end
end
