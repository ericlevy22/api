describe DealsController, type: :controller do
  before do
    create :deal

    log_in_as_admin!
  end

  describe 'searching for a deal by name' do
    let!(:deal) { create :deal }

    it 'returns the matching record' do
      queryTerm = deal.title[0..2].downcase
      get :index, account_email: @account.email, account_token: @token,
                      timestamp: @timestamp, q: queryTerm, limit: 1
      expect(Sunspot.session).to be_a_search_for(Deal)
    end
  end
end
