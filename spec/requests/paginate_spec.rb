describe MallsController, type: :controller do
  before(:all) do
    @mall = create :mall, id: rand(1000..2000)
    @id_1 = rand(1000..1999)
    @id_2 = rand(2000..2999)
    @id_3 = rand(3000..3999)
  end

  describe 'getting a list of articles for a mall' do
    before do
      log_in_as_admin!
      @mall.articles << create(:article, id: @id_2, display_at: 2.days.ago)
      @mall.articles << create(:article, id: @id_3, display_at: 3.days.ago)
      @mall.articles << create(:article, id: @id_1, display_at: 1.day.ago)
    end

    it 'returns a list ordered by display_at' do
      get :articles, id: @mall.id, account_id: @account.id, account_token: @token, timestamp: @timestamp
      @articles = JSON.parse(response.body)

      expect(@articles['articles'][0]['id']).to eql(@id_1)
      expect(@articles['articles'][1]['id']).to eql(@id_2)
      expect(@articles['articles'][2]['id']).to eql(@id_3)
    end
  end

  describe 'getting a list of contacts for a mall' do
    before do
      log_in_as_client!(resource: @mall, traits: :mall_channel)
      @mall.contacts << create(:contact, id: @id_2)
      @mall.contacts << create(:contact, id: @id_3)
      @mall.contacts << create(:contact, id: @id_1)
    end

    it 'returns a list ordered by the default order (id)' do
      get :contacts, id: @mall.id, account_id: @account.id, account_token: @token, timestamp: @timestamp
      @contacts = JSON.parse(response.body)

      expect(@contacts['contacts'][0]['id']).to eql(@id_1)
      expect(@contacts['contacts'][1]['id']).to eql(@id_2)
      expect(@contacts['contacts'][2]['id']).to eql(@id_3)
    end
  end

  describe 'getting a list of press releases for a mall' do
    before do
      log_in_as_client!(resource: @mall, traits: :mall_channel)
      @mall.press_releases << create(:press_release, id: @id_2, display_at: 2.days.ago)
      @mall.press_releases << create(:press_release, id: @id_3, display_at: 3.days.ago)
      @mall.press_releases << create(:press_release, id: @id_1, display_at: 1.day.ago)
    end

    it 'returns a list ordered by the default order (id)' do
      get :press_releases, id: @mall.id, account_id: @account.id, account_token: @token, timestamp: @timestamp
      @press_releases = JSON.parse(response.body)

      expect(@press_releases['press_releases'][0]['id']).to eql(@id_1)
      expect(@press_releases['press_releases'][1]['id']).to eql(@id_2)
      expect(@press_releases['press_releases'][2]['id']).to eql(@id_3)
    end
  end

  describe 'getting a list of stores for a mall' do
    before do
      log_in_as_client!(resource: @mall, traits: :mall_channel)
      @mall.stores << create(:store, id: @id_2)
      @mall.stores << create(:store, id: @id_3)
      @mall.stores << create(:store, id: @id_1)

      @ordered_stores = @mall.stores.reorder(:id)
    end

    it 'returns a list ordered by the default order (id)' do
      get :stores, id: @mall.id, account_id: @account.id, account_token: @token, timestamp: @timestamp
      @stores = JSON.parse(response.body)

      expect(@stores['stores'][0]['id']).to eql(@id_1)
      expect(@stores['stores'][1]['id']).to eql(@id_2)
      expect(@stores['stores'][2]['id']).to eql(@id_3)
    end
  end

  describe 'getting a list of events for a mall' do
    before do
      log_in_as_client!(resource: @mall, traits: :mall_channel)
      @mall.events << create(:event, id: @id_2, start_at: Time.now + 2.days)
      @mall.events << create(:event, id: @id_3, start_at: Time.now + 3.days)
      @mall.events << create(:event, id: @id_1, start_at: Time.now + 1.days)
    end

    it 'returns a list ordered by start_at date' do
      get :events, id: @mall.id, account_id: @account.id, account_token: @token, timestamp: @timestamp
      @events = JSON.parse(response.body)

      expect(@events['events'][0]['id']).to eql(@id_1)
      expect(@events['events'][1]['id']).to eql(@id_2)
      expect(@events['events'][2]['id']).to eql(@id_3)
    end
  end

  describe 'getting a list of ifeatures for a mall' do
    before do
      log_in_as_client!(resource: @mall, traits: :mall_channel)
      @mall.ifeatures << create(:ifeature, id: @id_2)
      @mall.ifeatures << create(:ifeature, id: @id_3)
      @mall.ifeatures << create(:ifeature, id: @id_1)
    end

    it 'returns a list ordered by the default order (id)' do
      get :ifeatures, id: @mall.id, account_id: @account.id, account_token: @token, timestamp: @timestamp
      @ifeatures = JSON.parse(response.body)

      expect(@ifeatures['i_features'][0]['id']).to eql(@id_1)
      expect(@ifeatures['i_features'][1]['id']).to eql(@id_2)
      expect(@ifeatures['i_features'][2]['id']).to eql(@id_3)
    end
  end

  describe 'getting a list of social_feeds for a mall' do
    before do
      log_in_as_client!(resource: @mall, traits: :mall_channel)
      @mall.social_feeds << create(:social_feed, id: @id_2)
      @mall.social_feeds << create(:social_feed, id: @id_3)
      @mall.social_feeds << create(:social_feed, id: @id_1)
    end

    it 'returns a list ordered by the default order (id)' do
      get :social_feeds, id: @mall.id, account_id: @account.id, account_token: @token, timestamp: @timestamp
      @social_feeds = JSON.parse(response.body)

      expect(@social_feeds['social_feeds'][0]['id']).to eql(@id_1)
      expect(@social_feeds['social_feeds'][1]['id']).to eql(@id_2)
      expect(@social_feeds['social_feeds'][2]['id']).to eql(@id_3)
    end
  end

  describe 'getting a list of job_listings for a mall' do
    before do
      log_in_as_client!(resource: @mall, traits: :mall_channel)
      @store = create :store
      @store.job_listings << create(:job_listing, id: @id_2)
      @store.job_listings << create(:job_listing, id: @id_3)
      @store.job_listings << create(:job_listing, id: @id_1)
      @mall.stores << @store
    end

    it 'returns a list ordered by the default order (id)' do
      get :job_listings, id: @mall.id, account_id: @account.id, account_token: @token, timestamp: @timestamp
      @job_listings = JSON.parse(response.body)

      expect(@job_listings['job_listings'][0]['id']).to eql(@id_1)
      expect(@job_listings['job_listings'][1]['id']).to eql(@id_2)
      expect(@job_listings['job_listings'][2]['id']).to eql(@id_3)
    end
  end

  describe 'getting a list of mall_messages for a mall' do
    before do
      log_in_as_client!(resource: @mall, traits: :mall_channel)
      @mall.mall_messages << create(:mall_message, id: @id_2)
      @mall.mall_messages << create(:mall_message, id: @id_3)
      @mall.mall_messages << create(:mall_message, id: @id_1)
    end

    it 'returns a list ordered by the default order (id)' do
      get :mall_messages, id: @mall.id, account_id: @account.id, account_token: @token, timestamp: @timestamp
      @mall_messages = JSON.parse(response.body)

      expect(@mall_messages['mall_messages'][0]['id']).to eql(@id_1)
      expect(@mall_messages['mall_messages'][1]['id']).to eql(@id_2)
      expect(@mall_messages['mall_messages'][2]['id']).to eql(@id_3)
    end
  end

  describe 'getting a list of programs for a mall' do
    before do
      log_in_as_client!(resource: @mall, traits: :mall_channel)
      @mall.programs << create(:program, id: @id_2)
      @mall.programs << create(:program, id: @id_3)
      @mall.programs << create(:program, id: @id_1)
    end

    it 'returns a list ordered by the default order (id)' do
      get :programs, id: @mall.id, account_id: @account.id, account_token: @token, timestamp: @timestamp
      @programs = JSON.parse(response.body)

      expect(@programs['programs'][0]['id']).to eql(@id_1)
      expect(@programs['programs'][1]['id']).to eql(@id_2)
      expect(@programs['programs'][2]['id']).to eql(@id_3)
    end
  end
end
