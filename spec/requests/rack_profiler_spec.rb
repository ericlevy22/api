describe MallsController, type: :controller do
  describe 'rack profile access by an admin' do
    before do
      log_in_as_admin!
      allow(Rack::MiniProfiler).to receive(:authorize_request)
      get :index, account_id: @account.id, account_token: @token, timestamp: @timestamp
    end

    it 'authorizes rack mini profiler' do
      expect(Rack::MiniProfiler).to have_received(:authorize_request)
    end
  end

  describe 'rack profile access by a client' do
    before do
      log_in_as_client!
      allow(Rack::MiniProfiler).to receive(:authorize_request)
      get :index, account_id: @account.id, account_token: @token, timestamp: @timestamp
    end

    it 'does not authorize rack mini profiler' do
      expect(Rack::MiniProfiler).to_not have_received(:authorize_request)
    end
  end
end
