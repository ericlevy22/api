describe Membership, type: :model do
  describe 'from a factory' do
    it 'can be built' do
      expect(build :membership).to be_valid
    end

    it 'can be created' do
      expect(create(:membership).id).to_not be_nil
    end
  end

  describe 'validations' do
    it 'should require a program' do
      expect(build :membership, program: nil).to be_invalid
    end

    it 'should require an identity' do
      expect(build :membership, identity: nil).to be_invalid
    end
  end
end
