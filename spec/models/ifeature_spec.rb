describe Ifeature, type: :model do
  describe '.current' do
    let!(:past) { create :ifeature, end_at: 1.month.ago }
    let!(:present) { create :ifeature }
    let!(:future) { create :ifeature, start_at: 1.month.from_now }
    let!(:inactive) { create :ifeature, is_active: false }

    it 'includes currently active ifeatures' do
      expect(Ifeature.current).to include(present)
    end

    it 'excludes inactive and expired ifeatures' do
      expect(Ifeature.current).to_not include(inactive)
      expect(Ifeature.current).to_not include(past)
      expect(Ifeature.current).to_not include(future)
    end
  end

  describe 'validation' do
    it 'should require a headline' do
      expect(build :ifeature, headline: nil).to be_invalid
    end

    it 'should require a start date' do
      expect(build :ifeature, start_at: nil).to be_invalid
    end

    it 'should require an end date' do
      expect(build :ifeature, end_at: nil).to be_invalid
    end

    it 'should require an seo slug' do
      expect(build :ifeature, seo_slug: nil).to be_invalid
    end
  end

  describe 'from a factory' do
    it 'can be built' do
      expect(build :ifeature).to be_valid
    end

    it 'can be created' do
      ifeature = create :ifeature
      expect(ifeature.id).to_not be_nil
    end
  end
end
