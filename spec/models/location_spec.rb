describe Location, type: :model do
  describe 'from a factory' do
    it 'can be built' do
      expect(build :location).to be_valid
    end

    it 'can be created' do
      location = create :location
      expect(location.id).to_not be_nil
    end
  end

  describe 'Association' do
    let(:location) { create(:location) }

    context 'has many' do
      it 'Stores' do
        3.times { location.stores << (build :store) }
        expect(location.stores.size).to eq(3)
      end

      it 'Malls' do
        3.times { location.malls << (build :mall) }
        expect(location.malls.size).to eq(3)
      end
    end
  end

  describe 'searchable', :sunspot do
    it 'has searchable field location' do
      have_searchable_field(:location)
    end

    it 'by lat and long' do
      Location.by_lat_and_long(39.74585, -104.998929, 50)
      expect(Sunspot.session).to be_a_search_for(Location)
    end
  end
end
