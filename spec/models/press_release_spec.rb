RSpec.describe PressRelease, type: :model do
  include_examples 'sinceable'

  describe 'from a factory' do
    it 'can be built' do
      expect(build :press_release).to be_valid
    end

    it 'can be created' do
      press_release = create :press_release
      expect(press_release.id).to_not be_nil
    end
  end

  describe 'validation' do
    it 'should require a mall' do
      expect(build :press_release, mall: nil).to be_invalid
    end

    it 'should require a headline' do
      expect(build :press_release, headline: nil).to be_invalid
    end

    it 'should require a press release type' do
      expect(build :press_release, pr_type: nil).to be_invalid
    end

    it 'should require a display at date' do
      expect(build :press_release, display_at: nil).to be_invalid
    end

    it 'should require an active indicator' do
      expect(build :press_release, is_active: nil).to be_invalid
    end

    it 'should require content' do
      expect(build :press_release, content: nil).to be_invalid
    end

    it 'should require an seo slug' do
      expect(build :press_release, seo_slug: nil).to be_invalid
    end
  end

  describe 'Association' do
    let(:press_release) { create(:press_release) }

    context 'belongs to' do
      it 'Mall' do
        mall = build(:mall)
        press_release = mall.press_releases << build(:press_release)
        expect(mall.press_releases).to be(press_release)
      end
    end
  end
end
