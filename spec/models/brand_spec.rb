describe Brand, type: :model do
  include_examples 'sinceable'

  describe 'from a factory' do
    it 'can be built' do
      expect(build :brand).to be_valid
    end

    it 'can be created' do
      brand = create :brand
      expect(brand.id).to_not be_nil
    end
  end

  describe 'validations' do
    it 'should require that a name is present' do
      expect(build :brand, name: nil).to be_invalid
    end

    it 'should require that a name is unique' do
      create :brand, name: 'Foo'
      expect(build :brand, name: 'Foo').to be_invalid
    end

    it 'should require an seo_slug' do
      expect(build :brand, seo_slug: nil).to be_invalid
    end

    it 'should require that a brand seo slug is unique' do
      create :brand, seo_slug: 'foo'
      expect(build :brand, seo_slug: 'foo').to be_invalid
    end
  end

  describe 'Association' do
    let(:brand) { build :brand }

    context 'has many' do
      it 'Images' do
        3.times { brand.images << (build :image) }
        expect(brand.images.size).to eq(3)
      end
    end
  end
end
