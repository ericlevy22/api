describe Group, type: :model do
  describe 'from a factory' do
    it 'can be built' do
      expect(build :group).to be_valid
    end

    it 'can be created' do
      group = create :group
      expect(group.id).to_not be_nil
    end
  end

  describe 'validations' do
    it 'requires a name' do
      expect(build :group, name: nil).to be_invalid
    end
  end
end
