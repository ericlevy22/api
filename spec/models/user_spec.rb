RSpec.describe User, type: :model do
  describe 'associations' do
    it 'identities' do
      user = create(:user)
      user.identities << create(:identity)
      expect(user.identities).to_not be_empty
    end

    it 'is really destroyable' do
      account = create(:account, malls: [create(:mall)])
      expect { account.user.destroy }.to_not raise_error
    end
  end
end
