describe Retailer, type: :model do
  include_examples 'sinceable'

  describe 'from a factory' do
    it 'can be built' do
      expect(build :retailer).to be_valid
    end

    it 'can be created' do
      retailer = create :retailer
      expect(retailer.id).to_not be_nil
    end
  end

  describe 'validation' do
    it 'requires an seo slug' do
      expect(build :retailer, seo_slug: nil).to be_invalid
    end
  end

  describe 'Association' do
    let(:retailer) { create :retailer }

    context 'has many' do
      it 'Stores' do
        3.times { retailer.stores << (build :store) }
        expect(retailer.stores.size).to eq(3)
      end

      it 'Deals' do
        3.times { retailer.deals << (build :deal) }
        expect(retailer.deals.size).to eq(3)
      end

      it 'Images' do
        3.times { retailer.images << (build :image) }
        expect(retailer.images.size).to eq(3)
      end

      it 'Products' do
        3.times { retailer.products << (build :product) }
        expect(retailer.products.size).to eq(3)
      end
    end

    context 'through a Store' do
      it 'has many Malls' do
        3.times { retailer.stores << (build :store, mall: (build :mall)) }
        expect(retailer.malls.size).to eq(3)
      end
    end

    describe 'is taggable' do
      context 'can add a new tag' do
        subject { lambda {
          retailer.tags << create(:tag, name: 'New Tag')
          retailer.reload
        } }

        it 'should create a new Tag' do
          expect(subject).to change(Tag, :count).by(1)
        end

        it 'should add the tag' do
          expect(subject).to change(retailer.tags, :count).by(1)
        end
      end

      context 'can add an existing tag' do
        before { create(:tag, name: 'Old Tag') }

        subject { lambda {
          retailer.tags << Tag.where(name: 'Old Tag').first_or_create(name: 'Old Tag')
          retailer.reload
        } }

        it 'should not create a new Tag' do
          expect(subject).to change(Tag, :count).by(0)
        end

        it 'should add the tag' do
          expect(subject).to change(retailer.tags, :count).by(1)
        end
      end
    end
  end
end
