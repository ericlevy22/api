describe Article, type: :model do
  include_examples 'sinceable'

  describe 'from a factory' do
    it 'can be built' do
      expect(build :article).to be_valid
    end

    it 'can be created' do
      article = create :article
      expect(article.id).to_not be_nil
    end
  end

  describe 'validation' do
    it 'requires an seo slug' do
      expect(build :article, seo_slug: nil).to have(1).error_on(:seo_slug)
    end
  end

  describe 'Association' do
    let(:article) { create :article }

    context 'has many' do
      it 'Images' do
        expect { article.images << build(:image) }.to change(article.images, :count).by(1)
      end
    end

    describe 'is taggable' do
      context 'can add a new tag' do
        subject { lambda {
          article.tags << create(:tag, name: 'New Tag')
          article
        } }

        it 'should create a new Tag' do
          expect(subject).to change(Tag, :count).by(1)
        end

        it 'should add the tag' do
          expect(subject).to change(article.tags, :count).by(1)
        end
      end

      context 'can add an existing tag' do
        before { create(:tag, name: 'Old Tag') }

        subject { lambda {
          article.tags << Tag.where(name: 'Old Tag').first_or_create(name: 'Old Tag')
          article
        } }

        it 'should not create a new Tag' do
          expect(subject).to change(Tag, :count).by(0)
        end

        it 'should add the tag' do
          expect(subject).to change(article.tags, :count).by(1)
        end
      end
    end
  end

  describe 'searchable', :sunspot do
    context 'has searchable fields' do
      it 'title' do
        have_searchable_field(:title)
      end

      it 'subtitle' do
        have_searchable_field(:subtitle)
      end

      it 'content' do
        have_searchable_field(:content)
      end

      it 'author' do
        have_searchable_field(:author)
      end
    end

    it 'by keyword' do
      Article.by_keyword('foo')
      expect(Sunspot.session).to be_a_search_for(Article)
    end
  end
end
