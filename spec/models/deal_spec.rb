describe Deal, type: :model do
  include_examples 'sinceable'

  describe 'from a factory' do
    it 'can be built' do
      expect(build :deal).to be_valid
    end

    it 'can be created' do
      deal = create :deal
      expect(deal.id).to_not be_nil
    end
  end

  describe 'validation' do
    it 'should require a title' do
      expect(build :deal, title: nil).to be_invalid
    end

    it 'should require a start_at date' do
      expect(build :deal, start_at: nil).to be_invalid
    end

    it 'should require a end_at date' do
      expect(build :deal, end_at: nil).to be_invalid
    end

    it 'should require a description' do
      expect(build :deal, description: nil).to be_invalid
    end

    it 'should require a retailer' do
      expect(build :deal, retailer: nil).to be_invalid
    end

    it 'should require an seo slug' do
      expect(build :deal, seo_slug: nil).to be_invalid
    end
  end

  describe 'Association' do
    let(:deal) { create(:deal) }

    context 'belongs to' do
      it 'Retailer' do
        expect(deal.build_retailer(attributes_for :retailer)).to be_valid
      end
    end

    context 'has many' do
      it 'Image' do
        expect{deal.images << (build :image)}.to change(deal.images, :size).by(1)
      end

      it 'stores' do
        expect{ deal.stores << (build :store) }.to change(deal.stores, :size).by(1)
      end
    end

    describe 'is taggable' do
      context 'can add a new tag' do
        subject { lambda {
          deal.tags << create(:tag)
          deal.reload
        } }

        it 'should create a new Tag' do
          expect(subject).to change(Tag, :count).by(1)
        end

        it 'should add the tag' do
          expect(subject).to change(deal.tags, :count).by(1)
        end
      end

      context 'can add an existing tag' do
        let!(:tag) { create(:tag, name: 'Old Tag') }

        subject { lambda {
          deal.tags << tag
          deal.reload
        } }

        it 'should not create a new Tag' do
          expect(subject).to change(Tag, :count).by(0)
        end

        it 'should add the tag' do
          expect(subject).to change(deal.tags, :count).by(1)
        end
      end
    end
  end

  describe 'searchable', :sunspot do
    context 'has searchable fields' do
      it 'title' do
        have_searchable_field(:title)
      end

      it 'description' do
        have_searchable_field(:description)
      end

      it 'store_name' do
        have_searchable_field(:store_name)
      end

      it 'store_sort_name' do
        have_searchable_field(:store_sort_name)
      end
    end

    it 'by keyword' do
      Deal.by_keyword('foo')
      expect(Sunspot.session).to be_a_search_for(Deal)
    end
  end
end
