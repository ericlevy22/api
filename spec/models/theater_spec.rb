RSpec.describe Theater, type: :model do
  describe 'from a factory' do
    it 'can be built' do
      expect(build :theater).to be_valid
    end

    it 'can be created' do
      theater = create :theater
      expect(theater.id).to_not be_nil
    end
  end
end
