describe Image, type: :model do
  describe 'from a factory' do
    it 'can be built' do
      expect(build :image).to be_valid
    end

    it 'can be created' do
      image = create :image
      expect(image.id).to_not be_nil
    end
  end

  describe 'validations' do
    it 'requires a url' do
      expect(build :image, url: nil).to have(1).error_on(:url)
    end
  end

  describe 'Association' do
    let(:image) { create :image }

    describe 'is taggable' do
      context 'can add a new tag' do
        subject { lambda {
          image.tags << create(:tag, name: 'New Tag')
          image.reload
        } }

        it 'should create a new Tag' do
          expect(subject).to change(Tag, :count).by(1)
        end

        it 'should add the tag' do
          expect(subject).to change(image.tags, :count).by(1)
        end
      end

      context 'can add an existing tag' do
        before do
          create(:tag, name: 'Old Tag')
        end

        subject { lambda {
            image.tags << Tag.where(name: 'Old Tag').first_or_create(name: 'Old Tag')
            image.reload
          } }

        it 'should not create a new Tag' do
          expect(subject).to change(Tag, :count).by(0)
        end

        it 'should add the tag' do
          expect(subject).to change(image.tags, :count).by(1)
        end
      end
    end
  end
end
