describe MallMessage, type: :model do
  describe 'from a factory' do
    it 'can be built' do
      expect(build :mall_message).to be_valid
    end

    it 'can be created' do
      message = create :mall_message
      expect(message.id).to_not be_nil
    end
  end

  describe 'validation' do
    it 'should require a start date' do
      expect(build :mall_message, start_at: nil).to be_invalid
    end

    it 'should require an ending date' do
      expect(build :mall_message, end_at: nil).to be_invalid
    end

    it 'should require a description' do
      expect(build :mall_message, description: nil).to be_invalid
    end
  end

  describe 'Association' do
    context 'belongs to' do
      it 'Mall' do
        mall_message = build :mall_message
        mall = build :mall
        mall.mall_messages << mall_message
        expect(mall.mall_messages).to eq([mall_message])
      end
    end
  end
end
