describe Product, type: :model do
  include_examples 'sinceable'

  describe 'from a factory' do
    it 'can be built' do
      expect(build :product).to be_valid
    end

    it 'can be created' do
      product = create :product
      expect(product.id).to_not be_nil
    end
  end

  describe 'validations' do
    it 'requires a retailer' do
      expect(build :product, retailer: nil, stores: [create(:store)]).to have(1).error_on(:retailer)
    end

    it 'requires a store' do
      expect(build :product, stores: []).to be_invalid
    end

    it 'requires an image' do
      expect(build :product, images: []).to be_invalid
    end

    it 'requires a name' do
      expect(build :product, name: nil).to be_invalid
    end

    it 'requires an seo slug' do
      expect(build :product, seo_slug: nil).to be_invalid
    end

    it 'requires all stores to have the same retailer' do
      expect(build :product, stores: [create(:store)], retailer: create(:retailer)).to have(1).error_on(:stores)
    end
  end

  describe 'Association' do
    let(:product) { create :product }

    context 'has many' do
      it 'Images' do
        expect { product.images << create(:image) }.to change(product.images, :count).by(1)
      end
    end

    context 'belongs to' do
      it 'Brand' do
        expect(product.build_brand((build :brand).attributes)).to be_valid
      end

      it 'Retailer' do
        expect(product.build_retailer((build :retailer).attributes)).to be_valid
      end
    end

    context 'through Content Location' do
      it 'has many stores' do
        store = build :store
        product.stores << store
        expect(product.stores.size).to eq(2)
      end
    end

    describe 'is taggable' do
      context 'can add a new tag' do
        subject { lambda {
          product.tags << create(:tag, name: 'New Tag')
          product.reload
        } }

        it 'should create a new Tag' do
          expect(subject).to change(Tag, :count).by(1)
        end

        it 'should add the tag' do
          expect(subject).to change(product.tags, :count).by(1)
        end
      end

      context 'can add an existing tag' do
        before { create(:tag, name: 'Old Tag') }

        subject { lambda {
          product.tags << Tag.where(name: 'Old Tag').first_or_create(name: 'Old Tag')
          product.reload
        } }

        it 'should not create a new Tag' do
          expect(subject).to change(Tag, :count).by(0)
        end

        it 'should add the tag' do
          expect(subject).to change(product.tags, :count).by(1)
        end
      end
    end
  end
end
