describe Account, type: :model do
  include_examples 'sinceable'

  describe 'from a factory' do
    it 'can be built' do
      expect(build :account).to be_valid
    end

    it 'can be created' do
      account = create :account
      expect(account.id).to_not be_nil
    end
  end

  describe 'associations' do
    let(:account) { build :account }

    context 'has many' do
      it 'roles' do
        account.add_role :foo
        expect(account.roles.size).to eq(1)
      end
    end
  end

  describe 'validations' do
    context 'email' do
      let(:account) { create(:account) }

      it 'does not allow duplicates' do
        expect(build(:account, email: account.email)).to_not be_valid
      end

      it 'does not allow plus sign aliases of existing accounts' do
        expect(build(:account, email: "#{account.email.gsub('@', '+123@')}")).to_not be_valid
      end

      it 'ignores case' do
        expect(build(:account, email: account.email.chars.map { |c| rand > 0.5 ? c.upcase : c }.join)).to_not be_valid
      end

      it 'does not allow dot aliases of existing accounts' do
        expect(build(:account, email: account.email.insert(1, '.'))).to_not be_valid
      end
    end
  end
end
