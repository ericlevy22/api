describe Keyword, type: :model do
  describe 'from a factory' do
    it 'can be built' do
      expect(build :keyword).to be_valid
    end

    it 'can be created' do
      keyword = create :keyword
      expect(keyword.id).to_not be_nil
    end
  end

  describe 'validations' do
    it 'requires a name' do
      expect(build :keyword, name: nil).to be_invalid
    end
  end
end
