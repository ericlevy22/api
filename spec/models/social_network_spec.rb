describe SocialNetwork, type: :model do
  describe 'from a factory' do
    it 'can be built' do
      expect(build :social_network).to be_valid
    end

    it 'can be created' do
      social_network = create :social_network
      expect(social_network.id).to_not be_nil
    end
  end
end
