describe Tag, type: :model do
  describe 'from a factory' do
    it 'can be built' do
      expect(build :tag).to be_valid
    end

    it 'can be created' do
      tag = create :tag
      expect(tag.id).to_not be_nil
    end
  end

  describe 'validations' do
    it 'requires a name' do
      expect(build :tag, name: nil).to be_invalid
    end
  end
end
