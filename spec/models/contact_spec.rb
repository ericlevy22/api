describe Contact, type: :model do
  include_examples 'sinceable'

  describe 'from a factory' do
    it 'can be built' do
      expect(build :contact).to be_valid
    end

    it 'can be created' do
      contact = create :contact
      expect(contact.id).to_not be_nil
    end
  end
end
