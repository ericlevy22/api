describe JobListing, type: :model do
  describe 'from a factory' do
    it 'can be built' do
      expect(build :job_listing).to be_valid
    end

    it 'can be created' do
      job_listing = create :job_listing
      expect(job_listing.id).to_not be_nil
    end
  end

  describe 'validations' do
    it 'should require a title' do
      expect(build :job_listing, title: nil).to be_invalid
    end

    it 'should require the number of openings' do
      expect(build :job_listing, number_of_openings: nil).to be_invalid
    end

    it 'should require a starting display date' do
      expect(build :job_listing, display_at: nil).to be_invalid
    end

    it 'should require a display ending date' do
      expect(build :job_listing, end_at: nil).to be_invalid
    end

    it 'should require an seo slug' do
      expect(build :job_listing, seo_slug: nil).to be_invalid
    end
  end

  describe 'Association' do
    let(:job_listing) { create :job_listing }

    context 'belongs_to' do
      it 'store' do
        expect(job_listing.build_store((build :store).attributes)).to be_valid
      end

      it 'store' do
        store = build :store
        store.job_listings << job_listing
        expect(store.job_listings).to include(job_listing)
      end
    end
  end
end
