describe Store, type: :model do
  describe 'from a factory' do
    it 'can be built' do
      expect(build :store).to be_valid
    end

    it 'can be created' do
      store = create :store
      expect(store.id).to_not be_nil
    end
  end

  describe 'validations' do
    it 'should require a name' do
      expect(build :store, name: nil).to be_invalid
    end

    it 'should require a sort name' do
      expect(build :store, sort_name: nil).to be_invalid
    end

    it 'should require a retailer' do
      expect(build :store, retailer: nil).to be_invalid
    end

    it 'should require a location' do
      expect(build :store, location: nil).to be_invalid
    end

    it 'should require an seo slug' do
      expect(build :store, seo_slug: nil).to be_invalid
    end
  end

  describe 'Association' do
    let(:store) { create(:store) }

    context 'has many' do
      it 'Images' do
        3.times { store.retailer.images << (build :image) }
        expect(store.images.size).to eq(3)
      end

      it 'deals' do
        expect { store.deals << build(:deal) }.to change(store.deals, :size).by(1)
      end
    end

    context 'belongs to' do
      it 'Retailer' do
        expect(store.build_retailer(attributes_for :retailer)).to be_valid
      end

      it 'excludes Retailers with id of 0' do
        invalid_store = create(:store, retailer: create(:retailer, id: 0))
        expect(Store.all).to_not include(invalid_store)
      end

      it 'Location' do
        expect(store.location).to be_valid
      end

      it 'Mall' do
        store.mall = build :mall
        expect(store.mall).to be_valid
      end
    end

    context 'through a mall' do
      let(:mall) { build :mall }

      it 'falls back to mall hours_text' do
        expect(build(:store, hours: nil, mall: mall).hours).to eq(mall.hours_text)
      end
    end

    context 'through a Retailer' do
      let(:retailer) { build(:retailer) }

      before :each do
        store.retailer = retailer
      end

      it 'falls back to retailer description' do
        store.description = nil
        expect(store.description).to_not be_nil
      end

      it 'falls back to retailer url' do
        store.url = nil
        expect(store.url).to_not be_nil
      end

      it 'falls back to retailer url_text' do
        store.url_text = nil
        expect(store.url_text).to_not be_nil
      end

      context 'has many' do
        it 'Products' do
          expect { store.products << build(:product) }.to change(store.products, :size).by(1)
        end
      end
    end
  end
end
