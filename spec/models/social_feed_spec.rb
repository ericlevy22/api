describe SocialFeed, type: :model do
  describe 'from a factory' do
    it 'can be built' do
      expect(build :social_feed).to be_valid
    end

    it 'can be created' do
      social_feed = create :social_feed
      expect(social_feed.id).to_not be_nil
    end
  end

  describe 'validations' do
    it 'is invalid without a properties hash' do
      social_feed = build(:social_feed)
      social_feed.properties = nil
      expect(social_feed).to_not be_valid
    end

    it 'is invalid without at least one property' do
      social_feed = build(:social_feed)
      social_feed.properties = {}
      expect(social_feed).to_not be_valid
    end
  end
end
