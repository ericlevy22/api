describe Affinity, type: :model do
  describe 'from a factory' do
    it 'can be built' do
      expect(build :affinity).to be_valid
    end

    it 'can be created' do
      affinity = create :affinity
      expect(affinity.id).to_not be_nil
    end
  end

  describe 'validates' do
    it 'heartable_type is present' do
      expect(build :affinity, heartable_type: nil).to be_invalid
    end

    it 'heartable_id is present' do
      expect(build :affinity, heartable_id: nil).to be_invalid
    end
  end
end
