describe Program, type: :model do
  include_examples 'sinceable'

  describe 'from a factory' do
    it 'can be built' do
      expect(build :program).to be_valid
    end

    it 'can be created' do
      expect(create(:program).id).to_not be_nil
    end
  end
end
