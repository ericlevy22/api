describe Hour, type: :model do
  describe 'from a factory' do
    it 'can be built' do
      expect(build(:hour)).to be_valid
    end

    it 'can be created' do
      expect(create(:hour).id).to_not be_nil
    end
  end

  describe 'by_range' do
    let(:hour) { create :hour, start_at: 1.day.ago, end_at: 1.day.from_now }

    it 'includes current hours' do
      expect(Hour.by_range(2.days.ago, 2.days.from_now)).to include(hour)
    end

    it 'excludes hours which have ended' do
      expect(Hour.by_range(2.days.from_now, 3.days.from_now)).to_not include(hour)
    end

    it 'excludes hours which have not started' do
      expect(Hour.by_range(3.days.ago, 2.days.ago)).to_not include(hour)
    end
  end

  describe 'current_hours' do
    let(:mall) { create(:mall) }

    let(:default_hours) { create(:default_hour, mall: mall) }
    let(:hours_1) { create(:hour, start_at: Date.today.beginning_of_week, end_at: Date.today.beginning_of_week, mall: mall) }
    let(:hours_2) { create(:hour, start_at: Date.today.end_of_week, end_at: Date.today.end_of_week, mall: mall) }

    it 'contains this_week in this_month' do
      hours = Hour.current_hours(Date.today, mall)
      expect(hours[:this_month]).to include(*hours[:this_week])
    end

    it 'contains today in this_week' do
      hours = Hour.current_hours(Date.today, mall)
      expect(hours[:this_week]).to include(*hours[:today])
    end

    it 'is empty when there are no hours' do
      hours = Hour.current_hours(Date.today, create(:mall))
      expect(hours[:today]).to be_empty
      expect(hours[:this_week]).to be_empty
      expect(hours[:this_month]).to be_empty
    end
  end
end
