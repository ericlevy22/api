describe Mall, type: :model do
  include_examples 'sinceable'

  describe 'from a factory' do
    it 'can be built' do
      expect(build :mall).to be_valid
    end

    it 'can be created' do
      mall = create :mall
      expect(mall.id).to_not be_nil
    end
  end

  describe 'validation' do
    it 'should require a name' do
      expect(build :mall, name: nil).to be_invalid
    end

    it 'should require a city, state, and zip code' do
      expect(build :mall, city: nil).to be_invalid
      expect(build :mall, state: nil).to be_invalid
      expect(build :mall, zip: nil).to be_invalid
    end

    it 'should require an email' do
      expect(build :mall, email: nil).to be_invalid
    end

    it 'should require a url' do
      expect(build :mall, url: nil).to be_invalid
    end

    it 'should require a nickname' do
      expect(build :mall, nick_name: nil).to be_invalid
    end

    it 'should require a location' do
      expect(build :mall, location: nil).to be_invalid
    end

    it 'should require a company' do
      expect(build :mall, company: nil).to be_invalid
    end

    it 'should require an seo slug' do
      expect(build :mall, seo_slug: nil).to be_invalid
    end
  end

  describe 'Association' do
    let(:mall) { create :mall }

    context 'belongs to' do
      it 'Company' do
        expect(mall.build_company(attributes_for :company)).to be_valid
      end

      it 'Location' do
        expect(mall.location).to be_valid
      end
    end

    context 'has many' do
      it 'Hours' do
        3.times { mall.hours << (build :hour) }
        expect(mall.hours.size).to eq(3)
      end

      it 'Images' do
        3.times { mall.images << (build :image) }
        expect(mall.images.size).to eq(3)
      end

      it 'Stores' do
        3.times { mall.stores << (build :store) }
        expect(mall.stores.size).to eq(3)
      end

      it 'Articles' do
        3.times { mall.articles << (build :article) }
        expect(mall.articles.size).to eq(3)
      end

      it 'Ifeatures' do
        3.times { mall.ifeatures << (build :ifeature) }
        expect(mall.ifeatures.size).to eq(3)
      end

      it 'Events' do
        3.times { mall.events << (build :event) }
        expect(mall.events.size).to eq(3)
      end
    end

    context 'through stores' do
      it 'has deals' do
        mall = create(:mall, stores: [create(:store)])
        expect { mall.stores.first.deals << create(:deal) }.to change { mall.deals.count }.by(1)
      end
    end
  end
end
