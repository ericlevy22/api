RSpec.describe Identity, type: :model do
  describe 'from a factory' do
    it 'can be built' do
      expect(build :identity).to be_valid
    end

    it 'can be created' do
      identity = create :identity
      expect(identity.id).to_not be_nil
    end
  end

  describe 'Association' do
    let(:identity) { create(:identity) }

    context 'belongs to' do
      it 'Account' do
        expect(identity.build_user).to be_valid
      end
    end
  end
end
