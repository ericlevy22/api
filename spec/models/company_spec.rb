describe Company, type: :model do
  include_examples 'sinceable'

  describe 'from a factory' do
    it 'can be built' do
      expect(build :company).to be_valid
    end

    it 'can be created' do
      company = create :company
      expect(company.id).to_not be_nil
    end
  end

  describe 'validation' do
    it 'should require an seo slug' do
      expect(build :company, seo_slug: nil).to be_invalid
    end
  end

  describe 'Association' do
    let(:company) { create(:company) }

    context 'has many' do
      it 'Contacts' do
        3.times { company.contacts << (build :contact) }
        expect(company.contacts.size).to eq(3)
      end

      it 'Images' do
        3.times { company.images << (build :image) }
        expect(company.images.size).to eq(3)
      end

      it 'Malls' do
        3.times { company.malls << (build :mall) }
        expect(company.malls.size).to eq(3)
      end
    end
  end
end
