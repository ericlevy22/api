describe Event, type: :model do
  let!(:past) { create :event, end_at: 1.month.ago }
  let!(:present) { create :event }
  let!(:inactive) { create :event, is_active: false }
  let!(:future) { create :event, display_at: 1.month.from_now }

  describe 'validation' do
    it 'should require a title' do
      expect(build :event, title: nil).to be_invalid
    end

    it 'should require a starting date' do
      expect(build :event, start_at: nil).to be_invalid
    end

    it 'should require an ending date' do
      expect(build :event, end_at: nil).to be_invalid
    end

    it 'should require a description' do
      expect(build :event, description: nil).to be_invalid
    end

    it 'should require a short text description' do
      expect(build :event, short_text: nil).to be_invalid
    end

    it 'should require an seo slug' do
      expect(build :event, seo_slug: nil).to be_invalid
    end
  end

  describe '.current' do
    it 'includes currently active events' do
      expect(Event.current).to include(present)
    end

    it 'excludes inactive and expired events' do
      expect(Event.current).to_not include(inactive)
      expect(Event.current).to_not include(past)
      expect(Event.current).to_not include(future)
    end
  end

  describe 'by_date_range' do
    it 'includes events within range' do
      expect(Event.by_date_range(1.week.ago, 1.day.from_now)).to include(present)
    end

    it 'excludes events out of range' do
      expect(Event.by_date_range(1.week.ago, 1.week.from_now)).to_not include(past)
      expect(Event.by_date_range(1.week.ago, 1.week.from_now)).to_not include(future)
    end
  end

  describe 'from a factory' do
    it 'can be built' do
      expect(build :event).to be_valid
    end

    it 'can be created' do
      event = create :event
      expect(event.id).to_not be_nil
    end
  end
end
