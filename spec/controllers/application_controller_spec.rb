describe ApplicationController, type: :controller do
  before(:all) { log_in_as_client! }

  controller do
    def index
      fail ActiveRecord::RecordNotFound
    end
  end

  describe 'handling exceptions' do
    it '404 Not Found' do
      get :index, account_id: @account.id, account_token: @token, timestamp: @timestamp
      expect(response.status).to equal(404)
    end
  end
end
