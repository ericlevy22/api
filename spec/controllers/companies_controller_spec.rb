describe CompaniesController, type: :controller do
  before(:all) do
    @company = create :company
    log_in_as_client!(resource: @company)
  end

  describe 'GET #index' do
    before do
      allow(Company).to receive_message_chain(:limit, :offset) { [@company] }
      get :index, account_id: @account.id, account_token: @token, timestamp: @timestamp
    end

    it '200 OK' do
      expect(response.status).to be(200)
    end

    it 'assigns @companies' do
      expect(assigns(:companies)).to include(@company)
    end

    context 'with invalid since date' do
      it '400 Bad Request' do
        get :index, account_id: @account.id, account_token: @token,
                    timestamp: @timestamp, since: '2003'
        expect(response.status).to be(400)
      end
    end
  end

  describe 'GET #show' do
    context 'with valid id' do
      before do
        allow(Company).to receive(:find) { @company }
        get :show, id: @company.id, account_id: @account.id,
                   account_token: @token, timestamp: @timestamp
      end

      it '200 OK' do
        expect(response.status).to be(200)
      end

      it 'assigns @company' do
        expect(assigns(:company)).to eq(@company)
      end
    end

    context 'with invalid id' do
      it '404 Not Found' do
        get :show, id: 'non_existant_id', account_id: @account.id,
                   account_token: @token, timestamp: @timestamp
        expect(response.status).to be(404)
      end
    end
  end

  describe 'GET #contacts' do
    let(:contact) { build :contact }
    before do
      allow(Company).to receive(:find) { @company }
      allow(@company).to receive(:contacts) { [contact] }
      get :contacts, id: @company.id, account_id: @account.id,
                     account_token: @token, timestamp: @timestamp
    end

    it '200 OK' do
      expect(response.status).to be(200)
    end
  end

  describe 'GET #malls' do
    let(:mall) { build :mall }
    before do
      allow(Company).to receive(:find) { @company }
      allow(@company).to receive(:malls) { [mall] }
      get :malls, id: @company.id, account_id: @account.id,
                  account_token: @token, timestamp: @timestamp
    end

    it '200 OK' do
      expect(response.status).to be(200)
    end
  end
end
