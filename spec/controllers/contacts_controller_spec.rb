describe ContactsController, type: :controller do
  before(:all) do
    @contact = create(:contact)
    @mall = create(:mall, contacts: [@contact])
    log_in_as_client!(resource: @mall, traits: :mall_channel)
  end

  describe 'GET #index' do
    before do
      allow(Contact).to receive_message_chain(:limit, :offset, :since) { [@contact] }
      get :index, account_id: @account.id,
                  account_token: @token, timestamp: @timestamp
    end

    it '200 OK' do
      expect(response.status).to be(200)
    end

    it 'assigns @contacts' do
      expect(assigns(:contacts)).to_not be_empty
    end

    context 'with invalid since date' do
      it '400 Bad Request' do
        get :index, account_id: @account.id, account_token: @token,
                    timestamp: @timestamp, since: '2003'
        expect(response.status).to be(400)
      end
    end
  end

  describe 'GET #show' do
    before do
      allow(Contact).to receive(:find) { @contact }
      get :show, id: @contact.id, account_id: @account.id, account_token: @token, timestamp: @timestamp
    end

    it '200 OK' do
      expect(response.status).to be(200)
    end

    it 'assigns @contact' do
      expect(assigns(:contact)).to eq(@contact)
    end
  end
end
