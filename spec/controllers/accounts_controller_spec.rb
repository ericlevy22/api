describe AccountsController, type: :controller do
  before do
    log_in_as_client!
    allow(Account).to receive(:find) { @account }
    allow(Account).to receive_message_chain(:limit, :offset) { [@account] }
  end

  context '#index' do
    before { get :index, account_id: @account.id, account_token: @token, timestamp: @timestamp }

    it '200 OK' do
      expect(response.status).to be(200)
    end

    it 'assigns @accounts' do
      expect(assigns(:accounts)).to include(@account)
    end
  end

  context '#show' do
    before { get :show, id: @account.id, account_id: @account.id, account_token: @token, timestamp: @timestamp }

    it '200 OK' do
      expect(response.status).to be(200)
    end

    it 'assigns @account' do
      expect(assigns(:account)).to eq(@account)
    end
  end

  context '#destroy' do
    context 'with valid request' do
      before { delete :destroy, id: @account.id, account_id: @account.id, account_token: @token, timestamp: @timestamp }

      it '204 No Content' do
        expect(response.status).to be(204)
      end
    end

    context 'with invalid request' do
      before do
        allow_any_instance_of(Account).to receive(:destroy).and_return(false)
        delete :destroy, id: @account.id, account_id: @account.id, account_token: @token, timestamp: @timestamp
      end

      it '422 Unprocessable Entity' do
        expect(response.status).to be(422)
      end
    end
  end

  context '#update' do
    before do
      log_in_as_admin!
    end

    context 'with valid request' do
      before do
        put :update, id: @account.id, account_id: @account.id,
                     account_token: @token, timestamp: @timestamp,
                     account: { properties: { gender: ['male', 'female'].sample } }
      end

      it '200 OK' do
        expect(response.status).to be(200)
      end
    end
  end

  context '#roles' do
    before { get :roles, id: @account.id, account_id: @account.id, account_token: @token, timestamp: @timestamp }

    it '200 OK' do
      expect(response.status).to be(200)
    end
  end

  context '#add_roles' do
    before do
      log_in_as_admin!
      post :add_roles, roles: 'foo,bar', id: @account.id, account_id: @account.id,
                       account_token: @token, timestamp: @timestamp, account: { password: 'qwerqwer' }
    end

    it '200 OK' do
      expect(response.status).to be(200)
    end
  end

  context '#remove_roles' do
    before do
      log_in_as_admin!
      post :remove_roles, roles: 'foo,bar', id: @account.id, account_id: @account.id,
                          account_token: @token, timestamp: @timestamp, account: { password: 'qwerqwer' }
    end

    it '204 No Content' do
      expect(response.status).to be(204)
    end
  end

  context 'POST /accounts with :facebook_access_token' do
    it '201 Created' do
      log_in_as_facebook!
      post :create, account: { facebook_access_token: @token }
      expect(response.status).to be(201)
    end

    it '400 Bad Request with a bad token' do
      post :create, account: { facebook_access_token: 'bad token!' }
      expect(response.status).to be(400)
    end
  end

  context '#memberships' do
    it '502 bad gateway' do
      allow(PwLegacy::EmailPrograms::GetMemberships).to receive(:call) { raise JSON::ParserError }
      log_in_as_client!
      get :memberships, id: @account.id, account_id: @account.id,
                        account_token: @token, timestamp: @timestamp, mall_id: 1

      expect(PwLegacy::EmailPrograms::GetMemberships).to have_received(:call)
      expect(response).to have_http_status(:bad_gateway)
    end
  end

  context '#subscribe' do
    let(:program) { create(:program, is_default_signup_program: true) }
    let(:mall) { create(:mall, programs: [program], company: create(:company, allow_unconfirmed_access: true)) }

    it 'handles bad JSON with 502 bad gateway' do
      allow(PwLegacy::EmailPrograms::Subscribe).to receive(:call) { raise JSON::ParserError }
      post :create, account: { email: Faker::Internet.email, password: 'asdfasdf'}, mall_id: mall.id

      expect(PwLegacy::EmailPrograms::Subscribe).to have_received(:call)
      expect(response).to have_http_status(:bad_gateway)
    end
  end
end
