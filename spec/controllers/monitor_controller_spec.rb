describe MonitorController, type: :controller do
  context 'GET #status' do
    before do
      get :status
    end

    it '200 OK' do
      expect(response.status).to be(200)
    end

    it 'returns online' do
      parsed_body = JSON.parse(response.body)
      expect(parsed_body['status']).to eq('online')
    end
  end
end
