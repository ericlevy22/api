describe BrandsController, type: :controller do
  before(:all) do
    @brand = create :brand
    log_in_as_client!(resource: @brand)
  end

  context '#index' do
    before do
      allow(Brand).to receive_message_chain(:limit, :offset) { [@brand] }
      get :index, account_id: @account.id, account_token: @token, timestamp: @timestamp
    end

    it '200 OK' do
      expect(response.status).to be(200)
    end

    it 'assigns @brands correctly' do
      expect(assigns(:brands)).to include(@brand)
    end

    context 'with invalid since date' do
      it '400 Bad Request' do
        get :index, account_id: @account.id, account_token: @token,
                    timestamp: @timestamp, since: '2003'
        expect(response.status).to be(400)
      end
    end
  end

  context '#show' do
    context 'with valid id' do
      before do
        get :show, id: @brand.id, account_id: @account.id, account_token: @token, timestamp: @timestamp
      end

      it '200 OK' do
        expect(response.status).to be(200)
      end

      it 'assigns @brand correctly' do
        expect(assigns(:brand)).to eq(@brand)
      end
    end

    context 'with invalid id' do
      it '404 Not Found' do
        get :show, id: 'non_existant_id', account_id: @account.id, account_token: @token, timestamp: @timestamp
        expect(response.status).to be(404)
      end
    end
  end
end
