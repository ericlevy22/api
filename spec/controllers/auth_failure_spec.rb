describe MallsController, type: :controller do
  before { log_in_as_client! }

  context 'without a valid token' do
    it '401 unauthorized' do
      get :index, account_id: @account.id, account_token: 'this is not a valid token', timestamp: @timestamp
      expect(response.status).to be(401)
    end
  end
end
