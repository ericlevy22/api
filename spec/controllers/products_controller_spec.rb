describe ProductsController, type: :controller do
  before(:all) do
    @product = create :product
    log_in_as_client!(resource: @product)
  end

  describe 'GET #index' do
    before do
      get :index, account_id: @account.id, account_token: @token, timestamp: @timestamp
    end

    it '200 OK' do
      expect(response.status).to be(200)
    end

    it 'assigns @products' do
      allow(Product).to receive_message_chain(:limit, :offset) { [@product] }
      expect(assigns(:products)).to include(@product)
    end

    context 'with invalid since date' do
      it '400 Bad Request' do
        get :index, account_id: @account.id, account_token: @token,
                    timestamp: @timestamp, since: '2003'
        expect(response.status).to be(400)
      end
    end
  end

  describe 'GET #show' do
    context 'with valid id' do
      before do
        allow(Product).to receive(:find) { @product }
        get :show, id: @product.id, account_id: @account.id,
                   account_token: @token, timestamp: @timestamp
      end

      it '200 OK' do
        expect(response.status).to be(200)
      end

      it 'assigns @product' do
        expect(assigns(:product)).to eq(@product)
      end
    end

    context 'with invalid id' do
      it '404 Not Found' do
        get :show, id: 0, account_id: @account.id, account_token: @token, timestamp: @timestamp
        expect(response.status).to be(404)
      end
    end
  end

  describe 'GET #stores' do
    context 'with valid id' do
      before do
        allow(Product).to receive(:find) { @product }
        get :stores, id: @product.id, account_id: @account.id,
                     account_token: @token, timestamp: @timestamp
      end

      it '200 OK' do
        expect(response.status).to be(200)
      end

      it 'assigns @product' do
        expect(assigns(:product)).to eq(@product)
      end
    end

    context 'with invalid id' do
      it '404 Not Found' do
        get :stores, id: 0, account_id: @account.id, account_token: @token, timestamp: @timestamp
        expect(response.status).to be(404)
      end
    end
  end
end
