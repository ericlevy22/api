describe ArticlesController, type: :controller do
  before(:all) do
    @article = create :article
    log_in_as_client!(resource: @article)
  end

  describe 'GET #index' do
    before do
      allow(Article).to receive_message_chain(:limit, :offset) { [@article] }
      get :index, account_id: @account.id, account_token: @token, timestamp: @timestamp
    end

    it '200 OK' do
      expect(response.status).to be(200)
    end

    it 'assigns @articles' do
      expect(assigns(:articles)).to include(@article)
    end

    context 'with invalid since date' do
      it '400 Bad Request' do
        get :index, account_id: @account.id, account_token: @token,
                    timestamp: @timestamp, since: '2003'
        expect(response.status).to be(400)
      end
    end
  end

  describe 'GET #show' do
    context 'with valid id' do
      before do
        allow(Article).to receive(:find)
        get :show, id: @article.id, account_id: @account.id, account_token: @token, timestamp: @timestamp
      end

      it '200 OK' do
        expect(response.status).to be(200)
      end

      it 'assigns @article' do
        expect(assigns(:article)).to eq(@article)
      end
    end

    context 'with invalid id' do
      it '404 Not Found' do
        get :show, id: 'non_existant_id', account_id: @account.id,
                   account_token: @token, timestamp: @timestamp
        expect(response.status).to be(404)
      end
    end
  end

  describe 'GET #search' do
    before do
      allow(Article).to receive(:by_keyword) { [@article] }
      get :search, q: 'foo', account_id: @account.id, account_token: @token, timestamp: @timestamp
    end

    it '200 OK' do
      expect(response.status).to be(200)
    end

    it 'by keyword assigns @articles' do
      expect(assigns(:articles)).to include(@article)
    end

    context 'with invalid since date' do
      it '400 Bad Request' do
        get :search, q: 'foo', account_id: @account.id, account_token: @token,
                     timestamp: @timestamp, since: '2003'
        expect(response.status).to be(400)
      end
    end
  end
end
