describe DealsController, type: :controller do
  before(:all) do
    @deal = create :deal
    log_in_as_client!(resource: @deal)
  end

  describe '#index' do
    before { get :index, account_id: @account.id, account_token: @token, timestamp: @timestamp }

    it '200 OK' do
      expect(response.status).to be(200)
    end

    it 'assigns @deals' do
      allow(Deal).to receive_message_chain(:limit, :offset) { [@deal] }
      expect(assigns(:deals)).to include(@deal)
    end

    context 'with invalid since date' do
      it '400 Bad Request' do
        get :index, account_id: @account.id, account_token: @token,
                    timestamp: @timestamp, since: '2003'
        expect(response.status).to be(400)
      end
    end
  end

  describe '#show' do
    context 'with valid id' do
      before do
        allow(Deal).to receive(:find) { @deal }
        get :show, id: @deal.id, account_id: @account.id, account_token: @token, timestamp: @timestamp
      end

      it '200 OK' do
        expect(response.status).to be(200)
      end

      it 'assigns @deal' do
        expect(assigns(:deal)).to eq(@deal)
      end
    end

    context 'with invalid id' do
      it '404 Not Found' do
        get :show, controller: 'deals', id: 'non_existent_deal', account_id: @account.id,
                   account_token: @token, timestamp: @timestamp
        expect(response.status).to be(404)
      end
    end
  end

  describe '#stores' do
    context 'with valid Deal id' do
      let!(:store) { create :store, deals: [@deal] }

      it '200 OK' do
        get :stores, id: @deal.id, account_id: @account.id, account_token: @token, timestamp: @timestamp
        expect(response.status).to be(200)
      end
    end
  end

  describe '#search' do
    before do
      allow(Deal).to receive(:by_keyword) { [@deal] }
      get :search, q: 'foo', account_id: @account.id, account_token: @token, timestamp: @timestamp
    end

    it '200 OK' do
      expect(response.status).to be(200)
    end

    it 'by keyword assigns @deals' do
      expect(assigns(:deals)).to include(@deal)
    end

    context 'with invalid since date' do
      it '400 Bad Request' do
        get :search, q: 'foo', account_id: @account.id, account_token: @token,
                     timestamp: @timestamp, since: '2003'
        expect(response.status).to be(400)
      end
    end
  end
end
