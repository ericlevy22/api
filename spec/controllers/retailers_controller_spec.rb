describe RetailersController, type: :controller do
  before(:all) do
    @retailer = create(:retailer)
    @mall = create(:mall, stores: [create(:store, retailer: @retailer)])
    log_in_as_client!(resource: @mall, traits: [:store_channel, :deal_channel])
  end

  describe 'GET #index' do
    it '200 OK' do
      get :index, account_id: @account.id, account_token: @token, timestamp: @timestamp
      expect(response.status).to be(200)
    end

    context 'with invalid since date' do
      it '400 Bad Request' do
        get :index, account_id: @account.id, account_token: @token,
                    timestamp: @timestamp, since: '2003'
        expect(response.status).to be(400)
      end
    end
  end

  describe 'GET #show' do
    context 'with valid id' do
      before do
        allow(Retailer).to receive(:find) { @retailer }
        get :show, id: @retailer.id, account_id: @account.id, account_token: @token, timestamp: @timestamp
      end

      it '200 OK' do
        expect(response.status).to be(200)
      end

      it 'assigns @retailer' do
        expect(assigns(:retailer)).to eq(@retailer)
      end
    end

    context 'with invalid id' do
      it '404 Not Found' do
        get :show, id: 0, account_id: @account.id, account_token: @token, timestamp: @timestamp
        expect(response.status).to be(404)
      end
    end
  end

  describe 'GET #stores' do
    context 'with valid id' do
      before do
        allow(Retailer).to receive(:find) { @retailer }
        get :stores, id: @retailer.id, account_id: @account.id, account_token: @token, timestamp: @timestamp
      end

      it '200 OK' do
        expect(response.status).to be(200)
      end

      it 'assigns @retailer' do
        expect(assigns(:retailer)).to eq(@retailer)
      end
    end

    context 'with invalid id' do
      it '404 Not Found' do
        get :stores, id: 0, account_id: @account.id, account_token: @token, timestamp: @timestamp
        expect(response.status).to be(404)
      end
    end
  end

  describe 'GET #deals' do
    context 'with valid id' do
      before do
        allow(Retailer).to receive(:find) { @retailer }
        get :deals, id: @retailer.id, account_id: @account.id, account_token: @token, timestamp: @timestamp
      end

      it '200 OK' do
        expect(response.status).to be(200)
      end

      it 'assigns @retailer' do
        expect(assigns(:retailer)).to eq(@retailer)
      end
    end

    context 'with invalid id' do
      it '404 Not Found' do
        get :deals, id: 0, account_id: @account.id, account_token: @token, timestamp: @timestamp
        expect(response.status).to be(404)
      end
    end
  end
end
