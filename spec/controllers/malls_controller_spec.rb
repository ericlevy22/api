describe MallsController, type: :controller do
  before(:all) do
    @mall = create :mall, location: (create :location)
    @mall_message = (build :mall_message)
    @mall.mall_messages << @mall_message
    @ifeature = (build :ifeature)
    @mall.ifeatures << @ifeature
    log_in_as_client!(resource: @mall)
  end

  before(:each) do
    allow(Mall).to receive(:find) { @mall }
  end

  describe 'GET #index' do
    before do
      allow(Mall).to receive_message_chain(:limit, :offset) { [mall] }
      get :index, account_id: @account.id, account_token: @token, timestamp: @timestamp
    end

    it '200 OK' do
      expect(response.status).to be(200)
    end

    it 'assigns @malls' do
      expect(assigns(:malls)).to include(@mall)
    end
  end

  describe 'GET #show' do
    context 'with valid id' do
      before do
        get :show, id: @mall.id, account_id: @account.id, account_token: @token, timestamp: @timestamp
      end

      it '200 OK' do
        expect(response.status).to be(200)
      end

      it 'assigns @mall' do
        expect(assigns(:mall)).to eq(@mall)
      end
    end

    context 'with invalid id' do
      it '404 Not Found' do
        get :show, id: 0, account_id: @account.id, account_token: @token, timestamp: @timestamp
        expect(response.status).to be(404)
      end
    end
  end

  describe 'GET #stores' do
    context 'with valid id' do
      before do
        get :stores, id: @mall.id, account_id: @account.id, account_token: @token, timestamp: @timestamp
      end

      it '200 OK' do
        expect(response.status).to be(200)
      end

      it 'assigns @mall' do
        expect(assigns(:mall)).to eq(@mall)
      end
    end

    context 'with invalid id' do
      it '404 Not Found' do
        get :stores, id: 0, account_id: @account.id, account_token: @token, timestamp: @timestamp
        expect(response.status).to be(404)
      end
    end
  end

  describe 'GET #hours' do
    context 'with valid id' do
      before do
        get :hours, id: @mall.id, account_id: @account.id, account_token: @token, timestamp: @timestamp
      end

      it '200 OK' do
        expect(response.status).to be(200)
      end

      it 'assigns @mall' do
        expect(assigns(:mall)).to eq(@mall)
      end
    end

    context 'with invalid id' do
      it '404 Not Found' do
        get :stores, id: 0, account_id: @account.id, account_token: @token, timestamp: @timestamp
        expect(response.status).to be(404)
      end
    end
  end

  describe 'GET #ifeatures' do
    let(:ifeature) { @mall.ifeatures << (build :ifeature) }

    context 'with valid id' do
      before do
        get :ifeatures, id: @mall.id, account_id: @account.id, account_token: @token, timestamp: @timestamp
      end

      it '200 OK' do
        expect(response.status).to be(200)
      end

      it 'assigns @mall.ifeatures' do
        expect(@mall.ifeatures).to eq([@ifeature])
      end
    end

    context 'with invalid id' do
      it '404 Not Found' do
        get :ifeatures, id: 0, account_id: @account.id, account_token: @token, timestamp: @timestamp
        expect(response.status).to be(404)
      end
    end
  end

  describe 'GET #mall messages' do
    context 'with valid id' do
      before do
        get :mall_messages, id: @mall.id, account_id: @account.id, account_token: @token, timestamp: @timestamp
      end

      it '200 OK' do
        expect(response.status).to be(200)
      end

      it 'assigns @mall_messages' do
        expect(assigns(:mall)).to eq(@mall)
        expect(@mall.mall_messages).to eq([@mall_message])
      end
    end
  end
end
