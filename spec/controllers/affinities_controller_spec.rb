describe AffinitiesController, type: :controller do
  before(:all) do
    log_in_as_client!
    @affinity = create :affinity, user: @account.user
  end

  describe 'GET #index' do
    before do
      allow(Affinity).to receive_message_chain(:by_heartable_type, :limit, :offset) { [@affinity] }
      get :index, account_id: @account.id, account_token: @token, timestamp: @timestamp
    end

    it '200 OK' do
      expect(response.status).to be(200)
    end

    it 'assigns @affinities' do
      expect(assigns(:affinities)).to include(@affinity)
    end
  end

  describe 'GET #show' do
    before do
      allow(Affinity).to receive(:find) { @affinity }
      get :show, id: @affinity.id, account_id: @account.id, account_token: @token, timestamp: @timestamp
    end

    it '200 OK' do
      expect(response.status).to be(200)
    end

    it 'assigns @affinity' do
      expect(assigns(:affinity)).to eq(@affinity)
    end
  end

  describe 'POST #create' do
    it 'creates a new affinity' do
      expect {
        post :create, affinity: attributes_for(:affinity), account_id: @account.id,
                      account_token: @token, timestamp: @timestamp
      }.to change(Affinity, :count).by(1)
      expect(response.status).to be(201)
    end

    context 'with unconfirmed account' do
      before do
        log_in_as_unconfirmed_account!
      end

      it 'should not create a new affinity and respond with 403' do
        expect {
          post :create, affinity: attributes_for(:affinity), account_id: @account.id,
                        account_token: @token, timestamp: @timestamp
        }.to change(Affinity, :count).by(0)
        expect(response.status).to be(403)
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys the affinity as inactive' do
      expect {
        delete :destroy, id: @affinity.id, account_id: @account.id, account_token: @token, timestamp: @timestamp
      }.to change(Affinity, :count).by(-1)
      expect(response.status).to be(204)
    end
  end
end
