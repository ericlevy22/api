describe SocialFeedsController, type: :controller do
  before(:all) do
    @social_feed = create(:social_feed)
    @mall = create(:mall, social_feeds: [@social_feed])
    log_in_as_client!(resource: @mall, traits: :mall_channel)
  end

  describe 'GET #index' do
    it '200 OK' do
      get :index, account_id: @account.id, account_token: @token, timestamp: @timestamp
      expect(response.status).to be(200)
    end

    it 'assigns @social_feeds' do
      get :index, account_id: @account.id, account_token: @token, timestamp: @timestamp
      expect(assigns(:social_feeds)).to_not be_empty
    end
  end
end
