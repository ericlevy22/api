describe StoresController, type: :controller do
  before(:all) do
    @store = create(:store, mall: create(:mall))
    log_in_as_client!(resource: @store.mall, traits: :mall_channel)
  end

  before { allow(@controller).to receive(:calculate_distance) { 23 } }

  describe 'GET #index' do
    before do
      allow(Store).to receive_message_chain(:limit, :offset) { [@store] }
      get :index, account_id: @account.id, account_token: @token, timestamp: @timestamp
    end

    it '200 OK' do
      expect(response.status).to be(200)
    end

    it 'assigns @stores' do
      expect(assigns(:stores)).to eq([@store])
    end

    context 'with invalid since date' do
      it '400 Bad Request' do
        get :index, account_id: @account.id, account_token: @token,
                    timestamp: @timestamp, since: '2003'
        expect(response.status).to be(400)
      end
    end
  end

  describe 'GET #show' do
    context 'with valid id' do
      before do
        allow(Store).to receive(:find) { @store }
        get :show, id: @store.id, account_id: @account.id, account_token: @token, timestamp: @timestamp
      end

      it '200 OK' do
        expect(response.status).to be(200)
      end

      it 'assigns @store' do
        expect(assigns(:store)).to eq(@store)
      end
    end

    context 'with invalid id' do
      it '404 Not Found' do
        get :show, id: 0, account_id: @account.id, account_token: @token, timestamp: @timestamp
        expect(response.status).to be(404)
      end
    end
  end

  describe 'GET #products' do
    before do
      allow(Store).to receive(:find) { @store }
      @store.products << build(:product)
    end

    it '200 OK' do
      get :show, id: @store.id, account_id: @account.id, account_token: @token, timestamp: @timestamp
      expect(response.status).to be(200)
    end
  end
end
