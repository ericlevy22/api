require 'spec_helper'
include LoginHelper

describe TagsController do
  before(:all) { log_in_as_client! }
  after(:all) { DatabaseCleaner.clean }
  let!(:tag) { create :tag }

  context '#index' do
    before do
      get :index, account_id: @account.id, account_token: @token, timestamp: @timestamp
    end

    it '200 OK' do
      expect(response.status).to be(200)
    end

    it 'assigns @tags correctly' do
      expect(assigns(:tags)).to include(tag)
    end

    context 'with invalid since date' do
      it '400 Bad Request' do
        get :index, account_id: @account.id, account_token: @token,
                    timestamp: @timestamp, since: '2003'
        expect(response.status).to be(400)
      end
    end
  end

  context '#show' do
    context 'with valid id' do
      before do
        get :show, id: tag.id, account_id: @account.id, account_token: @token, timestamp: @timestamp
      end

      it '200 OK' do
        expect(response.status).to be(200)
      end

      it 'assigns @tag correctly' do
        expect(assigns(:tag)).to eq(tag)
      end
    end

    context 'with invalid id' do
      it '404 Not Found' do
        get :show, id: 'non_existant_id', account_id: @account.id, account_token: @token, timestamp: @timestamp
        expect(response.status).to be(404)
      end
    end
  end

  context '#search' do
    before do
      get :search, q: tag.name, account_id: @account.id, account_token: @token, timestamp: @timestamp
    end

    it '200 OK' do
      expect(response.status).to be(200)
    end

    it 'assigns @tags correctly' do
      expect(assigns(:tags)).to include(tag)
    end

    context 'with invalid since date' do
      it '400 Bad Request' do
        get :search, q: tag.name, account_id: @account.id, account_token: @token,
                     timestamp: @timestamp, since: '2003'
        expect(response.status).to be(400)
      end
    end
  end
end
