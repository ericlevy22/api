describe LocationsController, type: :controller do
  before(:all) do
    @location = create :location
    log_in_as_client!(resource: @location)
  end

  describe 'GET #index' do
    before { allow(Location).to receive_message_chain(:limit, :offset) { [@location] } }

    before do
      get :index, lat: @location.latitude, long: @location.longitude,
                  account_id: @account.id, account_token: @token, timestamp: @timestamp
    end

    it '200 OK' do
      expect(response.status).to be(200)
    end

    it 'assigns @locations' do
      expect(assigns(:locations)).to include(@location)
    end
  end

  describe 'GET #show' do
    context 'with valid id' do
      before do
        allow(Location).to receive(:find) { @location }
        get :show, id: @location.id, lat: 33.333, long: 33.333, account_id: @account.id,
                   account_token: @token, timestamp: @timestamp
      end

      it '200 OK' do
        expect(response.status).to be(200)
      end

      it 'assigns @location' do
        allow(Location).to receive(:find) { @location }
        expect(assigns(:location)).to eq(@location)
      end
    end

    context 'with invalid id' do
      it '404 Not Found' do
        get :show, id: 'non_existant_id', account_id: @account.id,
                   account_token: @token, timestamp: @timestamp
        expect(response.status).to be(404)
      end
    end
  end

  describe 'GET #search' do
    before { allow(Location).to receive(:by_lat_and_long) { [@location] } }

    context 'with lat and long' do
      before do
        get :search, lat: @location.latitude, long: @location.longitude,
                     account_id: @account.id, account_token: @token, timestamp: @timestamp
      end

      it '200 OK' do
        expect(response.status).to be(200)
      end

      it 'assigns @locations' do
        expect(assigns(:locations)).to include(@location)
      end
    end

    context 'without lat and long' do
      it '400 Bad Request' do
        get :search, account_id: @account.id, account_token: @token, timestamp: @timestamp
        expect(response.status).to be(400)
      end
    end
  end
end
