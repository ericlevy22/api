resource 'Accounts', type: :feature do
  let!(:account) { create :account }

  get '/accounts' do
    parameter :account_email, 'The email address associated with your account', required: true
    parameter :account_token, 'The hash of your session token and timestamp', required: true
    parameter :timestamp, 'The timestamp used to sign the request', required: true
    parameter :limit, 'How many records to include, default: 10'
    parameter :offset, 'How many records to skip, default: 0'

    before { log_in_as_client! }

    example 'Get a list of accounts', document: [:admin] do
      do_request account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(String).at_path('accounts/0/email')
      expect(response_body).to have_json_type(String).at_path('accounts/0/authentication_token')
    end
  end

  post '/accounts/login' do
    parameter :account, 'The credentials for your account'
    parameter :email, 'The email address associated with your account', scope: :account, required: true
    parameter :password, 'The password for your account', scope: :account, required: true

    let(:account) { create(:account) }

    example 'Initiate a session' do
      do_request account: { email: account.email, password: account.password }

      expect(response_body).to have_json_type(String).at_path('account/email')
      expect(response_body).to have_json_type(String).at_path('account/authentication_token')
    end
  end

  post '/accounts' do
    parameter :account, 'The credentials for your account'
    parameter :email, 'The email address associated with your account', scope: :account, required: true
    parameter :password, 'The password for your account', scope: :account, required: true
    parameter :properties, 'A hash of miscellaneous user properties associated with the account', scope: :account

    example 'Create or log in to an account with an email address and password', document: [:admin] do
      do_request account: { email: (Faker::Internet.email), password: 'asdfasdf' }

      expect(response_body).to have_json_type(String).at_path('account/email')
      expect(response_body).to have_json_type(String).at_path('account/authentication_token')
    end
  end

  post '/accounts' do
    parameter :account, 'The credentials for your account'
    parameter :email, 'The email address associated with your account', scope: :account, required: true
    parameter :password, 'The password for your account', scope: :account, required: true
    parameter :properties, 'A hash of miscellaneous user properties associated with the account', scope: :account

    let(:account) { create(:account) }

    example 'Create or log into an account with an email address and password', document: false do
      do_request account: { email: account.email.insert(1, '.'), password: 'asdfasdf' }

      expect(response_body).to have_json_type(String).at_path('account/email')
      expect(response_body).to have_json_type(String).at_path('account/authentication_token')
    end
  end

  post '/accounts' do
    parameter :account, 'The credentials for your account'
    parameter :email, 'The email address associated with your account', scope: :account, required: true
    parameter :password, 'The password for your account', scope: :account, required: true
    parameter :properties, 'A hash of miscellaneous user properties associated with the account', scope: :account

    let(:account) { create(:account) }

    example 'Create or log into an account with an email address and password', document: false do
      do_request account: { email: account.email.upcase, password: 'asdfasdf' }

      expect(response_body).to have_json_type(String).at_path('account/email')
      expect(response_body).to have_json_type(String).at_path('account/authentication_token')
    end
  end

  post '/accounts' do
    parameter :facebook_access_token, 'Your OAuth token from Facebook', scope: :account, required: true
    parameter :properties, 'A hash of miscellaneous user properties associated with the account', scope: :account

    let(:facebook_access_token) { SecureRandom.base64(128) }
    before { log_in_as_facebook! }

    example 'Create or log into an account with an OAuth access token from Facebook', document: [:admin] do
      do_request

      expect(response_body).to have_json_type(String).at_path('account/email')
      expect(response_body).to have_json_type(String).at_path('account/authentication_token')
    end
  end

  post '/accounts/send-reset' do
    parameter :email, 'The email address associated with the account', scope: :account, required: true
    parameter :mall_id, 'The id of the mall where the password should be reset', required: true

    let(:account) { create(:account) }
    let(:email) { account.email }
    let(:mall) { create(:mall) }
    let(:mall_id) { mall.id }

    before { allow(account).to receive(:send_reset_password_instructions) { account } }

    example 'Request a password reset link to be sent to the email address for the account', document: [:admin] do
      do_request

      expect(status).to be(204)
    end
  end

  post '/accounts/reset' do
    parameter :reset_password_token, 'The reset token sent via email', scope: :account
    parameter :password, 'The new password', scope: :account
    parameter :password_confirmation, 'Confirm the new password', scope: :account

    let(:reset_password_token) { SecureRandom.hex(20) }
    let(:password) { SecureRandom.hex(25) }
    let(:password_confirmation) { password }

    before { allow(Account).to receive(:reset_password_by_token) { create(:account) } }

    example 'Use a token to reset the password for the account', document: [:admin] do
      do_request

      expect(status).to be(201)
    end
  end

  get '/accounts/:id' do
    parameter :id, 'The id of the account to show details for', required: true
    parameter :account_email, 'The email address associated with your account', required: true
    parameter :account_token, 'The hash of your session token and timestamp', required: true
    parameter :timestamp, 'The timestamp used to sign the request', required: true
    parameter :properties, 'A hash of miscellaneous user properties associated with the account', scope: :account

    before do
      log_in_as_client!
      @account.user.update_attribute :properties, name: { first: Faker::Name.first_name, last: Faker::Name.last_name }
    end

    example 'Get details for an account', document: [:admin] do
      do_request id: @account.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(String).at_path('account/email')
      expect(response_body).to have_json_type(String).at_path('account/authentication_token')
      expect(response_body).to have_json_type(Hash).at_path('account/properties')
      expect(response_body).to have_json_type(String).at_path('account/properties/name/first')
      expect(response_body).to have_json_type(String).at_path('account/properties/name/last')
      expect(response_body).to have_json_type(String).at_path('account/created_at')
      expect(response_body).to have_json_type(String).at_path('account/updated_at')
    end
  end

  patch '/accounts/:id' do
    parameter :account_email, 'The email address associated with your account', required: true
    parameter :account_token, 'The hash of your session token and timestamp', required: true
    parameter :timestamp, 'The timestamp used to sign the request', required: true
    parameter :password, 'The new password for the account', scope: :account
    parameter :password_confirmation, 'Confirm the new password', scope: :account
    parameter :current_password, 'You must provide the current password for the account to update your password', scope: :account
    parameter :properties, 'A hash of miscellaneous user properties associated with the account (*note: not all properties are changeable)', scope: :account
    parameter :mall_id, 'The id of the current mall'

    let(:password) { SecureRandom.hex(32) }
    let(:password_confirmation) { password }
    let(:current_password) { SecureRandom.hex(32) }
    let(:properties) { { location: { zip: "%.5d" % rand(99999) } } }
    let(:mall_id) { create(:mall, company: create(:company, allow_unconfirmed_access: true)).id }

    before do
      log_in_as_client!(traits: :unconfirmed)
      @account.update_attribute(:password, current_password)
    end

    example 'Update the details for your account (*Note: not all properties are updatable)', document: [:admin] do
      do_request id: @account.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(@account.reload.valid_password?(password)).to be(true)
      expect(response_body).to have_json_type(Integer).at_path('account/id')
      expect(response_body).to have_json_type(String).at_path('account/authentication_token')
      expect(response_body).to have_json_type(Hash).at_path('account/properties')
      expect(response_body).to have_json_type(String).at_path('account/properties/location/zip')
    end
  end

  get '/accounts/:id/affinities' do
    parameter :id, 'The id of the account to show affinities for', required: true
    parameter :timestamp, 'The timestamp used to sign the request', required: true

    before do
      log_in_as_client!
      @affinity = create :affinity, user: @account.user
    end

    example 'Get affinities belonging to an account', document: [:admin] do
      do_request id: @account.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('affinities')
      expect(response_body).to have_json_type(Integer).at_path('affinities/0/id')
      expect(response_body).to have_json_type(Integer).at_path('affinities/0/heartable_id')
      expect(response_body).to have_json_type(String).at_path('affinities/0/heartable_type')
      expect(response_body).to have_json_type(String).at_path('affinities/0/created_at')
      expect(response_body).to have_json_type(String).at_path('affinities/0/updated_at')
    end
  end

  post '/accounts/resend-confirmation' do
    parameter :email, 'The email address associated with your account', scope: :account, required: true
    parameter :mall_id, 'The id of the mall associated with your account', required: true

    let(:account) { create(:account, confirmed_at: nil) }
    let(:email) { account.email }
    let(:mall_id) { create(:mall).id }

    before { allow(account).to receive(:send_confirmation_instructions) }

    example 'Re-send the confirmation email for an unconfirmed account', document: [:admin] do
      do_request

      expect(status).to be(204)
    end
  end

  delete '/accounts/logout' do
    parameter :account, 'The credentials for your account'

    before do
      log_in_as_client!
      @old_token = @account.authentication_token
    end

    example 'Terminate a session' do
      do_request account_id: @account.id, account_token: @token, timestamp: @timestamp, format: :json

      expect(@account.reload.authentication_token).to_not eq(@old_token)
      expect(status).to be(204)
    end
  end

  delete '/accounts/:id' do
    parameter :id, 'The id of the account to be deleted', required: true
    parameter :account_email, 'The email address associated with your account', required: true
    parameter :account_token, 'The hash of your session token and timestamp', required: true
    parameter :timestamp, 'The timestamp used to sign the request', required: true

    before { log_in_as_admin! }

    example 'Delete an account', document: [:admin] do
      do_request id: account.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(status).to be(204)
    end
  end
end
