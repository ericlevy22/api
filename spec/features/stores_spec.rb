resource 'Stores', type: :feature do
  before(:all) do
    @store = create(:store, mall: create(:mall))
    log_in_as_client!(resource: @store.mall, traits: [:mall_channel, :deal_channel, :store_channel])
  end

  get '/stores' do
    parameter :limit, 'How many records to include, default: 10'
    parameter :offset, 'How many records to skip, default: 0'
    parameter :since, 'Skip records which have not been updated before this date (format: YYYY-MM-DD)'
    parameter :q, 'Term for searching stores by name'

    example 'Get a list of stores', document: [:store_channel] do
      do_request account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('stores')
      expect(response_body).to have_json_type(Integer).at_path('stores/0/id')
      expect(response_body).to have_json_type(String).at_path('stores/0/name')
      expect(response_body).to have_json_type(String).at_path('stores/0/seo_slug')
      expect(response_body).to have_json_type(String).at_path('stores/0/description')
      expect(response_body).to have_json_type(String).at_path('stores/0/address')
      expect(response_body).to have_json_type(String).at_path('stores/0/city')
      expect(response_body).to have_json_type(String).at_path('stores/0/state')
      expect(response_body).to have_json_type(String).at_path('stores/0/zip')
      expect(response_body).to have_json_type(String).at_path('stores/0/phone')
      expect(response_body).to have_json_type(String).at_path('stores/0/url')
      expect(response_body).to have_json_type(String).at_path('stores/0/url_text')
      expect(response_body).to have_json_type(String).at_path('stores/0/hours')
      expect(response_body).to have_json_type(String).at_path('stores/0/tenant_location')
      expect(response_body).to have_json_type(String).at_path('stores/0/tenant_location_map_key')
      expect(response_body).to have_json_type(String).at_path('stores/0/opening_date')
      expect(response_body).to have_json_type(String).at_path('stores/0/opening_text')
      expect(response_body).to have_json_type(String).at_path('stores/0/mall_name')
      expect(response_body).to have_json_type(:boolean).at_path('stores/0/exclude_national_deals')
      expect(response_body).to have_json_type(String).at_path('stores/0/created_at')
      expect(response_body).to have_json_type(String).at_path('stores/0/updated_at')
      expect(response_body).to have_json_type(Array).at_path('images')
      expect(response_body).to have_json_type(Array).at_path('groups')
      expect(response_body).to have_json_type(Array).at_path('keywords')
    end
  end

  get '/stores/:id' do
    parameter :id, 'The id of the store to retrieve', required: true

    example 'Get a specific store', document: [:store_channel] do
      do_request id: @store.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Integer).at_path('store/id')
      expect(response_body).to have_json_type(String).at_path('store/name')
      expect(response_body).to have_json_type(String).at_path('store/seo_slug')
      expect(response_body).to have_json_type(String).at_path('store/description')
      expect(response_body).to have_json_type(String).at_path('store/address')
      expect(response_body).to have_json_type(String).at_path('store/city')
      expect(response_body).to have_json_type(String).at_path('store/state')
      expect(response_body).to have_json_type(String).at_path('store/zip')
      expect(response_body).to have_json_type(String).at_path('store/phone')
      expect(response_body).to have_json_type(String).at_path('store/url')
      expect(response_body).to have_json_type(String).at_path('store/url_text')
      expect(response_body).to have_json_type(String).at_path('store/hours')
      expect(response_body).to have_json_type(String).at_path('store/tenant_location')
      expect(response_body).to have_json_type(String).at_path('store/tenant_location_map_key')
      expect(response_body).to have_json_type(String).at_path('store/opening_date')
      expect(response_body).to have_json_type(String).at_path('store/opening_text')
      expect(response_body).to have_json_type(String).at_path('store/mall_name')
      expect(response_body).to have_json_type(:boolean).at_path('store/exclude_national_deals')
      expect(response_body).to have_json_type(String).at_path('store/created_at')
      expect(response_body).to have_json_type(String).at_path('store/updated_at')
      expect(response_body).to have_json_type(Array).at_path('images')
      expect(response_body).to have_json_type(Array).at_path('groups')
      expect(response_body).to have_json_type(Array).at_path('keywords')
    end
  end

  get '/stores/:id/deals' do
    before do
      @store.deals << create(:deal, retailer: @store.retailer)
    end

    parameter :id, 'The id of the store for which to retrieve deals', required: true
    parameter :limit, 'How many records to include, default: 10'
    parameter :offset, 'How many records to skip, default: 0'

    example 'Get a list of deals for a specific store', document: [:deal_channel] do
      do_request id: @store.id, lat: 33.333, long: -104.567, account_id: @account.id,
                 account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('deals')
      expect(response_body).to have_json_type(Integer).at_path('deals/0/id')
      expect(response_body).to have_json_type(Integer).at_path('deals/0/retailer_id')
      expect(response_body).to have_json_type(String).at_path('deals/0/title')
      expect(response_body).to have_json_type(String).at_path('deals/0/seo_slug')
      expect(response_body).to have_json_type(String).at_path('deals/0/name')
      expect(response_body).to have_json_type(String).at_path('deals/0/sort_name')
      expect(response_body).to have_json_type(String).at_path('deals/0/retailer_name')
      expect(response_body).to have_json_type(String).at_path('deals/0/retailer_sort_name')
      expect(response_body).to have_json_type(String).at_path('deals/0/store_name')
      expect(response_body).to have_json_type(String).at_path('deals/0/store_sort_name')
      expect(response_body).to have_json_type(String).at_path('deals/0/description')
      expect(response_body).to have_json_type(:boolean).at_path('deals/0/is_local')
      expect(response_body).to have_json_type(:boolean).at_path('deals/0/is_featured')
      expect(response_body).to have_json_type(String).at_path('deals/0/created_at')
      expect(response_body).to have_json_type(String).at_path('deals/0/updated_at')
      expect(response_body).to have_json_type(Array).at_path('images')
    end
  end

  get '/stores/:id/social-feeds' do
    before { @store.social_feeds << create(:social_feed) }

    parameter :id, 'The id of the store for which to retrieve social network feeds', required: true
    parameter :limit, 'How many records to include, default: 10'
    parameter :offset, 'How many records to skip, default: 0'

    example 'Get a list of social network feeds for a specific store', document: [:store_channel] do
      do_request id: @store.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('social_feeds')
      expect(response_body).to have_json_type(Integer).at_path('social_feeds/0/id')
      expect(response_body).to have_json_type(String).at_path('social_feeds/0/name')
      expect(response_body).to have_json_type(Hash).at_path('social_feeds/0/properties')
      expect(response_body).to have_json_type(String).at_path('social_feeds/0/created_at')
      expect(response_body).to have_json_type(String).at_path('social_feeds/0/updated_at')
    end
  end

  get '/stores/:id/contacts' do
    before { @store.contacts << create(:contact) }

    parameter :id, 'The id of the store for which to retrieve contact information', required: true
    parameter :limit, 'How many records to include, default: 10'
    parameter :offset, 'How many records to skip, default: 0'

    example 'Get a list of contacts for a specific store', document: [:store_channel] do
      do_request id: @store.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('contacts')
      expect(response_body).to have_json_type(Integer).at_path('contacts/0/id')
      expect(response_body).to have_json_type(String).at_path('contacts/0/name')
      expect(response_body).to have_json_type(String).at_path('contacts/0/title')
      expect(response_body).to have_json_type(String).at_path('contacts/0/email')
      expect(response_body).to have_json_type(String).at_path('contacts/0/phone')
      expect(response_body).to have_json_type(String).at_path('contacts/0/created_at')
      expect(response_body).to have_json_type(String).at_path('contacts/0/updated_at')
    end
  end

  get '/stores/:id/products' do
    before do
      log_in_as_admin! # TODO: change this back to client once product_channel is implemented
      @store.products << create(:product)
    end

    parameter :id, 'The id of the store for which to retrieve curated products', required: true
    parameter :limit, 'How many records to include, default: 10'
    parameter :offset, 'How many records to skip, default: 0'

    example 'Get a list of curated products for a specific store', document: [:product_channel] do
      do_request id: @store.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('products')
      expect(response_body).to have_json_type(Integer).at_path('products/0/id')
      expect(response_body).to have_json_type(String).at_path('products/0/name')
      expect(response_body).to have_json_type(String).at_path('products/0/seo_slug')
      expect(response_body).to have_json_type(String).at_path('products/0/description')
      expect(response_body).to have_json_type(String).at_path('products/0/created_at')
      expect(response_body).to have_json_type(String).at_path('products/0/updated_at')
      expect(response_body).to have_json_type(Array).at_path('images')
    end
  end

  get '/stores/:id/job-listings' do
    before { @store.job_listings << create(:job_listing) }

    parameter :id, 'The id of the store for which to retrieve job listings', required: true
    parameter :limit, 'How many records to include, default: 10'
    parameter :offset, 'How many records to skip, default: 0'

    example 'Get a list of current job listings for a specific store', document: [:store_channel] do
      do_request id: @store.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('job_listings')
      expect(response_body).to have_json_type(Integer).at_path('job_listings/0/id')
      expect(response_body).to have_json_type(String).at_path('job_listings/0/store_name')
      expect(response_body).to have_json_type(String).at_path('job_listings/0/start_date_text')
      expect(response_body).to have_json_type(String).at_path('job_listings/0/hours_per_week')
      expect(response_body).to have_json_type(String).at_path('job_listings/0/number_of_openings')
      expect(response_body).to have_json_type(String).at_path('job_listings/0/fax_number')
      expect(response_body).to have_json_type(String).at_path('job_listings/0/email')
      expect(response_body).to have_json_type(String).at_path('job_listings/0/salary')
      expect(response_body).to have_json_type(String).at_path('job_listings/0/key_responsibilities')
      expect(response_body).to have_json_type(String).at_path('job_listings/0/required_experience')
      expect(response_body).to have_json_type(String).at_path('job_listings/0/special_information')
      expect(response_body).to have_json_type(String).at_path('job_listings/0/description')
      expect(response_body).to have_json_type(String).at_path('job_listings/0/display_at')
      expect(response_body).to have_json_type(String).at_path('job_listings/0/end_at')
      expect(response_body).to have_json_type(:boolean).at_path('job_listings/0/is_full_time')
      expect(response_body).to have_json_type(:boolean).at_path('job_listings/0/is_apply_online')
      expect(response_body).to have_json_type(String).at_path('job_listings/0/created_at')
      expect(response_body).to have_json_type(String).at_path('job_listings/0/updated_at')
    end
  end

  post '/stores' do
    before { log_in_as_admin! }

    parameter :mall_id, 'The id of the mall where this store is located (if applicable)', scope: :store
    parameter :retailer_id, 'The id of the retailer to which this store belongs', scope: :store, required: true
    parameter :location_id, 'The id of the location where this store exists', scope: :store, required: true
    parameter :name, 'The name of the store', scope: :store, required: true
    parameter :sort_name, 'The sortable name of the store', scope: :store, required: true
    parameter :seo_slug, 'The URL slug for the store', scope: :store, required: true, limit: 250
    parameter :address, 'The address', scope: :store
    parameter :city, 'The city', scope: :store
    parameter :state, 'The state', scope: :store
    parameter :zip, 'The ZIP', scope: :store
    parameter :phone, 'The primary phone number for the store', scope: :store
    parameter :url, 'The URL for the store', scope: :store, limit: 300
    parameter :url_text, 'The text that should be displayed when building links for the store', scope: :store, limit: 300
    parameter :hours, 'A text description of store hours', scope: :store
    parameter :email, 'The primary email address for the store', scope: :store
    parameter :fax, 'The fax number', scope: :store
    parameter :description, 'The store description', scope: :store
    parameter :is_featured, 'Whether the store is featured', scope: :store
    parameter :open_at, 'The date which the store opened for business', scope: :store
    parameter :close_at, 'The date which the store was closed', scope: :store
    parameter :tenant_location, 'The tenant location code for mall direcories', scope: :store
    parameter :opening_date, 'A date in the future when the store will open', scope: :store
    parameter :opening_text, 'The display text for when a store opens in the future', scope: :store
    parameter :exclude_national_deals, 'Whether or not to exclude global promotions', scope: :store

    let(:mall_id) { create(:mall).id }
    let(:retailer_id) { create(:retailer).id }
    let(:location_id) { create(:location).id }
    let(:name) { Faker::Lorem.sentence(3) }
    let(:sort_name) { name }
    let(:seo_slug) { name.slugify }
    let(:address) { Faker::Address.street_address }
    let(:city) { Faker::Address.city }
    let(:state) { Faker::Address.state_abbr }
    let(:zip) { Faker::Address.zip_code }
    let(:phone) { Faker::PhoneNumber.phone_number }
    let(:hours) { '9-9 daily' }
    let(:email) { Faker::Internet.email }
    let(:fax) { Faker::PhoneNumber.phone_number }
    let(:description) { Faker::Lorem.sentence }
    let(:is_featured) { true }
    let(:open_at) { 1.year.ago }
    let(:close_at) { 10.years.from_now }
    let(:tenant_location) { 'C4' }
    let(:opening_date) { Faker::Date.between(2.days.ago, Date.tomorrow) }
    let(:opening_text) { Faker::Lorem.sentence(3) }
    let(:exclude_national_deals) { false }

    example 'Create a new store', document: [:admin] do
      do_request account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Integer).at_path('store/id')
      expect(response_body).to have_json_type(String).at_path('store/name')
      expect(response_body).to have_json_type(String).at_path('store/seo_slug')
      expect(response_body).to have_json_type(String).at_path('store/description')
      expect(response_body).to have_json_type(String).at_path('store/address')
      expect(response_body).to have_json_type(String).at_path('store/city')
      expect(response_body).to have_json_type(String).at_path('store/state')
      expect(response_body).to have_json_type(String).at_path('store/zip')
      expect(response_body).to have_json_type(String).at_path('store/phone')
      expect(response_body).to have_json_type(String).at_path('store/hours')
      expect(response_body).to have_json_type(String).at_path('store/opening_date')
      expect(response_body).to have_json_type(String).at_path('store/opening_text')
      expect(response_body).to have_json_type(String).at_path('store/mall_name')
      expect(response_body).to have_json_type(:boolean).at_path('store/exclude_national_deals')
      expect(response_body).to have_json_type(Array).at_path('images')
      expect(response_body).to have_json_type(Array).at_path('groups')
      expect(response_body).to have_json_type(Array).at_path('keywords')
    end
  end

  patch '/stores/:id' do
    before { log_in_as_admin! }

    parameter :mall_id, 'The id of the mall where this store is located (if applicable)', scope: :store
    parameter :retailer_id, 'The id of the retailer to which this store belongs', scope: :store
    parameter :location_id, 'The id of the location where this store exists', scope: :store
    parameter :name, 'The name of the store', scope: :store
    parameter :sort_name, 'The sortable name of the store', scope: :store
    parameter :seo_slug, 'The URL slug for the store', scope: :store, limit: 250
    parameter :address, 'The address', scope: :store
    parameter :city, 'The city', scope: :store
    parameter :state, 'The state', scope: :store
    parameter :zip, 'The ZIP', scope: :store
    parameter :phone, 'The primary phone number for the store', scope: :store
    parameter :url, 'The URL for the store', scope: :store, limit: 300
    parameter :url_text, 'The text that should be displayed when building links for the store', scope: :store, limit: 300
    parameter :hours, 'A text description of store hours', scope: :store
    parameter :email, 'The primary email address for the store', scope: :store
    parameter :fax, 'The fax number', scope: :store
    parameter :description, 'The store description', scope: :store
    parameter :is_featured, 'Whether the store is featured', scope: :store
    parameter :open_at, 'The date which the store opened for business', scope: :store
    parameter :close_at, 'The date which the store was closed', scope: :store
    parameter :tenant_location, 'The tenant location description for mall direcories', scope: :store
    parameter :opening_date, 'A date in the future when the store will open', scope: :store
    parameter :opening_text, 'The display text for when a store opens in the future', scope: :store
    parameter :exclude_national_deals, 'Whether or not to exclude global promotions', scope: :store

    let(:mall_id) { create(:mall).id }
    let(:retailer_id) { create(:retailer).id }
    let(:location_id) { create(:location).id }
    let(:name) { Faker::Lorem.sentence(3) }
    let(:sort_name) { name }
    let(:seo_slug) { name.slugify }
    let(:address) { Faker::Address.street_address }
    let(:city) { Faker::Address.city }
    let(:state) { Faker::Address.state_abbr }
    let(:zip) { Faker::Address.zip_code }
    let(:phone) { Faker::PhoneNumber.phone_number }
    let(:hours) { '9-9 daily' }
    let(:email) { Faker::Internet.email }
    let(:fax) { Faker::PhoneNumber.phone_number }
    let(:description) { Faker::Lorem.sentence }
    let(:is_featured) { true }
    let(:open_at) { 1.year.ago }
    let(:close_at) { 10.years.from_now }
    let(:tenant_location) { 'C4' }
    let(:opening_date) { Faker::Date.between(2.days.ago, Date.tomorrow) }
    let(:opening_text) { Faker::Lorem.sentence(3) }

    example 'Update an existing store', document: [:admin] do
      do_request id: @store.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Integer).at_path('store/id')
      expect(response_body).to have_json_type(String).at_path('store/name')
      expect(response_body).to have_json_type(String).at_path('store/seo_slug')
      expect(response_body).to have_json_type(String).at_path('store/description')
      expect(response_body).to have_json_type(String).at_path('store/address')
      expect(response_body).to have_json_type(String).at_path('store/city')
      expect(response_body).to have_json_type(String).at_path('store/state')
      expect(response_body).to have_json_type(String).at_path('store/zip')
      expect(response_body).to have_json_type(String).at_path('store/phone')
      expect(response_body).to have_json_type(String).at_path('store/hours')
      expect(response_body).to have_json_type(String).at_path('store/tenant_location')
      expect(response_body).to have_json_type(String).at_path('store/tenant_location_map_key')
      expect(response_body).to have_json_type(String).at_path('store/opening_date')
      expect(response_body).to have_json_type(String).at_path('store/opening_text')
      expect(response_body).to have_json_type(String).at_path('store/mall_name')
      expect(response_body).to have_json_type(:boolean).at_path('store/exclude_national_deals')
      expect(response_body).to have_json_type(Array).at_path('images')
      expect(response_body).to have_json_type(Array).at_path('groups')
      expect(response_body).to have_json_type(Array).at_path('keywords')
    end
  end

  delete '/stores/:id' do
    before { log_in_as_admin! }

    parameter :id, 'The id of the store to delete', required: true

    example 'Delete an existing store', document: [:admin] do
      do_request id: @store.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(status).to eq(204)
    end
  end
end
