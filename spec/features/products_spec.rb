resource 'Products', type: :feature do
  before(:all) do
    @product = create :product
    log_in_as_client!(resource: @product)
  end

  get '/products' do
    before { allow(Product).to receive_message_chain(:limit, :offset) { [@product] } }

    parameter :limit, 'How many records to include, default: 10'
    parameter :offset, 'How many records to skip, default: 0'
    parameter :since, 'Skip records which have not been updated before this date (format: YYYY-MM-DD)'
    parameter :q, 'Term for searching products by name'

    example 'Get a list of curated products', document: [:product_channel] do
      do_request account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('products')
      expect(response_body).to have_json_type(Integer).at_path('products/0/id')
      expect(response_body).to have_json_type(String).at_path('products/0/name')
      expect(response_body).to have_json_type(String).at_path('products/0/seo_slug')
      expect(response_body).to have_json_type(String).at_path('products/0/description')
      expect(response_body).to have_json_type(Integer).at_path('products/0/brand_id')
      expect(response_body).to have_json_type(String).at_path('products/0/created_at')
      expect(response_body).to have_json_type(String).at_path('products/0/updated_at')
      expect(response_body).to have_json_type(Array).at_path('products/0/image_ids')
      expect(response_body).to have_json_type(Integer).at_path('products/0/retailer_id')
      expect(response_body).to have_json_type(Array).at_path('products/0/tag_ids')
      expect(response_body).to have_json_type(Array).at_path('images')
    end
  end

  get '/products/:id' do
    before { allow(Product).to receive(:find) { @product } }

    parameter :id, 'The product id', required: true

    example 'Get details about a curated product', document: [:product_channel] do
      do_request id: @product.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Integer).at_path('product/id')
      expect(response_body).to have_json_type(String).at_path('product/name')
      expect(response_body).to have_json_type(String).at_path('product/seo_slug')
      expect(response_body).to have_json_type(String).at_path('product/description')
      expect(response_body).to have_json_type(Integer).at_path('product/brand_id')
      expect(response_body).to have_json_type(String).at_path('product/created_at')
      expect(response_body).to have_json_type(String).at_path('product/updated_at')
      expect(response_body).to have_json_type(Integer).at_path('product/retailer_id')
      expect(response_body).to have_json_type(Array).at_path('product/tag_ids')
      expect(response_body).to have_json_type(Array).at_path('images')
    end
  end

  get '/products/:id/stores' do
    before { @product.stores << build(:store) }

    parameter :id, 'The product id', required: true
    parameter :limit, 'How many records to include, default: 10'
    parameter :offset, 'How many records to skip, default: 0'

    example 'Get a list of stores for a given product', document: [:product_channel] do
      do_request id: @product.id, lat: 33.333, long: -104.567,
                 account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('stores')
      expect(response_body).to have_json_type(Integer).at_path('stores/0/id')
      expect(response_body).to have_json_type(String).at_path('stores/0/name')
      expect(response_body).to have_json_type(String).at_path('stores/0/sort_name')
      expect(response_body).to have_json_type(String).at_path('stores/0/seo_slug')
      expect(response_body).to have_json_type(String).at_path('stores/0/description')
      expect(response_body).to have_json_type(String).at_path('stores/0/address')
      expect(response_body).to have_json_type(String).at_path('stores/0/city')
      expect(response_body).to have_json_type(String).at_path('stores/0/state')
      expect(response_body).to have_json_type(String).at_path('stores/0/zip')
      expect(response_body).to have_json_type(String).at_path('stores/0/phone')
      expect(response_body).to have_json_type(Integer).at_path('stores/0/mall_id')
      expect(response_body).to have_json_type(Integer).at_path('stores/0/retailer_id')
      expect(response_body).to have_json_type(Float).at_path('stores/0/latitude')
      expect(response_body).to have_json_type(Float).at_path('stores/0/longitude')
      expect(response_body).to have_json_type(Float).at_path('stores/0/distance')
      expect(response_body).to have_json_type(String).at_path('stores/0/created_at')
      expect(response_body).to have_json_type(String).at_path('stores/0/updated_at')
      expect(response_body).to have_json_type(Array).at_path('images')
    end
  end

  post '/products' do
    before { log_in_as_admin! }

    parameter :name, 'The name for the product', scope: :product, required: true
    parameter :seo_slug, 'The URL slug', scope: :product, required: true
    parameter :description, 'The product description', scope: :product
    parameter :price, 'An estimated price', scope: :product
    parameter :brand_id, 'The id of the brand for this product', scope: :product
    parameter :retailer_id, 'The id of the retailer for this product', scope: :product, required: true
    parameter :image_ids, 'An array of comma separated image ids associated with this product', scope: :product, required: true
    parameter :store_ids, 'An array of comma separated store ids associated with this product. All stores must have the same retailer id.', scope: :product, required: true

    let(:brand_id) { (create :brand).id }
    let(:name) { Faker::Lorem.sentence(3) }
    let(:seo_slug) { name.slugify }
    let(:description) { Faker::Lorem.paragraph }
    let(:price) { '$99.99' }
    let(:retailer_id) { create(:retailer).id }
    let(:store_ids) { [create(:store, retailer_id: retailer_id).id] }
    let(:image_ids) { [create(:image).id] }
    let(:tag_ids) { [create(:tag).id] }

    example 'Create a new product', document: [:admin] do
      do_request account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Integer).at_path('product/id')
      expect(response_body).to have_json_type(String).at_path('product/name')
      expect(response_body).to have_json_type(String).at_path('product/seo_slug')
      expect(response_body).to have_json_type(String).at_path('product/description')
      expect(response_body).to have_json_type(Integer).at_path('product/brand_id')
      expect(response_body).to have_json_type(Integer).at_path('product/retailer_id')
      expect(response_body).to have_json_type(Array).at_path('product/store_ids')
      expect(response_body).to have_json_type(Array).at_path('product/tag_ids')
      expect(response_body).to have_json_type(Array).at_path('images')
    end
  end

  patch '/products/:id' do
    before { log_in_as_admin! }

    parameter :name, 'The name for the product', scope: :product, required: true
    parameter :seo_slug, 'The URL slug', scope: :product
    parameter :description, 'The product description', scope: :product
    parameter :price, 'An estimated price', scope: :product
    parameter :brand_id, 'The id of the brand for this product', scope: :product
    parameter :retailer_id, 'The id of the retailer for this product', scope: :product
    parameter :image_ids, 'An array of comma separated image ids associated with this product', scope: :product
    parameter :store_ids, 'An array of comma separated store ids associated with this product. All stores must have the same retailer id.', scope: :product

    let(:brand_id) { (create :brand).id }
    let(:name) { Faker::Lorem.sentence(3) }
    let(:seo_slug) { name.slugify }
    let(:description) { Faker::Lorem.paragraph }
    let(:price) { '$99.99' }

    example 'Update an existing product', document: [:admin] do
      do_request id: @product.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Integer).at_path('product/id')
      expect(response_body).to have_json_type(String).at_path('product/name')
      expect(response_body).to have_json_type(String).at_path('product/seo_slug')
      expect(response_body).to have_json_type(String).at_path('product/description')
      expect(response_body).to have_json_type(Integer).at_path('product/brand_id')
      expect(response_body).to have_json_type(Integer).at_path('product/retailer_id')
      expect(response_body).to have_json_type(Array).at_path('product/tag_ids')
      expect(response_body).to have_json_type(Array).at_path('images')
    end
  end

  delete '/products/:id' do
    before { log_in_as_admin! }

    parameter :id, 'The id of the product to delete', required: true

    example 'Delete a product', document: [:admin] do
      do_request id: @product.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(status).to be(204)
    end
  end
end
