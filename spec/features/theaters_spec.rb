resource 'Theaters', type: :feature do
  before(:all) do
    @theater = create(:theater, mall: create(:mall), external_key: 8749)
    @account = log_in_as_client!(resource: @theater.mall, traits: :mall_channel)
  end

  get '/theaters/:id' do
    parameter :id, 'The id of the theater', required: true

    example 'Get details for a specific theater', document: [:admin] do
      do_request id: @theater.id, account_id: @account.id,
                 account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Integer).at_path('theater/id')
      expect(response_body).to have_json_type(String).at_path('theater/name')
      expect(response_body).to have_json_type(String).at_path('theater/sort_name')
      expect(response_body).to have_json_type(String).at_path('theater/seo_slug')
      expect(response_body).to have_json_type(String).at_path('theater/address')
      expect(response_body).to have_json_type(String).at_path('theater/city')
      expect(response_body).to have_json_type(String).at_path('theater/state')
      expect(response_body).to have_json_type(String).at_path('theater/zip')
      expect(response_body).to have_json_type(String).at_path('theater/phone')
      expect(response_body).to have_json_type(String).at_path('theater/url')
      expect(response_body).to have_json_type(String).at_path('theater/created_at')
      expect(response_body).to have_json_type(String).at_path('theater/updated_at')
      expect(response_body).to have_json_type(Array).at_path('images')
    end
  end

  get '/theaters/:id/showtimes' do
    parameter :id, 'The id of the theater for which to retrieve showtimes', required: true

    example 'Get a list of showtimes for a specific theater', document: [:admin] do
      do_request id: @theater.id, account_id: @account.id,
                 account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('showtimes')
      expect(response_body).to have_json_type(String).at_path('showtimes/0/id')
      expect(response_body).to have_json_type(String).at_path('showtimes/0/title')
      expect(response_body).to have_json_type(String).at_path('showtimes/0/seo_slug')
      expect(response_body).to have_json_type(String).at_path('showtimes/0/rating')
      expect(response_body).to have_json_type(String).at_path('showtimes/0/duration')
      expect(response_body).to have_json_type(Array).at_path('showtimes/0/genres')
      expect(response_body).to have_json_type(Array).at_path('showtimes/0/directors')
      expect(response_body).to have_json_path('showtimes/0/short_description')
      expect(response_body).to have_json_path('showtimes/0/long_description')
      expect(response_body).to have_json_type(String).at_path('showtimes/0/image_uri')
      expect(response_body).to have_json_path('showtimes/0/website_uri')
      expect(response_body).to have_json_type(Array).at_path('showtimes/0/showings')
      expect(response_body).to have_json_type(String).at_path('showtimes/0/showings/0/showing_at')
    end
  end
end
