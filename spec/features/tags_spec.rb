resource 'Tags', :feature do
  before(:all) do
    @tag = create :tag
    log_in_as_client!
  end
  # after(:all) { DatabaseCleaner.clean }

  post '/tags' do
    before { log_in_as_admin! }

    parameter :name, scope: :tag, required: true

    let(:name) { Faker::Lorem.sentence(2) }

    example 'Create a new tag', document: [:admin] do
      do_request account_id: @account.id, account_token: @token, timestamp: @timestamp,
                        tag: { name: name }

      expect(response_body).to have_json_type(Integer).at_path('tag/id')
      expect(response_body).to have_json_type(String).at_path('tag/name')
      expect(response_body).to have_json_type(String).at_path('tag/created_at')
      expect(response_body).to have_json_type(String).at_path('tag/updated_at')
    end
  end

  get '/tags' do
    parameter :limit, 'How many records to include, default: 10'
    parameter :offset, 'How many records to skip, default: 0'
    parameter :since, 'Skip records which have not been updated before this date (format: YYYY-MM-DD)'

    example 'Get a list of tags', document: [:admin] do
      do_request account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('tags')
      expect(response_body).to have_json_type(Integer).at_path('tags/0/id')
      expect(response_body).to have_json_type(String).at_path('tags/0/name')
      expect(response_body).to have_json_type(String).at_path('tags/0/created_at')
      expect(response_body).to have_json_type(String).at_path('tags/0/updated_at')
    end
  end

  get '/tags/:id' do
    parameter :id, 'The tag id', required: true

    example 'Get details for a specific tag', document: [:admin] do
      do_request id: @tag.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Integer).at_path('tag/id')
      expect(response_body).to have_json_type(String).at_path('tag/name')
      expect(response_body).to have_json_type(String).at_path('tag/created_at')
      expect(response_body).to have_json_type(String).at_path('tag/updated_at')
    end
  end

  get '/tags/search' do
    parameter :q, 'The term to search for', required: true
    parameter :since, 'Skip records which have not been updated before this date (format: YYYY-MM-DD)'

    example 'Search for tags by name', document: [:admin] do
      do_request q: @tag.name, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('tags')
      expect(response_body).to have_json_type(Integer).at_path('tags/0/id')
      expect(response_body).to have_json_type(String).at_path('tags/0/name')
      expect(response_body).to have_json_type(String).at_path('tags/0/created_at')
      expect(response_body).to have_json_type(String).at_path('tags/0/updated_at')
    end
  end
end
