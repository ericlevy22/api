resource 'Images', type: :feature do
  before(:all) do
    @image = create :image
    log_in_as_admin!
  end

  get '/images' do
    before { allow(Image).to receive_message_chain(:limit, :offset) { [@image] } }

    parameter :limit, 'How many records to include, default: 10'
    parameter :offset, 'How many records to skip, default: 0'
    parameter :since, 'Skip records which have not been updated before this date (format: YYYY-MM-DD)'

    example 'Get a list of images', document: [:admin] do
      do_request account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('images')
      expect(response_body).to have_json_type(String).at_path('images/0/url')
      expect(response_body).to have_json_type(String).at_path('images/0/size_parameter')
    end
  end

  get '/images/:id' do
    before { allow(Image).to receive(:find) { @product } }

    parameter :id, 'The image id', required: true

    example 'Get details about an image', document: [:admin] do
      do_request id: @image.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Integer).at_path('image/id')
      expect(response_body).to have_json_type(String).at_path('image/url')
      expect(response_body).to have_json_type(String).at_path('image/size_parameter')
    end
  end

  post '/images' do
    before { allow(Images::Upload).to receive(:upload_and_build) { @image } }

    parameter :url, 'The url of the image', scope: :image
    parameter :size_parameter, 'The pre-rendered size of the image', scope: :image
    parameter :cached_tag_list, 'A list of tags ids associated with this image', scope: :image

    let(:url) { Faker::Internet.url }
    let(:size_parameter) { '{width}' }
    let(:imageable_id) { create(:product).id }
    let(:imageable_type) { 'Product' }

    example 'Create a new image by providng a URL', document: [:admin] do
      do_request account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Integer).at_path('image/id')
      expect(response_body).to have_json_type(String).at_path('image/url')
      expect(response_body).to have_json_type(String).at_path('image/size_parameter')
    end
  end

  post '/images' do
    before { allow(Images::Upload).to receive(:upload_and_build) { @image } }

    parameter :file_data, 'A Base64 encoded version of the image to be uploaded', scope: :image
    parameter :file_name, 'The name of the file to be uploaded', scope: :image
    parameter :size_parameter, 'The pre-rendered size of the image', scope: :image
    parameter :cached_tag_list, 'A list of tags ids associated with this image', scope: :image

    let(:file_data) { "data:image/png;base64," + Base64.encode64('A very long image file') }
    let(:file_name) { Faker::Lorem.word }
    let(:size_parameter) { '{width}' }
    let(:imageable_id) { create(:product).id }
    let(:imageable_type) { 'Product' }

    example 'Upload a new image', document: [:admin] do
      do_request account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Integer).at_path('image/id')
      expect(response_body).to have_json_type(String).at_path('image/url')
      expect(response_body).to have_json_type(String).at_path('image/size_parameter')
    end
  end

  delete '/images/:id' do
    parameter :id, 'The id of the image to delete', required: true

    example 'Delete an image', document: [:admin] do
      do_request id: @image.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(status).to be(204)
    end
  end
end
