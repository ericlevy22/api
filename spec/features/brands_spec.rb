resource 'Brands', type: :feature do
  before do
    @brand = create :brand
    log_in_as_client!(resource: @brand)
  end

  get '/brands' do
    parameter :limit, 'How many records to include, default: 10'
    parameter :offset, 'How many records to skip, default: 0'
    parameter :since, 'Skip records which have not been updated before this date (format: YYYY-MM-DD)'
    parameter :q, 'Term for searching brands by name'

    example 'Get a list of brands', document: [:admin] do
      do_request account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('brands')
      expect(response_body).to have_json_type(Integer).at_path('brands/0/id')
      expect(response_body).to have_json_type(String).at_path('brands/0/name')
      expect(response_body).to have_json_type(String).at_path('brands/0/seo_slug')
      expect(response_body).to have_json_type(String).at_path('brands/0/description')
      expect(response_body).to have_json_type(String).at_path('brands/0/created_at')
      expect(response_body).to have_json_type(String).at_path('brands/0/updated_at')
      expect(response_body).to have_json_type(Array).at_path('images')
    end
  end

  get '/brands/:id' do
    parameter :id, 'The brand id', required: true

    example 'Get details about a brand', document: [:admin] do
      do_request id: @brand.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Integer).at_path('brand/id')
      expect(response_body).to have_json_type(String).at_path('brand/name')
      expect(response_body).to have_json_type(String).at_path('brand/seo_slug')
      expect(response_body).to have_json_type(String).at_path('brand/description')
      expect(response_body).to have_json_type(String).at_path('brand/created_at')
      expect(response_body).to have_json_type(String).at_path('brand/updated_at')
      expect(response_body).to have_json_type(Array).at_path('images')
    end
  end

  post '/brands' do
    before { log_in_as_admin! }

    parameter :name, 'The name of the brand', scope: :brand, required: true, unique: true
    parameter :description, 'A description for the brand', scope: :brand
    parameter :seo_slug, 'The URL slug for the brand', scope: :brand, required: true, unique: true
    parameter :url, 'Brand URL', scope: :brand
    parameter :is_approved, 'Whether this brand has is approved', scope: :brand

    let(:name) { Faker::Lorem.sentence(3) }
    let(:description) { Faker::Lorem.paragraph }
    let(:seo_slug) { name.slugify }
    let(:url) { Faker::Internet.url }
    let(:is_approved) { true }

    example 'Create a new brand', document: [:admin] do
      do_request account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Integer).at_path('brand/id')
      expect(response_body).to have_json_type(String).at_path('brand/name')
      expect(response_body).to have_json_type(String).at_path('brand/seo_slug')
      expect(response_body).to have_json_type(String).at_path('brand/description')
      expect(response_body).to have_json_type(Array).at_path('images')
    end
  end

  patch '/brands/:id' do
    before { log_in_as_admin! }

    parameter :name, 'The name of the brand', scope: :brand, required: true
    parameter :description, 'A description for the brand', scope: :brand
    parameter :seo_slug, 'The URL slug for the brand', scope: :brand
    parameter :url, 'Brand URL', scope: :brand
    parameter :is_approved, 'Whether this brand has is approved', scope: :brand

    let(:name) { Faker::Lorem.sentence(3) }
    let(:description) { Faker::Lorem.paragraph }
    let(:seo_slug) { name.slugify }
    let(:url) { Faker::Internet.url }
    let(:is_approved) { true }

    example 'Update an existing brand', document: [:admin] do
      do_request id: @brand.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Integer).at_path('brand/id')
      expect(response_body).to have_json_type(String).at_path('brand/name')
      expect(response_body).to have_json_type(String).at_path('brand/seo_slug')
      expect(response_body).to have_json_type(String).at_path('brand/description')
      expect(response_body).to have_json_type(Array).at_path('images')
    end
  end

  delete '/brands/:id' do
    before { log_in_as_admin! }

    parameter :id, 'The id of the brand to delete'

    example 'Delete a brand', document: [:admin] do
      do_request id: @brand.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(status).to be(204)
    end
  end
end
