resource 'SocialFeeds' do
  before(:all) do
    @social_feed = create(:social_feed)
    @mall = create(:mall, social_feeds: [@social_feed])
    log_in_as_client!(resource: @mall, traits: :mall_channel)
  end

  get '/social-feeds' do
    parameter :limit, 'How many records to include, default: 10'
    parameter :offset, 'How many records to skip, default: 0'
    parameter :since, 'Skip records which have not been updated before this date (format: YYYY-MM-DD)'

    example 'Get a list of all social network feeds', document: [:mall_channel] do
      do_request account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('social_feeds')
      expect(response_body).to have_json_type(Integer).at_path('social_feeds/0/id')
      expect(response_body).to have_json_type(String).at_path('social_feeds/0/name')
      expect(response_body).to have_json_type(Hash).at_path('social_feeds/0/properties')
      expect(response_body).to have_json_type(String).at_path('social_feeds/0/created_at')
      expect(response_body).to have_json_type(String).at_path('social_feeds/0/updated_at')
    end
  end

  get '/social-feeds/:id' do
    parameter :id, 'The id of the social feed to retrieve', required: true

    example 'Get a specific social network feed', document: [:mall_channel] do
      do_request id: @social_feed.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Integer).at_path('social_feed/id')
      expect(response_body).to have_json_type(String).at_path('social_feed/name')
      expect(response_body).to have_json_type(Hash).at_path('social_feed/properties')
      expect(response_body).to have_json_type(String).at_path('social_feed/created_at')
      expect(response_body).to have_json_type(String).at_path('social_feed/updated_at')
    end
  end

  post '/social-feeds' do
    before { log_in_as_admin! }

    parameter :social_network_id, 'The id of the social network', scope: :social_feed, required: true
    parameter :sociable_type, 'The name of the model to which this feed is associated', scope: :social_feed
    parameter :sociable_id, 'The id of the record to which this feed is associated', scope: :social_feed
    parameter :properties, 'A hash of the URLs or appids or whatnot of the feed/social network', scope: :social_feed, required: true
    parameter :is_active, 'Whether the feed is active', scope: :social_feed
    parameter :feed_key, 'The key for the feed', scope: :social_feed

    let(:social_network_id) { create(:social_network).id }
    let(:sociable_type) { ['Retailer', 'Mall', 'Store'].sample }
    let(:sociable_id) { 1 }
    let(:properties) { { url: Faker::Internet.url }.to_json }
    let(:is_active) { true }

    example 'Create a new social feed', document: [:admin] do
      do_request account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Integer).at_path('social_feed/id')
      expect(response_body).to have_json_type(String).at_path('social_feed/name')
      expect(response_body).to have_json_type(Hash).at_path('social_feed/properties')
    end
  end

  patch '/social-feeds/:id' do
    before do
      log_in_as_admin!
      @social_feed = create(:social_feed)
    end

    parameter :social_network_id, 'The id of the social network', scope: :social_feed
    parameter :sociable_type, 'The name of the model to which this feed is associated', scope: :social_feed
    parameter :sociable_id, 'The id of the record to which this feed is associated', scope: :social_feed
    parameter :url, 'The URL to the feed', scope: :social_feed
    parameter :is_active, 'Whether the feed is active', scope: :social_feed
    parameter :feed_key, 'The key for the feed', scope: :social_feed

    let(:social_network_id) { create(:social_network).id }
    let(:sociable_type) { ['Retailer', 'Mall', 'Store'].sample }
    let(:sociable_id) { 1 }
    let(:url) { Faker::Internet.url }
    let(:is_active) { true }
    let(:feed_key) { 'x8D3gkK92ct_3gaH.2+as3Fe' }

    example 'Update an existing social feed', document: [:admin] do
      do_request id: @social_feed.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Integer).at_path('social_feed/id')
      expect(response_body).to have_json_type(String).at_path('social_feed/name')
      expect(response_body).to have_json_type(Hash).at_path('social_feed/properties')
    end
  end

  delete '/social-feeds/:id' do
    before do
      log_in_as_admin!
      @social_feed = create(:social_feed)
    end

    parameter :id, 'The id of the social feed to delete', required: true

    example 'Delete a social feed', document: [:admin] do
      do_request id: @social_feed.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(status).to be(204)
    end
  end
end
