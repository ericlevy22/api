resource 'Monitor', type: :feature do
  get '/status' do
    example 'Get status of the API' do
      do_request
      expect(response_body).to have_json_type(String).at_path('status')
    end
  end
end
