resource 'Groups', :feature do
  before(:all) do
    @group = create :group
    log_in_as_client!
  end

  post '/groups' do
    before { log_in_as_admin! }

    parameter :name, scope: :group, required: true

    let(:name) { Faker::Lorem.sentence(2) }

    example 'Create a new group', document: [:admin] do
      do_request account_id: @account.id, account_token: @token, timestamp: @timestamp,
                        group: { name: name }

      expect(response_body).to have_json_type(Integer).at_path('group/id')
      expect(response_body).to have_json_type(String).at_path('group/name')
    end
  end

  get '/groups' do
    parameter :limit, 'How many records to include, default: 10'
    parameter :offset, 'How many records to skip, default: 0'
    parameter :since, 'Skip records which have not been updated before this date (format: YYYY-MM-DD)'

    example 'Get a list of groups', document: [:admin] do
      do_request account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('groups')
      expect(response_body).to have_json_type(Integer).at_path('groups/0/id')
      expect(response_body).to have_json_type(String).at_path('groups/0/name')
    end
  end

  get '/groups/:id' do
    parameter :id, 'The group id', required: true

    example 'Get details for a specific group', document: [:admin] do
      do_request id: @group.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Integer).at_path('group/id')
      expect(response_body).to have_json_type(String).at_path('group/name')
    end
  end
end
