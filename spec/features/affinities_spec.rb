resource 'Affinities', type: :feature do
  before do
    log_in_as_client!
    @affinity = create :affinity, user: @account.user
  end

  get '/affinities' do
    parameter :limit, 'Maximum number of affinities to return'
    parameter :offset, 'Row offset'

    example 'Get a list of affinities', document: [:admin] do
      do_request account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('affinities')
      expect(response_body).to have_json_type(Integer).at_path('affinities/0/id')
      expect(response_body).to have_json_type(Integer).at_path('affinities/0/heartable_id')
      expect(response_body).to have_json_type(String).at_path('affinities/0/heartable_type')
      expect(response_body).to have_json_type(String).at_path('affinities/0/created_at')
      expect(response_body).to have_json_type(String).at_path('affinities/0/updated_at')
    end
  end

  get '/affinities/:id' do
    parameter :id, 'The id of the affinity to retrieve', required: true

    example 'Get details for an affinity', document: [:admin] do
      do_request id: @affinity.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Integer).at_path('affinity/id')
      expect(response_body).to have_json_type(Integer).at_path('affinity/heartable_id')
      expect(response_body).to have_json_type(String).at_path('affinity/heartable_type')
      expect(response_body).to have_json_type(String).at_path('affinity/created_at')
      expect(response_body).to have_json_type(String).at_path('affinity/updated_at')
    end
  end

  post '/affinities' do
    parameter :heartable_type, 'The name of the resource type for the affinity being created', required: true, scope: :affinity
    parameter :heartable_id, 'The id of a resource type for the affinity being created', required: true, scope: :affinity

    let(:heartable_type) { 'Deal' }
    let(:heartable_id) { 1 }

    example 'Create a new affinity', document: [:admin] do
      do_request affinity: { heartable_type: heartable_type, heartable_id: heartable_id },
                 account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Integer).at_path('affinity/id')
      expect(response_body).to have_json_type(Integer).at_path('affinity/heartable_id')
      expect(response_body).to have_json_type(String).at_path('affinity/heartable_type')
    end
  end

  delete '/affinities/:id' do
    parameter :id, 'The id of the affinity to be deleted', required: true

    example 'Delete an affinity', document: [:admin] do
      do_request id: @affinity.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(status).to be(204)
    end
  end
end
