resource 'Deals', type: :feature do
  before(:all) do
    @deal = create(:deal)
    @mall = create(:mall, stores: [create(:store, deals: [@deal])])
    log_in_as_client!(resource: @mall, traits: [:deal_channel, :store_channel])
    @account.add_role :deal
  end

  get '/deals/search' do
    let(:q) { @deal.title.split(/\s/).sample }

    before { allow(Deal).to receive(:by_keyword) { [@deal] } }

    parameter :q, 'The query string to search for', required: true
    parameter :limit, 'How many records to include, default: 10'
    parameter :offset, 'How many records to skip, default: 0'
    parameter :since, 'Skip records which have not been updated before this date (format: YYYY-MM-DD)'
    parameter :tags, 'Filter by a comma-separated list of tags'
    parameter :strict, 'Only match records which include all provided tags'

    example 'Search deals by keyword', document: [:deal_channel] do
      do_request q: q, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('deals')
      expect(response_body).to have_json_type(Integer).at_path('deals/0/id')
      expect(response_body).to have_json_type(Integer).at_path('deals/0/retailer_id')
      expect(response_body).to have_json_type(String).at_path('deals/0/title')
      expect(response_body).to have_json_type(String).at_path('deals/0/seo_slug')
      expect(response_body).to have_json_type(String).at_path('deals/0/name')
      expect(response_body).to have_json_type(String).at_path('deals/0/sort_name')
      expect(response_body).to have_json_type(String).at_path('deals/0/retailer_name')
      expect(response_body).to have_json_type(String).at_path('deals/0/retailer_sort_name')
      expect(response_body).to have_json_type(String).at_path('deals/0/description')
      expect(response_body).to have_json_type(:boolean).at_path('deals/0/is_local')
      expect(response_body).to have_json_type(:boolean).at_path('deals/0/is_featured')
      expect(response_body).to have_json_type(String).at_path('deals/0/created_at')
      expect(response_body).to have_json_type(String).at_path('deals/0/updated_at')
      expect(response_body).to have_json_type(Array).at_path('images')
      expect(response_body).to have_json_type(Array).at_path('tags')
    end
  end

  get '/deals/:id/stores' do
    before do
      @deal.stores.last.location.calculate_distance(33.333, 45.678)
    end

    parameter :id, 'The id of the deal to find stores for', required: true
    parameter :limit, 'How many records to include, default: 10'
    parameter :offset, 'How many records to skip, default: 0'

    example 'Get a list of the stores where a deal is available', document: [:deal_channel] do
      do_request id: @deal.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('stores')
      expect(response_body).to have_json_type(Integer).at_path('stores/0/id')
      expect(response_body).to have_json_type(String).at_path('stores/0/name')
      expect(response_body).to have_json_type(String).at_path('stores/0/sort_name')
      expect(response_body).to have_json_type(String).at_path('stores/0/seo_slug')
      expect(response_body).to have_json_type(String).at_path('stores/0/description')
      expect(response_body).to have_json_type(String).at_path('stores/0/address')
      expect(response_body).to have_json_type(String).at_path('stores/0/city')
      expect(response_body).to have_json_type(String).at_path('stores/0/state')
      expect(response_body).to have_json_type(String).at_path('stores/0/zip')
      expect(response_body).to have_json_type(String).at_path('stores/0/phone')
      expect(response_body).to have_json_type(Integer).at_path('stores/0/mall_id')
      expect(response_body).to have_json_type(Integer).at_path('stores/0/retailer_id')
      expect(response_body).to have_json_type(Float).at_path('stores/0/latitude')
      expect(response_body).to have_json_type(Float).at_path('stores/0/longitude')
      expect(response_body).to have_json_type(String).at_path('stores/0/created_at')
      expect(response_body).to have_json_type(String).at_path('stores/0/updated_at')
      expect(response_body).to have_json_type(Array).at_path('images')
      expect(response_body).to have_json_type(Array).at_path('keywords')
    end
  end

  get '/deals' do
    parameter :limit, 'How many records to include, default: 10'
    parameter :offset, 'How many records to skip, default: 0'
    parameter :since, 'Skip records which have not been updated before this date (format: YYYY-MM-DD)'
    parameter :tags, 'Filter by a comma-separated list of tags'
    parameter :strict, 'Only match records which include all provided tags'
    parameter :q, 'Term for searching deals by name'

    example 'Get a list of deals', document: [:deal_channel] do
      do_request account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('deals')
      expect(response_body).to have_json_type(Integer).at_path('deals/0/id')
      expect(response_body).to have_json_type(Integer).at_path('deals/0/retailer_id')
      expect(response_body).to have_json_type(String).at_path('deals/0/title')
      expect(response_body).to have_json_type(String).at_path('deals/0/seo_slug')
      expect(response_body).to have_json_type(String).at_path('deals/0/name')
      expect(response_body).to have_json_type(String).at_path('deals/0/sort_name')
      expect(response_body).to have_json_type(String).at_path('deals/0/retailer_name')
      expect(response_body).to have_json_type(String).at_path('deals/0/retailer_sort_name')
      expect(response_body).to have_json_type(String).at_path('deals/0/description')
      expect(response_body).to have_json_type(:boolean).at_path('deals/0/is_local')
      expect(response_body).to have_json_type(:boolean).at_path('deals/0/is_featured')
      expect(response_body).to have_json_type(String).at_path('deals/0/created_at')
      expect(response_body).to have_json_type(String).at_path('deals/0/updated_at')
      expect(response_body).to have_json_type(Array).at_path('images')
      expect(response_body).to have_json_type(Array).at_path('tags')
    end
  end

  get '/deals/:id' do
    parameter :id, 'The deal id', required: true

    example 'Get details about a deal', document: [:deal_channel] do
      do_request id: @deal.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Integer).at_path('deal/id')
      expect(response_body).to have_json_type(Integer).at_path('deal/retailer_id')
      expect(response_body).to have_json_type(String).at_path('deal/title')
      expect(response_body).to have_json_type(String).at_path('deal/seo_slug')
      expect(response_body).to have_json_type(String).at_path('deal/name')
      expect(response_body).to have_json_type(String).at_path('deal/sort_name')
      expect(response_body).to have_json_type(String).at_path('deal/retailer_name')
      expect(response_body).to have_json_type(String).at_path('deal/retailer_sort_name')
      expect(response_body).to have_json_type(String).at_path('deal/description')
      expect(response_body).to have_json_type(:boolean).at_path('deal/is_local')
      expect(response_body).to have_json_type(:boolean).at_path('deal/is_featured')
      expect(response_body).to have_json_type(String).at_path('deal/created_at')
      expect(response_body).to have_json_type(String).at_path('deal/updated_at')
      expect(response_body).to have_json_type(Array).at_path('images')
      expect(response_body).to have_json_type(Array).at_path('tags')
    end
  end

  post '/deals' do
    before { log_in_as_admin! }

    parameter :retailer_id, 'The id of the Retailer for this deal', required: true, scope: :deal
    parameter :sales_type, 'The type of deal this is (in-store event, New Arrivals, etc)', scope: :deal
    parameter :title, 'The title', required: true, limit: 100, unique: true, scope: :deal
    parameter :seo_slug, 'The SEO slug for representing the deal in URLs', scope: :deal, required: true, limit: 250
    parameter :external_url, 'A URL to an external resource', scope: :deal, limit: 250
    parameter :display_at, 'The date to begin displaying the deal', scope: :deal
    parameter :start_at, 'The date when the deal becomes valid', required: true, scope: :deal
    parameter :end_at, 'The date when the deal expires', required: true, scope: :deal
    parameter :description, 'The full text description', required: true, scope: :deal
    parameter :fine_print_description, 'The fine print', scope: :deal
    # :is_local
    parameter :is_featured, 'Whether the deal is currently featured', scope: :deal
    parameter :is_active, 'Whether the deal is currently active', scope: :deal

    let(:retailer_id) { (create :retailer).id }
    let(:sales_type) { 'Sales and Promos' }
    let(:title) { Faker::Lorem.sentence }
    let(:seo_slug) { title.slugify }
    let(:external_url) { Faker::Internet.url }
    let(:display_at) { 1.week.ago }
    let(:start_at) { 1.day.ago }
    let(:end_at) { 1.week.from_now }
    let(:description) { Faker::Lorem.sentence }
    let(:fine_print_description) { Faker::Lorem.sentence }
    # :is_local
    let(:is_featured) { false }
    let(:is_active) { true }

    example 'Create a new deal', document: [:admin] do
      do_request account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Integer).at_path('deal/id')
      expect(response_body).to have_json_type(Integer).at_path('deal/retailer_id')
      expect(response_body).to have_json_type(String).at_path('deal/title')
      expect(response_body).to have_json_type(String).at_path('deal/seo_slug')
      expect(response_body).to have_json_type(String).at_path('deal/name')
      expect(response_body).to have_json_type(String).at_path('deal/sort_name')
      expect(response_body).to have_json_type(String).at_path('deal/retailer_name')
      expect(response_body).to have_json_type(String).at_path('deal/retailer_sort_name')
      expect(response_body).to have_json_type(String).at_path('deal/description')
      expect(response_body).to have_json_type(:boolean).at_path('deal/is_local')
      expect(response_body).to have_json_type(:boolean).at_path('deal/is_featured')
      expect(response_body).to have_json_type(Array).at_path('images')
      expect(response_body).to have_json_type(Array).at_path('tags')
    end
  end

  patch '/deals/:id' do
    before { log_in_as_admin! }

    parameter :id, 'The id of the deal to update', required: true
    parameter :retailer_id, 'The id of the Retailer for this deal', scope: :deal
    parameter :sales_type, 'The type of deal this is', scope: :deal
    parameter :title, 'The title', scope: :deal, limit: 100
    parameter :seo_slug, 'The SEO slug for representing the deal in URLs', scope: :deal, limit: 250
    parameter :external_url, 'A URL to an external resource', scope: :deal, limit: 250
    parameter :display_at, 'The date to begin displaying the deal', scope: :deal
    parameter :start_at, 'The date when the deal becomes valid', scope: :deal
    parameter :end_at, 'The date when the deal expires', scope: :deal
    parameter :description, 'The full text description', scope: :deal
    parameter :fine_print_description, 'The fine print', scope: :deal
    parameter :is_featured, 'Whether the deal is currently featured', scope: :deal
    parameter :is_active, 'Whether the deal is currently active', scope: :deal

    let(:retailer_id) { (create :retailer).id }
    let(:sales_type) { 'Sales and Promos' }
    let(:title) { Faker::Lorem.sentence }
    let(:external_url) { Faker::Internet.url }
    let(:display_at) { 1.week.ago }
    let(:start_at) { 1.day.ago }
    let(:end_at) { 1.week.from_now }
    let(:description) { Faker::Lorem.sentence }
    let(:fine_print_description) { Faker::Lorem.sentence }
    let(:is_featured) { false }
    let(:is_active) { true }

    example 'Update an existing deal', document: [:admin] do
      do_request id: @deal.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Integer).at_path('deal/id')
      expect(response_body).to have_json_type(Integer).at_path('deal/retailer_id')
      expect(response_body).to have_json_type(String).at_path('deal/title')
      expect(response_body).to have_json_type(String).at_path('deal/seo_slug')
      expect(response_body).to have_json_type(String).at_path('deal/name')
      expect(response_body).to have_json_type(String).at_path('deal/sort_name')
      expect(response_body).to have_json_type(String).at_path('deal/retailer_name')
      expect(response_body).to have_json_type(String).at_path('deal/retailer_sort_name')
      expect(response_body).to have_json_type(String).at_path('deal/description')
      expect(response_body).to have_json_type(:boolean).at_path('deal/is_local')
      expect(response_body).to have_json_type(:boolean).at_path('deal/is_featured')
      expect(response_body).to have_json_type(Array).at_path('images')
      expect(response_body).to have_json_type(Array).at_path('tags')
    end
  end

  delete '/deals/:id' do
    before { log_in_as_admin! }

    parameter :id, 'The id of the deal to delete', required: true

    example 'Delete an existing deal', document: [:admin] do
      do_request id: @deal.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(status).to eq(204)
    end
  end
end
