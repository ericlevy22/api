resource 'Retailers', type: :feature do
  before(:all) do
    @retailer = create(:retailer)
    @store = create(:store, retailer: @retailer)
    @mall = create(:mall, stores: [@store])
    log_in_as_client!(resource: @mall, traits: [:store_channel, :deal_channel])
  end

  get '/retailers' do
    parameter :limit, 'How many records to include, default: 10'
    parameter :offset, 'How many records to skip, default: 0'
    parameter :since, 'Skip records which have not been updated before this date (format: YYYY-MM-DD)'
    parameter :q, 'Term for searching retailers by name'

    example 'Get a list of retailers', document: [:store_channel] do
      do_request account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('retailers')
      expect(response_body).to have_json_type(Integer).at_path('retailers/0/id')
      expect(response_body).to have_json_type(String).at_path('retailers/0/name')
      expect(response_body).to have_json_type(String).at_path('retailers/0/sort_name')
      expect(response_body).to have_json_type(String).at_path('retailers/0/seo_slug')
      expect(response_body).to have_json_type(String).at_path('retailers/0/description')
      expect(response_body).to have_json_type(String).at_path('retailers/0/nick_name')
      expect(response_body).to have_json_type(String).at_path('retailers/0/email')
      expect(response_body).to have_json_type(String).at_path('retailers/0/phone')
      expect(response_body).to have_json_type(String).at_path('retailers/0/url')
      expect(response_body).to have_json_type(String).at_path('retailers/0/url_text')
      expect(response_body).to have_json_type(String).at_path('retailers/0/store_text')
      expect(response_body).to have_json_type(String).at_path('retailers/0/created_at')
      expect(response_body).to have_json_type(String).at_path('retailers/0/updated_at')
      expect(response_body).to have_json_type(Array).at_path('images')
      expect(response_body).to have_json_type(Array).at_path('keywords')
    end
  end

  get '/retailers/:id' do
    parameter :id, 'The id of the retailer to retrieve', required: true

    example 'Get a specific retailer', document: [:store_channel] do
      do_request id: @retailer.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Integer).at_path('retailer/id')
      expect(response_body).to have_json_type(String).at_path('retailer/name')
      expect(response_body).to have_json_type(String).at_path('retailer/sort_name')
      expect(response_body).to have_json_type(String).at_path('retailer/seo_slug')
      expect(response_body).to have_json_type(String).at_path('retailer/description')
      expect(response_body).to have_json_type(String).at_path('retailer/nick_name')
      expect(response_body).to have_json_type(String).at_path('retailer/email')
      expect(response_body).to have_json_type(String).at_path('retailer/phone')
      expect(response_body).to have_json_type(String).at_path('retailer/url')
      expect(response_body).to have_json_type(String).at_path('retailer/url_text')
      expect(response_body).to have_json_type(String).at_path('retailer/store_text')
      expect(response_body).to have_json_type(String).at_path('retailer/created_at')
      expect(response_body).to have_json_type(String).at_path('retailer/updated_at')
      expect(response_body).to have_json_type(Array).at_path('images')
      expect(response_body).to have_json_type(Array).at_path('keywords')
    end
  end

  get '/retailers/:id/stores' do
    before do
      @store = create(:store, retailer_id: @retailer.id)
      @store.location.calculate_distance(12.34567, 98.7654)
    end

    parameter :id, 'The id of the retailer for which to retrieve stores', required: true
    parameter :limit, 'How many records to include, default: 10'
    parameter :offset, 'How many records to skip, default: 0'

    example 'Get a list of stores for a specific retailer', document: [:store_channel] do
      do_request id: @retailer.id, lat: 33.333, long: -104.567,
                 account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('stores')
      expect(response_body).to have_json_type(Integer).at_path('stores/0/id')
      expect(response_body).to have_json_type(String).at_path('stores/0/name')
      expect(response_body).to have_json_type(String).at_path('stores/0/sort_name')
      expect(response_body).to have_json_type(String).at_path('stores/0/seo_slug')
      expect(response_body).to have_json_type(String).at_path('stores/0/description')
      expect(response_body).to have_json_type(String).at_path('stores/0/description')
      expect(response_body).to have_json_type(String).at_path('stores/0/address')
      expect(response_body).to have_json_type(String).at_path('stores/0/city')
      expect(response_body).to have_json_type(String).at_path('stores/0/state')
      expect(response_body).to have_json_type(String).at_path('stores/0/zip')
      expect(response_body).to have_json_type(String).at_path('stores/0/phone')
      expect(response_body).to have_json_type(Integer).at_path('stores/0/mall_id')
      expect(response_body).to have_json_type(Integer).at_path('stores/0/retailer_id')
      expect(response_body).to have_json_type(Float).at_path('stores/0/distance')
      expect(response_body).to have_json_type(Float).at_path('stores/0/latitude')
      expect(response_body).to have_json_type(Float).at_path('stores/0/longitude')
      expect(response_body).to have_json_type(String).at_path('stores/0/created_at')
      expect(response_body).to have_json_type(String).at_path('stores/0/updated_at')
      expect(response_body).to have_json_type(Array).at_path('images')
      expect(response_body).to have_json_type(Array).at_path('keywords')
    end
  end

  get '/retailers/:id/deals' do
    before { @store.deals << create(:deal, retailer: @retailer) }

    parameter :id, 'The id of the retailer for which to retrieve deals', required: true
    parameter :limit, 'How many records to include, default: 10'
    parameter :offset, 'How many records to skip, default: 0'

    example 'Get a list of deals for a specific retailer', document: [:admin] do
      do_request id: @retailer.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('deals')
      expect(response_body).to have_json_type(Integer).at_path('deals/0/id')
      expect(response_body).to have_json_type(Integer).at_path('deals/0/retailer_id')
      expect(response_body).to have_json_type(String).at_path('deals/0/title')
      expect(response_body).to have_json_type(String).at_path('deals/0/seo_slug')
      expect(response_body).to have_json_type(String).at_path('deals/0/name')
      expect(response_body).to have_json_type(String).at_path('deals/0/sort_name')
      expect(response_body).to have_json_type(String).at_path('deals/0/description')
      expect(response_body).to have_json_type(:boolean).at_path('deals/0/is_local')
      expect(response_body).to have_json_type(:boolean).at_path('deals/0/is_featured')
      expect(response_body).to have_json_type(String).at_path('deals/0/created_at')
      expect(response_body).to have_json_type(String).at_path('deals/0/updated_at')
      expect(response_body).to have_json_type(Array).at_path('images')
      expect(response_body).to have_json_type(Array).at_path('tags')
    end
  end

  get '/retailers/:id/social-feeds' do
    before { @retailer.social_feeds << create(:social_feed) }

    parameter :id, 'The id of the retailer for which to retrieve social network feeds', required: true
    parameter :limit, 'How many records to include, default: 10'
    parameter :offset, 'How many records to skip, default: 0'

    example 'Get a list of social network feeds for a specific retailer', document: [:store_channel] do
      do_request id: @retailer.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('social_feeds')
      expect(response_body).to have_json_type(Integer).at_path('social_feeds/0/id')
      expect(response_body).to have_json_type(String).at_path('social_feeds/0/name')
      expect(response_body).to have_json_type(Hash).at_path('social_feeds/0/properties')
      expect(response_body).to have_json_type(String).at_path('social_feeds/0/created_at')
      expect(response_body).to have_json_type(String).at_path('social_feeds/0/updated_at')
    end
  end

  get '/retailers/:id/contacts' do
    parameter :id, '(Deprecated) The id of the retailer for which to retrieve contact information', required: true
    parameter :limit, 'How many records to include, default: 10'
    parameter :offset, 'How many records to skip, default: 0'

    example 'Get a list of contacts for a specific retailer', document: [:store_channel] do
      do_request id: @retailer.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(status).to be(200)
    end
  end

  post '/retailers' do
    before { log_in_as_admin! }

    parameter :name, 'The name of the retailer', scope: :retailer
    parameter :sort_name, 'A version of the retailer name to be used when sorting many retailers.', scope: :retailer, required: true
    parameter :seo_slug, 'The URL slug for the retailer', scope: :retailer
    parameter :description, 'A short description of the retailer as might be used in a listing of many retailers', scope: :retailer, limit: 3000
    parameter :nick_name, 'The nick name for the retailer', scope: :retailer
    parameter :phone, 'The primary phone number for the retailer', scope: :retailer
    parameter :email, 'The primary email address for the retailer', scope: :retailer
    parameter :url, 'The external URL for the main corporate site of the retailer', scope: :retailer, limit: 300
    parameter :url_text, 'The text that should be displayed when building the external links for the retailer', scope: :retailer, limit: 300
    parameter :store_text, 'The description of the retailer', scope: :retailer, limit: 2000
    parameter :is_active, 'Whether the retailer is active', scope: :retailer

    let(:name) { Faker::Lorem.sentence(3) }
    let(:sort_name) { name }
    let(:seo_slug) { name.slugify }
    let(:description) { Faker::Lorem.sentence }
    let(:nick_name) { name }
    let(:phone) { Faker::PhoneNumber.phone_number }
    let(:email) { Faker::Internet.email }
    let(:url) { Faker::Internet.url }
    let(:url_text) { Faker::Internet.url }
    let(:store_text) { Faker::Lorem.sentence }
    let(:is_active) { true }

    example 'Create a new retailer', document: [:admin] do
      do_request account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Integer).at_path('retailer/id')
      expect(response_body).to have_json_type(String).at_path('retailer/name')
      expect(response_body).to have_json_type(String).at_path('retailer/sort_name')
      expect(response_body).to have_json_type(String).at_path('retailer/seo_slug')
      expect(response_body).to have_json_type(String).at_path('retailer/description')
      expect(response_body).to have_json_type(String).at_path('retailer/nick_name')
      expect(response_body).to have_json_type(String).at_path('retailer/email')
      expect(response_body).to have_json_type(String).at_path('retailer/phone')
      expect(response_body).to have_json_type(String).at_path('retailer/url')
      expect(response_body).to have_json_type(String).at_path('retailer/url_text')
      expect(response_body).to have_json_type(String).at_path('retailer/store_text')
      expect(response_body).to have_json_type(Array).at_path('images')
      expect(response_body).to have_json_type(Array).at_path('keywords')
    end
  end

  patch '/retailers/:id' do
    before { log_in_as_admin! }

    parameter :id, 'The id of the retailer to update', require: true
    parameter :name, 'The name of the retailer', scope: :retailer
    parameter :sort_name, 'A version of the retailer name to be used when sorting many retailers.', scope: :retailer
    parameter :seo_slug, 'The URL slug for the retailer', scope: :retailer
    parameter :description, 'A short description of the retailer as might be used in a listing of many retailers', scope: :retailer, limit: 3000
    parameter :nick_name, 'The nick name for the retailer', scope: :retailer
    parameter :phone, 'The primary phone number for the retailer', scope: :retailer
    parameter :email, 'The primary email address for the retailer', scope: :retailer
    parameter :url, 'The external URL for the main corporate site of the retailer', scope: :retailer, limit: 300
    parameter :url_text, 'The text that should be displayed when building the external links for the retailer', scope: :retailer, limit: 300
    parameter :store_text, 'The description of the retailer', scope: :retailer, limit: 2000
    parameter :is_active, 'Whether the retailer is active', scope: :retailer

    let(:name) { Faker::Lorem.sentence(3) }
    let(:sort_name) { name }
    let(:seo_slug) { name.slugify }
    let(:description) { Faker::Lorem.sentence }
    let(:nick_name) { name }
    let(:phone) { Faker::PhoneNumber.phone_number }
    let(:email) { Faker::Internet.email }
    let(:url) { Faker::Internet.url }
    let(:url_text) { Faker::Internet.url }
    let(:store_text) { Faker::Lorem.sentence }
    let(:is_active) { true }

    example 'Update an existing retailer', document: [:admin] do
      do_request id: @retailer.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Integer).at_path('retailer/id')
      expect(response_body).to have_json_type(String).at_path('retailer/name')
      expect(response_body).to have_json_type(String).at_path('retailer/sort_name')
      expect(response_body).to have_json_type(String).at_path('retailer/seo_slug')
      expect(response_body).to have_json_type(String).at_path('retailer/description')
      expect(response_body).to have_json_type(String).at_path('retailer/nick_name')
      expect(response_body).to have_json_type(String).at_path('retailer/email')
      expect(response_body).to have_json_type(String).at_path('retailer/phone')
      expect(response_body).to have_json_type(String).at_path('retailer/url')
      expect(response_body).to have_json_type(String).at_path('retailer/store_text')
      expect(response_body).to have_json_type(Array).at_path('images')
      expect(response_body).to have_json_type(Array).at_path('keywords')
    end
  end

  delete '/retailers/:id' do
    before { log_in_as_admin! }

    parameter :id, 'The id of the retailer to delete', required: true

    example 'Delete an existing', document: [:admin] do
      do_request id: @retailer.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(status).to be(204)
    end
  end
end
