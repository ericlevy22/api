resource 'Companies', type: :feature do
  before(:all) do
    @company = create :company
    log_in_as_client!(resource: @company)
  end

  get '/companies' do
    before { allow(Company).to receive_message_chain(:limit, :offset) { [@company] } }

    parameter :limit, 'How many records to include, default: 10'
    parameter :offset, 'How many records to skip, default: 0'
    parameter :since, 'Skip records which have not been updated before this date (format: YYYY-MM-DD)'

    example 'Get a list of companies', document: [:admin] do
      do_request account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('companies')
      expect(response_body).to have_json_type(Integer).at_path('companies/0/id')
      expect(response_body).to have_json_type(String).at_path('companies/0/name')
      expect(response_body).to have_json_type(String).at_path('companies/0/address')
      expect(response_body).to have_json_type(String).at_path('companies/0/city')
      expect(response_body).to have_json_type(String).at_path('companies/0/state')
      expect(response_body).to have_json_type(String).at_path('companies/0/zip')
      expect(response_body).to have_json_type(String).at_path('companies/0/phone')
      expect(response_body).to have_json_type(String).at_path('companies/0/fax')
      expect(response_body).to have_json_type(String).at_path('companies/0/url')
      expect(response_body).to have_json_type(String).at_path('companies/0/created_at')
      expect(response_body).to have_json_type(String).at_path('companies/0/updated_at')
    end
  end

  get '/companies/:id' do
    before { allow(Company).to receive(:find) { @company } }

    parameter :id, 'The id of the company to retrieve', required: true

    example 'Get a specific company', document: [:admin] do
      do_request id: @company.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Integer).at_path('company/id')
      expect(response_body).to have_json_type(String).at_path('company/name')
      expect(response_body).to have_json_type(String).at_path('company/address')
      expect(response_body).to have_json_type(String).at_path('company/city')
      expect(response_body).to have_json_type(String).at_path('company/state')
      expect(response_body).to have_json_type(String).at_path('company/zip')
      expect(response_body).to have_json_type(String).at_path('company/phone')
      expect(response_body).to have_json_type(String).at_path('company/fax')
      expect(response_body).to have_json_type(String).at_path('company/url')
      expect(response_body).to have_json_type(String).at_path('company/created_at')
      expect(response_body).to have_json_type(String).at_path('company/updated_at')
    end
  end

  get '/companies/:id/contacts' do
    before { @company.contacts << create(:contact) }

    parameter :id, 'The id of the company for which to retrieve contact information', required: true
    parameter :limit, 'How many records to include, default: 10'
    parameter :offset, 'How many records to skip, default: 0'

    example 'Get a list of contacts for a specific company', document: [:admin] do
      do_request id: @company.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('contacts')
      expect(response_body).to have_json_type(Integer).at_path('contacts/0/id')
      expect(response_body).to have_json_type(String).at_path('contacts/0/name')
      expect(response_body).to have_json_type(String).at_path('contacts/0/title')
      expect(response_body).to have_json_type(String).at_path('contacts/0/email')
      expect(response_body).to have_json_type(String).at_path('contacts/0/phone')
      expect(response_body).to have_json_type(String).at_path('contacts/0/created_at')
      expect(response_body).to have_json_type(String).at_path('contacts/0/updated_at')
    end
  end

  get '/companies/:id/malls' do
    before { @company.malls << (create :mall) }

    parameter :id, 'The id of the company for which to retrieve malls', required: true
    parameter :limit, 'How many records to include, default: 10'
    parameter :offset, 'How many records to skip, default: 0'

    example 'Get a list of malls associated with a particular management company', document: [:admin] do
      do_request id: @company.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('malls')
      expect(response_body).to have_json_type(Integer).at_path('malls/0/id')
      expect(response_body).to have_json_type(String).at_path('malls/0/name')
      expect(response_body).to have_json_type(String).at_path('malls/0/sort_name')
      expect(response_body).to have_json_type(String).at_path('malls/0/seo_slug')
      expect(response_body).to have_json_type(String).at_path('malls/0/nick_name')
      expect(response_body).to have_json_type(String).at_path('malls/0/address')
      expect(response_body).to have_json_type(String).at_path('malls/0/city')
      expect(response_body).to have_json_type(String).at_path('malls/0/state')
      expect(response_body).to have_json_type(String).at_path('malls/0/zip')
      expect(response_body).to have_json_type(String).at_path('malls/0/phone')
      expect(response_body).to have_json_type(Float).at_path('malls/0/latitude')
      expect(response_body).to have_json_type(Float).at_path('malls/0/longitude')
      expect(response_body).to have_json_type(String).at_path('malls/0/url')
      expect(response_body).to have_json_type(String).at_path('malls/0/created_at')
      expect(response_body).to have_json_type(String).at_path('malls/0/updated_at')
      expect(response_body).to have_json_type(Array).at_path('images')
    end
  end

  post '/companies' do
    before { log_in_as_admin! }

    parameter :name, 'The name of the company', scope: :company
    parameter :nick_name, 'The nickname of the company', scope: :company
    parameter :seo_slug, 'The URL segment', scope: :company, required: true
    parameter :address, 'The address', scope: :company
    parameter :city, 'The city', scope: :company
    parameter :state, 'The state', scope: :company, limit: 2
    parameter :zip, 'The ZIP code', scope: :company
    parameter :phone, 'The primary phone number', scope: :company
    parameter :fax, 'The fax number', scope: :company
    parameter :url, 'The primary URL for the company', scope: :company
    parameter :is_active, 'Whether the company is active', scope: :company

    let(:name) { Faker::Lorem.sentence(3) }
    let(:nick_name) { name }
    let(:seo_slug) { name.slugify }
    let(:address) { Faker::Address.street_address }
    let(:city) { Faker::Address.city }
    let(:state) { Faker::Address.state_abbr }
    let(:zip) { Faker::Address.zip_code }
    let(:phone) { Faker::PhoneNumber.phone_number }
    let(:fax) { Faker::PhoneNumber.phone_number }
    let(:url) { Faker::Internet.url }
    let(:is_active) { true }

    example 'Create a new company', document: [:admin] do
      do_request account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Integer).at_path('company/id')
      expect(response_body).to have_json_type(String).at_path('company/name')
      expect(response_body).to have_json_type(String).at_path('company/address')
      expect(response_body).to have_json_type(String).at_path('company/city')
      expect(response_body).to have_json_type(String).at_path('company/state')
      expect(response_body).to have_json_type(String).at_path('company/zip')
      expect(response_body).to have_json_type(String).at_path('company/phone')
      expect(response_body).to have_json_type(String).at_path('company/fax')
      expect(response_body).to have_json_type(String).at_path('company/url')
    end
  end

  patch '/companies/:id', document: [:admin] do
    before { log_in_as_admin! }

    parameter :name, 'The name of the company', scope: :company
    parameter :nick_name, 'The nickname of the company', scope: :company
    parameter :seo_slug, 'The URL segment', scope: :company
    parameter :address, 'The address', scope: :company
    parameter :city, 'The city', scope: :company
    parameter :state, 'The state', scope: :company, limit: 2
    parameter :zip, 'The ZIP code', scope: :company
    parameter :phone, 'The primary phone number', scope: :company
    parameter :fax, 'The fax number', scope: :company
    parameter :url, 'The primary URL for the company', scope: :company
    parameter :is_active, 'Whether the company is active', scope: :company

    let(:name) { Faker::Lorem.sentence(3) }
    let(:nick_name) { name }
    let(:seo_slug) { name.slugify }
    let(:address) { Faker::Address.street_address }
    let(:city) { Faker::Address.city }
    let(:state) { Faker::Address.state_abbr }
    let(:zip) { Faker::Address.zip_code }
    let(:phone) { Faker::PhoneNumber.phone_number }
    let(:fax) { Faker::PhoneNumber.phone_number }
    let(:url) { Faker::Internet.url }
    let(:is_active) { true }

    example 'Update an existing company' do
      do_request id: @company.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Integer).at_path('company/id')
      expect(response_body).to have_json_type(String).at_path('company/name')
      expect(response_body).to have_json_type(String).at_path('company/address')
      expect(response_body).to have_json_type(String).at_path('company/city')
      expect(response_body).to have_json_type(String).at_path('company/state')
      expect(response_body).to have_json_type(String).at_path('company/zip')
      expect(response_body).to have_json_type(String).at_path('company/phone')
      expect(response_body).to have_json_type(String).at_path('company/fax')
      expect(response_body).to have_json_type(String).at_path('company/url')
    end
  end

  delete '/companies/:id', document: [:admin] do
    before { log_in_as_admin! }

    parameter :id, 'The id of the company to delete'

    example 'Delete a company' do
      do_request id: @company.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(status).to be(204)
    end
  end
end
