resource 'Locations', type: :feature, slow: true, sunspot: true do
  before(:all) do
    @location = create :location, distance: 1.2345
    log_in_as_client!(resource: @location)
    @store = create(:store)
    @store.deals << create(:deal)
    @store.products << create(:product)
    @location.stores << @store
    @location.malls << create(:mall)
  end

  before(:each) do
    solr_setup
    @location.index!
  end

  let!(:lat) { @location.latitude + 0.0001 }
  let!(:long) { @location.longitude + 0.0001 }
  let!(:radius) { 50 }

  get '/locations/search' do
    parameter :lat, 'The latitude to search', required: true
    parameter :long, 'The longitude to search', required: true
    parameter :radius, 'The radius to search within'
    parameter :limit, 'How many locations to include, default: 10'
    parameter :offset, 'How many locations to skip, default: 0'

    example 'Search a given region for points of interest', document: [:admin] do
      do_request lat: lat, long: long, radius: radius, account_id: @account.id,
                 account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('locations')
      expect(response_body).to have_json_type(Integer).at_path('locations/0/id')
      expect(response_body).to have_json_type(String).at_path('locations/0/time_zone')
      expect(response_body).to have_json_type(Float).at_path('locations/0/latitude')
      expect(response_body).to have_json_type(Float).at_path('locations/0/longitude')
      expect(response_body).to have_json_type(String).at_path('locations/0/created_at')
      expect(response_body).to have_json_type(String).at_path('locations/0/updated_at')
    end
  end

  get '/locations/:id' do
    parameter :id, 'The id of the location record to look up', required: true
    parameter :lat, 'The latitude for optional distance calculation'
    parameter :long, 'The longitude for optional distance calculation'

    example 'Get the details for a particular location', document: [:admin] do
      do_request id: @location.id, lat: lat, long: long, account_id: @account.id,
                 account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_path('location')
      expect(response_body).to have_json_type(Integer).at_path('location/id')
      expect(response_body).to have_json_type(String).at_path('location/time_zone')
      expect(response_body).to have_json_type(Float).at_path('location/latitude')
      expect(response_body).to have_json_type(Float).at_path('location/longitude')
      expect(response_body).to have_json_type(Float).at_path('location/distance')
      expect(response_body).to have_json_type(String).at_path('location/created_at')
      expect(response_body).to have_json_type(String).at_path('location/updated_at')
    end
  end

  get '/locations/:id/stores' do
    parameter :id, 'The id of the location record to look up', required: true
    parameter :lat, 'The latitude for optional distance calculation'
    parameter :long, 'The longitude for optional distance calculation'

    example 'Get a list of stores in a given location', document: [:admin] do
      do_request id: @location.id, lat: lat, long: long, account_id: @account.id,
                 account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('stores')
      expect(response_body).to have_json_type(Integer).at_path('stores/0/id')
      expect(response_body).to have_json_type(String).at_path('stores/0/name')
      expect(response_body).to have_json_type(String).at_path('stores/0/seo_slug')
      expect(response_body).to have_json_type(String).at_path('stores/0/description')
      expect(response_body).to have_json_type(String).at_path('stores/0/address')
      expect(response_body).to have_json_type(String).at_path('stores/0/city')
      expect(response_body).to have_json_type(String).at_path('stores/0/state')
      expect(response_body).to have_json_type(String).at_path('stores/0/zip')
      expect(response_body).to have_json_type(String).at_path('stores/0/phone')
      expect(response_body).to have_json_type(String).at_path('stores/0/created_at')
      expect(response_body).to have_json_type(String).at_path('stores/0/updated_at')
    end
  end

  get '/locations/:id/malls' do
    parameter :id, 'The id of the location record to look up', required: true
    parameter :limit, 'How many malls to include, default: 10'
    parameter :offset, 'How many malls to skip, default: 0'
    parameter :lat, 'The latitude for optional distance calculation'
    parameter :long, 'The longitude for optional distance calculation'

    example 'Get a list of malls in a given location', document: [:admin] do
      do_request id: @location.id, lat: lat, long: long, account_id: @account.id,
                 account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('malls')
      expect(response_body).to have_json_type(Integer).at_path('malls/0/id')
      expect(response_body).to have_json_type(String).at_path('malls/0/name')
      expect(response_body).to have_json_type(String).at_path('malls/0/sort_name')
      expect(response_body).to have_json_type(String).at_path('malls/0/seo_slug')
      expect(response_body).to have_json_type(String).at_path('malls/0/nick_name')
      expect(response_body).to have_json_type(String).at_path('malls/0/address')
      expect(response_body).to have_json_type(String).at_path('malls/0/city')
      expect(response_body).to have_json_type(String).at_path('malls/0/state')
      expect(response_body).to have_json_type(String).at_path('malls/0/zip')
      expect(response_body).to have_json_type(String).at_path('malls/0/phone')
      expect(response_body).to have_json_type(Float).at_path('malls/0/latitude')
      expect(response_body).to have_json_type(Float).at_path('malls/0/longitude')
      expect(response_body).to have_json_type(String).at_path('malls/0/url')
      expect(response_body).to have_json_type(String).at_path('malls/0/created_at')
      expect(response_body).to have_json_type(String).at_path('malls/0/updated_at')
      expect(response_body).to have_json_type(Array).at_path('images')
    end
  end

  get '/locations/:id/deals' do
    parameter :id, 'The id of the location record to look up', required: true
    parameter :limit, 'How many deals to include, default: 10'
    parameter :offset, 'How many deals to skip, default: 0'

    example 'Get a list of deals in a given location', document: [:admin] do
      do_request id: @location.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('deals')
      expect(response_body).to have_json_type(Integer).at_path('deals/0/id')
      expect(response_body).to have_json_type(Integer).at_path('deals/0/retailer_id')
      expect(response_body).to have_json_type(String).at_path('deals/0/title')
      expect(response_body).to have_json_type(String).at_path('deals/0/seo_slug')
      expect(response_body).to have_json_type(String).at_path('deals/0/name')
      expect(response_body).to have_json_type(String).at_path('deals/0/sort_name')
      expect(response_body).to have_json_type(String).at_path('deals/0/description')
      expect(response_body).to have_json_type(:boolean).at_path('deals/0/is_local')
      expect(response_body).to have_json_type(:boolean).at_path('deals/0/is_featured')
      expect(response_body).to have_json_type(String).at_path('deals/0/created_at')
      expect(response_body).to have_json_type(String).at_path('deals/0/updated_at')
      expect(response_body).to have_json_type(Array).at_path('images')
    end
  end

  get '/locations/:id/products' do
    parameter :id, 'The id of the location record to look up', required: true
    parameter :limit, 'How many products to include, default: 10'
    parameter :offset, 'How many products to skip, default: 0'

    example 'Get a list of products in a given location', document: [:admin] do
      do_request id: @location.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('products')
      expect(response_body).to have_json_type(Integer).at_path('products/0/id')
      expect(response_body).to have_json_type(String).at_path('products/0/name')
      expect(response_body).to have_json_type(String).at_path('products/0/seo_slug')
      expect(response_body).to have_json_type(String).at_path('products/0/description')
      expect(response_body).to have_json_type(String).at_path('products/0/created_at')
      expect(response_body).to have_json_type(String).at_path('products/0/updated_at')
      expect(response_body).to have_json_type(Array).at_path('images')
    end
  end

  get '/locations' do
    parameter :limit, 'How many locations to include, default: 10'
    parameter :offset, 'How many locations to skip, default: 0'
    parameter :lat, 'The latitude for optional distance calculation'
    parameter :long, 'The longitude for optional distance calculation'

    example 'Get a list of available locations', document: [:admin] do
      do_request lat: lat, long: long, account_id: @account.id,
                 account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('locations')
      expect(response_body).to have_json_type(Integer).at_path('locations/0/id')
      expect(response_body).to have_json_type(String).at_path('locations/0/time_zone')
      expect(response_body).to have_json_type(Float).at_path('locations/0/latitude')
      expect(response_body).to have_json_type(Float).at_path('locations/0/longitude')
      expect(response_body).to have_json_type(String).at_path('locations/0/created_at')
      expect(response_body).to have_json_type(String).at_path('locations/0/updated_at')
    end
  end

  post '/locations' do
    before { log_in_as_admin! }

    parameter :latitude, 'The latitude for the location', required: true, scope: :location
    parameter :longitude, 'The longitude for the location', required: true, scope: :location
    parameter :time_zone, 'The time zone for the location', scope: :location

    let(:latitude) { Faker::Address.latitude }
    let(:longitude) { Faker::Address.longitude }
    let(:time_zone) { 1 }

    example 'Create a new location', document: [:admin] do
      do_request account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_path('location')
      expect(response_body).to have_json_type(Integer).at_path('location/id')
      expect(response_body).to have_json_type(String).at_path('location/time_zone')
      expect(response_body).to have_json_type(Float).at_path('location/latitude')
      expect(response_body).to have_json_type(Float).at_path('location/longitude')
    end
  end

  patch '/locations/:id' do
    before { log_in_as_admin! }

    parameter :latitude, 'The latitude for the location', scope: :location, required: true
    parameter :longitude, 'The longitude for the location', scope: :location, required: true
    parameter :time_zone, 'The time zone for the location', scope: :location

    let(:latitude) { Faker::Address.latitude }
    let(:longitude) { Faker::Address.longitude }
    let(:time_zone) { 1 }

    example 'Update an existing location', document: [:admin] do
      do_request id: @location.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_path('location')
      expect(response_body).to have_json_type(Integer).at_path('location/id')
      expect(response_body).to have_json_type(String).at_path('location/time_zone')
      expect(response_body).to have_json_type(Float).at_path('location/latitude')
      expect(response_body).to have_json_type(Float).at_path('location/longitude')
    end
  end

  delete '/locations/:id' do
    before { log_in_as_admin! }

    parameter :id, 'The id of the location to delete'

    example 'Delete a location', document: [:admin] do
      do_request id: @location.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(status).to be(204)
    end
  end
end
