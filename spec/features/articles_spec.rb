resource 'Articles', type: :feature do
  before(:all) do
    @article = create :article
    log_in_as_client!(resource: @article)
  end

  get '/articles' do
    parameter :limit, 'How many records to include, default: 10'
    parameter :offset, 'How many records to skip, default: 0'
    parameter :since, 'Skip records which have not been updated before this date (format: YYYY-MM-DD)'
    parameter :tags, 'Filter by a comma-separated list of tags'
    parameter :strict, 'Only match records which include all provided tags'

    example 'Get a list of articles', document: [:editorial_channel] do
      do_request account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('articles')
      expect(response_body).to have_json_type(Integer).at_path('articles/0/id')
      expect(response_body).to have_json_type(String).at_path('articles/0/title')
      expect(response_body).to have_json_type(String).at_path('articles/0/subtitle')
      expect(response_body).to have_json_type(String).at_path('articles/0/short_title')
      expect(response_body).to have_json_type(String).at_path('articles/0/author')
      expect(response_body).to have_json_type(String).at_path('articles/0/attribution')
      expect(response_body).to have_json_type(String).at_path('articles/0/teaser')
      expect(response_body).to have_json_type(String).at_path('articles/0/seo_slug')
      expect(response_body).to have_json_type(String).at_path('articles/0/content')
      expect(response_body).to have_json_type(String).at_path('articles/0/created_at')
      expect(response_body).to have_json_type(String).at_path('articles/0/updated_at')
      expect(response_body).to have_json_type(Array).at_path('articles/0/variant_ids')
      expect(response_body).to have_json_type(Array).at_path('images')
      expect(response_body).to have_json_type(Array).at_path('tags')
    end
  end

  get '/articles/:id' do
    parameter :id, 'The id of the article to retrieve', required: true

    example 'Get details for an article', document: [:editorial_channel] do
      do_request id: @article.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Integer).at_path('article/id')
      expect(response_body).to have_json_type(String).at_path('article/title')
      expect(response_body).to have_json_type(String).at_path('article/subtitle')
      expect(response_body).to have_json_type(String).at_path('article/short_title')
      expect(response_body).to have_json_type(String).at_path('article/author')
      expect(response_body).to have_json_type(String).at_path('article/attribution')
      expect(response_body).to have_json_type(String).at_path('article/teaser')
      expect(response_body).to have_json_type(String).at_path('article/seo_slug')
      expect(response_body).to have_json_type(String).at_path('article/content')
      expect(response_body).to have_json_type(Array).at_path('article/variant_ids')
      expect(response_body).to have_json_type(String).at_path('article/created_at')
      expect(response_body).to have_json_type(String).at_path('article/updated_at')
      expect(response_body).to have_json_type(Array).at_path('images')
      expect(response_body).to have_json_type(Array).at_path('tags')
    end
  end

  get '/articles/:id/products' do
    before { @article.products << create(:product) }

    parameter :id, 'The id of the article', required: true

    example 'Get products associated with a keyword', document: [:admin] do
      do_request id: @article.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('products')
      expect(response_body).to have_json_type(Integer).at_path('products/0/id')
      expect(response_body).to have_json_type(String).at_path('products/0/name')
      expect(response_body).to have_json_type(String).at_path('products/0/seo_slug')
      expect(response_body).to have_json_type(String).at_path('products/0/description')
      expect(response_body).to have_json_type(String).at_path('products/0/created_at')
      expect(response_body).to have_json_type(String).at_path('products/0/updated_at')
      expect(response_body).to have_json_type(Array).at_path('images')
    end
  end

  get '/articles/search', :sunspot do
    let(:q) { @article.title.split(/\s/).sample }

    parameter :q, 'The query string with which to search', required: true
    parameter :limit, 'How many records to include, default: 10'
    parameter :offset, 'How many records to skip, default: 0'
    parameter :since, 'Skip records which have not been updated before this date (format: YYYY-MM-DD)'
    parameter :tags, 'Filter by a comma-separated list of tags'
    parameter :strict, 'Only match records which include all provided tags'

    before do
      solr_setup
      @article.index!
    end

    example 'Search articles by keyword', document: [:editorial_channel] do
      do_request q: q, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('articles')
      expect(response_body).to have_json_type(Integer).at_path('articles/0/id')
      expect(response_body).to have_json_type(String).at_path('articles/0/title')
      expect(response_body).to have_json_type(String).at_path('articles/0/subtitle')
      expect(response_body).to have_json_type(String).at_path('articles/0/short_title')
      expect(response_body).to have_json_type(String).at_path('articles/0/author')
      expect(response_body).to have_json_type(String).at_path('articles/0/teaser')
      expect(response_body).to have_json_type(String).at_path('articles/0/seo_slug')
      expect(response_body).to have_json_type(String).at_path('articles/0/content')
      expect(response_body).to have_json_type(Array).at_path('articles/0/variant_ids')
      expect(response_body).to have_json_type(String).at_path('articles/0/created_at')
      expect(response_body).to have_json_type(String).at_path('articles/0/updated_at')
      expect(response_body).to have_json_type(Array).at_path('images')
      expect(response_body).to have_json_type(Array).at_path('tags')
    end
  end

  post '/articles', :sunspot do
    before { log_in_as_admin! }

    parameter :author, 'The author of the article', scope: :article, limit: 512
    parameter :attribution, 'The attribution line for the article', scope: :article
    parameter :title, 'The title for the article', scope: :article, required: true, limit: 256
    parameter :subtitle, 'The subtitle', scope: :article
    parameter :short_title, 'A short title', scope: :article
    parameter :teaser, 'A teaser', scope: :article
    parameter :content, 'The main body of the article', scope: :article
    parameter :seo_slug, 'The URL slug', scope: :article, required: true, limit: 256
    parameter :display_at, 'The date to begin displaying the article', scope: :article
    parameter :end_at, 'The date to stop displaying the article', scope: :article
    parameter :display_order, 'Override the display order', scope: :article
    parameter :is_active, 'Whether the article is active', scope: :article
    parameter :product_ids, 'An array of comma separated product ids associated with this article', scope: :article
    parameter :image_ids, 'An array of comma separated image ids associated with this article', scope: :article, required: true
    parameter :mall_ids, 'An array of comma separated mall ids associated with this article', scope: :article, required: true

    let(:author) { Faker::Name.name }
    let(:attribution) { Faker::Name.name }
    let(:title) { Faker::Lorem.sentence }
    let(:subtitle) { Faker::Lorem.sentence }
    let(:short_title) { title }
    let(:teaser) { Faker::Lorem.sentence }
    let(:content) { Faker::Lorem.paragraph }
    let(:seo_slug) { title.slugify }
    let(:display_at) { 1.day.ago }
    let(:end_at) { 1.year.from_now }
    let(:is_active) { true }
    let(:product_ids) { [create(:product).id] }
    let(:image_ids) { [create(:image).id] }
    let(:mall_ids) { [create(:mall).id] }

    example 'Create a new article', document: [:admin] do
      do_request account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Integer).at_path('article/id')
      expect(response_body).to have_json_type(String).at_path('article/title')
      expect(response_body).to have_json_type(String).at_path('article/subtitle')
      expect(response_body).to have_json_type(String).at_path('article/short_title')
      expect(response_body).to have_json_type(String).at_path('article/author')
      expect(response_body).to have_json_type(String).at_path('article/attribution')
      expect(response_body).to have_json_type(String).at_path('article/teaser')
      expect(response_body).to have_json_type(String).at_path('article/seo_slug')
      expect(response_body).to have_json_type(String).at_path('article/content')
      expect(response_body).to have_json_type(Array).at_path('article/variant_ids')
      expect(response_body).to have_json_type(Array).at_path('products')
      expect(response_body).to have_json_type(Array).at_path('images')
      expect(response_body).to have_json_type(Array).at_path('tags')
    end
  end

  patch '/articles/:id', :sunspot do
    before { log_in_as_admin! }

    parameter :author, 'The author of the article', scope: :article, limit: 512
    parameter :attribution, 'The attribution line for the article', scope: :article
    parameter :title, 'The title for the article', scope: :article, required: true, limit: 256
    parameter :subtitle, 'The subtitle', scope: :article
    parameter :short_title, 'A short title', scope: :article
    parameter :teaser, 'A teaser', scope: :article
    parameter :content, 'The main body of the article', scope: :article
    parameter :seo_slug, 'The URL slug', scope: :article, limit: 256
    parameter :display_at, 'The date to begin displaying the article', scope: :article
    parameter :end_at, 'The date to stop displaying the article', scope: :article
    parameter :display_order, 'Override the display order', scope: :article
    parameter :is_active, 'Whether the article is active', scope: :article
    parameter :product_ids, 'An array of comma separated product ids associated with this article', scope: :article
    parameter :image_ids, 'An array of comma separated image ids associated with this article', scope: :article, required: true
    parameter :mall_ids, 'An array of comma separated mall ids associated with this article', scope: :article, required: true

    let(:author) { Faker::Name.name }
    let(:attribution) { Faker::Name.name }
    let(:title) { Faker::Lorem.sentence }
    let(:subtitle) { Faker::Lorem.sentence }
    let(:short_title) { title }
    let(:teaser) { Faker::Lorem.sentence }
    let(:content) { Faker::Lorem.paragraph }
    let(:seo_slug) { title.slugify }
    let(:display_at) { 1.day.ago }
    let(:end_at) { 1.year.from_now }
    let(:is_active) { true }
    let(:product_ids) { [create(:product).id] }
    let(:image_ids) { [create(:image).id] }
    let(:mall_ids) { [create(:mall).id] }

    example 'Update an existing article', document: [:admin] do
      do_request id: @article.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Integer).at_path('article/id')
      expect(response_body).to have_json_type(String).at_path('article/title')
      expect(response_body).to have_json_type(String).at_path('article/subtitle')
      expect(response_body).to have_json_type(String).at_path('article/short_title')
      expect(response_body).to have_json_type(String).at_path('article/author')
      expect(response_body).to have_json_type(String).at_path('article/attribution')
      expect(response_body).to have_json_type(String).at_path('article/teaser')
      expect(response_body).to have_json_type(String).at_path('article/seo_slug')
      expect(response_body).to have_json_type(String).at_path('article/content')
      expect(response_body).to have_json_type(Array).at_path('article/variant_ids')
      expect(response_body).to have_json_type(Array).at_path('products')
      expect(response_body).to have_json_type(Array).at_path('images')
      expect(response_body).to have_json_type(Array).at_path('tags')
    end
  end

  delete '/articles/:id', :sunspot do
    before { log_in_as_admin! }

    parameter :id, 'The id of the article to delete'

    example 'Delete an article', document: [:admin] do
      do_request id: @article.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(status).to be(204)
    end
  end
end
