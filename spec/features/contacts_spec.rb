resource 'Contacts', type: :feature do
  before(:all) do
    @contact = create(:contact)
    @mall = create(:mall, contacts: [@contact])
    log_in_as_client!(resource: @mall, traits: :mall_channel)
  end

  get '/contacts' do
    parameter :limit, 'How many records to include, default: 10'
    parameter :offset, 'How many records to skip, default: 0'
    parameter :since, 'Skip records which have not been updated before this date (format: YYYY-MM-DD)'

    example 'Get a list of all contacts', document: [:mall_channel] do
      do_request account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('contacts')
      expect(response_body).to have_json_type(Integer).at_path('contacts/0/id')
      expect(response_body).to have_json_type(String).at_path('contacts/0/name')
      expect(response_body).to have_json_type(String).at_path('contacts/0/title')
      expect(response_body).to have_json_type(String).at_path('contacts/0/email')
      expect(response_body).to have_json_type(String).at_path('contacts/0/phone')
      expect(response_body).to have_json_type(String).at_path('contacts/0/created_at')
      expect(response_body).to have_json_type(String).at_path('contacts/0/updated_at')
    end
  end

  get '/contacts/:id' do
    parameter :id, 'The id of the contact to retrieve', required: true

    example 'Get details for a specific contact', document: [:mall_channel] do
      do_request id: @contact.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Integer).at_path('contact/id')
      expect(response_body).to have_json_type(String).at_path('contact/name')
      expect(response_body).to have_json_type(String).at_path('contact/title')
      expect(response_body).to have_json_type(String).at_path('contact/email')
      expect(response_body).to have_json_type(String).at_path('contact/phone')
      expect(response_body).to have_json_type(String).at_path('contact/created_at')
      expect(response_body).to have_json_type(String).at_path('contact/updated_at')
    end
  end

  post '/contacts' do
    before { log_in_as_admin! }

    parameter :name, 'Name', scope: :contact, required: true
    parameter :title, 'Title', scope: :contact
    parameter :email, 'Email address', scope: :contact
    parameter :phone, 'Phone number', scope: :contact
    parameter :contactable_id, 'The id of the entity to which this contact is attached', scope: :contact, required: true
    parameter :contactable_type, 'The type of resource of the entity to which this contact is attached', scope: :contact, required: true

    let(:name) { Faker::Name.name }
    let(:title) { Faker::Name.title }
    let(:email) { Faker::Internet.email }
    let(:phone) { Faker::PhoneNumber.phone_number }
    let(:contactable_id) { 1 }
    let(:contactable_type) { ['Mall', 'Company', 'Store', 'Retailer'].sample }

    example 'Create a new contact', document: [:admin] do
      do_request account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Integer).at_path('contact/id')
      expect(response_body).to have_json_type(String).at_path('contact/name')
      expect(response_body).to have_json_type(String).at_path('contact/title')
      expect(response_body).to have_json_type(String).at_path('contact/email')
      expect(response_body).to have_json_type(String).at_path('contact/phone')
    end
  end

  patch '/contacts/:id', document: [:admin] do
    before { log_in_as_admin! }

    parameter :name, 'Name', scope: :contact, required: true
    parameter :title, 'Title', scope: :contact
    parameter :email, 'Email address', scope: :contact
    parameter :phone, 'Phone number', scope: :contact
    parameter :contactable_id, 'The id of the entity to which this contact is attached', scope: :contact, required: true
    parameter :contactable_type, 'The type of resource of the entity to which this contact is attached', scope: :contact, required: true

    let(:name) { Faker::Name.name }
    let(:title) { Faker::Name.title }
    let(:email) { Faker::Internet.email }
    let(:phone) { Faker::PhoneNumber.phone_number }
    let(:contactable_id) { 1 }
    let(:contactable_type) { ['Mall', 'Company', 'Store', 'Retailer'].sample }

    example 'Update an existing contact' do
      do_request id: @contact.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Integer).at_path('contact/id')
      expect(response_body).to have_json_type(String).at_path('contact/name')
      expect(response_body).to have_json_type(String).at_path('contact/title')
      expect(response_body).to have_json_type(String).at_path('contact/email')
      expect(response_body).to have_json_type(String).at_path('contact/phone')
    end
  end

  delete '/contacts/:id', document: [:admin] do
    before { log_in_as_admin! }

    parameter :id, 'The id of the contact to delete', required: true

    example 'Delete a contact' do
      do_request id: @contact.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(status).to be(204)
    end
  end
end
