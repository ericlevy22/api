resource 'Malls', type: :feature do
  before(:all) do
    @mall = create(:mall)
    log_in_as_client!(resource: @mall)
    @account.add_role(:editorial_channel)
    @account.add_role(:deal_channel)
    @account.add_role(:mall_channel)
    @account.add_role(:store_channel)
  end

  get '/malls' do
    parameter :limit, 'How many records to include, default: 10'
    parameter :offset, 'How many records to skip, default: 0'
    parameter :since, 'Skip records which have not been updated before this date (format: YYYY-MM-DD)'
    parameter :q, 'Term for searching malls by name'

    example 'Get a list of malls' do
      do_request lat: 33.333, long: -104.567, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('malls')
      expect(response_body).to have_json_type(Integer).at_path('malls/0/id')
      expect(response_body).to have_json_type(String).at_path('malls/0/name')
      expect(response_body).to have_json_type(String).at_path('malls/0/sort_name')
      expect(response_body).to have_json_type(String).at_path('malls/0/seo_slug')
      expect(response_body).to have_json_type(String).at_path('malls/0/nick_name')
      expect(response_body).to have_json_type(String).at_path('malls/0/address')
      expect(response_body).to have_json_type(String).at_path('malls/0/city')
      expect(response_body).to have_json_type(String).at_path('malls/0/state')
      expect(response_body).to have_json_type(String).at_path('malls/0/zip')
      expect(response_body).to have_json_type(String).at_path('malls/0/phone')
      expect(response_body).to have_json_type(Float).at_path('malls/0/distance')
      expect(response_body).to have_json_type(Float).at_path('malls/0/latitude')
      expect(response_body).to have_json_type(Float).at_path('malls/0/longitude')
      expect(response_body).to have_json_type(String).at_path('malls/0/url')
      expect(response_body).to have_json_type(String).at_path('malls/0/hours_text')
      expect(response_body).to have_json_type(String).at_path('malls/0/description')
      expect(response_body).to have_json_type(String).at_path('malls/0/guest_services_description')
      expect(response_body).to have_json_type(:boolean).at_path('malls/0/exclude_national_deals')
      expect(response_body).to have_json_type(String).at_path('malls/0/created_at')
      expect(response_body).to have_json_type(String).at_path('malls/0/updated_at')
      expect(response_body).to have_json_type(Array).at_path('images')
    end
  end

  get '/malls/:id' do
    parameter :id, 'The id of the mall to retrieve', required: true
    parameter :lat, 'The latitude for optional distance calculation'
    parameter :long, 'The longitude for optional distance calculation'

    example 'Get a specific mall' do
      do_request id: @mall.id, lat: 33.333, long: -104.567, account_id: @account.id,
                 account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Hash).at_path('mall')
      expect(response_body).to have_json_type(Integer).at_path('mall/id')
      expect(response_body).to have_json_type(String).at_path('mall/name')
      expect(response_body).to have_json_type(String).at_path('mall/sort_name')
      expect(response_body).to have_json_type(String).at_path('mall/seo_slug')
      expect(response_body).to have_json_type(String).at_path('mall/nick_name')
      expect(response_body).to have_json_type(String).at_path('mall/address')
      expect(response_body).to have_json_type(String).at_path('mall/city')
      expect(response_body).to have_json_type(String).at_path('mall/state')
      expect(response_body).to have_json_type(String).at_path('mall/zip')
      expect(response_body).to have_json_type(String).at_path('mall/phone')
      expect(response_body).to have_json_type(Float).at_path('mall/distance')
      expect(response_body).to have_json_type(Float).at_path('mall/latitude')
      expect(response_body).to have_json_type(Float).at_path('mall/longitude')
      expect(response_body).to have_json_type(String).at_path('mall/url')
      expect(response_body).to have_json_type(String).at_path('mall/hours_text')
      expect(response_body).to have_json_type(String).at_path('mall/description')
      expect(response_body).to have_json_type(String).at_path('mall/guest_services_description')
      expect(response_body).to have_json_type(:boolean).at_path('mall/exclude_national_deals')
      expect(response_body).to have_json_type(String).at_path('mall/created_at')
      expect(response_body).to have_json_type(String).at_path('mall/updated_at')
      expect(response_body).to have_json_type(Array).at_path('images')
    end
  end

  get '/malls/:id/articles' do
    before { @mall.articles << create(:article) }

    parameter :id, 'The id of the mall', required: true
    parameter :limit, 'How many records to include, default: 10'
    parameter :offset, 'How many records to skip, default: 0'
    parameter :q, 'Term for searching articles by title'

    example 'Get a list of articles featured to a specific mall', document: [:editorial_channel] do
      do_request id: @mall.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('articles')
      expect(response_body).to have_json_type(Integer).at_path('articles/0/id')
      expect(response_body).to have_json_type(String).at_path('articles/0/title')
      expect(response_body).to have_json_type(String).at_path('articles/0/subtitle')
      expect(response_body).to have_json_type(String).at_path('articles/0/short_title')
      expect(response_body).to have_json_type(String).at_path('articles/0/author')
      expect(response_body).to have_json_type(String).at_path('articles/0/teaser')
      expect(response_body).to have_json_type(String).at_path('articles/0/seo_slug')
      expect(response_body).to have_json_type(String).at_path('articles/0/content')
      expect(response_body).to have_json_type(String).at_path('articles/0/created_at')
      expect(response_body).to have_json_type(String).at_path('articles/0/updated_at')
      expect(response_body).to have_json_type(Array).at_path('images')
    end
  end

  get '/malls/:id/stores' do
    before { @mall.stores << create(:store) }

    parameter :id, 'The id of the mall for which to retrieve stores', required: true
    parameter :limit, 'How many records to include, default: 10'
    parameter :offset, 'How many records to skip, default: 0'
    parameter :q, 'Term for searching stores by name'
    parameter :lat, 'The latitude for optional distance calculation'
    parameter :long, 'The longitude for optional distance calculation'

    example 'Get a list of stores for a specific mall', document: [:deal_channel, :store_channel] do
      do_request id: @mall.id, lat: 33.333, long: -104.567, account_id: @account.id,
                 account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('stores')
      expect(response_body).to have_json_type(Integer).at_path('stores/0/id')
      expect(response_body).to have_json_type(String).at_path('stores/0/name')
      expect(response_body).to have_json_type(String).at_path('stores/0/sort_name')
      expect(response_body).to have_json_type(String).at_path('stores/0/seo_slug')
      expect(response_body).to have_json_type(String).at_path('stores/0/description')
      expect(response_body).to have_json_type(String).at_path('stores/0/address')
      expect(response_body).to have_json_type(String).at_path('stores/0/city')
      expect(response_body).to have_json_type(String).at_path('stores/0/state')
      expect(response_body).to have_json_type(String).at_path('stores/0/zip')
      expect(response_body).to have_json_type(String).at_path('stores/0/phone')
      expect(response_body).to have_json_type(Float).at_path('stores/0/distance')
      expect(response_body).to have_json_type(Float).at_path('stores/0/latitude')
      expect(response_body).to have_json_type(Float).at_path('stores/0/longitude')
      expect(response_body).to have_json_type(Integer).at_path('stores/0/mall_id')
      expect(response_body).to have_json_type(Integer).at_path('stores/0/retailer_id')
      expect(response_body).to have_json_type(String).at_path('stores/0/tenant_location')
      expect(response_body).to have_json_type(String).at_path('stores/0/tenant_location_map_key')
      expect(response_body).to have_json_type(:boolean).at_path('stores/0/exclude_national_deals')
      expect(response_body).to have_json_type(String).at_path('stores/0/created_at')
      expect(response_body).to have_json_type(String).at_path('stores/0/updated_at')
      expect(response_body).to have_json_type(Array).at_path('images')
    end
  end

  get '/malls/:id/theaters' do
    before { @mall.theaters << create(:theater) }

    parameter :id, 'The id of the mall for which to retrieve theaters', required: true
    parameter :limit, 'How many records to include, default: 10'
    parameter :offset, 'How many records to skip, default: 0'

    example 'Get a list of theaters for a specific mall', document: [:admin] do
      do_request id: @mall.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('theaters')
      expect(response_body).to have_json_type(Integer).at_path('theaters/0/id')
      expect(response_body).to have_json_type(String).at_path('theaters/0/name')
      expect(response_body).to have_json_type(String).at_path('theaters/0/sort_name')
      expect(response_body).to have_json_type(String).at_path('theaters/0/seo_slug')
      expect(response_body).to have_json_type(String).at_path('theaters/0/address')
      expect(response_body).to have_json_type(String).at_path('theaters/0/city')
      expect(response_body).to have_json_type(String).at_path('theaters/0/state')
      expect(response_body).to have_json_type(String).at_path('theaters/0/zip')
      expect(response_body).to have_json_type(String).at_path('theaters/0/phone')
      expect(response_body).to have_json_type(String).at_path('theaters/0/created_at')
      expect(response_body).to have_json_type(String).at_path('theaters/0/updated_at')
    end
  end

  get '/malls/:id/deals' do
    let!(:deal) { create :deal }
    let!(:store) { create :store, mall_id: @mall.id, deals: [deal] }
    let!(:mall_2) { create :mall }
    let!(:store_2) { create :store, mall_id: mall_2.id, deals: [deal] }

    parameter :id, 'The id of the mall for which to retrieve deals', required: true
    parameter :limit, 'How many records to include, default: 10'
    parameter :offset, 'How many records to skip, default: 0'
    parameter :q, 'Term for searching deals by keyword'

    example 'Get a list of deals for a specific mall', document: [:deal_channel] do
      do_request id: @mall.id, lat: 33.333, long: -104.567, account_id: @account.id,
                 account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('deals')
      expect(response_body).to have_json_type(Integer).at_path('deals/0/id')
      expect(response_body).to have_json_type(Integer).at_path('deals/0/retailer_id')
      expect(response_body).to have_json_type(String).at_path('deals/0/title')
      expect(response_body).to have_json_type(String).at_path('deals/0/seo_slug')
      expect(response_body).to have_json_type(String).at_path('deals/0/name')
      expect(response_body).to have_json_type(String).at_path('deals/0/sort_name')
      expect(response_body).to have_json_type(String).at_path('deals/0/retailer_name')
      expect(response_body).to have_json_type(String).at_path('deals/0/retailer_sort_name')
      expect(response_body).to have_json_type(String).at_path('deals/0/store_name')
      expect(response_body).to have_json_type(String).at_path('deals/0/store_sort_name')
      expect(response_body).to have_json_type(String).at_path('deals/0/description')
      expect(response_body).to have_json_type(String).at_path('deals/0/created_at')
      expect(response_body).to have_json_type(String).at_path('deals/0/updated_at')
      expect(response_body).to have_json_type(:boolean).at_path('deals/0/is_local')
      expect(response_body).to have_json_type(:boolean).at_path('deals/0/is_featured')
      expect(response_body).to have_json_type(Array).at_path('images')

      expect(JSON.parse(response_body)['deals'][0]['store_name']).to eq(store.name)
      expect(JSON.parse(response_body)['deals'][0]['store_name']).to_not eq(store_2.name)
    end
  end

  get '/malls/:id/social-feeds' do
    before { @mall.social_feeds << create(:social_feed) }

    parameter :id, 'The id of the mall for which to retrieve social network feeds', required: true
    parameter :limit, 'How many records to include, default: 10'
    parameter :offset, 'How many records to skip, default: 0'

    example 'Get a list of social network feeds for a specific mall', document: [:mall_channel, :store_channel] do
      do_request id: @mall.id, account_id: @account.id,
                 account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('social_feeds')
      expect(response_body).to have_json_type(Integer).at_path('social_feeds/0/id')
      expect(response_body).to have_json_type(String).at_path('social_feeds/0/name')
      expect(response_body).to have_json_type(Hash).at_path('social_feeds/0/properties')
      expect(response_body).to have_json_type(String).at_path('social_feeds/0/created_at')
      expect(response_body).to have_json_type(String).at_path('social_feeds/0/updated_at')
    end
  end

  get '/malls/:id/contacts' do
    before { @mall.contacts << create(:contact) }

    parameter :id, 'The id of the mall for which to retrieve contact information', required: true
    parameter :limit, 'How many records to include, default: 10'
    parameter :offset, 'How many records to skip, default: 0'
    parameter :q, 'Term for searching contacts by name'

    example 'Get a list of contacts for a specific mall', document: [:mall_channel] do
      do_request id: @mall.id, account_id: @account.id,
                 account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('contacts')
      expect(response_body).to have_json_type(Integer).at_path('contacts/0/id')
      expect(response_body).to have_json_type(String).at_path('contacts/0/name')
      expect(response_body).to have_json_type(String).at_path('contacts/0/title')
      expect(response_body).to have_json_type(String).at_path('contacts/0/email')
      expect(response_body).to have_json_type(String).at_path('contacts/0/phone')
      expect(response_body).to have_json_type(String).at_path('contacts/0/created_at')
      expect(response_body).to have_json_type(String).at_path('contacts/0/updated_at')
    end
  end

  post '/malls/:id/contact-us' do
    before do
      log_in_as_admin!
      allow(Email::Send).to receive(:call) { true }
    end

    parameter :id, 'The id of the mall for which to contact', required: true
    parameter :subject, 'The subject line', prefix: :mail, required: true
    parameter :body, 'The message body', prefix: :mail, required: true
    parameter :from, 'The from address for the message', prefix: :mail, required: true

    example 'Send a "Contact Us" message to a mall', document: [:admin] do
      do_request id: @mall.id, account_id: @account.id,
                 account_token: @token, timestamp: @timestamp,
                 mail: { subject: Faker::Lorem.sentence, body: Faker::Lorem.paragraph, from: AppConfig.emails['system_events'] }

      expect(status).to eq(202)
    end
  end

  get '/malls/:id/job-listings' do
    before do
      store = create(:store)
      store.job_listings << create(:job_listing)
      @mall.stores << store
    end

    parameter :id, 'The id of the mall for which to retrieve job listings', required: true
    parameter :limit, 'How many records to include, default: 10'
    parameter :offset, 'How many records to skip, default: 0'
    parameter :q, 'Term for searching job listings by title'

    example 'Get a list of current job listings for a specific mall', document: [:mall_channel] do
      do_request id: @mall.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('job_listings')
      expect(response_body).to have_json_type(Integer).at_path('job_listings/0/id')
      expect(response_body).to have_json_type(String).at_path('job_listings/0/store_name')
      expect(response_body).to have_json_type(String).at_path('job_listings/0/start_date_text')
      expect(response_body).to have_json_type(String).at_path('job_listings/0/hours_per_week')
      expect(response_body).to have_json_type(String).at_path('job_listings/0/number_of_openings')
      expect(response_body).to have_json_type(String).at_path('job_listings/0/fax_number')
      expect(response_body).to have_json_type(String).at_path('job_listings/0/email')
      expect(response_body).to have_json_type(String).at_path('job_listings/0/salary')
      expect(response_body).to have_json_type(String).at_path('job_listings/0/key_responsibilities')
      expect(response_body).to have_json_type(String).at_path('job_listings/0/required_experience')
      expect(response_body).to have_json_type(String).at_path('job_listings/0/special_information')
      expect(response_body).to have_json_type(String).at_path('job_listings/0/description')
      expect(response_body).to have_json_type(String).at_path('job_listings/0/display_at')
      expect(response_body).to have_json_type(String).at_path('job_listings/0/end_at')
      expect(response_body).to have_json_type(String).at_path('job_listings/0/seo_slug')
      expect(response_body).to have_json_type(:boolean).at_path('job_listings/0/is_full_time')
      expect(response_body).to have_json_type(:boolean).at_path('job_listings/0/is_apply_online')
      expect(response_body).to have_json_type(String).at_path('job_listings/0/created_at')
      expect(response_body).to have_json_type(String).at_path('job_listings/0/updated_at')
    end
  end

  get '/malls/:id/press-releases' do
    let!(:press_release) { create :press_release, mall_id: @mall.id }

    parameter :id, 'The id of the mall for which to retrieve press releases', required: true
    parameter :limit, 'How many records to include, default: 10'
    parameter :offset, 'How many records to skip, default: 0'
    parameter :q, 'Term for searching press releases by name'

    example 'Get a list of current press releases for a specific mall', document: [:mall_channel] do
      do_request id: @mall.id, account_id: @account.id,
                 account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('press_releases')
      expect(response_body).to have_json_type(Integer).at_path('press_releases/0/id')
      expect(response_body).to have_json_type(Integer).at_path('press_releases/0/mall_id')
      expect(response_body).to have_json_type(String).at_path('press_releases/0/name')
      expect(response_body).to have_json_type(String).at_path('press_releases/0/seo_slug')
      expect(response_body).to have_json_type(String).at_path('press_releases/0/headline')
      expect(response_body).to have_json_type(String).at_path('press_releases/0/tag_line')
      expect(response_body).to have_json_type(String).at_path('press_releases/0/phone')
      expect(response_body).to have_json_type(String).at_path('press_releases/0/email')
      expect(response_body).to have_json_type(String).at_path('press_releases/0/pr_type')
      expect(response_body).to have_json_type(String).at_path('press_releases/0/display_at')
      expect(response_body).to have_json_type(:boolean).at_path('press_releases/0/is_active')
      expect(response_body).to have_json_type(String).at_path('press_releases/0/created_at')
      expect(response_body).to have_json_type(String).at_path('press_releases/0/updated_at')
    end
  end

  get '/malls/:id/programs' do
    before { @mall.programs << create(:program) }

    parameter :id, 'The id of the mall for which to retrieve programs', required: true
    parameter :limit, 'How many records to include, default: 10'
    parameter :offset, 'How many records to skip, default: 0'

    example 'Get a list of current programs for a specific mall', document: [:mall_channel] do
      do_request id: @mall.id, account_id: @account.id,
                 account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('programs')
      expect(response_body).to have_json_type(Integer).at_path('programs/0/id')
      expect(response_body).to have_json_type(Hash).at_path('programs/0/properties')
      expect(response_body).to have_json_type(String).at_path('programs/0/program_type')
      expect(response_body).to have_json_type(String).at_path('programs/0/created_at')
      expect(response_body).to have_json_type(String).at_path('programs/0/updated_at')
    end
  end

  get '/malls/:id/i-features' do
    let!(:ifeature) { create :ifeature, mall_id: @mall.id }

    parameter :id, 'The id of the mall', required: true
    parameter :limit, 'How many records to include, default: 10'
    parameter :offset, 'How many records to skip, default: 0'
    parameter :q, 'Term for searching ifeatures by keyword'

    example 'Get a list of current i-features for a specific mall', document: [:mall_channel] do
      do_request id: @mall.id, account_id: @account.id,
                 account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('i_features')
      expect(response_body).to have_json_type(Integer).at_path('i_features/0/id')
      expect(response_body).to have_json_type(String).at_path('i_features/0/seo_slug')
      expect(response_body).to have_json_type(String).at_path('i_features/0/headline')
      expect(response_body).to have_json_type(String).at_path('i_features/0/sub_heading')
      expect(response_body).to have_json_type(String).at_path('i_features/0/tag_line')
      expect(response_body).to have_json_type(String).at_path('i_features/0/description')
      expect(response_body).to have_json_type(String).at_path('i_features/0/start_at')
      expect(response_body).to have_json_type(String).at_path('i_features/0/end_at')
      expect(response_body).to have_json_type(String).at_path('i_features/0/url')
      expect(response_body).to have_json_type(Integer).at_path('i_features/0/pin_order')
      expect(response_body).to have_json_type(:boolean).at_path('i_features/0/is_static')
      expect(response_body).to have_json_type(:boolean).at_path('i_features/0/is_headline')
      expect(response_body).to have_json_type(:boolean).at_path('i_features/0/is_new_window')
      expect(response_body).to have_json_type(String).at_path('i_features/0/created_at')
      expect(response_body).to have_json_type(String).at_path('i_features/0/updated_at')
    end
  end

  get '/malls/:id/hours' do
    let(:hour) { create :default_hour }

    before do
      @mall.hours << hour
    end

    parameter :id, 'The id of the mall for which to retrieve hours', required: true
    parameter :from, 'The beginning date (in format YYYY-MM-DD) for the range being requested'
    parameter :to, 'The ending date (in format YYYY-MM-DD) for the range being requested'

    example 'Get a list of hours for a specific mall', document: [:mall_channel] do
      do_request id: @mall.id, account_id: @account.id,
                 account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('hours')
      expect(response_body).to have_json_path('hours/0/description')
      expect(response_body).to have_json_type(String).at_path('hours/0/date')
      expect(response_body).to have_json_type(String).at_path('hours/0/day')
      expect(response_body).to have_json_type(String).at_path('hours/0/hours_text')
    end
  end

  get '/malls/:id/current-hours' do
    let(:hour) { create :default_hour }

    before do
      @mall.hours << hour
    end

    parameter :id, 'The id of the mall for which to retrieve hours', required: true

    example 'Get the current hours for a specific mall', document: [:mall_channel] do
      do_request id: @mall.id, account_id: @account.id,
                 account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_path('this_week/0/description')
      expect(response_body).to have_json_type(String).at_path('this_week/0/date')
      expect(response_body).to have_json_type(String).at_path('this_week/0/day')
      expect(response_body).to have_json_type(String).at_path('this_week/0/hours_text')
      expect(response_body).to have_json_path('this_month/0/description')
      expect(response_body).to have_json_type(String).at_path('this_month/0/date')
      expect(response_body).to have_json_type(String).at_path('this_month/0/day')
      expect(response_body).to have_json_type(String).at_path('this_month/0/hours_text')
      expect(response_body).to have_json_path('today/0/description')
      expect(response_body).to have_json_type(String).at_path('today/0/date')
      expect(response_body).to have_json_type(String).at_path('today/0/day')
      expect(response_body).to have_json_type(String).at_path('today/0/hours_text')
    end
  end

  get '/malls/:id/events' do
    before { @mall.events << create(:event) }

    parameter :id, 'The id of the mall', required: true
    parameter :limit, 'How many records to include, default: 10'
    parameter :offset, 'How many records to skip, default: 0'
    parameter :groups, 'Filter by a comma-separated list of groups'
    parameter :strict, 'Only match records which include all provided groups'
    parameter :only_featured, 'Limit results to only featured events'
    parameter :from, 'The beginning of the date range -- defaults to today'
    parameter :to, 'The end of the date range -- defaults to one month from today'
    parameter :q, 'Term for searching events by name'

    example 'Get a collection of current events for a mall in a given date range', document: [:mall_channel] do
      do_request id: @mall.id, account_id: @account.id,
                 account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('events')
      expect(response_body).to have_json_type(Integer).at_path('events/0/id')
      expect(response_body).to have_json_type(String).at_path('events/0/title')
      expect(response_body).to have_json_type(String).at_path('events/0/seo_slug')
      expect(response_body).to have_json_type(String).at_path('events/0/short_text')
      expect(response_body).to have_json_type(String).at_path('events/0/description')
      expect(response_body).to have_json_type(String).at_path('events/0/start_at')
      expect(response_body).to have_json_type(String).at_path('events/0/end_at')
      expect(response_body).to have_json_type(String).at_path('events/0/link_url')
      expect(response_body).to have_json_type(String).at_path('events/0/link_text')
      expect(response_body).to have_json_type(Integer).at_path('events/0/pin_order')
      expect(response_body).to have_json_type(:boolean).at_path('events/0/is_featured')
      expect(response_body).to have_json_type(:boolean).at_path('events/0/is_events_page')
      expect(response_body).to have_json_type(String).at_path('events/0/date_text')
      expect(response_body).to have_json_type(String).at_path('events/0/location')
      expect(response_body).to have_json_type(String).at_path('events/0/created_at')
      expect(response_body).to have_json_type(String).at_path('events/0/updated_at')
    end
  end

  get '/malls/:id/mall-messages' do
    let(:mall_message) { create(:mall_message) }

    before do
      @mall.mall_messages << mall_message
      @mall.mall_messages << create(:mall_message, end_at: 1.week.ago)
    end

    parameter :id, 'The id of the mall', required: true
    parameter :limit, 'How many records to include, default: 10'
    parameter :offset, 'How many records to skip, default: 0'
    parameter :q, 'Term for searching mall messages by keyword'

    example 'Get a collection of mall messages for a mall', document: [:mall_channel] do
      do_request id: @mall.id, account_id: @account.id,
                 account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Array).at_path('mall_messages')
      expect(response_body).to have_json_type(Integer).at_path('mall_messages/0/mall_id')
      expect(response_body).to have_json_type(String).at_path('mall_messages/0/description')
      expect(response_body).to have_json_type(String).at_path('mall_messages/0/message_type')
      expect(response_body).to have_json_type(String).at_path('mall_messages/0/start_at')
      expect(response_body).to have_json_type(String).at_path('mall_messages/0/end_at')
      expect(response_body).to have_json_type(String).at_path('mall_messages/0/created_at')
      expect(response_body).to have_json_type(String).at_path('mall_messages/0/updated_at')
      expect(response_body).to have_json_size(1).at_path('mall_messages')
    end
  end

  post '/malls' do
    before { log_in_as_admin! }

    parameter :company_id, 'The id of the mall management company', scope: :mall, required: true
    parameter :location_id, 'The id of the location for the mall', scope: :mall, required: true
    parameter :name, 'The name of the mall', scope: :mall, required: true
    parameter :sort_name, 'The sortable version of the name', scope: :mall
    parameter :nick_name, 'The nickname for the mall', scope: :mall, required: true
    parameter :seo_slug, 'The seo url slug for the mall', scope: :mall, required: true
    parameter :address, 'The address for the mall', scope: :mall
    parameter :city, 'The city', scope: :mall, required: true
    parameter :state, 'The state', scope: :mall, required: true
    parameter :zip, 'The ZIP', scope: :mall, required: true
    parameter :phone, 'The phone number for the mall', scope: :mall
    parameter :url, 'The URL for the mall', scope: :mall, required: true
    parameter :email, 'The main email address for the mall', scope: :mall, required: true
    parameter :exclude_national_deals, 'Whether or not to exclude global promotions', scope: :mall
    parameter :hours_text, 'A string for describing mall hours', scope: :mall
    parameter :description, 'A string to describe attributes about a mall', scope: :mall
    parameter :guest_services_description, 'A string to describe the guest services for a mall', scope: :mall

    let(:company_id) { create(:company).id }
    let(:location_id) { create(:location).id }
    let(:name) { Faker::Lorem.sentence(3) }
    let(:sort_name) { name }
    let(:nick_name) { name }
    let(:seo_slug) { name.slugify }
    let(:address) { Faker::Address.street_address }
    let(:city) { Faker::Address.city }
    let(:state) { Faker::Address.state_abbr }
    let(:zip) { Faker::Address.zip_code }
    let(:phone) { Faker::PhoneNumber.phone_number }
    let(:url) { Faker::Internet.url }
    let(:email) { Faker::Internet.email }
    let(:is_live) { false }
    let(:is_development) { true }
    let(:is_archived) { false }
    let(:hours_text) { '8-10 Every day' }
    let(:description) { name }
    let(:guest_services_description) { 'Mall Office' }
    # let(:site_type) {  }
    # let(:lang_code) {  }
    let(:exclude_national_deals) { false }

    example 'Create a new mall', document: [:admin] do
      do_request account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Hash).at_path('mall')
      expect(response_body).to have_json_type(Integer).at_path('mall/id')
      expect(response_body).to have_json_type(String).at_path('mall/name')
      expect(response_body).to have_json_type(String).at_path('mall/sort_name')
      expect(response_body).to have_json_type(String).at_path('mall/seo_slug')
      expect(response_body).to have_json_type(String).at_path('mall/nick_name')
      expect(response_body).to have_json_type(String).at_path('mall/address')
      expect(response_body).to have_json_type(String).at_path('mall/city')
      expect(response_body).to have_json_type(String).at_path('mall/state')
      expect(response_body).to have_json_type(String).at_path('mall/zip')
      expect(response_body).to have_json_type(String).at_path('mall/phone')
      expect(response_body).to have_json_type(Float).at_path('mall/latitude')
      expect(response_body).to have_json_type(Float).at_path('mall/longitude')
      expect(response_body).to have_json_type(String).at_path('mall/url')
      expect(response_body).to have_json_type(String).at_path('mall/hours_text')
      expect(response_body).to have_json_type(String).at_path('mall/description')
      expect(response_body).to have_json_type(String).at_path('mall/guest_services_description')
      expect(response_body).to have_json_type(:boolean).at_path('mall/exclude_national_deals')
      expect(response_body).to have_json_type(Array).at_path('images')
    end
  end

  patch '/malls/:id' do
    before { log_in_as_admin! }

    parameter :company_id, 'The id of the mall management company', scope: :mall, required: true
    parameter :location_id, 'The id of the location for the mall', scope: :mall, required: true
    parameter :name, 'The name of the mall', scope: :mall, required: true
    parameter :sort_name, 'The sortable version of the name', scope: :mall
    parameter :nick_name, 'The nickname for the mall', scope: :mall
    parameter :seo_slug, 'The seo url slug for the mall', scope: :mall
    parameter :address, 'The address for the mall', scope: :mall
    parameter :city, 'The city', scope: :mall
    parameter :state, 'The state', scope: :mall
    parameter :zip, 'The ZIP', scope: :mall
    parameter :phone, 'The phone number for the mall', scope: :mall
    parameter :url, 'The URL for the mall', scope: :mall
    parameter :email, 'The main email address for the mall', scope: :mall
    parameter :exclude_national_deals, 'Whether or not to exclude global promotions', scope: :mall
    parameter :description, 'A string to describe attributes about a mall', scope: :mall
    parameter :guest_services_description, 'A string to describe the guest services for a mall', scope: :mall

    let(:company_id) { create(:company).id }
    let(:location_id) { create(:location).id }
    let(:name) { Faker::Lorem.sentence(3) }
    let(:sort_name) { name }
    let(:nick_name) { name }
    let(:seo_slug) { name.slugify }
    let(:address) { Faker::Address.street_address }
    let(:city) { Faker::Address.city }
    let(:state) { Faker::Address.state_abbr }
    let(:zip) { Faker::Address.zip_code }
    let(:phone) { Faker::PhoneNumber.phone_number }
    let(:url) { Faker::Internet.url }
    let(:email) { Faker::Internet.email }
    let(:is_live) { false }
    let(:is_development) { true }
    let(:is_archived) { false }
    let(:hours_text) { '8-10 Every day' }
    let(:description) { name }
    let(:guest_services_description) { 'Main Office' }
    # let(:site_type) {  }
    # let(:lang_code) {  }
    let(:exclude_national_deals) { false }

    example 'Update an existing mall', document: [:admin] do
      do_request id: @mall.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(response_body).to have_json_type(Hash).at_path('mall')
      expect(response_body).to have_json_type(Integer).at_path('mall/id')
      expect(response_body).to have_json_type(String).at_path('mall/name')
      expect(response_body).to have_json_type(String).at_path('mall/sort_name')
      expect(response_body).to have_json_type(String).at_path('mall/seo_slug')
      expect(response_body).to have_json_type(String).at_path('mall/nick_name')
      expect(response_body).to have_json_type(String).at_path('mall/address')
      expect(response_body).to have_json_type(String).at_path('mall/city')
      expect(response_body).to have_json_type(String).at_path('mall/state')
      expect(response_body).to have_json_type(String).at_path('mall/zip')
      expect(response_body).to have_json_type(String).at_path('mall/phone')
      expect(response_body).to have_json_type(Float).at_path('mall/latitude')
      expect(response_body).to have_json_type(Float).at_path('mall/longitude')
      expect(response_body).to have_json_type(String).at_path('mall/url')
      expect(response_body).to have_json_type(String).at_path('mall/hours_text')
      expect(response_body).to have_json_type(String).at_path('mall/description')
      expect(response_body).to have_json_type(String).at_path('mall/guest_services_description')
      expect(response_body).to have_json_type(:boolean).at_path('mall/exclude_national_deals')
      expect(response_body).to have_json_type(Array).at_path('images')
    end
  end

  delete '/malls/:id' do
    before { log_in_as_admin! }

    parameter :id, 'The id of the mall to delete', required: true

    example 'Delete an existing mall', document: [:admin] do
      do_request id: @mall.id, account_id: @account.id, account_token: @token, timestamp: @timestamp

      expect(status).to eq(204)
    end
  end
end
