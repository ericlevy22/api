# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :mall do
    association :location
    association :company
    name { Faker::Company.name }
    sort_name { Faker::Lorem.word }
    seo_slug { name.try(:slugify) }
    nick_name { Faker::Lorem.word }
    address { Faker::Address.street_address }
    city { Faker::Address.city }
    state { Faker::Address.state }
    zip { Faker::Address.zip }
    phone { Faker::PhoneNumber.phone_number }
    url { Faker::Internet.url }
    email { Faker::Internet.email }
    hours_text { 'Monday-Thursday: 10AM - 9pm <br>Friday & Saturday: 11AM - Midnight<br>Sunday' }
    description { name }
    guest_services_description { 'Guest Service Desk' }
  end
end
