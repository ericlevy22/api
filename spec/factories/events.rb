# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :event do
    title { Faker::Lorem.sentence }
    seo_slug { title.try(:slugify) }
    short_text { Faker::Lorem.sentence }
    description { Faker::Lorem.paragraph }
    display_at { 1.week.ago }
    link_url 'https://www.example.com'
    link_text 'Example Link'
    start_at { 1.day.ago }
    end_at { 1.day.from_now }
    is_active { true }
    is_events_page { true }
    date_text { "#{start_at} - #{end_at}" }
    location { "In front of DICK'S Sporting Goods" }

    trait :grouped do
      after(:create) { |event| event.update_attributes(group_list: 'foo,bar,baz') }
    end
  end
end
