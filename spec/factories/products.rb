# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :product do
    name { Faker::Company.name }
    seo_slug { name.try(:slugify) }
    description { Faker::Lorem.sentence(2) }
    association :brand
    association :retailer
    stores { [create(:store, retailer: retailer)] }
    images { [create(:image)] }
  end
end
