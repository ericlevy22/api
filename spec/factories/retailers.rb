# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :retailer do
    name { Faker::Company.name }
    sort_name { name }
    nick_name { name }
    seo_slug { name.try(:slugify) }
    description { Faker::Lorem.sentence(5) }
    phone { Faker::PhoneNumber.phone_number }
    email { Faker::Internet.email }
    store_text { Faker::Lorem.sentence(5) }
    url { Faker::Internet.url }
    url_text { Faker::Internet.url }
  end
end
