FactoryGirl.define do
  factory :keyword do
    name { Faker::Lorem.sentence(3) }
  end
end
