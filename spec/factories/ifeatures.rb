# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :ifeature do
    headline { Faker::Lorem.sentence }
    sub_heading { Faker::Lorem.sentence(2)[0..49] }
    tag_line { Faker::Lorem.sentence(2) }
    seo_slug { headline.try(:slugify) }
    description { Faker::Lorem.sentence(2) }
    url { Faker::Internet.url }
    is_active true
    start_at { 1.day.ago }
    end_at { 1.day.from_now }
  end
end
