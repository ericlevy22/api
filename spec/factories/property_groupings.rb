# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :property_grouping do
    property_group nil
    groupable_id 1
    groupable_type 'MyString'
  end
end
