# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :hour do
    mall_id { rand(1..1000) }
    sunday '9-6'
    monday '9-9'
    tuesday '9-9'
    wednesday '9-9'
    thursday '9-9'
    friday '9-10'
    saturday '9-10'
    start_at { 1.day.ago }
    end_at { 1.week.from_now }
    is_default false

    trait :default do
      is_default true
      sunday '10-6'
      monday '10-9'
      tuesday '10-9'
      wednesday '10-9'
      thursday '10-9'
      friday '10-10'
      saturday '10-10'
      start_at { Date.strptime('1970-01-01 00:00:00.000') }
      end_at { Date.strptime('2038-01-19 00:00:00.000') }
    end

    factory :default_hour, traits: [:default]
  end
end
