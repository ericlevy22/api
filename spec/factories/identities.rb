FactoryGirl.define do
  factory :identity do
    identity_value { Faker::Internet.email }
    identity_type 'email'
    association :user
  end
end
