FactoryGirl.define do
  factory :membership do
    association :program
    properties { {} }
    association :identity
  end
end
