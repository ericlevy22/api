# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :deal do
    title Faker::Lorem.sentence(5)
    seo_slug { title.try(:slugify) }
    start_at { 1.day.ago }
    display_at { start_at }
    end_at { 1.week.from_now }
    description { Faker::Lorem.sentence(10) }
    association :retailer
    is_active true
  end
end
