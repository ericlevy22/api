# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :mall_message do
    description { Faker::Lorem.sentence }
    start_at Time.now - 1.day
    end_at Time.now + 3.days
    created_at Time.now
    message_type 'operational'
  end
end
