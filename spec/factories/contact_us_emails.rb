FactoryGirl.define do
  factory :contact_us_email do
    from { Faker::Internet.email }
    to { Faker::Internet.email }
    subject { Faker::Lorem.word }
    body { Faker::Lorem.sentences }
    sent_at { Time.now }
  end
end
