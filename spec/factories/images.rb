# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :image do
    url 'http://cdn.example.com/{width}/some-folder/rad-image.png'
    size_parameter '{width}'
    imageable_id 1
    imageable_type 'Article'
  end
end
