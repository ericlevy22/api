# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :account do
    email { Faker::Internet.email }
    password { 'asdfasdf' }
    confirmed_at { Time.now }

    association :user

    trait :unconfirmed do
      confirmed_at { nil }
    end

    trait :admin do
      after(:create) { |account| account.add_role :admin }
    end

    trait :sales do
      after(:create) { |account| account.add_role :sales }
    end

    trait :client do
      ignore { resource Mall.first }
      after(:create) { |account, evaluator| account.add_role :client, evaluator.resource }
    end

    trait :mall_client do
      ignore { resource Mall.first }
      after(:create) { |account, evaluator| account.add_role :mall_client, evaluator.resource }
    end

    trait :mall_channel do
      after(:create) { |account| account.add_role :mall_channel }
    end

    trait :store_channel do
      after(:create) { |account| account.add_role :store_channel }
    end

    trait :deal_channel do
      after(:create) { |account| account.add_role :deal_channel }
    end

    trait :editorial_channel do
      after(:create) { |account| account.add_role :deal_channel }
    end

    factory :unconfirmed, traits: [:unconfirmed]
    factory :admin, traits: [:admin]
    factory :client, traits: [:client]
    factory :mall_client, traits: [:mall_client]
    factory :sales, traits: [:sales]
  end
end
