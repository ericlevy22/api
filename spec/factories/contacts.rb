# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :contact do
    name { Faker::Name.name }
    title 'Manager'
    email { Faker::Internet.email }
    phone { Faker::PhoneNumber.phone_number }
    contactable_id 1
    contactable_type 'Mall'
  end
end
