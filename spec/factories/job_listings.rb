# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :job_listing do
    title { Faker::Lorem.sentence }
    seo_slug { title.try(:slugify) }
    start_date_text 'ASAP'
    hours_per_week '30+'
    number_of_openings '3'
    is_full_time false
    is_apply_online false
    manager_name { Faker::Name.name }
    key_responsibilities { Faker::Lorem.sentence }
    required_experience { Faker::Lorem.sentence }
    salary 'negotiable'
    special_information { Faker::Lorem.sentence }
    description { Faker::Lorem.sentence }
    legacy_requisition_id { rand(1_000_000) }
    display_at { 1.day.ago }
    end_at { 1.day.from_now }
    association :store
    fax_number { Faker::PhoneNumber.phone_number }
    email { Faker::Internet.email }
    is_active true
  end
end
