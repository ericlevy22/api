# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :affinity do
    heartable_type { Affinity::HEARTABLE_TYPES.sample }
    heartable_id { rand(10_000) }
    association :user
  end
end
