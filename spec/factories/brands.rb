# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :brand do
    name { Faker::Company.name }
    seo_slug { name.try(:slugify) }
    is_approved 1
    description { Faker::Lorem.sentence(2) }
    url { Faker::Internet.url }
  end
end
