FactoryGirl.define do
  factory :program do
    properties { {} }
    owner nil
    is_internal_only false
    is_default_signup_program false
    is_active true
    is_subscribable true
    program_type 2
    legacy_id { rand(1000) }
  end
end
