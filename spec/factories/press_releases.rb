FactoryGirl.define do
  factory :press_release do
    name { Faker::Lorem.sentence }
    seo_slug { name.try(:slugify) }
    headline { Faker::Lorem.sentence }
    tag_line { Faker::Lorem.sentence }
    content { Faker::Lorem.paragraph }
    email { Faker::Internet.email }
    phone { Faker::PhoneNumber.phone_number }
    pr_type { 1 }
    association :mall
    display_at { 1.day.ago }
    is_active { true }
  end
end
