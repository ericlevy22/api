# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :location do
    time_zone { 'America/Denver' }
    latitude { Faker::Address.latitude }
    longitude { Faker::Address.longitude }

    trait :with_distance do
      latitude { 39.74585 }
      longitude { -104.998929 }
      distance { 3.333 }
    end

    factory :location_with_distance, traits: [:with_distance]
  end
end
