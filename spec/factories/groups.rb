FactoryGirl.define do
  factory :group do
    name { Faker::Lorem.sentence(3) }
  end
end
