# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :article do
    seo_slug { title.try(:slugify) }
    author { Faker::Name.name }
    attribution { Faker::Name.name }
    title { Faker::Lorem.sentence }
    subtitle { Faker::Lorem.sentence }
    short_title { Faker::Lorem.sentence }
    teaser { Faker::Lorem.sentence }
    content { Faker::Lorem.paragraph }
    is_active true
    display_at { 1.day.ago }
    end_at { 1.week.from_now }
  end
end
