# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :social_feed do
    social_network_id 1
    sociable_type 'Mall'
    sociable_id 1
    properties { { 'url' => 'https://myface.com/Mall1', 'appID' => '0123abcf0123abcf0123abcf0123abcf' } }
    association :social_network
  end
end
