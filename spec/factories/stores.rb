# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :store do
    name { Faker::Company.name }
    sort_name { name }
    seo_slug { name.try(:slugify) }
    description { Faker::Lorem.sentence }
    address { Faker::Address.street_address }
    city { Faker::Address.city }
    state { Faker::Address.state_abbr }
    zip { Faker::Address.zip_code }
    phone { Faker::PhoneNumber.phone_number }
    url { Faker::Internet.url }
    url_text { Faker::Internet.url }
    hours { 'Sunday-Thursday: 11AM- Midnight <br>Friday & Saturday: 11AM - 2AM' }
    tenant_location { 'Downstairs by the Food Court' }
    tenant_location_map_key { 'C-37' }
    opening_date { Faker::Date.between(2.days.ago, Date.tomorrow) }
    opening_text { Faker::Lorem.sentence(3) }

    association :mall
    association :retailer
    association :location
  end
end
