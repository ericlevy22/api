# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :company do
    name { Faker::Company.name }
    nick_name { Faker::Lorem.word }
    seo_slug { name.try(:slugify) }
    address { Faker::Address.street_address }
    city { Faker::Address.city }
    state { Faker::Address.state_abbr }
    zip { Faker::Address.zip }
    phone { Faker::PhoneNumber.phone_number }
    fax { Faker::PhoneNumber.phone_number }
    url { Faker::Internet.url }
    test_url { Faker::Internet.url }
    is_active { true }
  end
end
