FactoryGirl.define do
  factory :theater do
    association :store
    mall nil
    external_key { rand(10_000).to_s }
  end
end
