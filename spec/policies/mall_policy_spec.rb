describe MallPolicy do
  subject { described_class }

  before(:all) do
    @mall        = create(:mall)
    @client      = create(:client, resource: @mall)
    @sales       = create(:sales)
    @mall_client = create(:mall_client)
    @unconfirmed = create(:unconfirmed)
    @user        = create(:account)
    @admin       = create(:admin)
  end

  permissions :create? do
    it 'allows admins to create malls' do
      expect(subject).to permit(@admin, @mall)
    end

    it 'does not allow non-admins to create malls' do
      expect(subject).to_not permit(@client, @mall)
      expect(subject).to_not permit(@sales, @mall)
      expect(subject).to_not permit(@mall_client, @mall)
      expect(subject).to_not permit(@unconfirmed, @mall)
      expect(subject).to_not permit(@user, @mall)
    end
  end

  permissions :update? do
    it 'allows admins to update malls' do
      expect(subject).to permit(@admin, @mall)
    end

    it 'does not allow non-admins to update malls' do
      expect(subject).to_not permit(@client, @mall)
      expect(subject).to_not permit(@sales, @mall)
      expect(subject).to_not permit(@mall_client, @mall)
      expect(subject).to_not permit(@unconfirmed, @mall)
      expect(subject).to_not permit(@user, @mall)
    end
  end

  permissions :destroy? do
    it 'allows admins to destroy malls' do
      expect(subject).to permit(@admin, @mall)
    end

    it 'does not allow non-admins to destroy malls' do
      expect(subject).to_not permit(@client, @mall)
      expect(subject).to_not permit(@sales, @mall)
      expect(subject).to_not permit(@mall_client, @mall)
      expect(subject).to_not permit(@unconfirmed, @mall)
      expect(subject).to_not permit(@user, @mall)
    end
  end
end
