describe RetailerPolicy do
  subject { described_class }

  before(:all) do
    @retailer     = FactoryGirl.create(:retailer)
    @client       = FactoryGirl.create(:client)
    @sales        = FactoryGirl.create(:sales)
    @mall_client  = FactoryGirl.create(:mall_client)
    @unconfirmed  = FactoryGirl.create(:unconfirmed)
    @user         = FactoryGirl.create(:account)
    @admin        = FactoryGirl.create(:admin)
  end

  permissions :create? do
    it 'allows admins to create retailers' do
      expect(subject).to permit(@admin, @retailer)
    end

    it 'does not allow non-admins to create retailers' do
      expect(subject).to_not permit(@client, @retailer)
      expect(subject).to_not permit(@sales, @retailer)
      expect(subject).to_not permit(@mall_client, @retailer)
      expect(subject).to_not permit(@unconfirmed, @retailer)
      expect(subject).to_not permit(@user, @retailer)
    end
  end

  permissions :update? do
    it 'allows admins to update retailers' do
      expect(subject).to permit(@admin, @retailer)
    end

    it 'does not allow non-admins to update retailers' do
      expect(subject).to_not permit(@client, @retailer)
      expect(subject).to_not permit(@sales, @retailer)
      expect(subject).to_not permit(@mall_client, @retailer)
      expect(subject).to_not permit(@unconfirmed, @retailer)
      expect(subject).to_not permit(@user, @retailer)
    end
  end

  permissions :destroy? do
    it 'allows admins to destroy retailers' do
      expect(subject).to permit(@admin, @retailer)
    end

    it 'does not allow non-admins to destroy retailers' do
      expect(subject).to_not permit(@client, @retailer)
      expect(subject).to_not permit(@sales, @retailer)
      expect(subject).to_not permit(@mall_client, @retailer)
      expect(subject).to_not permit(@unconfirmed, @retailer)
      expect(subject).to_not permit(@user, @retailer)
    end
  end
end
