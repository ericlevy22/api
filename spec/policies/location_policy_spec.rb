describe LocationPolicy do
  subject { described_class }

  before(:all) do
    @location     = FactoryGirl.create(:location)
    @client       = FactoryGirl.create(:client)
    @sales        = FactoryGirl.create(:sales)
    @mall_client  = FactoryGirl.create(:mall_client)
    @unconfirmed  = FactoryGirl.create(:unconfirmed)
    @user         = FactoryGirl.create(:account)
    @admin        = FactoryGirl.create(:admin)
  end

  permissions :create? do
    it 'allows admins to create locations' do
      expect(subject).to permit(@admin, @location)
    end

    it 'does not allow non-admins to create locations' do
      expect(subject).to_not permit(@client, @location)
      expect(subject).to_not permit(@sales, @location)
      expect(subject).to_not permit(@mall_client, @location)
      expect(subject).to_not permit(@unconfirmed, @location)
      expect(subject).to_not permit(@user, @location)
    end
  end

  permissions :update? do
    it 'allows admins to update locations' do
      expect(subject).to permit(@admin, @location)
    end

    it 'does not allow non-admins to update locations' do
      expect(subject).to_not permit(@client, @location)
      expect(subject).to_not permit(@sales, @location)
      expect(subject).to_not permit(@mall_client, @location)
      expect(subject).to_not permit(@unconfirmed, @location)
      expect(subject).to_not permit(@user, @location)
    end
  end

  permissions :destroy? do
    it 'allows admins to destroy locations' do
      expect(subject).to permit(@admin, @location)
    end

    it 'does not allow non-admins to destroy locations' do
      expect(subject).to_not permit(@client, @location)
      expect(subject).to_not permit(@sales, @location)
      expect(subject).to_not permit(@mall_client, @location)
      expect(subject).to_not permit(@unconfirmed, @location)
      expect(subject).to_not permit(@user, @location)
    end
  end
end
