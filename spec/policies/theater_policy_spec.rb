describe TheaterPolicy do
  subject { described_class }

  let(:mall)             { create(:mall) }
  let(:theater)            { create(:theater, mall: mall) }
  let(:disallowed_mall)  { create(:mall) }
  let(:disallowed_theater) { create(:theater, mall: disallowed_mall) }

  let(:client)      { create(:client, resource: mall) }
  let(:mall_client) { create(:mall_client, resource: mall) }
  let(:sales)       { create(:sales) }
  let(:unconfirmed) { create(:unconfirmed) }
  let(:user)        { create(:account) }
  let(:admin)       { create(:admin) }

  permissions :scope do
    it 'allows a client with store_channel to access theaters at its mall' do
      client.add_role(:store_channel)
      client.remove_role(:mall_channel)
      expect(Pundit.policy_scope(client, mall.theaters)).to include(theater)
    end

    it 'allows a client with mall_channel to access theaters at its mall' do
      client.add_role(:mall_channel)
      client.remove_role(:store_channel)
      expect(Pundit.policy_scope(client, mall.theaters)).to include(theater)
    end

    it 'does not allow a client to access theaters at other malls' do
      expect(Pundit.policy_scope(client, disallowed_mall.theaters)).to_not include(disallowed_theater)
    end
  end

  permissions :create? do
    it 'allows admins to create theaters' do
      expect(subject).to permit(admin, theater)
    end

    it 'does not allow non-admins to create theaters' do
      expect(subject).to_not permit(client, theater)
      expect(subject).to_not permit(sales, theater)
      expect(subject).to_not permit(mall_client, theater)
      expect(subject).to_not permit(unconfirmed, theater)
      expect(subject).to_not permit(user, theater)
    end
  end

  permissions :update? do
    it 'allows admins to update theaters' do
      expect(subject).to permit(admin, theater)
    end

    it 'does not allow non-admins to update theaters' do
      expect(subject).to_not permit(client, theater)
      expect(subject).to_not permit(sales, theater)
      expect(subject).to_not permit(mall_client, theater)
      expect(subject).to_not permit(unconfirmed, theater)
      expect(subject).to_not permit(user, theater)
    end
  end

  permissions :destroy? do
    it 'allows admins to destroy theaters' do
      expect(subject).to permit(admin, theater)
    end

    it 'does not allow non-admins to destroy theaters' do
      expect(subject).to_not permit(client, theater)
      expect(subject).to_not permit(sales, theater)
      expect(subject).to_not permit(mall_client, theater)
      expect(subject).to_not permit(unconfirmed, theater)
      expect(subject).to_not permit(user, theater)
    end
  end
end
