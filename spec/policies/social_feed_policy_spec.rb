describe SocialFeedPolicy do
  subject { described_class }

  let(:allowed_mall_social_feed)    { create(:social_feed) }
  let(:disallowed_mall_social_feed) { create(:social_feed) }

  let(:allowed_store_social_feed)    { create(:social_feed) }
  let(:disallowed_store_social_feed) { create(:social_feed) }

  let(:allowed_retailer_social_feed)    { create(:social_feed) }
  let(:disallowed_retailer_social_feed) { create(:social_feed) }

  let(:allowed_mall)    { create(:mall, social_feeds: [allowed_mall_social_feed]) }
  let(:disallowed_mall) { create(:mall, social_feeds: [disallowed_mall_social_feed]) }

  let!(:allowed_retailer)    { create(:retailer, social_feeds: [allowed_retailer_social_feed]) }
  let!(:disallowed_retailer) { create(:retailer, social_feeds: [disallowed_retailer_social_feed]) }
  let!(:allowed_store)       { create(:store, retailer: allowed_retailer, mall: allowed_mall, social_feeds: [allowed_store_social_feed]) }
  let!(:disallowed_store)    { create(:store, retailer: disallowed_retailer, mall: disallowed_mall, social_feeds: [disallowed_store_social_feed]) }

  let(:admin)        { create(:admin) }
  let(:sales)        { create(:sales) }
  let(:client)       { create(:client, resource: allowed_mall) }
  let(:mall_client)  { create(:mall_client, resource: allowed_mall) }
  let(:unconfirmed)  { create(:unconfirmed) }
  let(:user)         { create(:account) }

  permissions :scope do
    it 'allows admins and sales to access all social_feeds' do
      expect(Pundit.policy_scope(admin, SocialFeed.all)).to include(allowed_mall_social_feed, disallowed_mall_social_feed)
      expect(Pundit.policy_scope(sales, SocialFeed.all)).to include(allowed_mall_social_feed, disallowed_mall_social_feed)
    end

    it 'allows clients with mall_channel to access their social_feeds' do
      client.add_role(:mall_channel)
      expect(Pundit.policy_scope(client, SocialFeed.all)).to include(allowed_mall_social_feed)
    end

    it 'does not allow clients with mall_channel to access other social_feeds' do
      client.add_role(:mall_channel)
      expect(Pundit.policy_scope(client, SocialFeed.all)).to_not include(disallowed_mall_social_feed)
    end

    it 'does not allow clients without mall_channel to access social_feeds' do
      client.remove_role(:mall_channel)
      client.remove_role(:store_channel)
      expect(Pundit.policy_scope(client, SocialFeed.all)).to be_empty
    end

    it 'allows clients with store_channel to access their social_feeds' do
      client.add_role(:store_channel)
      expect(Pundit.policy_scope(client, SocialFeed.all)).to include(allowed_store_social_feed)
      expect(Pundit.policy_scope(client, SocialFeed.all)).to include(allowed_retailer_social_feed)
    end

    it 'does not allow clients with store_channel to access other social_feeds' do
      client.add_role(:store_channel)
      expect(Pundit.policy_scope(client, SocialFeed.all)).to_not include(disallowed_store_social_feed)
      expect(Pundit.policy_scope(client, SocialFeed.all)).to_not include(disallowed_retailer_social_feed)
    end

    it 'does not allow clients without store_channel to access social_feeds' do
      client.remove_role(:store_channel)
      client.remove_role(:mall_channel)
      expect(Pundit.policy_scope(client, SocialFeed.all)).to be_empty
    end
  end

  permissions :create? do
    it 'allows admins to create social feeds' do
      expect(subject).to permit(admin, allowed_mall_social_feed)
    end

    it 'does not allow non-admins to create social feeds' do
      expect(subject).to_not permit(client, allowed_mall_social_feed)
      expect(subject).to_not permit(sales, allowed_mall_social_feed)
      expect(subject).to_not permit(mall_client, allowed_mall_social_feed)
      expect(subject).to_not permit(unconfirmed, allowed_mall_social_feed)
      expect(subject).to_not permit(user, allowed_mall_social_feed)
    end
  end

  permissions :update? do
    it 'allows admins to update social feeds' do
      expect(subject).to permit(admin, allowed_mall_social_feed)
    end

    it 'does not allow non-admins to update social feeds' do
      expect(subject).to_not permit(client, allowed_mall_social_feed)
      expect(subject).to_not permit(sales, allowed_mall_social_feed)
      expect(subject).to_not permit(mall_client, allowed_mall_social_feed)
      expect(subject).to_not permit(unconfirmed, allowed_mall_social_feed)
      expect(subject).to_not permit(user, allowed_mall_social_feed)
    end
  end

  permissions :destroy? do
    it 'allows admins to destroy social feeds' do
      expect(subject).to permit(admin, allowed_mall_social_feed)
    end

    it 'does not allow non-admins to destroy social feeds' do
      expect(subject).to_not permit(client, allowed_mall_social_feed)
      expect(subject).to_not permit(sales, allowed_mall_social_feed)
      expect(subject).to_not permit(mall_client, allowed_mall_social_feed)
      expect(subject).to_not permit(unconfirmed, allowed_mall_social_feed)
      expect(subject).to_not permit(user, allowed_mall_social_feed)
    end
  end
end
