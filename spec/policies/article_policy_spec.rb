describe ArticlePolicy do
  subject { described_class }

  before(:all) do
    @article     = FactoryGirl.create(:article)
    @mall        = FactoryGirl.create(:mall)
    @admin       = FactoryGirl.create(:admin)
    @client      = FactoryGirl.create(:account)
    @sales       = FactoryGirl.create(:sales)
    @mall_client = FactoryGirl.create(:mall_client)
    @user        = FactoryGirl.create(:account)

    @disallowed_article = FactoryGirl.create(:article)
    @client.add_role :client, @mall
    @client.add_role :editorial_channel
    @mall.articles << @article
  end

  permissions :scope do
    it 'allows an admin to see all articles' do
      expect(Pundit.policy_scope(@admin, Article.all)).to include(@article)
      expect(Pundit.policy_scope(@admin, Article.all)).to include(@disallowed_article)
    end

    it 'allows a client with editorial_channel access to see articles for their mall' do
      expect(Pundit.policy_scope(@client, Article.all)).to include(@article)
      expect(Pundit.policy_scope(@client, Article.all)).to_not include(@disallowed_article)
    end

    it 'does not allow non-article-channel clients to see articles' do
      expect(Pundit.policy_scope(FactoryGirl.create(:client), Article.all)).to_not include(@article)
      expect(Pundit.policy_scope(FactoryGirl.create(:client), Article.all)).to_not include(@disallowed_article)
    end

    it 'does not allow non-clients or non-admins to see articles' do
      expect(Pundit.policy_scope(@sales, Article.all)).to_not include(@article)
      expect(Pundit.policy_scope(@sales, Article.all)).to_not include(@disallowed_article)
      expect(Pundit.policy_scope(@mall_client, Article.all)).to_not include(@article)
      expect(Pundit.policy_scope(@mall_client, Article.all)).to_not include(@disallowed_article)
      expect(Pundit.policy_scope(@user, Article.all)).to_not include(@article)
      expect(Pundit.policy_scope(@user, Article.all)).to_not include(@disallowed_article)
    end
  end

  permissions :create? do
    it 'allows admins to create articles' do
      expect(subject).to permit(@admin, @article)
    end

    it 'does not allow non-admins to create articles' do
      expect(subject).to_not permit(@client, @article)
      expect(subject).to_not permit(@sales, @article)
      expect(subject).to_not permit(@mall_client, @article)
      expect(subject).to_not permit(@user, @article)
    end
  end

  permissions :update? do
    it 'allows admins to update articles' do
      expect(subject).to permit(@admin, @article)
    end

    it 'does not allow non-admins to update articles' do
      expect(subject).to_not permit(@client, @article)
      expect(subject).to_not permit(@sales, @article)
      expect(subject).to_not permit(@mall_client, @article)
      expect(subject).to_not permit(@user, @article)
    end
  end

  permissions :destroy? do
    it 'allows admins to destroy articles' do
      expect(subject).to permit(@admin, @article)
    end

    it 'does not allow non-admins to destroy articles' do
      expect(subject).to_not permit(@client, @article)
      expect(subject).to_not permit(@sales, @article)
      expect(subject).to_not permit(@mall_client, @article)
      expect(subject).to_not permit(@user, @article)
    end
  end
end
