require 'spec_helper'

describe HourPolicy do
  subject { described_class }

  let(:allowed_hour)    { create(:hour) }
  let(:disallowed_hour) { create(:hour) }

  let(:allowed_mall)    { create(:mall, hours: [allowed_hour]) }
  let(:disallowed_mall) { create(:mall, hours: [disallowed_hour]) }

  let(:admin)       { create(:admin) }
  let(:client)      { create(:client, resource: allowed_mall) }
  let(:mall_client) { create(:mall_client, resource: allowed_mall) }

  permissions ".scope" do
    it 'allows admins to see all hours' do
      expect(Pundit.policy_scope(admin, Hour.all)).to include(allowed_hour, disallowed_hour)
    end

    it 'allows clients with mall_channel to see hours for their malls' do
      client.add_role :mall_channel
      expect(Pundit.policy_scope(client, Hour.all)).to include(allowed_hour)
    end

    it 'does not allow clients with mall_channel to see hours for other malls' do
      client.add_role :mall_channel
      expect(Pundit.policy_scope(client, Hour.all)).to_not include(disallowed_hour)
    end

    it 'does not allow clients without mall_channel to see hours' do
      client.remove_role :mall_channel
      expect(Pundit.policy_scope(client, Hour.all)).to be_empty
    end

    it 'allows mall_clients to see hours for their malls' do
      expect(Pundit.policy_scope(mall_client, allowed_mall.hours)).to include(allowed_hour)
    end

    it 'does not allow mall_clients to see hours for other malls' do
      expect(Pundit.policy_scope(mall_client, disallowed_mall.hours)).to be_empty
    end
  end
end
