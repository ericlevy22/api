require 'spec_helper'

describe JobListingPolicy do
  subject { described_class }

  let(:allowed_job_listing)    { create(:job_listing) }
  let(:disallowed_job_listing) { create(:job_listing) }

  let(:allowed_mall)    { create(:mall, stores: [create(:store, job_listings: [allowed_job_listing])]) }
  let(:disallowed_mall) { create(:mall, stores: [create(:store, job_listings: [disallowed_job_listing])]) }

  let(:admin)       { create(:admin) }
  let(:client)      { create(:client, resource: allowed_mall) }
  let(:mall_client) { create(:mall_client, resource: allowed_mall) }

  permissions ".scope" do
    it 'allows admins to see all job_listings' do
      expect(Pundit.policy_scope(admin, JobListing.all)).to include(allowed_job_listing, disallowed_job_listing)
    end

    it 'allows clients with mall_channel to see job_listings for their malls' do
      client.add_role :mall_channel
      expect(Pundit.policy_scope(client, JobListing.all)).to include(allowed_job_listing)
    end

    it 'does not allow clients with mall_channel to see job_listings for other malls' do
      client.add_role :mall_channel
      expect(Pundit.policy_scope(client, JobListing.all)).to_not include(disallowed_job_listing)
    end

    it 'does not allow clients without mall_channel to see job_listings' do
      client.remove_role :mall_channel
      expect(Pundit.policy_scope(client, JobListing.all)).to be_empty
    end

    it 'allows mall_clients to see job_listings for their malls' do
      expect(Pundit.policy_scope(mall_client, allowed_mall.job_listings)).to include(allowed_job_listing)
    end

    it 'does not allow mall_clients to see job_listings for other malls' do
      expect(Pundit.policy_scope(mall_client, disallowed_mall.job_listings)).to be_empty
    end
  end
end
