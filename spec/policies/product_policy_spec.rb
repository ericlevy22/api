describe ProductPolicy do
  subject { described_class }

  before(:all) do
    @product      = FactoryGirl.create(:product)
    @client       = FactoryGirl.create(:client)
    @sales        = FactoryGirl.create(:sales)
    @mall_client  = FactoryGirl.create(:mall_client)
    @unconfirmed  = FactoryGirl.create(:unconfirmed)
    @user         = FactoryGirl.create(:account)
    @admin        = FactoryGirl.create(:admin)

    @disallowed_product = FactoryGirl.create(:product)
    @product_channel    = FactoryGirl.create(:client, resource: @product)
  end

  permissions :scope do
    it 'allows admins to access all products' do
      expect(Pundit.policy_scope(@admin, Product.all)).to include(@product)
      expect(Pundit.policy_scope(@admin, Product.all)).to include(@disallowed_product)
    end

    it 'allows product channel clients to access their products' do
      expect(Pundit.policy_scope(@product_channel, Product.all)).to include(@product)
      expect(Pundit.policy_scope(@product_channel, Product.all)).to_not include(@disallowed_product)
    end

    it 'does not allow non-admins or non-product channel clients to access any products' do
      expect(Pundit.policy_scope(@client, Product.all)).to_not include(@product)
      expect(Pundit.policy_scope(@client, Product.all)).to_not include(@disallowed_product)
      expect(Pundit.policy_scope(@sales, Product.all)).to_not include(@product)
      expect(Pundit.policy_scope(@sales, Product.all)).to_not include(@disallowed_product)
      expect(Pundit.policy_scope(@mall_client, Product.all)).to_not include(@product)
      expect(Pundit.policy_scope(@mall_client, Product.all)).to_not include(@disallowed_product)
      expect(Pundit.policy_scope(@unconfirmed, Product.all)).to_not include(@product)
      expect(Pundit.policy_scope(@unconfirmed, Product.all)).to_not include(@disallowed_product)
      expect(Pundit.policy_scope(@user, Product.all)).to_not include(@product)
      expect(Pundit.policy_scope(@user, Product.all)).to_not include(@disallowed_product)
    end
  end

  permissions :create? do
    it 'allows admins to create products' do
      expect(subject).to permit(@admin, @product)
    end

    it 'does not allow non-admins to create products' do
      expect(subject).to_not permit(@client, @product)
      expect(subject).to_not permit(@sales, @product)
      expect(subject).to_not permit(@mall_client, @product)
      expect(subject).to_not permit(@unconfirmed, @product)
      expect(subject).to_not permit(@user, @product)
      expect(subject).to_not permit(@product_channel, @product)
    end
  end

  permissions :update? do
    it 'allows admins to update products' do
      expect(subject).to permit(@admin, @product)
    end

    it 'does not allow non-admins to update products' do
      expect(subject).to_not permit(@client, @product)
      expect(subject).to_not permit(@sales, @product)
      expect(subject).to_not permit(@mall_client, @product)
      expect(subject).to_not permit(@unconfirmed, @product)
      expect(subject).to_not permit(@user, @product)
      expect(subject).to_not permit(@product_channel, @product)
    end
  end

  permissions :destroy? do
    it 'allows admins to destroy products' do
      expect(subject).to permit(@admin, @product)
    end

    it 'does not allow non-admins to destroy products' do
      expect(subject).to_not permit(@client, @product)
      expect(subject).to_not permit(@sales, @product)
      expect(subject).to_not permit(@mall_client, @product)
      expect(subject).to_not permit(@unconfirmed, @product)
      expect(subject).to_not permit(@user, @product)
      expect(subject).to_not permit(@product_channel, @product)
    end
  end
end
