describe CompanyPolicy do
  subject { described_class }

  before(:all) do
    @company     = FactoryGirl.create(:company)
    @admin       = FactoryGirl.create(:admin)
    @client      = FactoryGirl.create(:account)
    @sales       = FactoryGirl.create(:sales)
    @mall_client = FactoryGirl.create(:mall_client)
    @user        = FactoryGirl.create(:account)
    @unconfirmed = FactoryGirl.create(:unconfirmed)
  end

  permissions :create? do
    it 'allows admins to create companies' do
      expect(subject).to permit(@admin, @company)
    end

    it 'does not allow non-admins to create companies' do
      expect(subject).to_not permit(@client, @company)
      expect(subject).to_not permit(@sales, @company)
      expect(subject).to_not permit(@mall_client, @company)
      expect(subject).to_not permit(@user, @company)
      expect(subject).to_not permit(@unconfirmed, @contact)
    end
  end

  permissions :update? do
    it 'allows admins to update companies' do
      expect(subject).to permit(@admin, @company)
    end

    it 'does not allow non-admins to update companies' do
      expect(subject).to_not permit(@client, @company)
      expect(subject).to_not permit(@sales, @company)
      expect(subject).to_not permit(@mall_client, @company)
      expect(subject).to_not permit(@user, @company)
      expect(subject).to_not permit(@unconfirmed, @contact)
    end
  end

  permissions :destroy? do
    it 'allows admins to destroy companies' do
      expect(subject).to permit(@admin, @company)
    end

    it 'does not allow non-admins to destroy companies' do
      expect(subject).to_not permit(@client, @company)
      expect(subject).to_not permit(@sales, @company)
      expect(subject).to_not permit(@mall_client, @company)
      expect(subject).to_not permit(@user, @company)
      expect(subject).to_not permit(@unconfirmed, @contact)
    end
  end
end
