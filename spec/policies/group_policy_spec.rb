describe GroupPolicy do
  subject { described_class }

  let(:group)       { create(:group) }
  let(:client)      { create(:client) }
  let(:sales)       { create(:sales) }
  let(:mall_client) { create(:mall_client) }
  let(:unconfirmed) { create(:unconfirmed) }
  let(:user)        { create(:account) }
  let(:admin)       { create(:admin) }

  permissions :create? do
    it 'allows admins to create groups' do
      expect(subject).to permit(admin, group)
    end

    it 'does not allow non-admins to create groups' do
      expect(subject).to_not permit(client, group)
      expect(subject).to_not permit(sales, group)
      expect(subject).to_not permit(mall_client, group)
      expect(subject).to_not permit(unconfirmed, group)
      expect(subject).to_not permit(user, group)
    end
  end
end
