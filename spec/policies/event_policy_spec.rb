require 'spec_helper'

describe EventPolicy do
  subject { described_class }

  let(:allowed_event)    { create(:event) }
  let(:disallowed_event) { create(:event) }

  let(:allowed_mall)    { create(:mall, events: [allowed_event]) }
  let(:disallowed_mall) { create(:mall, events: [disallowed_event]) }

  let(:admin)       { create(:admin) }
  let(:client)      { create(:client, resource: allowed_mall) }
  let(:mall_client) { create(:mall_client, resource: allowed_mall) }

  permissions ".scope" do
    it 'allows admins to see all events' do
      expect(Pundit.policy_scope(admin, Event.all)).to include(allowed_event, disallowed_event)
    end

    it 'allows clients with mall_channel to see events for their malls' do
      client.add_role :mall_channel
      expect(Pundit.policy_scope(client, Event.all)).to include(allowed_event)
    end

    it 'does not allow clients mall_channel to see events for other malls' do
      client.add_role :mall_channel
      expect(Pundit.policy_scope(client, Event.all)).to_not include(disallowed_event)
    end

    it 'does not allow clients without mall_channel to see events' do
      client.remove_role :mall_channel
      expect(Pundit.policy_scope(client, Event.all)).to be_empty
    end

    it 'allows mall_clients to see events for their malls' do
      expect(Pundit.policy_scope(mall_client, allowed_mall.events)).to include(allowed_event)
    end

    it 'does not allow mall_clients to see events for other malls' do
      expect(Pundit.policy_scope(mall_client, disallowed_mall.events)).to be_empty
    end
  end
end
