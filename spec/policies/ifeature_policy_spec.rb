require 'spec_helper'

describe IfeaturePolicy do
  subject { described_class }

  let(:allowed_ifeature)    { create(:ifeature) }
  let(:disallowed_ifeature) { create(:ifeature) }

  let(:allowed_mall)    { create(:mall, ifeatures: [allowed_ifeature]) }
  let(:disallowed_mall) { create(:mall, ifeatures: [disallowed_ifeature]) }

  let(:admin)       { create(:admin) }
  let(:client)      { create(:client, resource: allowed_mall) }
  let(:mall_client) { create(:mall_client, resource: allowed_mall) }

  permissions ".scope" do
    it 'allows admins to see all ifeatures' do
      expect(Pundit.policy_scope(admin, Ifeature.all)).to include(allowed_ifeature, disallowed_ifeature)
    end

    it 'allows clients with mall_channel to see ifeatures for their malls' do
      client.add_role :mall_channel
      expect(Pundit.policy_scope(client, Ifeature.all)).to include(allowed_ifeature)
    end

    it 'does not allow clients with mall_channel to see ifeatures for other malls' do
      client.add_role :mall_channel
      expect(Pundit.policy_scope(client, Ifeature.all)).to_not include(disallowed_ifeature)
    end

    it 'does not allow clients without mall_channel to see ifeatures' do
      client.remove_role :mall_channel
      expect(Pundit.policy_scope(client, Ifeature.all)).to be_empty
    end

    it 'allows mall_clients to see ifeatures for their malls' do
      expect(Pundit.policy_scope(mall_client, allowed_mall.ifeatures)).to include(allowed_ifeature)
    end

    it 'does not allow mall_clients to see ifeatures for other malls' do
      expect(Pundit.policy_scope(mall_client, disallowed_mall.ifeatures)).to be_empty
    end
  end
end
