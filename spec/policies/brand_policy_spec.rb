describe BrandPolicy do
  subject { described_class }

  before(:all) do
    @brand        = FactoryGirl.create(:brand)
    @client       = FactoryGirl.create(:client)
    @sales        = FactoryGirl.create(:sales)
    @mall_client  = FactoryGirl.create(:mall_client)
    @unconfirmed  = FactoryGirl.create(:unconfirmed)
    @admin        = FactoryGirl.create(:admin)
    @user         = FactoryGirl.create(:account)
  end

  permissions :update? do
    it 'allows admins to update brands' do
      expect(subject).to permit(@admin, @brand)
    end

    it 'does not allow non-admins to update brands' do
      expect(subject).to_not permit(@client, @brand)
      expect(subject).to_not permit(@sales, @brand)
      expect(subject).to_not permit(@mall_client, @brand)
      expect(subject).to_not permit(@unconfirmed, @brand)
      expect(subject).to_not permit(@user, @brand)
    end
  end

  permissions :create? do
    it 'allows admins to create brands' do
      expect(subject).to permit(@admin, @brand)
    end

    it 'does not allow non-admins to create brands' do
      expect(subject).to_not permit(@client, @brand)
      expect(subject).to_not permit(@sales, @brand)
      expect(subject).to_not permit(@mall_client, @brand)
      expect(subject).to_not permit(@unconfirmed, @brand)
      expect(subject).to_not permit(@user, @brand)
    end
  end

  permissions :destroy? do
    it 'allows admins to destroy brands' do
      expect(subject).to permit(@admin, @brand)
    end

    it 'does not allow non-admins to destroy brands' do
      expect(subject).to_not permit(@client, @brand)
      expect(subject).to_not permit(@sales, @brand)
      expect(subject).to_not permit(@mall_client, @brand)
      expect(subject).to_not permit(@unconfirmed, @brand)
      expect(subject).to_not permit(@user, @brand)
    end
  end
end
