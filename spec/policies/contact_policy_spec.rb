describe ContactPolicy do
  subject { described_class }

  let(:contact)            { create(:contact) }
  let(:disallowed_contact) { create(:contact) }
  let(:allowed_mall)       { create(:mall, contacts: [contact]) }
  let(:disallowed_mall)    { create(:mall, contacts: [disallowed_contact]) }

  let(:admin)        { create(:admin) }
  let(:client)       { create(:client) }
  let(:sales)        { create(:sales) }
  let(:client)       { create(:client, resource: allowed_mall) }
  let(:mall_channel) { create(:client, :mall_channel, resource: allowed_mall) }
  let(:mall_client)  { create(:mall_client, resource: allowed_mall) }
  let(:user)         { create(:account) }
  let(:unconfirmed)  { create(:unconfirmed) }

  permissions :scope do
    it 'allows a mall_client to access its contacts' do
      expect(Pundit.policy_scope(mall_client, allowed_mall.contacts)).to include(contact)
    end

    it 'allows a client with mall_channel to access its contacts' do
      expect(Pundit.policy_scope(mall_channel, allowed_mall.contacts)).to include(contact)
    end

    it 'does not allow a client with mall_channel to access other contacts' do
      expect(Pundit.policy_scope(mall_channel, disallowed_mall.contacts)).to_not include(disallowed_contact)
    end

    it 'does not allow a client without mall_channel to access its contacts' do
      expect(Pundit.policy_scope(client, allowed_mall.contacts)).to_not include(contact)
    end

    it 'does not allow a mall_client to access contacts from other malls' do
      expect(Pundit.policy_scope(mall_client, disallowed_mall.contacts)).to be_empty
    end
  end

  permissions :create? do
    it 'allows admins to create contacts' do
      expect(subject).to permit(admin, contact)
    end

    it 'does not allow non-admins to create contacts' do
      expect(subject).to_not permit(client, contact)
      expect(subject).to_not permit(sales, contact)
      expect(subject).to_not permit(mall_client, contact)
      expect(subject).to_not permit(user, contact)
      expect(subject).to_not permit(unconfirmed, contact)
    end
  end

  permissions :update? do
    it 'allows admins to update contacts' do
      expect(subject).to permit(admin, contact)
    end

    it 'does not allow non-admins to update contacts' do
      expect(subject).to_not permit(client, contact)
      expect(subject).to_not permit(sales, contact)
      expect(subject).to_not permit(mall_client, contact)
      expect(subject).to_not permit(user, contact)
      expect(subject).to_not permit(unconfirmed, contact)
    end
  end

  permissions :destroy? do
    it 'allows admins to destroy contacts' do
      expect(subject).to permit(admin, contact)
    end

    it 'does not allow non-admins to destroy contacts' do
      expect(subject).to_not permit(client, contact)
      expect(subject).to_not permit(sales, contact)
      expect(subject).to_not permit(mall_client, contact)
      expect(subject).to_not permit(user, contact)
      expect(subject).to_not permit(unconfirmed, contact)
    end
  end
end
