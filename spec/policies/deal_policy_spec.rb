describe DealPolicy do
  subject { described_class }

  before(:all) do
    @deal         = FactoryGirl.create(:deal)
    @client       = FactoryGirl.create(:client)
    @sales        = FactoryGirl.create(:sales)
    @mall_client  = FactoryGirl.create(:mall_client)
    @unconfirmed  = FactoryGirl.create(:unconfirmed)
    @user         = FactoryGirl.create(:account)
    @admin        = FactoryGirl.create(:admin)
    @mall         = FactoryGirl.create(:mall)
    @deal_channel = FactoryGirl.create(:client, resource: @mall)

    @store = FactoryGirl.create(:store, mall: @mall)
    @store.deals << @deal
    @disallowed_deal = FactoryGirl.create(:deal)
    @deal_channel.add_role :deal_channel
  end

  permissions :scope do
    it 'allows admins to see all deals' do
      expect(Pundit.policy_scope(@admin, Deal.all)).to include(@deal)
      expect(Pundit.policy_scope(@admin, Deal.all)).to include(@disallowed_deal)
    end

    it 'allows client with deal_channel privileges to only see their deals' do
      expect(Pundit.policy_scope(@deal_channel, Deal.all)).to include(@deal)
      expect(Pundit.policy_scope(@deal_channel, Deal.all)).to_not include(@disallowed_deal)
    end

    it 'does not allow clients without deal_channel privileges to see deals' do
      expect(Pundit.policy_scope(@client, Deal.all)).to_not include(@deal)
      expect(Pundit.policy_scope(@client, Deal.all)).to_not include(@disallowed_deal)
    end

    it 'does not allow non-clients to see any deals' do
      expect(Pundit.policy_scope(@sales, Deal.all)).to_not include(@deal)
      expect(Pundit.policy_scope(@sales, Deal.all)).to_not include(@disallowed_deal)
      expect(Pundit.policy_scope(@mall_client, Deal.all)).to_not include(@deal)
      expect(Pundit.policy_scope(@mall_client, Deal.all)).to_not include(@disallowed_deal)
      expect(Pundit.policy_scope(@unconfirmed, Deal.all)).to_not include(@deal)
      expect(Pundit.policy_scope(@unconfirmed, Deal.all)).to_not include(@disallowed_deal)
      expect(Pundit.policy_scope(@user, Deal.all)).to_not include(@deal)
      expect(Pundit.policy_scope(@user, Deal.all)).to_not include(@disallowed_deal)
    end
  end

  permissions :create? do
    it 'allows admins to create deals' do
      expect(subject).to permit(@admin, @deal)
    end

    it 'does not allow non-admins to create deals' do
      expect(subject).to_not permit(@client, @deal)
      expect(subject).to_not permit(@sales, @deal)
      expect(subject).to_not permit(@mall_client, @deal)
      expect(subject).to_not permit(@unconfirmed, @deal)
      expect(subject).to_not permit(@user, @deal)
      expect(subject).to_not permit(@deal_channel, @deal)
    end
  end

  permissions :update? do
    it 'allows admins to update deals' do
      expect(subject).to permit(@admin, @deal)
    end

    it 'does not allow non-admins to update deals' do
      expect(subject).to_not permit(@client, @deal)
      expect(subject).to_not permit(@sales, @deal)
      expect(subject).to_not permit(@mall_client, @deal)
      expect(subject).to_not permit(@unconfirmed, @deal)
      expect(subject).to_not permit(@user, @deal)
      expect(subject).to_not permit(@deal_channel, @deal)
    end
  end

  permissions :destroy? do
    it 'allows admins to destroy deals' do
      expect(subject).to permit(@admin, @deal)
    end

    it 'does not allow non-admins to destroy deals' do
      expect(subject).to_not permit(@client, @deal)
      expect(subject).to_not permit(@sales, @deal)
      expect(subject).to_not permit(@mall_client, @deal)
      expect(subject).to_not permit(@unconfirmed, @deal)
      expect(subject).to_not permit(@user, @deal)
      expect(subject).to_not permit(@deal_channel, @deal)
    end
  end
end
