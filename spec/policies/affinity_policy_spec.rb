describe AffinityPolicy do
  subject { described_class }

  before(:all) do
    @affinity     = FactoryGirl.create(:affinity)
    @client       = FactoryGirl.create(:client)
    @sales        = FactoryGirl.create(:sales)
    @mall_client  = FactoryGirl.create(:mall_client)
    @unconfirmed  = FactoryGirl.create(:unconfirmed)
    @user         = FactoryGirl.create(:account)
    @admin        = FactoryGirl.create(:admin)

    @disallowed_affinity = FactoryGirl.create(:affinity)
    @user.user.affinities << @affinity
  end

  permissions :scope do
    it 'allows an admin to see all affinities' do
      expect(Pundit.policy_scope(@admin, Affinity.all)).to include(@affinity)
      expect(Pundit.policy_scope(@admin, Affinity.all)).to include(@disallowed_affinity)
    end

    it 'allows a user to see their affinities' do
      expect(Pundit.policy_scope(@user, Affinity.all)).to include(@affinity)
    end

    it 'does not allow a user to see other affinities' do
      expect(Pundit.policy_scope(@user, Affinity.all)).to_not include(@disallowed_affinity)
    end
  end

  permissions :update? do
    it 'does not allow anyone to update affinities' do
      expect(subject).to_not permit(@admin, @affinity)
      expect(subject).to_not permit(@client, @affinity)
      expect(subject).to_not permit(@sales, @affinity)
      expect(subject).to_not permit(@mall_client, @affinity)
      expect(subject).to_not permit(@unconfirmed, @affinity)
      expect(subject).to_not permit(@user, @affinity)
    end
  end

  permissions :destroy? do
    it 'allows admins to destroy affinities' do
      expect(subject).to permit(@admin, @affinity)
    end

    it 'allows users to destroy their own affinities' do
      expect(subject).to permit(@user, @affinity)
    end

    it 'does not allow users to destroy other affinities' do
      expect(subject).to_not permit(@user, @disallowed_affinity)
    end

    it 'does not allow non-owners or non-admins to destroy other affinities' do
      expect(subject).to_not permit(@client, @affinity)
      expect(subject).to_not permit(@sales, @affinity)
      expect(subject).to_not permit(@mall_client, @affinity)
      expect(subject).to_not permit(@unconfirmed, @affinity)
    end
  end

  permissions :create? do
    it 'allows confirmed accounts to create affinities' do
      expect(subject).to permit(@admin, @affinity)
      expect(subject).to permit(@client, @affinity)
      expect(subject).to permit(@sales, @affinity)
      expect(subject).to permit(@mall_client, @affinity)
    end

    it 'does not allow unconfirmed users to create affinities if current_mall is not present' do
      @unconfirmed.current_mall = nil
      expect(subject).to_not permit(@unconfirmed, @affinity)
    end

    it 'does not allow unconfirmed users to create affinities if current_mall disallows' do
      @unconfirmed.current_mall = create(:mall)
      expect(subject).to_not permit(@unconfirmed, @affinity)
    end

    it 'allows unconfirmed accounts to create affinities if current_mall permits' do
      @unconfirmed.current_mall = create(:mall, company: create(:company, allow_unconfirmed_access: true))
      expect(subject).to permit(@unconfirmed, @affinity)
    end
  end
end
