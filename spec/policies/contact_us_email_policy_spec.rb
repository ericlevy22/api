describe ContactUsEmailPolicy do
  subject { described_class }

  let(:client)      { create(:client) }
  let(:sales)       { create(:sales) }
  let(:mall_client) { create(:mall_client) }
  let(:unconfirmed) { create(:unconfirmed) }
  let(:user)        { create(:account) }
  let(:admin)       { create(:admin) }

  permissions :contact_us? do
    it 'allows admins and mall clients to see contact us emails' do
      expect(subject).to permit(admin)
      expect(subject).to permit(mall_client)
    end

    it 'does not allow non-admins or non-mall clients to see contact us' do
      expect(subject).to_not permit(client)
      expect(subject).to_not permit(sales)
      expect(subject).to_not permit(unconfirmed)
      expect(subject).to_not permit(user)
    end
  end
end
