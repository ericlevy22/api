describe ProgramPolicy do
  subject { described_class }

  let(:program_1)          { create(:program) }
  let(:program_2)          { create(:program) }
  let(:disallowed_program) { create(:program) }

  let(:mall_1) { create(:mall, programs: [program_1]) }
  let(:mall_2) { create(:mall, programs: [program_2]) }

  let(:client)      { create(:client, resource: mall_1) }
  let(:sales)       { create(:sales) }
  let(:mall_client) { create(:mall_client, resource: mall_2) }
  let(:unconfirmed) { create(:unconfirmed) }
  let(:user)        { create(:account) }
  let(:admin)       { create(:admin) }

  permissions :scope do
    it 'allows admins and sales to access all programs' do
      expect(Pundit.policy_scope(admin, Program.all)).to include(program_1, program_2, disallowed_program)
      expect(Pundit.policy_scope(sales, Program.all)).to include(program_1, program_2, disallowed_program)
    end

    it 'allows clients with mall_channel to access their programs' do
      client.add_role(:mall_channel)
      expect(Pundit.policy_scope(client, Program.all)).to include(program_1)
    end

    it 'does not allow clients with mall_channel to access other programs' do
      client.add_role(:mall_channel)
      expect(Pundit.policy_scope(client, Program.all)).to_not include(program_2)
    end

    it 'does not allow clients without mall_channel to access programs' do
      client.remove_role(:mall_channel)
      expect(Pundit.policy_scope(client, Program.all)).to be_empty
    end
  end

  permissions :create? do
    it 'allows admins to create programs' do
      expect(subject).to permit(admin, program_1)
    end

    it 'does not allow non-admins to create programs' do
      expect(subject).to_not permit(client, program_1)
      expect(subject).to_not permit(sales, program_1)
      expect(subject).to_not permit(mall_client, program_1)
      expect(subject).to_not permit(unconfirmed, program_1)
      expect(subject).to_not permit(user, program_1)
    end
  end

  permissions :update? do
    it 'allows admins to update programs' do
      expect(subject).to permit(admin, program_1)
    end

    it 'does not allow non-admins to update programs' do
      expect(subject).to_not permit(client, program_1)
      expect(subject).to_not permit(sales, program_1)
      expect(subject).to_not permit(mall_client, program_1)
      expect(subject).to_not permit(unconfirmed, program_1)
      expect(subject).to_not permit(user, program_1)
    end
  end

  permissions :destroy? do
    it 'allows admins to destroy programs' do
      expect(subject).to permit(admin, program_1)
    end

    it 'does not allow non-admins to destroy programs' do
      expect(subject).to_not permit(client, program_1)
      expect(subject).to_not permit(sales, program_1)
      expect(subject).to_not permit(mall_client, program_1)
      expect(subject).to_not permit(unconfirmed, program_1)
      expect(subject).to_not permit(user, program_1)
    end
  end
end
