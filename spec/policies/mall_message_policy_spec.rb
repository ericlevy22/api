require 'spec_helper'

describe MallMessagePolicy do
  subject { described_class }

  let(:allowed_mall_message)    { create(:mall_message) }
  let(:disallowed_mall_message) { create(:mall_message) }

  let(:allowed_mall)    { create(:mall, mall_messages: [allowed_mall_message]) }
  let(:disallowed_mall) { create(:mall, mall_messages: [disallowed_mall_message]) }

  let(:admin)       { create(:admin) }
  let(:client)      { create(:client, resource: allowed_mall) }
  let(:mall_client) { create(:mall_client, resource: allowed_mall) }

  permissions ".scope" do
    it 'allows admins to see all mall_messages' do
      expect(Pundit.policy_scope(admin, MallMessage.all)).to include(allowed_mall_message, disallowed_mall_message)
    end

    it 'allows clients with mall_channel to see mall_messages for their malls' do
      client.add_role :mall_channel
      expect(Pundit.policy_scope(client, MallMessage.all)).to include(allowed_mall_message)
    end

    it 'does not allow clients with mall_channel to see mall_messages for other malls' do
      client.add_role :mall_channel
      expect(Pundit.policy_scope(client, MallMessage.all)).to_not include(disallowed_mall_message)
    end

    it 'does not allow clients without mall_channel to see mall_messages' do
      client.remove_role :mall_channel
      expect(Pundit.policy_scope(client, MallMessage.all)).to be_empty
    end

    it 'allows mall_clients to see mall_messages for their malls' do
      expect(Pundit.policy_scope(mall_client, allowed_mall.mall_messages)).to include(allowed_mall_message)
    end

    it 'does not allow mall_clients to see mall_messages for other malls' do
      expect(Pundit.policy_scope(mall_client, disallowed_mall.mall_messages)).to be_empty
    end
  end
end
