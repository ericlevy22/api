require 'spec_helper'

describe PressReleasePolicy do
  subject { described_class }

  let(:allowed_press_release)    { create(:press_release) }
  let(:disallowed_press_release) { create(:press_release) }

  let(:allowed_mall)    { create(:mall, press_releases: [allowed_press_release]) }
  let(:disallowed_mall) { create(:mall, press_releases: [disallowed_press_release]) }

  let(:admin)       { create(:admin) }
  let(:client)      { create(:client, resource: allowed_mall) }
  let(:mall_client) { create(:mall_client, resource: allowed_mall) }

  permissions ".scope" do
    it 'allows admins to see all press_releases' do
      expect(Pundit.policy_scope(admin, PressRelease.all)).to include(allowed_press_release, disallowed_press_release)
    end

    it 'allows clients with mall_channel to see press_releases for their malls' do
      client.add_role :mall_channel
      expect(Pundit.policy_scope(client, PressRelease.all)).to include(allowed_press_release)
    end

    it 'does not allow clients with mall_channel to see press_releases for other malls' do
      client.add_role :mall_channel
      expect(Pundit.policy_scope(client, PressRelease.all)).to_not include(disallowed_press_release)
    end

    it 'does not allow clients without mall_channel to see press_releases' do
      client.remove_role :mall_channel
      expect(Pundit.policy_scope(client, PressRelease.all)).to be_empty
    end

    it 'allows mall_clients to see press_releases for their malls' do
      expect(Pundit.policy_scope(mall_client, allowed_mall.press_releases)).to include(allowed_press_release)
    end

    it 'does not allow mall_clients to see press_releases for other malls' do
      expect(Pundit.policy_scope(mall_client, disallowed_mall.press_releases)).to be_empty
    end
  end
end
