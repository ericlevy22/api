describe RolePolicy do
  subject { described_class }

  before(:all) do
    @client       = FactoryGirl.create(:client)
    @sales        = FactoryGirl.create(:sales)
    @mall_client  = FactoryGirl.create(:mall_client)
    @unconfirmed  = FactoryGirl.create(:unconfirmed)
    @user         = FactoryGirl.create(:account)
    @admin        = FactoryGirl.create(:admin)
  end

  permissions :scope do
    it 'allows admins to access all roles' do
      expect(Pundit.policy_scope(@admin, Role.all)).to include(@client.roles.first)
      expect(Pundit.policy_scope(@admin, Role.all)).to include(@sales.roles.first)
      expect(Pundit.policy_scope(@admin, Role.all)).to include(@mall_client.roles.first)
      expect(Pundit.policy_scope(@admin, Role.all)).to include(@admin.roles.first)
    end

    it 'does not allow non-admins to access roles' do
      expect(Pundit.policy_scope(@client, Role.all)).to be_empty
      expect(Pundit.policy_scope(@mall_client, Role.all)).to be_empty
      expect(Pundit.policy_scope(@sales, Role.all)).to be_empty
      expect(Pundit.policy_scope(@user, Role.all)).to be_empty
      expect(Pundit.policy_scope(@unconfirmed, Role.all)).to be_empty
    end
  end
end
