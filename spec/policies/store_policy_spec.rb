describe StorePolicy do
  subject { described_class }

  let(:mall)             { create(:mall) }
  let(:store)            { create(:store, mall: mall) }
  let(:disallowed_mall)  { create(:mall) }
  let(:disallowed_store) { create(:store, mall: disallowed_mall) }

  let(:client)      { create(:client, resource: mall) }
  let(:mall_client) { create(:mall_client, resource: mall) }
  let(:sales)       { create(:sales) }
  let(:unconfirmed) { create(:unconfirmed) }
  let(:user)        { create(:account) }
  let(:admin)       { create(:admin) }

  permissions :scope do
    it 'allows a client with store_channel to access stores at its mall' do
      client.add_role(:store_channel)
      client.remove_role(:mall_channel)
      expect(Pundit.policy_scope(client, mall.stores)).to include(store)
    end

    it 'allows a client with mall_channel to access stores at its mall' do
      client.add_role(:mall_channel)
      client.remove_role(:store_channel)
      expect(Pundit.policy_scope(client, mall.stores)).to include(store)
    end

    it 'does not allow a client to access stores at other malls' do
      expect(Pundit.policy_scope(client, disallowed_mall.stores)).to_not include(disallowed_store)
    end
  end

  permissions :create? do
    it 'allows admins to create stores' do
      expect(subject).to permit(admin, store)
    end

    it 'does not allow non-admins to create stores' do
      expect(subject).to_not permit(client, store)
      expect(subject).to_not permit(sales, store)
      expect(subject).to_not permit(mall_client, store)
      expect(subject).to_not permit(unconfirmed, store)
      expect(subject).to_not permit(user, store)
    end
  end

  permissions :update? do
    it 'allows admins to update stores' do
      expect(subject).to permit(admin, store)
    end

    it 'does not allow non-admins to update stores' do
      expect(subject).to_not permit(client, store)
      expect(subject).to_not permit(sales, store)
      expect(subject).to_not permit(mall_client, store)
      expect(subject).to_not permit(unconfirmed, store)
      expect(subject).to_not permit(user, store)
    end
  end

  permissions :destroy? do
    it 'allows admins to destroy stores' do
      expect(subject).to permit(admin, store)
    end

    it 'does not allow non-admins to destroy stores' do
      expect(subject).to_not permit(client, store)
      expect(subject).to_not permit(sales, store)
      expect(subject).to_not permit(mall_client, store)
      expect(subject).to_not permit(unconfirmed, store)
      expect(subject).to_not permit(user, store)
    end
  end
end
