describe MembershipPolicy do
  subject { described_class }

  before(:all) do
    @membership   = FactoryGirl.create(:membership)
    @client       = FactoryGirl.create(:client)
    @sales        = FactoryGirl.create(:sales)
    @mall_client  = FactoryGirl.create(:mall_client)
    @unconfirmed  = FactoryGirl.create(:unconfirmed)
    @user         = FactoryGirl.create(:account)
    @admin        = FactoryGirl.create(:admin)
  end

  permissions :create? do
    it 'allows admins to create memberships' do
      expect(subject).to permit(@admin, @membership)
    end

    it 'does not allow non-admins to create memberships' do
      expect(subject).to_not permit(@client, @membership)
      expect(subject).to_not permit(@sales, @membership)
      expect(subject).to_not permit(@mall_client, @membership)
      expect(subject).to_not permit(@unconfirmed, @membership)
      expect(subject).to_not permit(@user, @membership)
    end
  end
end
