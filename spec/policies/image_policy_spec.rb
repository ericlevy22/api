describe ImagePolicy do
  subject { described_class }

  before(:all) do
    @image       = create :image
    @admin       = create :admin
    @client      = create :client
    @sales       = create :sales
    @mall_client = create :mall_client
    @unconfirmed = create :unconfirmed
    @user        = create :account
  end

  permissions :scope do
    it 'allows admins to access all images' do
      expect(Pundit.policy_scope(@admin, Image.all)).to include(@image)
    end

    it 'does not allow non-admins to access images through policy' do
      expect(Pundit.policy_scope(@client, Image.all)).to_not include(@image)
      expect(Pundit.policy_scope(@sales, Image.all)).to_not include(@image)
      expect(Pundit.policy_scope(@mall_client, Image.all)).to_not include(@image)
      expect(Pundit.policy_scope(@unconfirmed, Image.all)).to_not include(@image)
      expect(Pundit.policy_scope(@user, Image.all)).to_not include(@image)
    end
  end

  permissions :create? do
    it 'allows admin to create images' do
      expect(subject).to permit(@admin, @product)
    end

    it 'does not allow non-admins to create images' do
      expect(subject).to_not permit(@client, @image)
      expect(subject).to_not permit(@sales, @image)
      expect(subject).to_not permit(@mall_client, @image)
      expect(subject).to_not permit(@unconfirmed, @image)
      expect(subject).to_not permit(@user, @image)
    end
  end

  permissions :destroy? do
    it 'allows admins to destroy images' do
      expect(subject).to permit(@admin, @image)
    end

    it 'does not allow non-admins to destroy images' do
      expect(subject).to_not permit(@client, @image)
      expect(subject).to_not permit(@sales, @image)
      expect(subject).to_not permit(@mall_client, @image)
      expect(subject).to_not permit(@unconfirmed, @image)
      expect(subject).to_not permit(@user, @image)
    end
  end
end
