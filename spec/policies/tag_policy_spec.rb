describe TagPolicy do
  subject { described_class }

  before(:all) do
    @tag          = FactoryGirl.create(:tag)
    @client       = FactoryGirl.create(:client)
    @sales        = FactoryGirl.create(:sales)
    @mall_client  = FactoryGirl.create(:mall_client)
    @unconfirmed  = FactoryGirl.create(:unconfirmed)
    @user         = FactoryGirl.create(:account)
    @admin        = FactoryGirl.create(:admin)
  end

  permissions :create? do
    it 'allows admins to create tags' do
      expect(subject).to permit(@admin, @tag)
    end

    it 'does not allow non-admins to create tags' do
      expect(subject).to_not permit(@client, @tag)
      expect(subject).to_not permit(@sales, @tag)
      expect(subject).to_not permit(@mall_client, @tag)
      expect(subject).to_not permit(@unconfirmed, @tag)
      expect(subject).to_not permit(@user, @tag)
    end
  end
end
