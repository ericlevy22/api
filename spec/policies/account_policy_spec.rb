describe AccountPolicy do
  subject { described_class }

  before(:all) do
    @mall = create(:mall)
    @disallowed_mall = create(:mall)
    @mall_client = create(:account)
    @mall_client.add_role :mall_client, @mall
    @account = create(:account)
    @unconfirmed = create(:unconfirmed)
  end

  permissions :scope do
    it 'allows a mall_client to access its mall' do
      expect(Pundit.policy_scope(@mall_client, Mall.all)).to include(@mall)
    end

    it 'does not allow a mall_client to access other malls' do
      expect(Pundit.policy_scope(@mall_client, Mall.all)).to_not include(@disallowed_mall)
    end
  end

  permissions :update? do
    it 'allows admins to update accounts' do
      expect(subject).to permit(create(:admin), @account)
    end

    it 'does not allow mall_clients to update themselves' do
      expect(subject).to_not permit(@mall_client, @mall_client)
    end

    it 'does not allow mall_clients to update their malls' do
      expect(subject).to_not permit(@mall_client, @mall)
    end

    it 'allows confirmed accounts to update themselves' do
      expect(subject).to permit(@account, @account)
    end

    it 'allows unconfirmed accounts to update themselves if current_mall permits' do
      @unconfirmed.current_mall = create(:mall, company: create(:company, allow_unconfirmed_access: true))
      expect(subject).to permit(@unconfirmed, @unconfirmed)
    end

    it 'does not allow unconfirmed accounts to update themselves' do
      @unconfirmed.current_mall = nil
      expect(subject).to_not permit(@unconfirmed, @unconfirmed)
    end

    it 'does not allow non-admins to update other accounts' do
      expect(subject).not_to permit(@account, create(:account))
    end
  end
end
