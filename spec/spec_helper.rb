require 'rubygems'

ENV['RAILS_ENV'] ||= 'test'

require File.expand_path('../../config/environment', __FILE__)
require 'rspec/rails'
require 'email_spec'
require 'pry'
require 'rspec_api_documentation/dsl'
require 'sunspot/rails/spec_helper'
require 'sunspot_test/rspec'
require 'pundit/rspec'
require 'factory_girl_rails'

# Requires supporting ruby files with custom matchers and macros, etc,
# in spec/support/ and its subdirectories.
Dir[Rails.root.join('spec/concerns/**/*.rb')].each { |f| require f }
Dir[Rails.root.join('spec/support/**/*.rb')].each { |f| require f }


# Checks for pending migrations before tests are run.
# If you are not using ActiveRecord, you can remove this line.
ActiveRecord::Migration.maintain_test_schema!

# Model specs: type: :model
# Controller specs: type: :controller
# Request specs: type: :request
# Feature specs: type: :feature
# View specs: type: :view
# Helper specs: type: :helper
# Mailer specs: type: :mailer
# Routing specs: type: :routing

RSpec.configure do |config|
  config.order = 'random'
  config.seed = srand % 0xFFFF
  config.infer_spec_type_from_file_location!
  config.use_transactional_fixtures = false
  config.infer_base_class_for_anonymous_controllers = false
  config.before(:each) { GC.disable }
  config.after(:each) { GC.enable }

  config.include FactoryGirl::Syntax::Methods
  config.include JsonSpec::Helpers
  config.include Stubs
  config.include LoginHelper
  config.include SolrSpecHelper
  config.include SunspotMatchers
  config.include Devise::TestHelpers, type: :controller

  config.before do
    Sunspot.session = SunspotMatchers::SunspotSessionSpy.new(Sunspot.session)
  end

  config.before(:suite) do
    DatabaseCleaner.strategy = :truncation
    DatabaseCleaner.clean_with(:truncation)
  end

  config.before(:each) do
    DatabaseCleaner.strategy = :transaction
  end

  config.before(:each) do
    DatabaseCleaner.start
  end

  config.after(:each) do
    DatabaseCleaner.clean
  end
end

RspecApiDocumentation.configure do |config|
  config.format = :json
end
