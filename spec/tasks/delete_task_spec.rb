require 'rake'
require 'date'
require 'time'
require File.expand_path("#{Rails.root}/lib/modules/helper_functions", __FILE__)
include HelperFunctions

describe 'placewise:delete:from_etl' do
  before(:all) do
    load File.expand_path("#{Rails.root}/lib/tasks/placewise/delete.rake", __FILE__)
    Rake::Task.define_task(:environment)
  end

  describe 'deletes' do
    before(:all) do
      # custom categories
      @target_tag      = create(:tag)
      @canary_tag      = create(:tag)
      @target_taggable = create(:store, tags: [@target_tag, @canary_tag])
      @canary_taggable = create(:store, tags: [@target_tag, @canary_tag])
      Etl::Deletions.add_deletion('CustomCategoryInstance', @target_taggable.id, typeis_id_value: @target_tag.name)

      # job listings
      @target_job      = create(:job_listing)
      @canary_job      = create(:job_listing)
      Etl::Deletions.add_deletion('JobReqs', @target_job.legacy_requisition_id)

      # operation messages
      @mall            = create(:mall)
      @target_message  = create(:mall_message, mall: @mall)
      Etl::Deletions.add_deletion('Operational', @mall.id)

      # everything else...
      @target_store    = create(:store)
      @canary_store    = create(:store)
      Etl::Deletions.add_deletion('StoreInstance', @target_store.id)

      Rake::Task['placewise:delete:from_etl'].invoke
    end

    it 'deletes the target store' do
      expect(Store.pluck(:id)).to_not include(@target_store.id)
    end

    it 'spares the canary store' do
      expect(Store.pluck(:id)).to include(@canary_store.id)
    end

    it 'deletes the correct tag from the correct store' do
      expect(@target_taggable.reload.tags).to_not include(@target_tag)
    end

    it 'deletes the correct job_listing' do
      expect(JobListing.pluck(:id)).to match_array([@canary_job.id])
    end

    it 'deletes the correct mall_message' do
      expect(@mall.reload.mall_messages).to be_empty
    end
  end
end
