module Stubs
  def stub_mall
    Mall.stub(:find) { mall }
    mall.stub(:id) { 1 }
  end
end
