module LoginHelper
  def log_in_as_admin!
    set_account(FactoryGirl.create(:admin))
  end

  def log_in_as_client!(resource: Mall, traits: nil)
    set_account(FactoryGirl.create(:client, *traits, resource: resource))
  end

  def log_in_as_unconfirmed_account!
    set_account(FactoryGirl.create(:account, :unconfirmed))
  end

  def log_in_as_facebook!(resource: Mall, email: true, traits: nil)
    allow(Facebook::Login).to receive(:call) { create(:account, *traits) }
    set_account(Facebook::Login.call('Facebook Login Token Goes Here!')).save
  end

  private

  def set_account(account)
    @account = account
    @timestamp = Time.now.to_i
    @token = Digest::MD5.hexdigest "#{@timestamp}:#{@account.authentication_token}"
    account
  end
end
