class Fyphoeus
  def initialize(body, status = 200)
    @body, @status = body, status
  end

  def run
    true
  end

  def response
    self
  end

  def response_body
    @body.to_json
  end
end
