class Fakebook
  def initialize(email: Faker::Internet.email, verified: true)
    @data = {
      'id' => rand(100_000_000).to_s,
      'name' => Faker::Name.name,
      'likes' => {
        'data' => [
          {
            'name' => 'LEGO',
            'category' => 'Games/Toys',
            'id' => '6665038402',
            'created_time' => '2013-06-17T21:19:57+0000'
          },
          {
            'name' => 'Xbox',
            'category' => 'Product/Service',
            'id' => '16547831022',
            'created_time' => '2014-06-28T21:29:23+0000'
          },
          {
            'name' => 'TechCrunch',
            'category' => 'News/Media Website',
            'id' => '8062627951',
            'created_time' => '2013-09-09T16:37:45+0000'
          },
          {
            'name' => 'Hexagon Mill',
            'category' => 'Company',
            'category_list''' => [
              {
                'id' => '2200',
                'name' => 'Company'
              }
            ],
            'id' => '1401615653474172',
            'created_time' => '2015-02-06T00:00:24+0000'
          },
          {
            'name' => 'Shark Mounted Lasers',
            'category' => 'Small Business',
            'id' => '501747719875006',
            'created_time' => '2014-10-13T03:57:33+0000'
          }
        ],
        'paging' => {
          'cursors' => {
            'before' => 'nDI4mDc4NzQ3Mzq2NTIy',
            'after' => 'Mz520DE5OTk4MTyNzUxQ'
          }
        }
      },
      'verified' => verified,
      'birthday' => Time.at(rand * Time.now.to_i).strftime('%m/%d/%Y')
    }
    @data["email"] = email if email
  end

  def get_object(*args)
    @data
  end
end
