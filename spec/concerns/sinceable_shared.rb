shared_examples_for 'sinceable' do
  context 'sinceable' do
    let(:model)  { described_class }
    let(:record) { create(model.to_s.underscore.to_sym, created_at: 1.day.ago, updated_at: 1.day.ago) }

    it 'excludes records not updated since provided date' do
      expect(model.since(DateTime.now.strftime('%Y-%m-%d %H:%M:%S'))).to_not include(record)
    end

    it 'includes records updated since provided date' do
      expect(model.since(2.days.ago.strftime('%Y-%m-%d %H:%M:%S'))).to include(record)
    end
  end
end
