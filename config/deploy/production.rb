set :stage, :production
set :rails_env, 'production'

set :pty, false
set :passenger_roles, 'api'
set :application_name, 'api'

set :branch, 'master'

role :app, ['deploy@208.94.36.66', 'deploy@208.94.36.67']
role :web, ['deploy@208.94.36.66', 'deploy@208.94.36.67']
