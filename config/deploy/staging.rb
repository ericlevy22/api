set :stage, :staging
set :rails_env, 'staging'

set :pty, false
set :passenger_roles, 'api'
set :application_name, 'api'

# Staging should always be on develop, unless testing a specific branch
set :branch, 'develop'

role :app, 'deploy@208.94.36.146'
role :web, 'deploy@208.94.36.146'
