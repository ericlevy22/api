ActiveSupport::Notifications.subscribe 'sql.active_record' do |_name, start, finish, _id, payload|

  duration = (finish - start) * 1000

  def time_in_ms(start, finish)
    (((finish - start).to_f * 100_000).round / 100.0).to_s
  end

  if duration > 250
    Rails.logger.tagged('DB_LONG_QRY') do
      Rails.logger.fatal { "(#{time_in_ms(start, finish)}) slow query (#{duration.to_i}ms): #{payload[:sql]}" }
    end
  end
end
