require 'raven'

if Rails.env.production? || ENV['LOG_ERRORS_TO_EXTERNAL_SERVICE']
  Raven.configure do |config|
    # DSN Tied to API Version in Sentry
    config.dsn = Rails.application.secrets.sentry['api']
    config.excluded_exceptions = ['ActionController::RoutingError', 'ActiveRecord::RecordNotFound']
    config.tags = { environment: Rails.env }
    config.async = lambda { |event|
      Thread.new { Raven.send(event) }
    }
  end
end
