Gracenote::OnConnect.configure do |config|
  config.api_key = Rails.application.secrets.onconnect_api_key
end
