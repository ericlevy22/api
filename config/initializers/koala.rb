case RUBY_PLATFORM
when "x86_64-darwin14"
  Koala.http_service.http_options[:ssl] = { ca_file: '/usr/local/etc/openssl/osx_cert.pem' }
when "x86_64-linux"
  Koala.http_service.http_options[:ssl] = { ca_path: '/etc/ssl/certs' }
end
