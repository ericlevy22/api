Rails.application.routes.draw do
  devise_for :accounts, skip: :all

  devise_scope :account do
    resources :accounts do
      post :confirm, on: :collection, to: 'accounts/confirmations'
      get :affinities, on: :member
      get :memberships, on: :member
      get :roles, on: :member
      post :roles, on: :member, action: :add_roles
      delete :roles, on: :member, action: :remove_roles
      delete :logout, on: :collection
      post :reset, on: :collection, to: 'accounts/passwords'
      post :send_reset, on: :collection, to: 'accounts/passwords', path: 'send-reset'
      post :resend_confirmation, on: :collection, to: 'accounts/confirmations', path: 'resend-confirmation'
      post :login, on: :collection, to: 'accounts/login#create'
    end
  end

  get :docs, to: 'docs#index'

  get :status, to: 'monitor#status'

  resources :affinities, except: :update

  resources :articles, except: [:new, :edit] do
    get :products, on: :member
    get :search, to: 'articles#search', on: :collection
  end

  resources :brands, except: [:new, :edit]

  resources :companies, except: [:new, :edit] do
    get :malls, on: :member
    get :contacts, on: :member
  end

  resources :contacts, except: [:new, :edit]

  resources :deals, except: [:new, :edit] do
    get :stores, on: :member
    get :search, on: :collection
  end

  resources :images, except: [:new, :edit]

  resources :groups, except: [:new, :edit]

  resources :locations, except: [:new, :edit] do
    get :stores, on: :member
    get :malls, on: :member
    get :deals, on: :member
    get :products, on: :member
    get :search, on: :collection
  end

  resources :malls, except: [:new, :edit] do
    get :articles, on: :member
    get :stores, on: :member
    get :hours, on: :member
    get :current_hours, on: :member, path: 'current-hours'
    get :events, on: :member
    get :deals, on: :member
    get :ifeatures, on: :member, path: 'i-features'
    get :mall_messages, on: :member, path: 'mall-messages'
    get :contacts, on: :member
    post :contact_us, on: :member, path: 'contact-us'
    get :social_feeds, on: :member, path: 'social-feeds'
    get :job_listings, on: :member, path: 'job-listings'
    get :press_releases, on: :member, path: 'press-releases'
    get :programs, on: :member
    get :theaters, on: :member
  end

  # TODO: re-enable this resource once the data has been migrated from legacy
  # resources :memberships, except: [:new, :edit]
  resources :memberships, only: [:create, :destroy]

  resources :products, except: [:new, :edit] do
    get :stores, on: :member
  end

  resources :programs, except: [:new, :edit]

  resources :retailers, except: [:new, :edit] do
    get :deals, on: :member
    get :stores, on: :member
    get :contacts, on: :member
    get :social_feeds, on: :member, path: 'social-feeds'
  end

  resources :stores, except: [:new, :edit] do
    get :deals, on: :member
    get :products, on: :member
    get :contacts, on: :member
    get :social_feeds, on: :member, path: 'social-feeds'
    get :job_listings, on: :member, path: 'job-listings'
  end

  resources :roles

  resources :social_feeds, except: [:new, :edit], path: 'social-feeds'

  resources :tags, only: [:show, :index, :create] do
    get :search, on: :collection
  end

  resources :theaters, only: [:show] do
    get :showtimes, on: :member
  end

  root to: 'docs#index'

  match '*path' => 'options#options', via: :options
end
