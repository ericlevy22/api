if ENV['COVERAGE']
  require 'simplecov'
  SimpleCov.start do
    coverage_dir 'tmp/coverage'
    add_filter '/.bundle/'
    add_filter '/spec/'
    add_filter '/config/'
    add_group 'Models', 'app/models'
    add_group 'Controllers', 'app/controllers'
    add_group 'Services', 'app/services'
    add_group 'Serializers', 'app/serializers'
    add_group 'Policies', 'app/policies'
    add_group 'Helpers', 'app/helpers'
    add_group 'Libraries', 'lib'
    add_group 'Mailers', 'app/mailers'
    add_group "Long Files" do |src_file|
      src_file.lines.count > 300
    end
    add_group 'Ignored Code' do |src_file|
      File.readlines(src_file.filename).grep(/:nocov:/).any?
    end
  end
  SimpleCov.formatter = SimpleCov::Formatter::MultiFormatter[
    SimpleCov::Formatter::HTMLFormatter
  ]
  SimpleCov.minimum_coverage 95
end

if ENV['CC']
  ENV['CODECLIMATE_REPO_TOKEN'] = '2b1fb19914c047843778969c7e144e3a2d2047d6f9d9646c339265a79e384028'
  require 'codeclimate-test-reporter'
  CodeClimate::TestReporter.start
  puts "Running CodeClimate...\n"
end
