set :output, { error: 'log/whenever_error.log', standard: 'log/whenever_cron.log'}

every 25.minutes, roles: [:app] do
  rake 'placewise:solr:reindex'
end

every 5.minutes, roles: [:app] do
  rake 'placewise:delete:from_etl'
end

every 60.minutes, roles: [:app] do
  rake 'placewise:archive:all'
end

every 15.minutes, roles: [:app] do
  rake 'placewise:contact-us:process'
end
