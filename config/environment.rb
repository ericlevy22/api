# Load the Rails application.
require File.expand_path('../application', __FILE__)

ActionMailer::Base.smtp_settings = {
  :user_name => Rails.application.secrets.email_gateway_username,
  :password => Rails.application.secrets.email_gateway_password,
  :address => 'smtp.sendgrid.net',
  :port => 587,
  :authentication => :plain,
  :enable_starttls_auto => true
}

ActiveSupport::Deprecation.silenced = true if Rails.env.test?

# Initialize the Rails application.
Rails.application.initialize!
