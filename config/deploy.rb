set :application, 'api'

set :scm, :git
set :repo_url, 'git@github.com:PlacewiseMedia/API.git'
set :deploy_to, '/home/deploy/apps/api'

set :deploy_via, :remote_cache
set :keep_releases, 10
set :account, 'deploy'
set :use_sudo, false

set :rbenv_type, :system
set :rbenv_ruby, '2.2.0'
set :rbenv_path, '/opt/rbenv'

set :whenever_roles,      -> { :app }
set :whenever_identifier, -> { "#{fetch(:application)}_#{fetch(:stage)}" }
set :whenever_roles, [:worker]
set :whenever_clear_flags, -> { "--clear-crontab #{fetch :whenever_identifier}" }

set :passenger_in_gemfile, true

namespace :deploy do
  desc 'Add Client Accounts to the API'
  task :update_client_accounts do
    on roles(:worker), in: :sequence, wait: 5 do
      within(release_path) do
        with rails_env: fetch(:rails_env) do
          spacer('Loading client api accounts')
          execute :rake, 'placewise:seed:client_accounts', '--trace'
          spacer
        end
      end
    end
  end

  desc 'Add Static Site Accounts to the API'
  task :update_static_site_accounts do
    on roles(:worker), in: :sequence, wait: 5 do
      within(release_path) do
        with rails_env: fetch(:rails_env) do
          spacer('Updating static site accounts')
          execute :rake, 'placewise:seed:static_site_accounts', '--trace'
          spacer
        end
      end
    end
  end

  desc 'Add Placewise Internal Accounts to the API'
  task :update_placewise_internal_accounts do
    on roles(:worker), in: :sequence, wait: 5 do
      within(release_path) do
        with rails_env: fetch(:rails_env) do
          spacer('Updating placewise internal accounts')
          execute :rake, 'placewise:seed:placewise_internal_accounts', '--trace'
          spacer
        end
      end
    end
  end

  desc 'Run Migrations'
  task :update_database do
    on roles(:worker), in: :sequence, wait: 5 do
      within(release_path) do
        with rails_env: fetch(:rails_env) do
          spacer('Updating the database')
          execute :rake, 'db:migrate', '--trace'
          spacer
        end
      end
    end
  end

  desc 'Create application symlinks'
  task :shared_links do
    on roles(:all), in: :sequence, wait: 5 do
      spacer('Creating application symlinks')
      execute "ln -s #{shared_path}/config/secrets.yml #{release_path}/config/secrets.yml"

      execute "ln -s #{shared_path}/config/active_api_client_account.seeds.rb #{release_path}/db/seeds/active_api_client_account.seeds.rb"
      execute "ln -s #{shared_path}/config/active_api_placewise_internal_account.seeds.rb #{release_path}/db/seeds/active_api_placewise_internal_account.seeds.rb"
      execute "ln -s #{shared_path}/config/Passengerfile.json #{release_path}/Passengerfile.json"
      execute "ln -s #{shared_path}/log #{release_path}/log"
      spacer
    end
  end

  desc "Update application's crontab entries using Whenever"
  task :update_crontab do
    on roles(:worker), in: :groups, limit: 3, wait: 10 do
      setup_whenever_task do |host|
        roles = host.roles_array.join(',')
        [fetch(:whenever_update_flags), "--roles=#{roles}"]
      end
    end
  end

  desc "Clear application's crontab entries using Whenever"
  task :clear_crontab do
    on roles(:worker), in: :groups, limit: 3, wait: 10 do
      setup_whenever_task(fetch(:whenever_clear_flags))
    end
  end

  desc "Restart the app"
  task :restart_app do
    on roles(:all), in: :sequence do
      spacer('Restarting application')
      execute "sudo service #{fetch(:application_name)}-passenger restart"
    end
  end

  after 'deploy:updated', 'deploy:shared_links'
  after :finishing, 'deploy:update_database'

  after :finishing, 'deploy:update_crontab'

  after :finishing, 'deploy:cleanup'
  after :finishing, 'deploy:restart_app'
end

namespace :setup do
  desc 'Upload the secrets and database configurations'
  task :config do
    on roles(:all) do
      execute "mkdir -p #{shared_path}/config"
      execute "mkdir -p #{shared_path}/tmp"
      upload! 'config/secrets.yml', "#{shared_path}/config"
      upload! "config/#{fetch(:rails_env)}_Passengerfile.json", "#{shared_path}/config/Passengerfile.json"

      execute "rm -f #{shared_path}/config/active_api_client_account.seeds.rb"
      execute "rm -f #{shared_path}/config/active_api_placewise_internal_account.seeds.rb"

      execute "touch #{shared_path}/config/active_api_client_account.seeds.rb"
      execute "touch #{shared_path}/config/active_api_placewise_internal_account.seeds.rb"
    end
  end

  desc 'Upload the active client account seeds'
  task client_accounts: ['db/seeds/accounts/active_api_client_account.seeds.rb'] do |t|
    on roles(:worker) do
      execute "mkdir -p #{shared_path}/config"
      t.prerequisites.each do |file|
        upload! file, "#{shared_path}/config"
      end
    end
  end

  desc 'Upload the active placewise internal account seeds'
  task internal_accounts: ['db/seeds/accounts/active_api_placewise_internal_account.seeds.rb'] do |t|
    on roles(:worker) do
      execute "mkdir -p #{shared_path}/config"
      t.prerequisites.each do |file|
        upload! file, "#{shared_path}/config"
      end
    end
  end
end

def spacer(desc = nil)
  puts '-----------------------------------------------------------------------------'
  return unless desc
  puts desc
  puts '-----------------------------------------------------------------------------'
end
