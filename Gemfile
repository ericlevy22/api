source 'https://rubygems.org'
ruby '2.2.0'

gem 'rails', '~> 4.2.0'
gem 'newrelic_rpm'
gem 'sentry-raven', git: 'https://github.com/getsentry/raven-ruby.git'
gem 'rack-cors', require: 'rack/cors'
gem 'passenger', '5.0.25', require: "phusion_passenger/rack_handler"

# Database
gem 'pg'

# Cache
gem 'memcachier'
gem 'dalli'

# Search
gem 'sunspot_rails'
gem 'progress_bar'

# Security
gem 'devise', '~> 3.4.1'
gem 'pundit'
gem 'rolify'

# Utilities
gem 'validates_email_format_of'
gem 'active_model_serializers', '~> 0.8.1'
gem 'apitome', github: 'namack/apitome'
gem 'geocoder'
gem 'rspec_api_documentation', '~> 4.4.0'
gem 'seedbank'
gem 'whenever', :require => false
gem 'sendgrid-ruby'
gem 'koala', '~> 2.2.0'
gem 'typhoeus'
gem 'gracenote-on_connect', github: 'matt-morris/gracenote-on_connect'
gem 'has_archive'
gem 'rack-mini-profiler'
gem 'flamegraph'
gem 'stackprof'
gem 'memory_profiler'
gem 'aws-sdk', '~> 2'

group :development do
  gem 'capistrano'
  gem 'capistrano-rails'
  gem 'capistrano-bundler'
  gem 'capistrano-rbenv'
  gem 'capistrano-passenger'
  gem 'brakeman', require: false
  gem 'rubocop', require: false
  gem 'bullet'
end

group :development, :test do
  gem 'pry-rails'
  gem 'pry-rescue'
  gem 'pry-nav'
  gem 'spring'
  gem 'sunspot_solr', '2.2.0'
  gem 'rspec-rails', '~> 3.3.0'
  gem 'spring-commands-rspec'
  gem 'factory_girl_rails', require: false
  gem 'faker'
  gem 'highline'
  gem 'awesome_print'
  gem 'table_print'
  gem 'guard-rspec'
end

group :test do
  gem 'rspec-collection_matchers'
  gem "codeclimate-test-reporter", require: nil
  gem 'simplecov', '~> 0.7.1'
  gem 'json_spec', '~> 1.1.4'
  gem 'sunspot_matchers'
  gem 'sunspot_test'
  gem 'email_spec'
  gem 'database_cleaner'
  gem 'emoji-rspec', git: 'https://github.com/carhartl/emoji-rspec.git', branch: 'rspec-3'
end
