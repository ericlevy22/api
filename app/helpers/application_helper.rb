module ApplicationHelper
  def phone_with_parens_and_dash(phone)
    digits = phone.gsub(/[^\d]/, '')
    "(#{digits[0..2]}) #{digits[3..5]}-#{digits[6..9]}"
  end
end
