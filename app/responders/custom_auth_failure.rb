class CustomAuthFailure < Devise::FailureApp
  def respond
    self.status = :unauthorized
  end
end
