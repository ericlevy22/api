class MallMessageSerializer < ActiveModel::Serializer
  attributes :id, :mall_id, :description, :message_type,
             :start_at, :end_at, :created_at, :updated_at
end
