class ProgramSerializer < ActiveModel::Serializer
  attributes :id, :properties, :program_type,
             :created_at, :updated_at
end
