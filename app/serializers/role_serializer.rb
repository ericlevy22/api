class RoleSerializer < ActiveModel::Serializer
  attributes :id, :name, :resource_id, :resource_type
end
