class JobListingSerializer < ActiveModel::Serializer
  attributes :id, :title, :seo_slug, :start_date_text, :hours_per_week, :number_of_openings,
             :manager_name, :fax_number, :email, :salary, :store_name, :store_id,
             :key_responsibilities, :required_experience, :special_information,
             :description, :display_at, :end_at, :is_full_time, :is_apply_online,
             :created_at, :updated_at

  has_many :images, embed: :ids, include: true
end
