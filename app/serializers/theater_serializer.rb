class TheaterSerializer < ActiveModel::Serializer
  attributes :id, :name, :sort_name, :store_id, :seo_slug,
             :address, :city, :state, :zip, :phone, :open_at,
             :close_at, :url, :created_at, :updated_at

  has_many :images, embed: :ids, include: true
end
