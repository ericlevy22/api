class AffinitySerializer < ActiveModel::Serializer
  attributes :id, :heartable_id, :heartable_type,
             :created_at, :updated_at
end
