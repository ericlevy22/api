class ProductSerializer < ActiveModel::Serializer
  attributes :id, :name, :seo_slug, :description, :price, :brand_id,
             :retailer_id, :created_at, :updated_at

  has_many :images, embed: :ids, include: true
  has_many :stores, embed: :ids, include: false
  has_many :tags, embed: :ids, include: false
end
