class ImageSerializer < ActiveModel::Serializer
  attributes :id, :url, :size_parameter, :cached_tag_list,
             :created_at, :updated_at
end
