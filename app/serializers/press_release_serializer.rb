class PressReleaseSerializer < ActiveModel::Serializer
  attributes :id, :name, :seo_slug, :headline, :tag_line, :content,
             :phone, :email, :mall_id, :is_active, :pr_type,
             :display_at, :created_at, :updated_at
end
