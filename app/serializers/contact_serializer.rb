class ContactSerializer < ActiveModel::Serializer
  attributes :id, :name, :title, :email, :phone,
             :created_at, :updated_at
end
