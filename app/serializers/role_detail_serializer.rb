class RoleDetailSerializer < ActiveModel::Serializer
  attributes :id, :name
  has_many :users, embed: :ids, include: false
end
