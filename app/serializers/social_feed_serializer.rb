class SocialFeedSerializer < ActiveModel::Serializer
  attributes :id, :properties, :name, :created_at, :updated_at
end
