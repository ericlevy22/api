class EventSerializer < ActiveModel::Serializer
  attributes :id, :title, :short_text, :description, :seo_slug,
             :start_at, :end_at, :link_url, :link_text, :pin_order,
             :location, :is_events_page, :is_featured, :date_text,
             :created_at, :updated_at

  has_many :images, embed: :ids, include: true
end
