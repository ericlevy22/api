class CompanySerializer < ActiveModel::Serializer
  attributes :id, :name, :nick_name, :seo_slug, :address,
             :city, :state, :zip, :phone, :fax, :url, :test_url,
             :is_active, :created_at, :updated_at
end
