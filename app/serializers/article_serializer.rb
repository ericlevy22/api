class ArticleSerializer < ActiveModel::Serializer
  attributes :id, :title, :subtitle, :short_title, :author, :attribution,
             :teaser, :seo_slug, :content, :created_at, :updated_at,
             :parent_id, :display_order, :feature_location,
             :display_at, :end_at, :created_at, :updated_at

  has_many :variants, embed: :ids, include: false
  has_many :images, embed: :ids, include: true
  has_many :malls, embed: :ids, include: false
  has_many :products, embed: :ids, include: true
  has_many :tags, embed: :ids, include: true, serializer: TagSerializer

  def include_products?
    scope.has_role?(:admin) || scope.has_role?(:product_channel)
  end
end
