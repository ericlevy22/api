class MallSerializer < ActiveModel::Serializer
  attributes :id, :name, :sort_name, :seo_slug, :nick_name,
             :address, :city, :state, :zip, :phone,
             :distance, :latitude, :longitude, :exclude_national_deals,
             :url, :company_id, :hours_text, :description,
             :guest_services_description, :created_at, :updated_at

  has_many :images, embed: :ids, include: true
  has_many :groups, embed: :ids, include: true
end
