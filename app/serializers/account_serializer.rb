class AccountSerializer < ActiveModel::Serializer
  attributes :id, :email, :authentication_token,
             :created_at, :updated_at, :confirmed_at,
             :properties

  has_many :malls, embed: :ids, include: false
end
