class HourSerializer < ActiveModel::Serializer
  attributes :sunday, :monday, :tuesday, :wednesday, :thursday,
             :friday, :saturday, :start_at, :end_at, :is_default,
             :description, :created_at, :updated_at
end
