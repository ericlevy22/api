class RetailerSerializer < ActiveModel::Serializer
  attributes :id, :name, :sort_name, :seo_slug, :description,
             :nick_name, :phone, :email, :url, :url_text,
             :store_text, :created_at, :updated_at

  has_many :images, embed: :ids, include: true
  has_many :tags, embed: :ids, include: true, serializer: TagSerializer
  has_many :keywords, embed: :ids, include: true, serializer: KeywordSerializer
  has_many :stores, embed: :ids, include: false
end
