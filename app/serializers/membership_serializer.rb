class MembershipSerializer < ActiveModel::Serializer
  attributes :id, :program_id, :identity_id, :properties, :created_at, :updated_at
end
