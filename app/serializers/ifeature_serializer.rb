class IfeatureSerializer < ActiveModel::Serializer
  attributes :id, :seo_slug, :headline, :sub_heading, :tag_line, :description,
             :pin_order, :start_at, :end_at, :url, :is_static, :is_headline,
             :is_new_window, :created_at, :updated_at
end
