class BrandSerializer < ActiveModel::Serializer
  attributes :id, :name, :seo_slug, :description, :url,
             :created_at, :updated_at

  has_many :images, embed: :ids, include: true
end
