class LocationSerializer < ActiveModel::Serializer
  attributes :id, :time_zone, :latitude, :longitude, :distance,
             :created_at, :updated_at

  has_many :malls, embed: :ids
  has_many :stores, embed: :ids
end
