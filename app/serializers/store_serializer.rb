class StoreSerializer < ActiveModel::Serializer
  attributes :id, :name, :sort_name, :seo_slug,
             :address, :city, :state, :zip, :phone,
             :mall_name, :tenant_location, :tenant_location_map_key, :exclude_national_deals,
             :mall_id, :retailer_id, :distance, :hours, :description,
             :latitude, :longitude, :url, :url_text, :retailer_url,
             :opening_date, :opening_text, :created_at, :updated_at

  has_many :images, embed: :ids, include: true
  has_many :tags, embed: :ids, include: true, serializer: TagSerializer
  has_many :groups, embed: :ids, include: true, serializer: GroupSerializer
  has_many :keywords, embed: :ids, include: true, serializer: KeywordSerializer

  def retailer_url
    object.retailer_url
  end
end
