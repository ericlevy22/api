class TaggingSerializer < ActiveModel::Serializer
  attributes :id, :taggable_id, :taggable_type
end
