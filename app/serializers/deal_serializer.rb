class DealSerializer < ActiveModel::Serializer
  attributes :id, :retailer_id, :sales_type,
             :title, :seo_slug,
             :name, :sort_name,
             :retailer_name, :retailer_sort_name,
             :store_name, :store_sort_name,
             :external_url,
             :display_at, :start_at, :end_at,
             :description, :fine_print_description,
             :is_local, :is_featured, :is_active,
             :cached_tag_list, :created_at, :updated_at

  has_many :images, embed: :ids, include: true
  has_many :tags, embed: :ids, include: true, serializer: TagSerializer

  def include_store_name?
    object.store_name
  end

  def include_store_sort_name?
    object.store_sort_name
  end
end
