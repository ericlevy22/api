class BasicStoreSerializer < ActiveModel::Serializer
  attributes :id, :name, :sort_name, :seo_slug,
             :mall_id, :retailer_id,
             :created_at, :updated_at
end
