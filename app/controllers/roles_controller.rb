class RolesController < ApplicationController
  before_action :set_role, except: [:index, :create]

  def show
    respond_with @role, root: 'role', serializer: RoleDetailSerializer
  end

  def destroy
    if @role.destroy
      head :no_content
    else
      head :unprocessable_entity
    end
  end

  def update
    if @role.update_attributes role_params
      head :ok
    else
      head :unprocessable_entity
    end
  end

  def create
    @role = Role.new role_params
    if @role.save
      respond_with @role
    else
      head :unprocessable_entity
    end
  end

  def index
    @roles = paginate policy_scope(Role)
    respond_with @roles if stale?(@roles)
  end

  private

  def set_role
    @role = policy_scope(Role).find(params[:id])
  end

  def role_params
    params.require(:role).permit(:name)
  end
end
