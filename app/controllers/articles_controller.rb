class ArticlesController < ApplicationController
  before_action :set_article, except: [:create, :index, :search]
  before_action :set_articles, only: [:index, :search]

  def show
    respond_with @article if stale?(@article)
  end

  def create
    @article = Article.new article_params
    authorize @article
    if @article.save
      respond_with @article
    else
      head :unprocessable_entity
    end
  end

  def update
    authorize @article
    if @article.update_attributes article_params
      render json: ArticleSerializer.new(@article, scope: current_account)
    else
      head :unprocessable_entity
    end
  end

  def destroy
    authorize @article
    @article.destroy
    head :no_content
  end

  def index
    @articles = @articles.by_keyword(params[:q]) if params[:q]
    respond_with @articles if stale?(@articles)
  end

  def search
    @articles = @articles.by_keyword(params[:q])
    respond_with @articles if stale?(@articles)
  end

  def products
    @products = @article.products
    respond_with @products, root: 'products' if stale?(@products)
  end

  private

  def article_params
    params.require(:article)
          .permit(:author, :attribution, :title, :subtitle,
                  :short_title, :teaser, :content, :seo_slug,
                  :display_at, :end_at, :display_order, :is_active,
                  product_ids: [], image_ids: [], mall_ids: [])
  end

  def set_article
    @article = policy_scope(Article).find(params[:id])
  end

  def set_articles
    @articles = paginate tagged current since policy_scope(Article)
  end
end
