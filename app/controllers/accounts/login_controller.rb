module Accounts
  class LoginController < Devise::SessionsController
    def create
      if Account.find_by_email(params[:account].try(:[], :email)).present?
        @account = SetupAccount.call(account_params: params[:account], mall_id: params[:mall_id])
        if @account.valid?
          sign_in @account, store: :false
          respond_with @account
        else
          head :bad_request
        end
      else
        head :not_found
      end
    end
  end
end
