module Accounts
  class PasswordsController < Devise::PasswordsController
    def reset
      @result = Account.reset_password_by_token(reset_params)
      if @result && @result.errors.empty?
        @result.confirm! unless @result.confirmed?
        sign_in @result, store: false
        respond_with @result
      else
        head :bad_request
      end
    end

    def send_reset
      if params[:account].try(:[], :email) && params[:mall_id]
        account = Account.find_by_email(params[:account][:email])
        current_mall = Mall.find_by_id(params[:mall_id])
        if account.present? && current_mall.present?
          account.current_mall = current_mall
          account.send_reset_password_instructions
        end
        head :no_content
      else
        head :bad_request
      end
    end

    private

    def reset_params
      params.require(:account)
            .permit(:reset_password_token, :password, :password_confirmation)
    end
  end
end
