module Accounts
  class ConfirmationsController < Devise::ConfirmationsController
    before_action :set_confirmable, only: [:confirm]

    def confirm
      if @confirmable.errors.empty?
        @confirmable.confirm!
        after_confirmation if params[:mall_id].present?
        sign_in @confirmable, store: false
        respond_with @confirmable
      else
        head :bad_request
      end
    end

    def resend_confirmation
      if params[:account].try(:[], :email) && params[:mall_id]
        account = Account.find_by_email(params[:account][:email])
        current_mall = Mall.find_by_id(params[:mall_id])
        if account.present? && !account.confirmed? && current_mall.present?
          account.current_mall = current_mall
          account.send_confirmation_instructions
        end
        head :no_content
      else
        head :bad_request
      end
    end

    private

    def set_confirmable
      original_token = params[:confirmation_token] || params[:account].try(:confirmation_token)
      confirmation_token = Devise.token_generator.digest(Account, :confirmation_token, original_token)
      @confirmable = Account.find_or_initialize_with_error_by(:confirmation_token, confirmation_token)
    end

    def after_confirmation
      if current_mall = Mall.find_by_id(params[:mall_id])
        PwLegacy::SewUsers::Create.call(@confirmable, params[:mall_id])
        @confirmable.malls << current_mall unless @confirmable.malls.include? current_mall
        list_ids = current_mall.programs.default.pluck(:legacy_id).join(',')
        PwLegacy::EmailPrograms::Subscribe.call(@confirmable, list_ids) unless list_ids.empty?
      end
    end
  end
end
