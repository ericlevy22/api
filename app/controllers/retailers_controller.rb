class RetailersController < ApplicationController
  before_action :set_retailer, except: [:index, :create]

  def show
    respond_with @retailer if stale?(@retailer)
  end

  def index
    @retailers = paginate tagged since policy_scope(Retailer)
    @retailers = @retailers.by_name(params[:q]) if params[:q]
    respond_with @retailers if stale?(@retailers)
  end

  def create
    @retailer = Retailer.new retailer_params
    authorize @retailer
    if @retailer.save
      respond_with @retailer
    else
      head :unprocessable_entity
    end
  end

  def update
    authorize @retailer
    if @retailer.update_attributes retailer_params
      render json: RetailerSerializer.new(@retailer)
    else
      head :unprocessable_entity
    end
  end

  def destroy
    authorize @retailer
    @retailer.destroy
    head :no_content
  end

  def contacts
    @contacts = paginate since policy_scope(@retailer.contacts)
    respond_with @contacts, root: 'contacts' if stale?(@contacts)
  end

  def stores
    @stores = paginate since policy_scope(@retailer.stores)
    if params[:lat] && params[:long]
      @stores.each { |store| store.location.calculate_distance(params[:lat], params[:long]) }
    end
    respond_with @stores, root: 'stores' if stale?(@stores)
  end

  def deals
    @deals = paginate current since policy_scope(@retailer.deals)
    respond_with @deals, root: 'deals' if stale?(@deals)
  end

  def social_feeds
    @social_feeds = paginate since policy_scope(@retailer.social_feeds)
    respond_with @social_feeds, root: 'social_feeds' if stale?(@social_feeds)
  end

  private

  def retailer_params
    params.require(:retailer)
          .permit(:name, :sort_name, :seo_slug, :description,
                  :nick_name, :phone, :email, :url,
                  :url_text, :store_text, :is_active)
  end

  def set_retailer
    @retailer = policy_scope(Retailer).find(params[:id])
  end
end
