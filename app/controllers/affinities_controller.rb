class AffinitiesController < ApplicationController
  before_action :set_affinity, except: [:index, :create]

  def index
    @affinities = paginate policy_scope(Affinity).by_heartable_type(heartable_type)
    respond_with @affinities if stale?(@affinities)
  end

  def show
    respond_with @affinity if stale?(@affinity)
  end

  def create
    @current_account = current_account
    @current_account.current_mall = Mall.find_by_id(params[:mall_id])

    @affinity = Affinity.where(user_id: @current_account.user.id,
                               heartable_type: 'Store',
                               heartable_id: affinity_params[:heartable_id])
                        .first_or_initialize(affinity_params)
    @affinity.user_id = @current_account.user.id
    authorize @affinity

    if @affinity.save
      respond_with @affinity
    else
      head :unprocessable_entity
    end
  end

  def destroy
    authorize @affinity
    @affinity.destroy
    head :no_content
  end

  private

  def heartable_type
    return if params[:heartable_type].blank?
    params[:heartable_type] = params[:heartable_type].split(/\s+,\s+/)
    Affinity::HEARTABLE_TYPES & params[:heartable_type]
  end

  def set_affinity
    @affinity = policy_scope(Affinity).find(params[:id])
  end

  def affinity_params
    params.require(:affinity).permit(:heartable_id, :heartable_type)
  end
end
