class DealsController < ApplicationController
  before_action :set_deal, only: [:show, :stores, :update, :destroy]
  before_action :set_deals, only: [:index, :search]

  def show
    respond_with @deal if stale?(@deal)
  end

  def index
    @deals = @deals.by_keyword(params[:q]) if params[:q]
    respond_with @deals if stale?(@deals)
  end

  def stores
    @stores = paginate since policy_scope(@deal.stores)
    respond_with @stores, root: 'stores' if stale?(@stores)
  end

  def search
    @deals = @deals.by_keyword(params[:q])
    respond_with @deals if stale?(@deals)
  end

  def create
    @deal = Deal.new(deal_params)
    authorize @deal
    if @deal.save
      respond_with @deal
    else
      head :unprocessable_entity
    end
  end

  def update
    authorize @deal
    if @deal.update_attributes deal_params
      render json: DealSerializer.new(@deal)
    else
      head :unprocessable_entity
    end
  end

  def destroy
    authorize @deal
    @deal.destroy
    head :no_content
  end

  private

  def set_deals
    @deals = Deal.all.includes(:tags, :images, :retailer)
    @deals = paginate tagged current since policy_scope(@deals)
  end

  def set_deal
    @deal = policy_scope(Deal).find(params[:id])
  end

  def deal_params
    params.require(:deal)
          .permit(:retailer_id, :promo_type, :sales_type,
                  :valid_at_type_id, :title, :seo_slug,
                  :external_url, :display_at, :start_at,
                  :end_at, :description,
                  :fine_print_description, :is_local,
                  :is_featured, :is_active)
  end
end
