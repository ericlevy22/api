class StoresController < ApplicationController
  before_action :set_store, except: [:index, :create]

  def show
    @store.location.calculate_distance(params[:lat], params[:long]) if params[:lat] && params[:long]
    respond_with @store if stale?(@store)
  end

  def index
    @stores = paginate since policy_scope(Store).includes(:location, :mall, :retailer, :groups, :tags, :keywords, retailer: [:images, :tags, :keywords])
    @stores = @stores.by_keyword(params[:q]) if params[:q]
    if params[:lat] && params[:long]
      @stores.each { |s| s.location.calculate_distance(params[:lat], params[:long]) }
    end
    respond_with @stores if stale?(@stores)
  end

  def create
    @store = Store.new store_params
    authorize @store
    if @store.save
      respond_with @store
    else
      head :unprocessable_entity
    end
  end

  def update
    authorize @store
    if @store.update_attributes store_params
      render json: StoreSerializer.new(@store)
    else
      head :unprocessable_entity
    end
  end

  def destroy
    authorize @store
    @store.destroy
    head :no_content
  end

  def products
    @products = paginate since policy_scope(@store.products)
    respond_with @products, root: 'products' if stale?(@products)
  end

  def contacts
    @contacts = paginate since policy_scope(@store.contacts)
    respond_with @contacts, root: 'contacts' if stale?(@contacts)
  end

  def deals
    @deals = paginate current since policy_scope(@store.deals)
    @deals.each { |deal| deal.current_store = @store }
    respond_with @deals, root: 'deals' if stale?(@deals)
  end

  def job_listings
    @job_listings = paginate since policy_scope(@store.job_listings)
    respond_with @job_listings, root: 'job_listings' if stale?(@job_listings)
  end

  def social_feeds
    @social_feeds = (paginate since policy_scope(@store.social_feeds)).map { |feed| SocialFeedSerializer.new(feed, root: false) }
    @retailer_social_feeds = (paginate since policy_scope(@store.retailer_social_feeds)).map { |feed| SocialFeedSerializer.new(feed, root: false) }
    @feeds = { social_feeds: @social_feeds, retailer_social_feeds: @retailer_social_feeds }
    respond_with @feeds if stale?(@social_feeds) || stale?(@retailer_social_feeds)
  end

  private

  def store_params
    params.require(:store)
          .permit(:mall_id, :retailer_id, :location_id, :name,
                  :sort_name, :seo_slug, :address, :city, :state,
                  :zip, :phone, :hours, :directory_tags, :email,
                  :url, :url_text, :fax, :description, :job_text,
                  :is_featured, :open_at, :close_at, :tenant_location,
                  :opening_date, :opening_text, :exclude_national_deals)
  end

  def set_store
    @store = policy_scope(Store).find(params[:id])
  end
end
