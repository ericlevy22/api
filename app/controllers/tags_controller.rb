class TagsController < ApplicationController
  def show
    @tag = policy_scope(Tag).find(params[:id])
    respond_with @tag, root: 'tag', serializer: TagDetailSerializer if stale?(@tag)
  end

  def index
    @tags = paginate tagged since policy_scope(Tag)
    @tags = @tags.by_name(params[:q]) if params[:q]
    respond_with @tags if stale?(@tags)
  end

  def create
    @tag = Tag.new tag_params
    authorize @tag
    if @tag.save
      respond_with @tag
    else
      head :unprocessable_entity
    end
  end

  def search
    @tags = paginate since policy_scope(Tag).where('tags.name like ?', "%#{params[:q]}%")
    respond_with @tags if stale?(@tags)
  end

  private

  def tag_params
    params.require(:tag).permit(:name)
  end
end
