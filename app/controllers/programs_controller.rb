class ProgramsController < ApplicationController
  before_action :set_program, except: [:create, :index]

  def show
    respond_with @program if stale?(@program)
  end

  def index
    @programs = paginate since policy_scope(Program)
    respond_with @programs if stale?(@programs)
  end

  def create
    @program = Program.new program_params
    authorize @program
    if @program.save
      respond_with @program
    else
      head :unprocessable_entity
    end
  end

  def update
    authorize @program
    if @program.update_attributes program_params
      render json: ProgramSerializer.new(@program)
    else
      head :unprocessable_entity
    end
  end

  def destroy
    authorize @program
    @program.destroy
    head :no_content
  end

  private

  def program_params
    params.require(:program)
          .permit(:owner_id, :owner_type, :properties, :program_type,
                  :is_default_signup_program, :is_internal_only,
                  :is_subscribable)
  end

  def set_program
    @program = policy_scope(Program).find(params[:id])
  end
end
