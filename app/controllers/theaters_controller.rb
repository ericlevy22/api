class TheatersController < ApplicationController
  before_action :set_theater, except: [:index]

  def show
    respond_with @theater if stale?(@theater)
  end

  def showtimes
    @showtimes = Gracenote::OnConnect::Theater.showtimes(@theater.external_key, size: 'Ms')
    @showtimes.map! do |movie|
      {
        id: movie['tmsId'],
        title: movie['title'],
        seo_slug: movie['title'].slugify,
        genres: movie['genres'],
        rating: extract_rating(movie['ratings']),
        duration: translate_duration(movie['runTime']),
        directors: movie['directors'],
        short_description: movie['shortDescription'],
        long_description: movie['longDescription'],
        image_uri: "#{AppConfig.urls['onconnect_images_base_url']}#{movie['preferredImage']['uri']}",
        website_uri: movie['officialUrl'],
        showings: extract_showtimes(movie['showtimes'])
      }
    end
    render json: { showtimes: @showtimes }
  end

  private

  def set_theater
    @theater = policy_scope(Theater).find(params[:id])
  end

  def translate_duration(duration)
    return nil unless duration
    hours   = duration[/\d+H/].to_i
    minutes = duration[/\d+M/].to_i
    hours   = hours.try(:>, 0)   ? "#{hours} #{'hour'.pluralize(hours)}"       : ""
    minutes = minutes.try(:>, 0) ? "#{minutes} #{'minute'.pluralize(minutes)}" : ""
    "#{hours} #{minutes}".strip
  end

  def extract_rating(ratings)
    (ratings || []).map { |rating| rating['code'] if rating['body'] == 'Motion Picture Association of America' }.compact.join.presence
  end

  def extract_showtimes(showtimes)
    (showtimes || []).map { |showtime| { showing_at: showtime['dateTime'] } }
  end
end
