class AccountsController < ApplicationController
  before_action :set_account, except: [:index, :create, :logout]

  def create
    @account = SetupAccount.call(account_params: account_params,
                                 properties_params: properties_params,
                                 mall_id: params[:mall_id])
    if @account.valid?
      sign_in @account, store: :false
      respond_with @account
    else
      head :bad_request
    end
  end

  def show
    respond_with @account
  end

  def update
    authorize @account
    compare_sms
    new_params = updatable_account_params
    new_params[:properties] = (@account.properties || {}).deep_merge(new_params[:properties] || {})

    if @account.update(new_params)
      render json: AccountSerializer.new(@account)
    else
      head :unprocessable_entity
    end
  end

  def destroy
    if @account.destroy
      head :no_content
    else
      head :unprocessable_entity
    end
  end

  def logout
    authorize current_account
    current_account.update_attributes(authentication_token: nil)
    sign_out
    head :no_content
  end

  def affinities
    @affinities = paginate @account.user.affinities.by_heartable_type(heartable_type)
    respond_with @affinities, root: 'affinities' if stale?(@affinities)
  end

  def memberships
    # TODO: re-enable the normal mechanism once data has been migrated from legacy
    # @memberships = paginate @account.memberships
    # respond_with @memberships, root: 'memberships' if stale?(@memberships)
    if params[:mall_id].blank?
      head :bad_request
    else
      begin
        identity = Identity.find_by_email(@account.email)
        render json: PwLegacy::EmailPrograms::GetMemberships.call(identity, params[:mall_id])
      rescue JSON::ParserError
        head :bad_gateway
      end
    end
  end

  def roles
    @roles = paginate @account.roles
    respond_with @roles, root: 'roles' if stale?(@roles)
  end

  def add_roles
    authorize @account, :add_roles?
    params[:roles].split(/,\s*/).each { |role| @account.add_role role } if params[:roles].present?
    head :ok
  end

  def remove_roles
    authorize @account, :remove_roles?
    params[:roles].split(/,\s*/).each { |role| @account.remove_role role } if params[:roles].present?
    head :no_content
  end

  def index
    fail Pundit::AuthorizationNotPerformedError unless current_account
    @accounts = paginate since policy_scope(Account)
    respond_with @accounts if stale?(@accounts)
  end

  private

  def devise_controller?
    true
  end

  def heartable_type
    return if params[:heartable_type].blank?
    params[:heartable_type] = params[:heartable_type].split(/\s+,\s+/)
    Affinity::HEARTABLE_TYPES & params[:heartable_type]
  end

  def set_account
    @account = policy_scope(Account).find(params[:id])
    if @account && params[:mall_id]
      @account.current_mall ||= Mall.find_by_id(params[:mall_id])
    end
  end

  def account_params
    params.require(:account).permit(:email, :password, :facebook_access_token)
  end

  def properties_params
    params.require(:account)
          .permit(:email, :password, :facebook_access_token,
                  properties: [
                                { name: [:first, :last] },
                                { date_of_birth: [:day, :month, :year] },
                                { location: [:address, :city, :state, :zip] },
                                { phone: [:sms, :home, :work, :other] },
                                :gender, :redirect_url, :unconfirmed_mall_id,
                                :unsubscribed_sms_number, :unsubscribed_program_ids
                  ])
  end

  def updatable_account_params
    fail ActionController::ParameterMissing unless params[:account].present?

    permissible = [
      properties: [
        { location: [:address, :city, :state, :zip] },
        { phone: [:sms, :home, :work, :other] },
        :gender
      ]
    ]

    if params[:account][:password].present? && params[:account][:password_confirmation].present?
      if params[:account][:password] == params[:account][:password_confirmation]
        fail Pundit::NotAuthorizedError unless @account.valid_password?(params[:account][:current_password])
        permissible.unshift(:password)
      end
    end
    params.require(:account).permit(permissible)
  end

  def compare_sms
    return unless params[:mall_id]
    sms = params.try(:[], :account).try(:[], :properties).try(:[], :phone).try(:[], :sms)
    old_sms = @account.properties.try(:[], 'phone').try(:[], 'sms')
    PwLegacy::SmsPrograms::Subscribe.call(sms, params[:mall_id]) if sms && sms != old_sms
  end
end
