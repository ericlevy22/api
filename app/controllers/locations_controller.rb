class LocationsController < ApplicationController
  before_action :validate_lat_and_long, only: :search
  before_action :set_location, except: [:index, :search, :create]
  before_action :set_locations, only: [:index, :search]

  def show
    respond_with @location if stale?(@location, last_modified: @location.updated_at)
  end

  def index
    respond_with @locations, each_serializer: LocationSerializer if stale?(@locations)
  end

  def create
    @location = Location.new location_params
    authorize @location
    if @location.save
      respond_with @location
    else
      head :unprocessable_entity
    end
  end

  def update
    authorize @location
    if @location.update_attributes location_params
      render json: LocationSerializer.new(@location)
    else
      head :unprocessable_entity
    end
  end

  def destroy
    authorize @location
    @location.destroy
    head :no_content
  end

  def stores
    @stores = paginate since @location.stores
    respond_with @stores, root: 'stores' if stale?(@stores)
  end

  def malls
    @malls = paginate since @location.malls
    respond_with @malls, root: 'malls' if stale?(@malls)
  end

  def deals
    @deals = paginate since @location.deals
    respond_with @deals, root: 'deals' if stale?(@deals)
  end

  def products
    @products = paginate since @location.products
    respond_with @products, root: 'products' if stale?(@products)
  end

  def search
    @locations = @locations.by_lat_and_long(params[:lat], params[:long], params[:radius])
    set_distances
    respond_with @locations if stale?(@locations)
  end

  private

  def location_params
    params.require(:location).permit(:latitude, :longitude, :time_zone)
  end

  def set_location
    @location = policy_scope(Location).find params[:id]
    @location.calculate_distance(params[:lat], params[:long]) if params[:lat] && params[:long]
  end

  def set_locations
    @locations = paginate since policy_scope(Location)
    set_distances
  end

  def set_distances
    return unless params[:lat] && params[:long]
    @locations.each { |l| l.calculate_distance(params[:lat], params[:long]) }
  end

  def validate_lat_and_long
    head :bad_request unless params[:lat] && params[:long]
  end
end
