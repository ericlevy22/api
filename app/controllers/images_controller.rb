class ImagesController < ApplicationController
  before_filter :set_image, except: [:index, :create]

  def index
    @images = paginate since policy_scope(Image)
    respond_with @images if stale?(@images)
  end

  def show
    respond_with @image if stale?(@image)
  end

  def create
    @image = Images::Upload.upload_and_build(image_params)
    authorize @image
    if @image.save
      respond_with @image
    else
      head :unprocessable_entity
    end
  end

  def destroy
    authorize @image
    @image.destroy
    head :no_content
  end

  private

  def image_params
    params.require(:image)
          .permit(:url, :size_parameter, :imageable_id,
                  :imageable_type, :file_data, :file_name)
  end

  def set_image
    @image = policy_scope(Image).find(params[:id])
  end
end
