class GroupsController < ApplicationController
  before_action :set_group, except: [:create, :index]

  def index
    @groups = paginate policy_scope(Group)
    @groups = @groups.by_name(params[:q]) if params[:q]
    respond_with @groups if stale? @groups
  end

  def show
    @group = policy_scope(Group).find(params[:id])
    respond_with @group if stale? @group
  end

  def create
    @group = Group.new(group_params)
    authorize @group
    if @group.save
      respond_with @group
    else
      head :unprocessable_entity
    end
  end

  def update
    authorize @group
    if @group.update_attributes group_params
      render json: GroupSerializer.new(@group)
    else
      head :unprocessable_entity
    end
  end

  def destroy
    authorize @group
    @group.destroy
    head :no_content
  end

  private

  def group_params
    params.require(:group).permit(:name)
  end

  def set_group
    @group = policy_scope(Group).find(params[:id])
  end
end
