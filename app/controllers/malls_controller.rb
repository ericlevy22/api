class MallsController < ApplicationController
  before_action :set_mall, :set_distance, except: [:index, :create]

  def index
    @malls = paginate since policy_scope(Mall).includes(:location, :images)
    @malls = @malls.by_group(params[:group_ids]) if params[:group_ids]
    @malls = @malls.by_name(params[:q]) if params[:q]
    set_distance
    respond_with @malls if stale?(@malls)
  end

  def show
    respond_with @mall if stale?(@mall)
  end

  def create
    @mall = Mall.new mall_params
    authorize @mall
    if @mall.save
      respond_with @mall
    else
      head :unprocessable_entity
    end
  end

  def update
    authorize @mall
    if @mall.update_attributes mall_params
      render json: MallSerializer.new(@mall)
    else
      head :unprocessable_entity
    end
  end

  def destroy
    authorize @mall
    @mall.destroy
    head :no_content
  end

  def articles
    @articles = paginate current since policy_scope(@mall.articles)
    @articles = @articles.by_keyword(params[:q]) if params[:q]
    @articles.map { |x| x.current_mall = @mall }
    respond_with @articles, root: 'articles' if stale?(@articles)
  end

  def contacts
    @contacts = paginate since policy_scope(@mall.contacts)
    @contacts = @contacts.by_name(params[:q]) if params[:q]
    respond_with @contacts, root: 'contacts' if stale?(@contacts)
  end

  def contact_us
    @contact_us = ContactUsEmail.new mail_params
    authorize @contact_us
    if @contact_us.save
      head :accepted
    else
      head :bad_request
    end
  end

  def press_releases
    @press_releases = paginate current since policy_scope(@mall.press_releases)
    @press_releases = @press_releases.by_name(params[:q]) if params[:q]
    respond_with @press_releases, root: 'press_releases' if stale?(@press_releases)
  end

  def stores
    @stores = paginate since @mall.stores.includes(:tags, :keywords)
    @stores = @stores.by_keyword(params[:q]) if params[:q]
    if params[:lat] && params[:long]
      @stores.each { |store| store.location.calculate_distance(params[:lat], params[:long]) }
    end

    serializer = if current_account.has_role? :admin
      StoreSerializer
    elsif current_account.has_role? :store_channel
      StoreSerializer
    else
      BasicStoreSerializer
    end

    respond_with @stores, root: 'stores', each_serializer: serializer if stale?(@stores)
  end

  def deals
    @deals = @mall.deals.includes(:tags, :images, :retailer)
    @deals = paginate current since policy_scope(@deals)
    @deals = @deals.by_keyword(params[:q]) if params[:q]
    if params[:limit].to_i < 45
      @deals.each do |deal|
        deal.current_store = deal.stores.where("stores.id": @mall.stores.pluck(:id)).first
      end
    else
      @stores = Store.includes(:retailer).where(mall_id: @mall.id)
      @deals.each do |deal|
        @stores.each do |store|
          deal.current_store = store if deal.retailer == store.retailer
        end
      end
    end
    respond_with @deals, root: 'deals' if stale?(@deals)
  end

  def hours
    render json: { hours: Hour.effective_hours(from, to, @mall) }
  end

  def current_hours
    start = if params[:start]
      Date.parse(params[:start])
    else
      Date.today
    end
    render json: Hour.current_hours(start, @mall)
  end

  def events
    @events = paginate grouped since policy_scope(@mall.events.by_date_range(from, to))
    @events = @events.by_title(params[:q]) if params[:q]
    @events = @events.where(is_featured: true) if params[:only_featured].present?
    respond_with @events, root: 'events' if stale?(@events)
  end

  def ifeatures
    @ifeatures = paginate current since policy_scope(@mall.ifeatures)
    @ifeatures = @ifeatures.by_keyword(params[:q]) if params[:q]
    respond_with @ifeatures, root: 'i_features' if stale?(@ifeatures)
  end

  def social_feeds
    @social_feeds = paginate since policy_scope(@mall.social_feeds)
    respond_with @social_feeds, root: 'social_feeds' if stale?(@social_feeds)
  end

  def job_listings
    @job_listings = paginate current since policy_scope(@mall.job_listings)
    @job_listings = @job_listings.by_title(params[:q]) if params[:q]
    respond_with @job_listings, root: 'job_listings' if stale?(@job_listings)
  end

  def mall_messages
    @mall_messages = paginate since policy_scope(@mall.mall_messages.current)
    @mall_messages = @mall_messages.by_description(params[:q]) if params[:q]
    respond_with @mall_messages, root: 'mall_messages' if stale?(@mall_messages)
  end

  def programs
    @programs = paginate since policy_scope(@mall.programs)
    respond_with @programs, root: 'programs' if stale?(@programs)
  end

  def theaters
    respond_with policy_scope(@mall.theaters), root: 'theaters' if stale?(@mall.theaters)
  end

  private

  def mail_params
    params[:mail][:to] = @mall.contact_us_email || @mall.email
    params.require(:mail).permit(:from, :to, :subject, :body)
  end

  def mall_params
    params.require(:mall)
          .permit(:company_id, :location_id, :name, :sort_name,
                  :nick_name, :seo_slug, :address, :city, :state,
                  :zip, :phone, :url, :email, :is_live, :is_development,
                  :is_archived, :hours_text, :site_type, :lang_code,
                  :created_at, :updated_at, :exclude_national_deals,
                  :description, :guest_services_description)
  end

  def from
    params[:from].present? ? Date.strptime(params[:from], '%Y-%m-%d') : Date.today.beginning_of_month
  end

  def to
    params[:to].present? ? Date.strptime(params[:to], '%Y-%m-%d') : Date.today + 1.month
  end

  def set_mall
    @mall = policy_scope(Mall).find(params[:id])
  end

  def set_distance
    return unless params[:lat] && params[:long]
    Array(@mall || @malls).each { |mall| mall.location.calculate_distance(params[:lat], params[:long]) }
  end
end
