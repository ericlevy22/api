class ProductsController < ApplicationController
  before_filter :set_product, except: [:index, :create]

  def index
    @products = paginate since policy_scope(Product)
    @products = @products.by_name(params[:q]) if params[:q]
    respond_with @products if stale?(@products)
  end

  def show
    respond_with @product if stale?(@product)
  end

  def create
    @product = Product.new product_params
    authorize @product
    if @product.save
      respond_with @product
    else
      head :unprocessable_entity
    end
  end

  def update
    authorize @product
    if @product.update_attributes product_params
      render json: ProductSerializer.new(@product)
    else
      head :unprocessable_entity
    end
  end

  def destroy
    authorize @product
    @product.destroy
    head :no_content
  end

  def stores
    @stores = paginate since @product.stores
    if params[:lat] && params[:long]
      @stores.each { |s| s.location.calculate_distance(params[:lat], params[:long]) }
    end
    respond_with @stores, root: 'stores' if stale?(@stores)
  end

  private

  def product_params
    params.require(:product)
          .permit(:brand_id, :name, :seo_slug, :image_file_name,
                  :description, :price, :sale_price, :retailer_id, store_ids: [],
                  tag_ids: [], image_ids: [])
  end

  def set_product
    @product = policy_scope(Product).find(params[:id])
  end
end
