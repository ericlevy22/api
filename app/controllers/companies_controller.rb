class CompaniesController < ApplicationController
  before_action :set_company, except: [:index, :create]

  def show
    respond_with @company if stale?(@company)
  end

  def index
    @companies = paginate since policy_scope(Company)
    respond_with @companies if stale?(@companies)
  end

  def create
    @company = Company.new company_params
    authorize @company
    if @company.save
      respond_with @company
    else
      head :unprocessable_entity
    end
  end

  def update
    authorize @company
    if @company.update_attributes company_params
      render json: CompanySerializer.new(@company)
    else
      head :unprocessable_entity
    end
  end

  def destroy
    authorize @company
    @company.destroy
    head :no_content
  end

  def malls
    @malls = paginate since @company.malls
    respond_with @malls, root: 'malls' if stale?(@malls)
  end

  def contacts
    @contacts = paginate since @company.contacts
    respond_with @contacts, root: 'contacts' if stale?(@contacts)
  end

  private

  def company_params
    params.require(:company)
          .permit(:is_active, :url, :fax, :phone, :zip,
                  :state, :city, :address, :seo_slug,
                  :test_url, :nick_name, :name)
  end

  def set_company
    @company = policy_scope(Company).find(params[:id])
  end
end
