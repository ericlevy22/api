class JobListingsController < ApplicationController
  def mall
    @job_listings = paginate current since policy_scope(JobListing).by_mall(params[:id])
    respond_with @job_listings if stale?(@job_listings)
  end

  def store
    @job_listings = paginate current since policy_scope(JobListing).by_store(params[:id])
    respond_with @job_listings if stale?(@job_listings)
  end
end
