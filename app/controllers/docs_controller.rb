class DocsController < Apitome::DocsController
  def index
    @authorized_resources = resources.map do |resource|
      resource['examples'].select! do |example|
        params[:admin] == "true" ? true : (Array(example['groups']) & ["admin"]).size == 0
      end
      resource['examples'].compact!
      resource if resource['examples'].size > 0
    end.compact
    super
  end
end
