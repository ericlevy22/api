class BrandsController < ApplicationController
  before_action :set_brand, except: [:create, :index]

  def show
    respond_with @brand if stale?(@brand)
  end

  def index
    @brands = paginate since policy_scope(Brand)
    @brands = @brands.by_name(params[:q]) if params[:q]
    respond_with @brands if stale?(@brands)
  end

  def create
    @brand = Brand.new brand_params
    authorize @brand
    if @brand.save
      respond_with @brand
    else
      head :unprocessable_entity
    end
  end

  def destroy
    authorize @brand
    @brand.destroy
    head :no_content
  end

  def update
    authorize @brand
    if @brand.update_attributes brand_params
      render json: BrandSerializer.new(@brand)
    else
      head :unprocessable_entity
    end
  end

  private

  def brand_params
    params.require(:brand).permit(:name, :description, :seo_slug, :url, :is_approved)
  end

  def set_brand
    @brand = policy_scope(Brand).find(params[:id])
  end
end
