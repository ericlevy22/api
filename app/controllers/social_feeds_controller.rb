class SocialFeedsController < ApplicationController
  before_action :set_social_feed, except: [:index, :create]

  def show
    respond_with @social_feed, root: 'social_feed' if stale?(@social_feed)
  end

  def index
    @social_feeds = paginate since policy_scope(SocialFeed)
    respond_with @social_feeds, root: 'social_feeds' if stale?(@social_feeds)
  end

  def create
    @social_feed = SocialFeed.new social_feed_params
    authorize @social_feed
    if @social_feed.save
      respond_with @social_feed
    else
      head :unprocessable_entity
    end
  end

  def update
    authorize @social_feed
    if @social_feed.update_attributes social_feed_params
      render json: SocialFeedSerializer.new(@social_feed)
    else
      head :unprocessable_entity
    end
  end

  def destroy
    authorize @social_feed
    @social_feed.destroy
    head :no_content
  end

  private

  def social_feed_params
    params.require(:social_feed)
          .permit(:social_network_id, :sociable_type, :sociable_id,
                  :properties, :is_active)
  end

  def set_social_feed
    @social_feed = policy_scope(SocialFeed).find(params[:id])
  end
end
