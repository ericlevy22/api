class ApplicationController < ActionController::Base
  include Pundit
  respond_to :json
  protect_from_forgery with: :null_session

  before_action :authenticate_account_from_token!, :authenticate_account!, unless: :publicly_accessible?
  before_action :set_format, :set_default_params, :inject_version
  before_action :mini_profiler
  after_action :verify_policy_scoped, except: [:create, :options], unless: :devise_controller?
  after_action :verify_authorized, only: [:create, :update, :delete], unless: :devise_controller?


  rescue_from Pundit::NotAuthorizedError, with: :four_oh_three
  rescue_from Pundit::AuthorizationNotPerformedError, with: :four_oh_one
  rescue_from SetupAccount::NotAuthorizedError, with: :four_oh_one
  rescue_from ActiveRecord::RecordNotFound, with: :four_oh_four
  rescue_from ArgumentError, with: :four_hundred
  rescue_from ActionController::ParameterMissing, with: :four_hundred
  rescue_from JSON::ParserError, with: :five_oh_two

  private

  def paginate(collection)
    collection = collection.order(:id) if collection.try(:order_values).try(:empty?)
    collection.limit(params[:limit]).offset(params[:offset])
  end

  def current(collection)
    collection = collection.current unless include_archived?
    collection
  end

  def since(collection)
    collection = collection.since(params[:since]) if params[:since]
    collection
  end

  def tagged(collection, tags: params[:tags], context: :tags)
    if tags.present?
      any = true unless params[:strict].present?
      tags = tags.split(/\s*,\s*/).reject(&:empty?)
      collection = collection.tagged_with(tags, on: context, any: any)
    end
    collection
  end

  def grouped(collection)
    tagged(collection, tags: params[:groups], context: :groups)
  end

  def inject_version
    response.headers['X-API-Version'] = AppConfig.settings['version']
  end

  def include_archived?
    return true if params.key?(:show_archived) && current_account.has_role?(:admin)
    return true if params.key?(:show_archived) && current_account.has_role?(:show_archived)
  end

  def set_format
    request.format = 'json'
  end

  def four_hundred
    head :bad_request
  end

  def four_oh_four
    head :not_found
  end

  def four_oh_one
    head :unauthorized
  end

  def four_oh_three
    head :forbidden
  end

  def five_oh_two
    head :bad_gateway
  end

  def set_default_params
    params[:limit]  = 500 if params[:limit].to_i > 500
    params[:limit]  = (1..500).include?(params[:limit].to_i)    ? params[:limit].to_i  :  10
    params[:offset] = params[:offset].to_i > 0                  ? params[:offset]      :  0
    params[:lat]    = (-90..90).include?(params[:lat].to_f)     ? params[:lat]         :  39.74585
    params[:long]   = (-180..180).include?(params[:long].to_f)  ? params[:long]        : -104.998929
    params[:radius] = ((1..100).include?(params[:radius].to_f)  ? params[:radius].to_i :  50)
  end

  def authenticate_account_from_token!
    account   = params[:account_email].presence && Account.find_by_email(params[:account_email])
    account ||= params[:account_id].presence    && Account.find_by_id(params[:account_id])

    return unless account &&
                  timestamp_good? &&
                  Devise.secure_compare(account_token(account), params[:account_token].downcase)
    sign_in account, store: false
  end

  def timestamp
    params[:timestamp]
  end

  def timestamp_good?
    return false unless timestamp
    time = DateTime.strptime(params[:timestamp], '%s')
    timestamp &&
      (time < AppConfig.settings['request_valid_within'].seconds.from_now &&
       time > AppConfig.settings['request_valid_within'].seconds.ago)
  rescue ArgumentError
    false
  end

  def account_token(account)
    Digest::MD5.hexdigest("#{params[:timestamp]}:#{account.authentication_token}").downcase
  end

  def current_user # this is only here because Pundit calls current_user no matter what...   :(
    account = current_account
    if account && params[:mall_id]
      account.current_mall ||= Mall.find_by_id(params[:mall_id])
    end
    account
  end

  def publicly_accessible?
    false
  end

  def mini_profiler
    return unless current_account.try(:has_role?, :admin)
    Rack::MiniProfiler.authorize_request
    render '/rack_mini_profiler', format: 'html' if params[:rack_results] == 'true'
  end
end
