class MonitorController < ApplicationController
  def status
    skip_policy_scope
    @test_db = Mall.connected?
    if @test_db
      render json: { status: 'online' }, status: :ok
    else
      render json: { status: 'db connection lost' }, status: :service_unavailable
    end
  end

  private

  def publicly_accessible?
    true
  end
end
