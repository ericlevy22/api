class ContactsController < ApplicationController
  def show
    @contact = policy_scope(Contact).find(params[:id])
    respond_with @contact if stale?(@contact)
  end

  def index
    @contacts = paginate since policy_scope(Contact)
    respond_with @contacts, root: 'contacts' if stale?(@contacts)
  end

  def create
    @contact = Contact.new contact_params
    authorize @contact
    if @contact.save
      respond_with @contact
    else
      head :unprocessable_entity
    end
  end

  def update
    @contact = policy_scope(Contact).find(params[:id])
    authorize @contact
    if @contact.update_attributes contact_params
      render json: ContactSerializer.new(@contact)
    else
      head :unprocessable_entity
    end
  end

  def destroy
    @contact = policy_scope(Contact).find(params[:id])
    authorize @contact
    @contact.destroy
    head :no_content
  end

  private

  def contact_params
    params.require(:contact)
          .permit(:name, :title, :email, :phone,
                  :contactable_id, :contactable_type)
  end
end
