class MembershipsController < ApplicationController
  # before_action :set_membership, except: [:create, :index]

  # def show
  #   respond_with @membership if stale?(@membership)
  # end

  # def index
  #   @memberships = paginate since policy_scope(Membership)
  #   respond_with @memberships if stale?(@memberships)
  # end

  def create
    # TODO: reenable all this once our data is migrated
    # @membership = Membership.new(membership_params)
    # @membership.identity_id = params[:identity_id]
    # authorize @membership
    # if @membership.save
    #   respond_with @membership
    # else
    #   head :unprocessable_entity
    # end
    skip_authorization
    if params[:membership].try(:[], :program_id).blank?
      head :bad_request
    elsif PwLegacy::EmailPrograms::Subscribe.call(current_account, params[:membership][:program_id])
      head :created
    else
      # is this a thing? do we make it here?
      head :unprocessable_entity
    end
  end

  # def update
  #   authorize @membership
  #   if @membership.update_attributes membership_params
  #     render json: MembershipSerializer.new(@membership)
  #   else
  #     head :unprocessable_entity
  #   end
  # end

  def destroy
    # TODO: reenable the normal logic once we have data  :/
    # authorize @membership
    # @membership.destroy
    # head :no_content
    skip_authorization
    skip_policy_scope
    if params[:membership].try(:[], :program_id).blank?
      head :bad_request
    elsif PwLegacy::EmailPrograms::Unsubscribe.call(current_account.email, params[:membership][:program_id])
      skip_authorization
      head :no_content
    else
      # is this a thing? do we make it here?
      head :unprocessable_entity
    end
  end

  # private

  # def membership_params
  #   params[:identity_id] = Identity.find_by_email(current_account.email).id if params[:identity_id].blank?
  #   params.require(:membership).permit(:program_id, :identity_id, :properties)
  # end

  # def set_membership
  #   @membership = policy_scope(Membership).find(params[:id])
  # end
end
