class AccountsMailer < Devise::Mailer
  def self.confirmation_instructions(record, token, opts = {})
    @record, @token, @opts = record, token, opts
    self.mailer.confirmation_email(@record, @token)
  end

  def self.send_welcome_email(record, opts = {})
    @record, @opts = record, opts
    generate_confirmation_token
    return self.mailer.welcome_email(@record, @token).deliver unless @record.current_mall.present?

    # gift_card_malls = if Date.today >= Date.new(2017, 05, 01)
    #                     [619, 621, 622, 862, 623, 801, 744, 941, 1091]
    #                   else
    #                     [619, 620, 621, 622, 862, 623, 801, 744, 941, 1091]
    #                   end
    gift_card_malls = []

    if gift_card_malls.include? @record.current_mall.id
      @newsletter_user_id = @opts[:newsletter_user_id]
      PwLegacy::EmailPrograms::GetBarcode.call(@newsletter_user_id , @record.current_mall.id)
      self.mailer.gift_card_welcome_email(@record, @token, @newsletter_user_id).deliver
    else
      self.mailer.welcome_email(@record, @token).deliver
    end
  end

  def self.reset_password_instructions(record, token, opts = {})
    @record, @token, @opts = record, token, opts
    self.mailer.reset_password_email(@record, token)
  end

  def initialize(method, record, token, opts = {})
    @method, @record, @token, @opts = method, record, token, opts
  end

  private

  def self.generate_confirmation_token
    @token, token_hash = Devise.token_generator.generate(Account, :confirmation_token)

    @record.confirmation_token   = token_hash
    @record.confirmation_sent_at = Time.now.utc
    @record.save(validate: false)
  end

  def self.mailer
    if @record.current_mall.present?
      if [813, 849].include? @record.current_mall.id
        ForestCity::UrbanMailer
      elsif [619, 620, 863, 622, 862, 621, 1096, 868].include? @record.current_mall.id
        ForestCity::MidSuburbanMailer
      elsif [801, 941, 623, 820, 1091, 744].include? @record.current_mall.id
        ForestCity::LifestyleMailer
      else
        AccountMailer
      end
    else
      AccountMailer
    end
  end
end
