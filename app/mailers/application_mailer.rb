class ApplicationMailer < ActionMailer::Base
  default from: AppConfig.emails['system_events']
  layout 'mailer'
end
