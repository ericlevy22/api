include ApplicationHelper

module ForestCity
  class BaseMailer < ApplicationMailer
    def confirmation_email(account, token)
      welcome_email(account, token)
    end

    def welcome_email(account, token)
      @account, @token, @mall = account, token, account.current_mall
      @confirmation_url = confirmation_url
      set_social_handles
      mail(
        to: account.email,
        from: @mall.email,
        subject: "You're IN!",
        template_name: 'welcome_email'
      )
    end

    def gift_card_welcome_email(account, token, newsletter_user_id)
      @account, @token, @mall = account, token, account.current_mall
      @newsletter_user_id = newsletter_user_id
      @confirmation_url = confirmation_url
      set_social_handles
      headers['X-SMTPAPI'] = {
                                'category' => [
                                  "fce-insider-incentive-#{@mall.id}-#{@mall.nick_name}"
                                ]
                             }.to_json
      mail(
        to: @account.email,
        from: @mall.email,
        subject: "You're In! FREE* $20 Gift Card is waiting for You!!",
        template_name: 'gift_card_welcome_email'
      )
    end

    def reset_password_email(account, token)
      @account, @token, @mall = account, token, account.current_mall
      @reset_password_url = reset_password_url
      set_social_handles
      mail(
        to: account.email,
        from: @mall.email,
        subject: "Password Reset"
      )
    end

    private

    def confirmation_url
      "#{@mall.url.gsub(/\/$/, '')}/profile?confirmation_token=#{@token}"
    end

    def reset_password_url
      "#{@mall.url.gsub(/\/$/, '')}/reset-password?reset_password_token=#{@token}"
    end

    def set_social_handles
      @twitter_handle  = @mall.social_feeds.where(social_network_id: 1).first.try(:[], :properties).try(:[], 'handle')
      @facebook_handle = @mall.social_feeds.where(social_network_id: 2).first.try(:[], :properties).try(:[], 'handle')
    end
  end
end
