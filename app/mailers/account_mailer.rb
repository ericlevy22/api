class AccountMailer < ApplicationMailer
  def confirmation_email(account, confirmation_url)
    @account, @confirmation_url = account, confirmation_url
    mail(
      to: account.email,
      from: AppConfig.emails['system_events'],
      subject: "Confirm your email address (#{account.email})"
    )
  end

  def reset_password_email(account, reset_password_url)
    @account, @reset_password_url = account, reset_password_url
    mail(
      to: account.email,
      from: AppConfig.emails['system_events'],
      subject: "Password reset instructions"
    )
  end
end
