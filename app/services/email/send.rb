module Email
  class Send
    def self.call(options)
      client = SendGrid::Client.new do |c|
        c.api_user = Rails.application.secrets.email_gateway_username
        c.api_key  = Rails.application.secrets.email_gateway_password
      end

      mail = SendGrid::Mail.new do |m|
        m.to      = options[:to]
        m.from    = options[:from]
        m.subject = options[:subject]
        m.html    = options[:body]
      end

      client.send(mail).body == { "message" => "success" }
    end
  end
end
