module Etl
  class Deletions < ActiveRecord::Base
    def self.execute(sql)
      self.connection.execute(sanitize_sql(sql))
    end

    def self.get_deletions(limit: 250)
      execute(["select * from api_execution_deletion where etl_processed_date is null and typeis in ('#{table_map.keys.join("','")}') limit ?", limit])
    end

    def self.resolve_deletion(id)
      raise ArgumentError unless id.present?
      execute(["update api_execution_deletion set etl_processed_date = '#{Time.now}' where id = ?", id])
    end

    # this bit of ugliness is just here for testing purposes.
    # normally, these values will be populated by the ETL, but
    # for test coverage, we need to be able to create them...
    def self.add_deletion(typeis, typeis_id, id: nil, processed_date: Time.now.to_s, etl_processed_date: nil, typeis_id_value: nil)
      id ||= execute(['select coalesce((select id from api_execution_deletion order by id desc limit 1), 0) + 1'])[0]['?column?'].to_i
      hash = { id: id, typeis: typeis, typeis_id: typeis_id, processed_date: processed_date,
               etl_processed_date: etl_processed_date, typeis_id_value: typeis_id_value }.compact
      columns = hash.keys.map(&:to_s).join(', ')
      values = hash.values.map {|v| v.is_a?(String) ? "'#{v}'" : v }.join(', ')
      execute(["insert into api_execution_deletion (#{columns}) values (#{values})"])
    end

    def self.table_map
      {
        # 'Administrators' => Account, # TODO: fix the trigger to record email address so this can be implemented
        'StoreInstance'          => Store,
        'Store'                  => Retailer,
        'FPURL'                  => Ifeature,
        'MallEvent'              => Event,
        'StorePromos'            => Deal,
        'JobReqs'                => JobListing,
        'Mall'                   => Mall,
        'Companies'              => Company,
        'mf_brandName'           => Brand,
        'Product'                => Product,
        'Operational'            => MallMessage,
        'Hours'                  => MallMessage,
        'CustomCategoryInstance' => Tagging
      }
    end
  end
end
