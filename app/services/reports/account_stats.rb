module Reports
  class AccountStats
    # count the total number of accounts created on or after a given date (by company)
    def self.accounts_created_since(company_id, time = 1.month.ago)
      account_ids(company_id, time).size
    end

    # count the total number of accounts with store affinities for a company
    def self.accounts_with_affinities(company_id, time = 1.month.ago)
      total_affinities(company_id, time).size
    end

    # get the average number of stores hearted
    def self.average_number_of_affinities(company_id, time = 1.month.ago)
      total_affinities(company_id, time).map(&:size).instance_eval { reduce(:+) / size.to_f }
    end

    # get the top most frequently hearted stores by center for a company
    def self.top_five_stores_per_mall(company_id, time = 1.month.ago)
      Company.find(company_id).malls.map do |mall|
        mall_id = mall.id
        store_ids = Store.where(mall_id: mall_id).pluck(:id)
        hearted_stores = []
        Account.where("accounts.created_at >= '#{time}'").joins(:accounts_malls).references(:accounts_malls).where("accounts_malls.mall_id": mall_id).find_each do |account|
          stores = account.user.stores.where(mall_id: mall_id).pluck(:id)
          hearted_stores << stores if stores.any?
        end

        top_five = hearted_stores.flatten.group_by(&:itself).values.max_by(5) {|x| x.size }.map {|a| { store_id: a.first, count: a.size } }

        { mall_id: mall_id, mall_name: mall.name, top_five: top_five }
      end
    end

    # count the total number of accounts created in a date range by mall
    def self.accounts_created_by_mall(mall_id, start_date = 1.month.ago, end_date = 1.day.ago)
      account_ids_by_mall(mall_id, start_date, end_date).size
    end

    def self.accounts_with_affinities_by_mall(mall_id, start_date = 1.month.ago, end_date = 1.day.ago)
      total_affinities_by_mall(mall_id, start_date, end_date).size
    end

    ## helpers ##################################################################################################
    def self.account_ids(company_id, since)
      @account_ids ||= Account.where("accounts.created_at >= '#{since}'").joins(:accounts_malls).references(:accounts_malls)
                              .where("accounts_malls.mall_id": Company.find(company_id).malls.pluck(:id)).pluck(:account_id).uniq
    end

    def self.account_ids_by_mall(mall_id, start_date, end_date)
      start_date = Date.parse(start_date)
      end_date   = Date.parse(end_date)
      Account.where(created_at: start_date..end_date).joins(:accounts_malls)
             .references(:accounts_malls).where("accounts_malls.mall_id": mall_id).uniq
    end

    def self.total_affinities(company_id, time = 1.month.ago)
      @total_affinities ||= []
      unless @total_affinities.any?
        Account.where(id: account_ids(company_id, time)).find_each do |account|
          affinities = account.user.affinities.where(heartable_type: 'Store').pluck(:heartable_id)
          @total_affinities << affinities if affinities.any?
        end
      end
      @total_affinities
    end

    def self.total_affinities_by_mall(mall_id, start_date, end_date)
      total_affinities = [] 
      Account.where(id: account_ids_by_mall(mall_id, start_date, end_date)).find_each do |account|
        affinities = account.user.affinities.where(heartable_type: 'Store').pluck(:heartable_id)
        total_affinities << affinities if affinities.any?
      end
      total_affinities
    end
  end
end
