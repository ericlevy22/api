module PwLegacy::SmsPrograms
  class Subscribe
    def self.call(phone_number, mall_id)
      request = Typhoeus::Request.new(
        'bass.shoptopia.com/webService',
        method: :get,
        params: {
          method: 'subscribe',
          task: 'sendSMSInvite_vm1',
          mall_id: mall_id,
          msisdn: phone_number
        }
      )
      request.run
      response = request.response
      response.success?
    end
  end
end
