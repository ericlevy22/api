module PwLegacy::SewUsers
  class Create
    def self.call(account, mall_id)
      @account = account
      request = Typhoeus::Request.new(
        'nope.shoptopia.com/register',
        method: :get,
        params: {
          v: 2,
          firstName:  extract('name/first'),
          lastName:   extract('name/last'),
          gender:     extract('gender'),
          Address:    extract('location/address'),
          City:       extract('location/city'),
          State:      extract('location/state'),
          zip:        extract('location/zip'),
          birthMonth: extract('date_of_birth/month'),
          birthDay:   extract('date_of_birth/day'),
          birthYear:  extract('date_of_birth/year'),
          Phone:      extract('phone/primary'),
          smsPhone:   extract('phone/sms'),
          password:   "api-#{SecureRandom.hex(32)}",
          optInToAll: false,
          email:      account.email,
          mallID:     mall_id
        }.compact,
        headers: { Accept: "text/html" }
      )
      request.run
      JSON.parse(request.response.response_body)['error'] == false
    end

    private

    def self.extract(path)
      path.split('/').reduce(@account.properties) { |hash, segment| hash.try(:[], segment) }
    end
  end
end
