module PwLegacy::EmailPrograms
  class Unsubscribe
    def self.call(email, list_ids)
      request = Typhoeus::Request.new(
        'apps.mallfinder.com/svc/vm1/subscriptions.cfc',
        method: :get,
        params: {
          method: 'unsubscribe',
          subscriber: email,
          target: list_ids
        }
      )
      request.run
      response = request.response
      response.success?
    end
  end
end
