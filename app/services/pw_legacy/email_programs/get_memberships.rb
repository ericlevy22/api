module PwLegacy::EmailPrograms
  class GetMemberships
    def self.call(identity, mall_id)
      @identity, @mall_id = identity, mall_id

      request = Typhoeus::Request.new(
        'apps.mallfinder.com/svc/emailsvc.cfc',
        method: :get,
        params: {
          method: 'getAllSubscriptions',
          email: identity.identity_value,
          mallid: mall_id
        }
      )
      request.run
      self.normalize_memberships(JSON.parse(request.response.response_body))
    end

    private

    def self.normalize_memberships(input)
      {
        memberships: (input['SUBSCRIPTIONS'] || []).map.with_index do |el, i|
          {
            id: i,
            program_id: el['EPID'],
            identity_id: @identity.id,
            properties: {
              legacy: {
                NAME: el['NAME'],
                DESCRIPTION: el['DESCRIPTION']
              }
            },
            created_at: self.safely_parse_datetime(el['SIGNUPDATE']),
            updated_at: self.safely_parse_datetime(el['SIGNUPDATE'])
          }
        end
      }
    end

    def self.safely_parse_datetime(input)
      DateTime.parse(input)
    rescue ArgumentError
      DateTime.parse("January, 1 1980 13:37:00")
    end
  end
end
