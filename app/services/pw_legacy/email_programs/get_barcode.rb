module PwLegacy::EmailPrograms
  class GetBarcode
    def self.call(user_id, mall_id)
      request = Typhoeus::Request.new(
        'api.mallfinder.com/svc/vm1/insider_incentive.cfc',
        params: {
          method: :issue_barcode,
          nluid: user_id,
          mall_id: mall_id
        }
      )
      request.run
      JSON.parse(request.response.response_body)['ISSUE_BARCODE']
    end
  end
end
