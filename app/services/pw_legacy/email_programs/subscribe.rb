module PwLegacy::EmailPrograms
  class Subscribe
    def self.call(account, list_ids)
      @account = account
      request = Typhoeus::Request.new(
        'apps.mallfinder.com/svc/vm1/subscriptions.cfc',
        method: :get,
        params: {
          method:     'subscribe',
          subscriber: account.email,
          target:     list_ids,
          firstName:  extract('name/first'),
          lastName:   extract('name/last'),
          gender:     extract('gender'),
          Address:    extract('location/address'),
          City:       extract('location/city'),
          State:      extract('location/state'),
          zip:        extract('location/zip'),
          birthMonth: extract('date_of_birth/month'),
          birthDay:   extract('date_of_birth/day'),
          birthYear:  extract('date_of_birth/year'),
          Phone:      extract('phone/primary'),
          smsPhone:   extract('phone/sms')
        }
      )
      request.run
      JSON.parse(request.response.response_body)['NEWSLETTERUSERID']
    end

    private

    def self.extract(path)
      path.split('/').reduce(@account.properties) { |hash, segment| hash.try(:[], segment) }
    end
  end
end
