class SetupAccount
  class NotAuthorizedError < StandardError; end

  def self.call(account_params: {}, properties_params: {}, mall_id: nil)
    if account_params[:facebook_access_token].present?
      @account = Facebook::Login.call(account_params[:facebook_access_token],
                                      properties_params[:properties])
    elsif account_params[:email].present? && account_params[:password].present?
      @account =
        Account.find_by_email(account_params[:email]) ||
        Account.new(email: account_params[:email],
                    password: account_params[:password])
      @account.properties = properties_params.try(:[], :properties) || {} if @account.properties.empty?
      fail NotAuthorizedError unless @account.valid_password?(account_params[:password])
    else
      fail ArgumentError
    end

    @account.current_mall = Mall.find_by_id(mall_id) if mall_id
    if @account.new_record?
      if @account.current_mall
        if @account.confirmed?
          create_legacy_account
          @newsletter_user_id = create_legacy_email_subscriptions
          AccountsMailer.send_welcome_email(@account, newsletter_user_id: @newsletter_user_id)
        elsif @account.current_mall.allow_unconfirmed_access?
          create_legacy_account
          @newsletter_user_id = create_legacy_email_subscriptions
          AccountsMailer.send_welcome_email(@account, newsletter_user_id: @newsletter_user_id)
        else
          @account.send_confirmation_instructions
        end
        @account.malls << @account.current_mall
      end
    else
      if @account.current_mall
        unless @account.malls.include?(@account.current_mall)
          if (@account.confirmed? && @account.malls.empty?) || @account.current_mall.allow_unconfirmed_access?
            @newsletter_user_id = create_legacy_email_subscriptions
            AccountsMailer.send_welcome_email(@account, newsletter_user_id: @newsletter_user_id)
            create_legacy_account
          elsif @account.confirmed?
            @newsletter_user_id = create_legacy_email_subscriptions
            AccountsMailer.send_welcome_email(@account, newsletter_user_id: @newsletter_user_id)
          end
          @account.malls << @account.current_mall
        end
      end
    end

    @account.save if @account.changed?
    @account
  end

  private

  def self.after_confirmation
    @account.confirm_identity
    create_legacy_account
    create_legacy_email_subscriptions
    create_legacy_sms_subscription
  end

  def self.create_legacy_account
    PwLegacy::SewUsers::Create.call(@account, @account.current_mall.id)
  end

  def self.create_legacy_email_subscriptions
    list_ids = @account.current_mall.programs.default.pluck(:legacy_id).join(',')
    PwLegacy::EmailPrograms::Subscribe.call(@account, list_ids) unless list_ids.empty?
  end

  def self.create_legacy_sms_subscription
    return unless mall_id = @account.current_mall.try(:id)
    return unless sms_number = @account.properties.try(:[], 'unsubscribed_sms_number')
    return unless PwLegacy::SmsPrograms::Subscribe.call(sms_number, mall_id)
    identity = Identity.where(identity_value: sms_number, identity_type: 'sms')
                       .first_or_create(identity_value: sms_number, identity_type: 'sms')
    identities << identity
    @account.properties.try(:delete, 'unsubscribed_sms_number')
  end
end
