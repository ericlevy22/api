module Images
  class Upload
    def self.upload_and_build(params)
      params[:url] = if params[:file_data]
        upload(image_from_base64(params[:file_data]))
      elsif params[:url]
        upload(image_from_url(params[:url]))
      end
      Image.new(params.except(:file_name, :file_data))
    end

    def self.image_from_base64(data)
      data.match(%r{^data:(.*?);(.*?),(.*)$})
      temp_file(Base64.decode64($3))
    end

    def self.image_from_url(url)
      temp_file(Typhoeus.get(url).response_body)
    end

    def self.upload(file)
      s3 = Aws::S3::Resource.new
      key = "images/api/retailhubjs/#{File.basename(file)}"
      obj = s3.bucket('placewisesitecontent').object(key)
      if obj.upload_file(file.path, acl:'public-read')
        "https://placewise.imgix.net/#{key}"
      else
        false
      end
    end

    def self.temp_file(file_data)
      temp = Tempfile.new("")
      temp.binmode
      temp << file_data
      temp
    end
  end
end
