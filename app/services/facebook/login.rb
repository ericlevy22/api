require 'koala'

module Facebook
  class Login
    def self.call(token, properties = {})
      graph = Koala::Facebook::API.new(token)
      fb_user = graph.get_object('me')

      account = Account.where(email: fb_user['email']).first_or_initialize do |account|
        account.email = fb_user['email']
        account.password = SecureRandom.hex(32)
      end

      unless account.confirmed?
        account.confirmed_at = DateTime.now if fb_user['verified']
      end

      account.properties = properties || {}
      account.properties['name'] = { 'first' => fb_user['first_name'], 'last' => fb_user['last_name'] }
      account.properties['gender'] = fb_user['gender']

      unless fb_user['birthday'].blank?
        account.properties['date_of_birth'] = self.extract_date_of_birth(fb_user['birthday'])
      end

      account.properties['facebook_cache'] = fb_user

      account.user.identities.where(identity_value: fb_user['id'], identity_type: 'Facebook').first_or_initialize do |identity|
        identity.identity_value = fb_user['id']
        identity_type = 'Facebook'
      end

      account
    rescue Koala::Facebook::AuthenticationError
      return Account.new
    end

    private

    def self.extract_date_of_birth(birthday)
      {
        'month' => birthday[0..1],
        'day' => birthday[3..4],
        'year' => birthday[6..10]
      }.compact
    end
  end
end
