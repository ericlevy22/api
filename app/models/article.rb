class Article < ActiveRecord::Base
  include Comparable
  include Sinceable

  attr_accessor :current_mall

  has_archive
  has_many :taggings, as: :taggable
  has_many :tags, through: :taggings
  has_many :affinities, as: :heartable
  has_many :images, as: :imageable
  has_and_belongs_to_many :malls
  has_and_belongs_to_many :products

  has_many :variants, foreign_key: 'parent_id', class_name: 'Article'

  resourcify

  validates_presence_of :seo_slug

  searchable do
    text :title
    text :subtitle
    text :content
    text :author
  end

  def products
    products = super
    if self.current_mall
      store_ids = self.current_mall.stores.pluck(:id)
      products = products.to_a.reject { |product| (product.stores.pluck(:id) & store_ids).empty? }
    end
    products
  end

  default_scope { order(display_at: :desc).order(id: :desc) }

  def self.current
    where(is_active: true).where('display_at < :time
                                  AND (end_at is null OR end_at > :time)',
                                 time: Time.now)
  end

  def self.by_keyword(keyword)
    search do
      fulltext keyword
    end.results
  end
end
