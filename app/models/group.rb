class Group < ActiveRecord::Base
  has_many :groupings, dependent: :destroy
  validates_presence_of :name

  def self.by_name(name)
    where("lower(name) like ?", "%#{name.downcase}%")
  end

  def malls
    groupings.where(groupable_type: 'Mall')
  end
end
