class User < ActiveRecord::Base
  has_archive

  has_many :accounts, dependent: :destroy
  has_many :identities, dependent: :destroy, autosave: true
  has_many :affinities, dependent: :destroy

  after_initialize :ensure_properties

  def ensure_properties
    self.properties ||= {}
  end

  Affinity::HEARTABLE_TYPES.each do |type|
    has_many type.pluralize.downcase.to_sym, through: :affinities, source: :heartable, source_type: type
  end
end
