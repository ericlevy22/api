class Tag < ActiveRecord::Base
  include Sinceable
  has_archive
  has_many :taggings, as: :taggable
  has_many :tags, through: :taggings
  resourcify
  validates_presence_of :name

  def self.by_name(term)
    where("lower(name) like :term", term: "%#{term.downcase}%")
  end
end
