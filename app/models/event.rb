class Event < ActiveRecord::Base
  include Sinceable

  has_archive

  has_many :groupings
  has_many :groups, through: :groupings

  has_many :images, as: :imageable

  belongs_to :mall

  validates_presence_of :title, :start_at, :end_at, :description, :short_text, :seo_slug

  def self.current
    where(is_active: true).where('display_at <= ?', Time.now)
                          .where('start_at <= ?', Time.now)
                          .where('end_at > ?', Time.now)
  end

  def self.by_date_range(from, to)
    where(is_active: true).where('display_at <= ?', Time.now)
                          .where('start_at <= ?', to)
                          .where('end_at > ?', from)
                          .order(:start_at, :end_at, :display_at, :id)
  end

  def self.by_title(term)
    where("lower(title) like :term or lower(seo_slug) like :term", term: "%#{term.downcase}%")
  end
end
