class Mall < ActiveRecord::Base
  include Sinceable

  has_archive
  resourcify

  belongs_to :location
  belongs_to :company

  has_many :stores
  has_many :theaters
  has_many :deals, -> { distinct }, through: :stores
  has_many :retailers, -> { distinct }, through: :stores
  has_many :products, -> { distinct }, through: :stores
  has_many :hours
  has_many :events
  has_many :ifeatures
  has_many :job_listings, through: :stores
  has_many :press_releases
  has_many :affinities, as: :heartable
  has_many :social_feeds, as: :sociable
  has_many :mall_messages, dependent: :destroy
  has_many :images, as: :imageable
  has_many :contacts, as: :contactable
  has_many :programs, as: :owner

  has_many :accounts_malls
  has_many :accounts, through: :accounts_malls

  has_many :groupings, as: :groupable
  has_many :groups, through: :groupings

  has_and_belongs_to_many :articles

  delegate :latitude, :longitude, to: :location, allow_nil: true
  delegate :distance, to: :location, allow_nil: true
  delegate :allow_unconfirmed_access?, to: :company

  validates_presence_of :location, :company, :name, :city, :state, :zip,
                        :email, :url, :nick_name, :seo_slug

  def url
    if Rails.env.staging?
      self[:url].gsub(/^http:\/\/www\./, 'http://staging.')
    else
      self[:url]
    end
  end

  def self.by_name(term)
    where("lower(name) like :term or lower(sort_name) like :term", term: "%#{term.downcase}%")
  end

  def self.by_group(group_ids)
    group_ids = group_ids.try(:split, ',') || []
    joins(:groupings).where("groupings.group_id": group_ids)
  end
end
