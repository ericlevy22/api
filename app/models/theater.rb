class Theater < ActiveRecord::Base
  belongs_to :store
  belongs_to :mall

  delegate :name, :sort_name, :retailer, :location,
           :seo_slug, :address, :city, :state, :zip,
           :phone, :open_at, :close_at, :images, :url, to: :store
end
