class Company < ActiveRecord::Base
  include Sinceable

  has_archive

  resourcify

  has_many :malls
  has_many :contacts, as: :contactable
  has_many :images, as: :imageable

  validates_presence_of :seo_slug
end
