class Store < ActiveRecord::Base
  include Sinceable

  has_archive
  resourcify

  has_many :taggings, as: :taggable
  has_many :tags, through: :taggings

  has_many :groupings, as: :groupable
  has_many :groups, through: :groupings

  has_many :keywordings, as: :keywordable
  has_many :keywords, through: :keywordings

  belongs_to :retailer
  belongs_to :location
  belongs_to :mall
  has_many :products_stores
  has_many :products, through: :products_stores
  has_many :deals_stores
  has_many :deals, through: :deals_stores
  has_many :job_listings
  has_many :social_feeds, as: :sociable
  has_many :contacts, as: :contactable

  delegate :images, to: :retailer
  delegate :latitude, :longitude, :distance, to: :location, allow_nil: true
  delegate :url, :social_feeds, to: :retailer, prefix: true, allow_nil: true
  delegate :name, to: :mall, prefix: true

  validates_presence_of :retailer, :name, :sort_name, :location, :seo_slug

  default_scope { where.not(retailer_id: 0) }

  alias_method :tags_actual, :tags
  def tags
    tags = tags_actual
    tags.present? ? tags : retailer.tags
  end

  alias_method :keywords_actual, :keywords
  def keywords
    (keywords_actual || []) + retailer.keywords
  end

  def description
    self[:description] || retailer.description
  end

  def url
    self[:url] || retailer.url
  end

  def url_text
    self[:url_text] || retailer.url_text || self[:url]
  end

  def hours
    self[:hours] || mall.hours_text
  end

  searchable do
    integer :id
    integer :retailer_id
    text :name
    latlon(:location) { Sunspot::Util::Coordinates.new latitude, longitude }
  end

  def self.by_keyword(keyword)
    searchResult = search { fulltext keyword }.results
  end
end
