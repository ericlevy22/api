class PressRelease < ActiveRecord::Base
  include Sinceable

  has_archive

  belongs_to :mall
  enum pr_type: { 'Press Release' => 1, 'News/Media Post' => 2 }

  validates_presence_of :mall, :headline, :pr_type, :display_at, :is_active, :content, :seo_slug

  def self.current
    where(is_active: true).where('display_at <= ?', Time.now)
                          .order(display_at: :desc)
  end

  def self.by_name(term)
    where("lower(name) like :term or lower(headline) like :term", term: "%#{term.downcase}%")
  end
end
