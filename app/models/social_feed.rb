class SocialFeed < ActiveRecord::Base
  include Sinceable

  has_archive

  resourcify

  belongs_to :social_network
  delegate :name, to: :social_network
  validates_presence_of :social_network
  validate :must_have_properties

  def must_have_properties
    return if properties.is_a?(Hash) && properties.size > 0
    errors.add(:properties, 'Gotta have at least one property in this hash, such as a url or appid')
  end
end
