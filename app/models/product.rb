class Product < ActiveRecord::Base
  include Sinceable

  has_archive

  has_many :taggings, as: :taggable
  has_many :tags, through: :taggings

  resourcify

  belongs_to :brand
  belongs_to :retailer
  has_and_belongs_to_many :articles
  has_many :products_stores, dependent: :destroy
  has_many :stores, through: :products_stores
  has_many :affinities, as: :heartable
  has_many :images, as: :imageable

  before_validation(on: [:save, :create]) do
    self.expired_at ||= (created_at || Time.now) + 3.months
  end

  validates_presence_of :name, :seo_slug, :retailer

  validate do |product|
    errors.add(:images, 'Must have at least 1 image') unless images.any?
    errors.add(:stores, 'Must have at least 1 store') unless stores.any?
    stores.each do |store|
      errors.add(:stores, 'Stores must all have the same retailer') unless store.retailer.id == retailer.try(:id)
    end
  end

  def expired?
    expired_at <= Time.now
  end

  def self.current
    where("expired_at > ?", Time.now)
  end

  def self.expired
    where("expired_at <= ?", Time.now)
  end

  def self.by_name(term)
    where("lower(name) like :term or lower(seo_slug) like :term", term: "%#{term.downcase}%")
  end
end
