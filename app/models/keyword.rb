class Keyword < ActiveRecord::Base
  has_many :keywordings, dependent: :destroy
  validates_presence_of :name

  def self.by_name(name)
    where("lower(name) like ?", "%#{name.downcase}%")
  end
end
