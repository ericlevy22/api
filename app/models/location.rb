class Location < ActiveRecord::Base
  include Sinceable

  attr_accessor :distance

  has_archive

  resourcify

  has_many :stores
  has_many :malls
  has_many :deals, -> { distinct }, through: :stores
  has_many :products, -> { distinct }, through: :stores

  validates :latitude, presence: true, numericality: true
  validates :longitude, presence: true, numericality: true

  searchable do
    latlon(:location) { Sunspot::Util::Coordinates.new latitude, longitude }
  end

  def self.by_lat_and_long(lat, long, radius)
    search do
      with(:location).in_radius(lat, long, radius, bbox: true)
      order_by_geodist(:location, lat, long)
    end.results
  end

  def calculate_distance(lat, long)
    this_point = [latitude, longitude]
    origin = [lat, long]
    self.distance = Geocoder::Calculations.distance_between(this_point, origin, units: :mi)
  end
end
