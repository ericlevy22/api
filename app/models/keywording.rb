class Keywording < ActiveRecord::Base
  belongs_to :keyword
  belongs_to :keywordable, polymorphic: true
end
