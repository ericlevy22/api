class JobListing < ActiveRecord::Base
  include Sinceable

  has_archive

  resourcify

  belongs_to :store

  delegate :name, :mall_id, to: :store, prefix: true
  delegate :images, to: :store

  validates_presence_of :title, :number_of_openings, :display_at, :end_at, :seo_slug

  def self.current
    where(is_active: true).where('display_at <= ?', Time.now)
                          .where('end_at is null or end_at > ?', Time.now)
  end

  def self.by_mall(mall_id)
    where(mall_id: mall_id)
  end

  def self.by_store(store_id)
    where(store_id: store_id)
  end

  def self.by_title(term)
    where("lower(title) like :term", term: "%#{term.downcase}%")
  end
end
