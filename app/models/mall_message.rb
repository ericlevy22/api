class MallMessage < ActiveRecord::Base
  include Sinceable

  has_archive

  belongs_to :mall

  validates_presence_of :start_at, :end_at, :description

  def self.current
    where('start_at <= ?', Time.now).where('end_at > ?', Time.now)
  end

  def self.by_description(term)
    where("lower(description) like :term", term: "%#{term.downcase}%")
  end
end
