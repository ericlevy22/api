class Membership < ActiveRecord::Base
  resourcify
  has_archive
  
  belongs_to :program
  belongs_to :identity

  validates_presence_of :program, :identity
end
