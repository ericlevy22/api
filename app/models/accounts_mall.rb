class AccountsMall < ActiveRecord::Base
  belongs_to :account
  belongs_to :mall

  validates_presence_of :account, :mall
end
