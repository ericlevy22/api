class Image < ActiveRecord::Base
  has_archive
  has_many :taggings, as: :taggable
  has_many :tags, through: :taggings
  belongs_to :imageable, polymorphic: true
  attr_accessor :file_data

  validates_presence_of :url
end
