module Sinceable
  extend ActiveSupport::Concern

  module ClassMethods
    def since(date)
      table = self.to_s.underscore.pluralize
      date = DateTime.parse(date).strftime('%Y-%m-%d %H:%M:%S')
      where "#{table}.updated_at >= ? or #{table}.created_at >= ?", date, date
    end
  end
end
