class Program < ActiveRecord::Base
  include Sinceable

  has_archive
  resourcify
  belongs_to :owner, polymorphic: true

  has_many :memberships
  has_many :identities, through: :memberships

  enum program_type: { 'Email' => 2 }

  def self.default
    where(is_default_signup_program: true).where(is_active: true)
  end
end
