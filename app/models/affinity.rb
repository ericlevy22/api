class Affinity < ActiveRecord::Base
  HEARTABLE_TYPES = %w(Store Retailer Mall Product Location Deal Brand)

  belongs_to :heartable, polymorphic: true
  belongs_to :user

  validates_presence_of :heartable_id, :heartable_type, :user_id

  has_archive

  def self.by_heartable_type(heartable_type = nil)
    if heartable_type.blank?
      all
    else
      where heartable_type: heartable_type
    end
  end
end
