class Identity < ActiveRecord::Base
  has_archive

  belongs_to :user
  has_many :memberships
  has_many :programs, through: :memberships

  validates_presence_of :user, :identity_value, :identity_type

  default_scope { order(id: :asc) }

  def self.where(opts = :chain, *rest)
    if opts.is_a?(Hash) && opts[:email]
      address, domain = opts.delete(:email).split('@')
      super(
        "identity_type = 'email' and identity_value like lower(:domain) and lower(substring(replace(identity_type = 'email' and identity_value, '.', ''), 1, length(:address))) = lower(:address) and substring(replace(identity_type = 'email' and identity_value, '.', ''), length(:address) + 1, 1) in ('.', '+', '@')",
        address: address.gsub(/(\+.*|\.)/, ''), domain: "%@#{domain}"
      )
    else
      super
    end
  end

  def self.find_by_email(email)
    where(identity_value: email, identity_type: 'email').first
  end
end
