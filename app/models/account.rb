class Account < ActiveRecord::Base
  include Sinceable

  attr_accessor :current_mall

  has_archive

  devise :database_authenticatable, :registerable, :timeoutable,
         :recoverable, :rememberable, :trackable, :validatable,
         :confirmable
  rolify

  belongs_to :user, touch: true, autosave: true
  delegate :properties, :properties=, to: :user

  has_many :identities, through: :user
  has_many :memberships, through: :identities
  has_many :accounts_malls, dependent: :destroy
  has_many :malls, through: :accounts_malls

  after_initialize :ensure_user, :ensure_identity
  before_save :ensure_authentication_token

  default_scope { order(id: :asc) }

  def self.where(opts = :chain, *rest)
    if opts.is_a?(Hash) && opts[:email]
      address, domain = opts.delete(:email).split('@')
      super(
        "email like lower(:domain) and lower(substring(replace(email, '.', ''), 1, length(:address))) = lower(:address) and substring(replace(email, '.', ''), length(:address) + 1, 1) in ('.', '+', '@')",
        address: address.try(:gsub, /(\+.*|\.)/, ''), domain: "%@#{domain}"
      )
    else
      super
    end
  end

  def self.find_by_email(email)
    where(email: email).first
  end

  before_validation do |account|
    account.sanitized_email ||= account.sanitize_email(account.email)
  end

  validate do |account|
    matches = Account.where(email: account.email)
    errors.add(:email, 'has already been taken') unless matches.empty? || matches == [account]
  end

  def confirm_identity
    Identity.where(identity_value: email).where(identity_type: 'email')
            .first_or_create(identity_value: email, identity_type: 'email', user_id: user.id)
            .update_attributes(confirmed_at: confirmed_at)
  end

  protected

  def confirmation_required?
    false
  end

  def sanitize_email(e)
    self.class.sanitize_email(e)
  end

  def self.sanitize_email(e)
    return unless e
    e.gsub(/\.(?=.*@gmail.com)/, '').gsub(/\+.*(?=@)/, '').downcase
  end

  private

  def ensure_authentication_token
    self.authentication_token = generate_authentication_token if authentication_token.blank?
  end

  def generate_authentication_token
    loop do
      token = Devise.friendly_token
      break token unless Account.where(authentication_token: token).first
    end
  end

  def ensure_user
    self.user ||= User.new
  end

  def ensure_identity
    identity = Identity.where(identity_value: email, identity_type: 'email')
                       .first_or_create(identity_value: email, identity_type: 'email', user: self.user)
    self.user.identities << identity
  end
end
