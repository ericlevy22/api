class ContactUsEmail < ActiveRecord::Base
  validates_presence_of :from, :to, :subject, :body

  def process
    if Email::Send.call(self)
      self.sent_at = Time.now
      self.save
    else
      Raven.capture_message("Error sending ContactUsEmail with id: #{self.id}", error: 'Email::Send.call failed')
      Rails.logger.warn "Error sending ContactUsEmail with id: #{self.id}"
    end
  end
end
