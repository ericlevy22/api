class Ifeature < ActiveRecord::Base
  include Sinceable

  has_archive

  resourcify

  belongs_to :mall

  validates_presence_of :headline, :start_at, :end_at, :seo_slug

  def self.current
    where(is_active: true).where('start_at <= ?', Time.now)
                          .where('end_at > ?', Time.now)
  end

  def self.by_keyword(term)
    where("lower(seo_slug) like :term or lower(headline) like :term or lower(tag_line) like :term or lower(description) like :term or lower(sub_heading) like :term", term: "%#{term.downcase}%")
  end
end
