class Tagging < ActiveRecord::Base
  has_archive
  belongs_to :tag
  belongs_to :taggable, polymorphic: true
end
