class Brand < ActiveRecord::Base
  include Sinceable

  has_archive

  resourcify

  has_many :products
  has_many :images, as: :imageable

  validates :name, presence: true, uniqueness: true
  validates :seo_slug, presence: true, uniqueness: true

  def self.by_name(term)
    where("lower(name) like :term", term: "%#{term.downcase}%")
  end
end
