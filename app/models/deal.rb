class Deal < ActiveRecord::Base
  attr_accessor :current_store

  include Sinceable

  has_archive

  has_many :taggings, as: :taggable
  has_many :tags, through: :taggings

  has_many :groupings
  has_many :groups, through: :groupings

  resourcify

  belongs_to :retailer
  has_many :deals_stores, dependent: :destroy
  has_many :stores, through: :deals_stores
  has_many :locations, -> { distinct }, through: :stores
  has_many :affinities, as: :heartable
  has_many :images, as: :imageable

  delegate :name, to: :retailer
  delegate :sort_name, to: :retailer
  delegate :name, to: :retailer, prefix: true
  delegate :sort_name, to: :retailer, prefix: true

  enum sales_type: { 'Sales and Promos' => 11, 'New Arrivals' => 12, 'In-Store Event' => 13, 'Style Notes' => 15 }

  validates_presence_of :retailer, :title, :start_at, :end_at, :description, :seo_slug
  # validates :images,      presence: true # TODO: Need to decide on a way to handle image uploading

  default_scope { order(:end_at) }

  def store_name
    @current_store ? @current_store.name : nil
  end

  def store_sort_name
    @current_store ? @current_store.sort_name : nil
  end

  searchable do
    text :title
    text :name
    text :sort_name
    text :description
  end

  def self.by_keyword(keyword)
    search { fulltext keyword }.results
  end

  def self.current
    where(is_active: true).where('display_at <= ?', Time.now)
                          .where('end_at > ?', Time.now)
  end
end
