class Retailer < ActiveRecord::Base
  include Sinceable

  has_archive
  resourcify

  has_many :stores, dependent: :destroy
  has_many :deals, dependent: :destroy
  has_many :products
  has_many :malls, -> { distinct }, through: :stores
  has_many :affinities, as: :heartable, dependent: :destroy
  has_many :social_feeds, as: :sociable, dependent: :destroy
  has_many :images, as: :imageable, dependent: :destroy
  has_many :contacts, as: :contactable, dependent: :destroy

  has_many :taggings, as: :taggable
  has_many :tags, through: :taggings

  has_many :keywordings, as: :keywordable
  has_many :keywords, through: :keywordings

  validates_presence_of :seo_slug

  def self.by_name(term)
    where("lower(name) like :term or lower(sort_name) like :term", term: "%#{term.downcase}%")
  end
end
