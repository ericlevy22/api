class Contact < ActiveRecord::Base
  include Sinceable

  has_archive

  resourcify

  def self.contactable(id, type)
    where(contactable_id: id, contactable_type: type)
  end

  def self.by_name(term)
    where("lower(name) like :term", term: "%#{term.downcase}%")
  end
end
