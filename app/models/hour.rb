class Hour < ActiveRecord::Base
  belongs_to :mall

  def self.by_range(from, to)
    where('start_at <= ?', to).where('end_at >= ? or end_at is null', from)
                              .order(:start_at, :end_at)
  end

  def self.current_hours(start, mall)
    {
      this_week:  self.effective_hours(start.beginning_of_week, start.end_of_week, mall),
      this_month: self.effective_hours(start.beginning_of_month, start.end_of_month, mall),
      today:      self.effective_hours(start, start, mall)
    }
  end

  def self.effective_hours(from, to, mall)
    base_hours = (from..to).map { |d| { date: d, hours: nil } }.to_a
    default_hours = default_hours(mall, base_hours)
    alternative_hours(mall, default_hours, from, to)
  end

  private

  def self.default_hours(mall, days)
    default_pattern = Hour.where(mall_id: mall.id, is_default: true).first
    return days unless default_pattern
    days.map do |day|
      {
        date:        day[:date],
        day:         Date::DAYNAMES[day[:date].wday],
        hours_text:  default_pattern[Date::DAYNAMES[day[:date].wday].downcase.to_sym],
        description: day[:description]
      }
    end
  end

  def self.alternative_hours(mall, days, from, to)
    patterns = mall.hours.by_range(from.beginning_of_day, to.end_of_day)
    return patterns if patterns.empty?
    days.map do |day|
      patterns.map do |pattern|
        daily_hours = pattern.send Date::DAYNAMES[day[:date].wday].downcase.to_sym
        if daily_hours && day[:date] >= pattern.start_at && day[:date] <= pattern.end_at
          day[:hours_text] = daily_hours
          day[:description] = pattern.description
        end
      end
      day
    end
  end
end
