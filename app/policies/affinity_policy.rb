class AffinityPolicy < ApplicationPolicy
  Scope = Struct.new(:account, :scope) do
    def resolve
      if account
        if account.has_role? :admin
          scope.all
        else
          scope.where(user_id: account.user_id)
        end
      else
        scope.none
      end
    end
  end

  def update?
    false
  end

  def destroy?
    return true if account.has_role? :admin
    return true if account.user_id == record.user_id
  end

  def create?
    if account.current_mall.present? && account.current_mall.allow_unconfirmed_access?
      true
    else
      account.confirmed?
    end
  end
end
