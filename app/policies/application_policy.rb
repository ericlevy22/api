class ApplicationPolicy
  attr_reader :account, :record

  Scope = Struct.new(:account, :scope) do
    def resolve
      if account.has_role? :admin
        scope.all
      elsif account.has_role? :sales, :any
        scope.with_role :sales, account
      elsif account.has_role? :client, :any
        scope.with_role :client, account
      elsif account.has_role? :mall_client, :any
        scope.with_role :mall_client, account
      else
        scope.none
      end
    end
  end

  def initialize(account, record)
    @account = account
    @record = record
  end

  def index?
    return true if account.has_role? :admin
  end

  def show?
    scope.where(id: record.id).exists?
  end

  def create?
    false
  end

  def new?
    create?
  end

  def update?
    false
  end

  def edit?
    update?
  end

  def destroy?
    false
  end

  def scope
    Pundit.policy_scope!(account, record.class)
  end
end
