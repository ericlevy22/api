class TheaterPolicy < ApplicationPolicy
  Scope = Struct.new(:account, :scope) do
    def resolve
      return scope.none unless account.present?
      if account.has_role? :admin
        scope.all
      elsif account.has_role? :store_channel
        mall_ids = account.roles
                          .where(name: 'client')
                          .where(resource_type: 'Mall')
                          .pluck(:resource_id)

        scope.where(mall_id: mall_ids)
      elsif account.has_role? :mall_channel
        mall_ids = account.roles
                          .where(name: 'client')
                          .where(resource_type: 'Mall')
                          .pluck(:resource_id)

        scope.where(mall_id: mall_ids)
      else
        scope.none
      end
    end
  end

  def create?
    return true if account.has_role? :admin
  end

  def update?
    return true if account.has_role? :admin
  end

  def destroy?
    return true if account.has_role? :admin
  end
end
