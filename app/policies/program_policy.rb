class ProgramPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      if account.has_role? :admin
        scope
      elsif account.has_role? :sales, :any
        scope
      elsif account.has_role? :mall_client, :any
        mall_ids = account.roles
                          .where(name: 'mall_client')
                          .where(resource_type: 'Mall')
                          .pluck(:resource_id)
        scope.where(owner_id: mall_ids,
                    owner_type: 'Mall',
                    is_internal_only: false,
                    is_active: true,
                    is_subscribable: true)
      elsif account.has_role? :mall_channel
        mall_ids = account.roles
                          .where(name: 'client')
                          .where(resource_type: 'Mall')
                          .pluck(:resource_id)

        scope.where(owner_id: mall_ids, owner_type: 'Mall', is_internal_only: false, is_active: true)
      else
        scope.none
      end
    end
  end

  def create?
    return true if account.has_role? :admin
  end

  def update?
    return true if account.has_role? :admin
  end

  def destroy?
    return true if account.has_role? :admin
  end
end
