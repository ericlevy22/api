class ContactPolicy < ApplicationPolicy
  Scope = Struct.new(:account, :scope) do
    def resolve
      return scope.none unless account
      if account.has_role? :admin
        scope.all
      elsif account.has_role? :store_channel
        mall_ids, store_ids = [], []
        Mall.where(id: account.roles.where(name: 'client')
                              .where(resource_type: 'Mall')
                              .pluck(:resource_id)).map do |mall|
                                mall_ids << mall.id
                                store_ids += mall.stores.pluck(:id)
                              end

        if mall_ids.any?
          if store_ids.any?
            scope.where(
              <<-EOT
                (contactable_id in (#{store_ids.join(',')}) and contactable_type = 'Store') or
                (contactable_id in (#{mall_ids.join(',')}) and contactable_type = 'Mall')
              EOT
            )
          else
            scope.where(contactable_id: mall_ids, contactable_type: 'Mall')
          end
        else
          scope.none
        end
      elsif account.has_role? :mall_channel
        mall_ids = account.roles.where(name: 'client')
                          .where(resource_type: 'Mall')
                          .pluck(:resource_id)

        scope.where(contactable_id: mall_ids, contactable_type: 'Mall')
      elsif account.has_role? :store_channel
        store_ids = Mall.where(id: account.roles.where(name: 'client')
                                          .where(resource_type: 'Mall')
                                          .pluck(:resource_id)).map { |m| m.stores.pluck(:id) }.flatten

        scope.where(contactable_id: store_ids, contactable_type: 'Store')
      elsif account.has_role? :mall_client, :any
        mall_ids, store_ids = [], []
        Mall.where(id: account.roles.where(name: 'mall_client')
                              .where(resource_type: 'Mall')
                              .pluck(:resource_id)).map do |mall|
                                mall_ids << mall.id
                                store_ids += mall.stores.pluck(:id)
                              end

        if mall_ids.any?
          if store_ids.any?
            scope.where(
              <<-EOT
                (contactable_id in (#{store_ids.join(',')}) and contactable_type = 'Store') or
                (contactable_id in (#{mall_ids.join(',')}) and contactable_type = 'Mall')
              EOT
            )
          else
            scope.where(contactable_id: mall_ids, contactable_type: 'Mall')
          end
        else
          scope.none
        end
      else
        scope.none
      end
    end
  end

  def create?
    return true if account.has_role? :admin
  end

  def update?
    return true if account.has_role? :admin
  end

  def destroy?
    return true if account.has_role? :admin
  end
end
