class TagPolicy < ApplicationPolicy
  Scope = Struct.new(:account, :scope) do
    def resolve
      scope
    end
  end

  def create?
    return true if account.has_role? :admin
  end
end
