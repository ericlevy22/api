class MallMessagePolicy < ApplicationPolicy
  Scope = Struct.new(:account, :scope) do
    def resolve
      if account
        if account.has_role? :admin
          scope.all
        elsif account.has_role? :mall_channel
          mall_ids = account.roles.where(name: 'client')
                                  .where(resource_type: 'Mall')
                                  .pluck(:resource_id)

          scope.where(mall_id: mall_ids)
        elsif account.has_role? :mall_client, :any
          mall_ids = account.roles.where(name: 'mall_client')
                                  .where(resource_type: 'Mall')
                                  .pluck(:resource_id)

          scope.where(mall_id: mall_ids)
        else
          scope.none
        end
      else
        scope.none
      end
    end
  end
end
