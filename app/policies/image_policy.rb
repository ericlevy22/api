class ImagePolicy < ApplicationPolicy
  Scope = Struct.new(:account, :scope) do
    def resolve
      if account.has_role? :admin
        scope.all
      else
        scope.none
      end
    end
  end

  def create?
    return true if account.has_role? :admin
  end

  def destroy?
    return true if account.has_role? :admin
  end
end
