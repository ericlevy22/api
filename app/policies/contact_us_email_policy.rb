class ContactUsEmailPolicy < ApplicationPolicy
  def contact_us?
    return true if account.has_role? :admin
    return true if account.has_role? :mall_client, :any
  end
end
