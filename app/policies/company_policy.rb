class CompanyPolicy < ApplicationPolicy
  def create?
    return true if account.has_role? :admin
  end

  def update?
    return true if account.has_role? :admin
  end

  def destroy?
    return true if account.has_role? :admin
  end
end
