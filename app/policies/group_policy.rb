class GroupPolicy < ApplicationPolicy
  Scope = Struct.new(:account, :scope) do
    def resolve
      scope
    end
  end

  def create?
    return true if account.has_role? :admin
  end

  def update?
    return true if account.has_role? :admin
  end

  def destroy?
    return true if account.has_role? :admin
  end
end
