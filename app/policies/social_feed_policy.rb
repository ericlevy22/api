class SocialFeedPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      return scope.none unless account.present?
      if account.has_role? :admin
        scope
      elsif account.has_role? :sales, :any
        scope
      elsif account.has_role? :mall_client, :any
        mall_ids = account.roles
                          .where(name: 'mall_client')
                          .where(resource_type: 'Mall')
                          .pluck(:resource_id)
        scope.where(sociable_id: mall_ids,
                    sociable_type: 'Mall')
      elsif account.has_role?(:store_channel) && account.has_role?(:mall_channel)
        mall_ids, store_ids, retailer_ids = [], [], []
        Mall.where(id: account.roles.where(name: 'client')
                                    .where(resource_type: 'Mall')
                                    .pluck(:resource_id)).map do |mall|
                                      mall_ids << mall.id
                                      mall.stores.map do |s|
                                        store_ids << s.id
                                        retailer_ids << s.retailer_id
                                      end
                                    end

        if mall_ids.any?
          if store_ids.any?
            if retailer_ids.any?
              scope.where(
                <<-EOT
                  (sociable_id in (#{store_ids.join(',')}) and sociable_type = 'Store')
                  or (sociable_id in (#{retailer_ids.join(',')}) and sociable_type = 'Retailer')
                  or (sociable_id in (#{mall_ids.join(',')}) and sociable_type = 'Mall')
                EOT
              )
            else
              scope.where(
                <<-EOT
                  (sociable_id in (#{store_ids.join(',')}) and sociable_type = 'Store')
                  or (sociable_id in (#{mall_ids.join(',')}) and sociable_type = 'Mall')
                EOT
              )
            end
          else
            scope.where(sociable_id: mall_ids, sociable_type: 'Mall')
          end
        else
          scope.none
        end
      elsif account.has_role? :mall_channel
        mall_ids = account.roles
                          .where(name: 'client')
                          .where(resource_type: 'Mall')
                          .pluck(:resource_id)
        scope.where(sociable_id: mall_ids,
                    sociable_type: 'Mall')
      elsif account.has_role? :store_channel
        store_ids, retailer_ids = [], []
        Mall.where(id: account.roles.where(name: 'client')
                                    .where(resource_type: 'Mall')
                                    .pluck(:resource_id)).map do |mall|
                                      mall.stores.map do |s|
                                        store_ids << s.id
                                        retailer_ids << s.retailer_id
                                      end
                                    end

        if store_ids.any?
          scope.where(
            <<-EOT
              (sociable_id in (#{store_ids.join(',')}) and sociable_type = 'Store')
              or (sociable_id in (#{retailer_ids.join(',')}) and sociable_type = 'Retailer')
            EOT
          )
        else
          scope.none
        end
      else
        scope.none
      end
    end
  end

  def create?
    return true if account.has_role? :admin
  end

  def update?
    return true if account.has_role? :admin
  end

  def destroy?
    return true if account.has_role? :admin
  end
end
