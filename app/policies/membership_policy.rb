class MembershipPolicy < ApplicationPolicy
  # TODO: create appropriate policy rules for memberships!
  def create?
    return true if account.has_role? :admin
  end
end
