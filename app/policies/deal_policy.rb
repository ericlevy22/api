class DealPolicy < ApplicationPolicy
  Scope = Struct.new(:account, :scope) do
    def resolve
      if account.has_role? :admin
        scope.all
      elsif account.has_role? :client, :any
        if account.has_role? :deal_channel
          mall_ids = account.roles
                            .where(resource_type: 'Mall')
                            .pluck(:resource_id)

          scope.includes(:stores)
               .references(:stores)
               .where("stores.mall_id": mall_ids)
        elsif account.has_role? :client, :any
          scope.with_role :client, account
        else
          scope.none
        end
      else
        scope.none
      end
    end
  end

  def create?
    return true if account.has_role? :admin
  end

  def update?
    return true if account.has_role? :admin
  end

  def destroy?
    return true if account.has_role? :admin
  end
end
