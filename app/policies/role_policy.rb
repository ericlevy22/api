class RolePolicy < ApplicationPolicy
  Scope = Struct.new(:account, :scope) do
    def resolve
      if account.has_role? :admin
        scope.all
      else
        scope.none
      end
    end
  end
end
