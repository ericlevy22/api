class AccountPolicy < ApplicationPolicy
  def add_roles?
    if account.has_role? :admin
      return true
    else
      false
    end
  end

  def remove_roles?
    if account.has_role? :admin
      return true
    else
      false
    end
  end

  def update?
    if account.has_role? :admin
      true
    elsif account.has_role? :sales, :any
      false
    elsif account.has_role? :mall_client, :any
      false
    elsif account.confirmed? || account.current_mall && account.current_mall.allow_unconfirmed_access?
      account.id == record.id
    else
      false
    end
  end

  def logout?
    if account.has_role? :admin
      true
    else
      account.id == record.id
    end
  end

  Scope = Struct.new(:account, :scope) do
    def resolve
      if account
        if account.has_role? :admin
          scope.all
        else
          scope.where(id: account.id)
        end
      else
        scope.none
      end
    end
  end
end
