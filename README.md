## [PlaceWise Media](http://placewise.com/) API

*API for External and Internal Applications + Static Sites + API Customers*

[![Code Climate](https://codeclimate.com/repos/539b452369568065ec000494/badges/82cba3b39bd7b1da7be8/gpa.png)](https://codeclimate.com/repos/539b452369568065ec000494/feed)

***

#### [Rake Tasks](http://rake.rubyforge.org/)

This application has a series of helpful rake tasks baked into it.  To find out the full list run ```rake tasks``` from the command prompt! You can pass an environment variable ```TRY=true``` before any rake task except ```rake tasks``` to see which commands will be executed during the process.  This is great for building new rake tasks.

* New rake tasks should be added to the ```lib/tasks/placewise``` folder and have the correct namespace.
* Follow any of the previous examples to copy and paste the namespace configuration.

```
rake placewise:db:recreate[env]                             # Drop and create the current database, the argument [env] = environment
rake placewise:delete:all                                   # Delete ALL temporary and build files
rake placewise:delete:static_site_account_files             # Remove Account Files
rake placewise:docs:all                                     # Run current rspec tests and generate the help docs
rake placewise:seed:build_static_site_account_files         # Build static site account files
rake placewise:seed:client_accounts                         # Seed the current active api users
rake placewise:seed:env[env]                                # Seed the current database, the argument [env] = environment
rake placewise:seed:static_site_accounts                    # Update Active Static Site Accounts
rake placewise:server:start_redis                           # Start Up Redis Manually
rake placewise:solr:reindex                                 # Re-Index current environment
rake placewise:test:run[spec_path]                          # Run current rspec test, the optional [spec_path] arg
rake placewise:test:simplecov                               # Run current rspec tests including Simplecov tests
```
***

#### API Version

The version for the api is set in `config/unsecure.yml` and each release should be tagged in git.

#### API Urls

production: api.placewise.com
staging: api-staging.placewise.com
pending production release: api-release.placewise.com
pending production release backup: api-backup.placewise.com

***

#### Have questions regarding data management?

Please refer the [API-ETL](https://github.com/PlacewiseMedia/API-ETL) Project.

***

#### Looking for the static site accounts on a server?

There will be on for production and one for staging:
```
example path: deploy@apistaging:/home/apps/api/current/db/seeds/accounts
yml files:

db/seeds/accounts/active_api_accounts.seeds.rb
db/seeds/accounts/active_api_ajax_static_site_accounts.seeds.rb
db/seeds/accounts/remove_api_ajax_static_site_accounts.seeds.rb
db/seeds/accounts/active_api_ajax_static_site_accounts.yml
```

***

#### Want to turn on various external services?

```
LOG_ERRORS_TO_EXTERNAL_SERVICE=true rake placewise:alert:verify_etl
```
