module HelperFunctions
  # Permitted environments
  def environments(env, restrict: nil)
    env = 'development' unless env.present?
    approved_environments = %w(development development_loading test production production_loading staging staging_loading)
    if approved_environments.include?(env)
      if env == 'production' && restrict == 'production'
        error("#{env.capitalize} environment not allowed for this task")
        exit
      else
        puts
        msg('Environment parameter is valid')
        return env
      end
    else
      error('Invalid environment parameter')
      exit
    end
  end

  # Console message handler
  def msg(txt, periods: 'yes', new_line: 'yes')
    unless Rails.env.test?
      txt += '...' if periods == 'yes'
      puts '===> ' + txt
      puts if new_line == 'yes'
    end
  end

  def error(reason)
    puts "**** ERROR! ABORTING: #{reason}!"
  end

  # Execute Shell Commands
  def shell(cmd, step: nil)
    msg("Starting step #{step}", new_line: 'no') if step.present?
    if ENV['TRY']
      puts '**>> ' + cmd
    else
      puts '-->> ' + cmd
      system(cmd)
    end
    msg("#{step} completed!", periods: 'no')
  end

  def start_time(configured_start_time)
    today = Date.today
    Time.local(today.year, today.month, today.day, configured_start_time)
  end

  def stop_time(configured_stop_time)
    today = Date.today
    Time.local(today.year, today.month, today.day, configured_stop_time)
  end

end
