class String
  def slugify
    String.new(self).slugify!
  end

  def slugify!
    gsub!(/['`]/, '')
    gsub!(/\s*@\s*/, ' at ')
    gsub!(/\s*&\s*/, ' and ')
    parameterize
  end
end
