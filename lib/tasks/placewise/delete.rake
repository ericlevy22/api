namespace :placewise do
  namespace :delete do
    if Rails.env.development?
      desc 'Delete ALL temporary and build files'
      task :tmp_files do
        msg('Deleting ALL Tmp files and directories')
        FileUtils.rm_rf ['tmp/coverage']
        FileUtils.rm_rf ['coverage']
      end
    end

    desc 'Remove Account Files'
    task :static_site_account_files do
      msg('Deleting static site account files')
      FileUtils.rm_rf ['db/seeds/accounts/active_api_ajax_static_site_accounts.seeds.rb']
      FileUtils.rm_rf ['db/seeds/accounts/remove_api_ajax_static_site_accounts.seeds.rb']
      FileUtils.rm_rf ['db/seeds/accounts/active_api_ajax_static_site_accounts.yml']
    end

    desc 'Remove stores with invalid retailers'
    task bad_stores: :environment do
      msg('Deleting stores with an invalid retailer')
      Store.find_each do |store|
        store.destroy if store.retailer.nil?
      end
      msg('deletion completed')
    end

    desc 'Process list of records marked for deletion by legacy systems'
    task from_etl: :environment do
      start_time = start_time(AppConfig.jobs['start_time_delete_data'])
      stop_time = stop_time(AppConfig.jobs['end_time_delete_data'])
      if (start_time..stop_time).cover? Time.now
        msg 'Processing deletions...'
        Etl::Deletions.get_deletions.each do |d|
          model = Etl::Deletions.table_map[d['typeis']]
          unless model.present?
            msg "no mapping for #{model}:  skipping #{d['typeis_id']}..."
            return
          end
          msg "Processing deletions for model: #{model}, deletion id: #{d['id']}"
          case model.to_s
          when JobListing.to_s
            model.where(legacy_requisition_id: d['typeis_id']).map {|record| record.destroy(for_real: true) }
          when MallMessage.to_s
            model.where(mall_id: d['typeis_id'], message_type: d['typeis'].downcase).map {|record| record.destroy(for_real: true) }
          when Tagging.to_s
            store = Store.find_by_id(d['typeis_id'])
            if store.present?
              # store.tag_list.remove(d['typeis_id_value'])
              store.taggings.joins(:tag).where("tags.name": d['typeis_id_value']).map(&:destroy)
              # store.save
            end
          else
            model.where(id: d['typeis_id']).map {|record| record.destroy(for_real: true) }
          end
          Etl::Deletions.resolve_deletion(d['id'])
        end
        msg 'Processing completed'
      end
    end
  end
end
