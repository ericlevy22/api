namespace :placewise do
  namespace :gracenote do
    desc 'Find a match for one of our Stores in Gracenote::OnConnect'
    task :"theater-match" => :environment do
      puts "What's the Store's ID?"
      store_id = STDIN.gets.chomp
      store = Store.find_by_id(store_id)
      if store
        puts "[[ #{store.id}: #{store.name} ]]\n"
        theaters = Gracenote::OnConnect::Theater.find_by_zip(store.zip)
        theaters.map! { |t| { name: t['name'], external_key: t['theatreId'] } }
        theaters.each.with_index do |t, i|
          puts "#{i}: #{t[:name]}"
        end
        puts "What's the match's number?"
        n = STDIN.gets.chomp
        choice = theaters[n.to_i]
        if choice
          puts "Match! #{choice[:name]}"
          theater = Theater.new(store: store, mall: store.mall, external_key: choice[:external_key])
          if theater.save
            puts 'Success!'
          else
            puts 'Something went wrong...'
          end
        else
          puts 'No match!'
        end
      else
        puts "Store not found... #{store_id}"
      end
    end
  end
end
