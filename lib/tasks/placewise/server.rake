namespace :placewise do
  namespace :server do
    desc 'Start Up Redis Manually'
    task :start_redis do
      HelperFunctions.msg('Starting up the Redis DB')
      HelperFunctions.shell('redis-server /usr/local/etc/redis.conf')
    end
  end
end
