namespace :placewise do
  namespace :"contact-us" do
    desc 'Process a batch of contact-us emails'
    task process: :environment do
      ContactUsEmail.where(sent_at: nil).find_each(&:process)
    end
  end
end
