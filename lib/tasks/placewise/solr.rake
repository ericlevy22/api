namespace :placewise do
  namespace :solr do
    desc 'Re-Index current environment'
    task reindex: :environment do
      HelperFunctions.msg('Time to reindex the current solr classes')

      logfile = "#{Rails.root}/log/rake-reindex-solr.log"
      dir = File.dirname("#{Rails.root}/log/rake-reindex-solr.log")

      FileUtils.mkdir_p(dir) unless File.directory?(dir)
      logger = Logger.new(logfile)

      start_time = start_time(AppConfig.jobs['start_time_solr'])
      stop_time = stop_time(AppConfig.jobs['end_time_solr'])

      if (start_time..stop_time).cover? Time.now
        HelperFunctions.shell("RAILS_ENV=#{Rails.env} bundle exec rake sunspot:solr:reindex")
        logger.info 'Solr Successfully Re-Indexed'
      end
    end
  end
end
