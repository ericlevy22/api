namespace :placewise do
  namespace :archive do
    desc 'Archive all expired records'
    task all: [:articles, :deals, :events, :ifeatures,
               :job_listings, :mall_messages, :products]

    task articles: :environment do |t|
      n = Article.where(end_at: Time.at(0)...Time.now).find_each.map(&:archive).compact.size
      puts "Archived #{n} articles..."
    end

    task deals: :environment do |t|
      n = Deal.where(end_at: Time.at(0)...Time.now).find_each.map(&:archive).compact.size
      puts "Archived #{n} deals..."
    end

    task events: :environment do |t|
      n = Event.where(end_at: Time.at(0)...Time.now).find_each.map(&:archive).compact.size
      puts "Archived #{n} events..."
    end

    task ifeatures: :environment do |t|
      n = Ifeature.where(end_at: Time.at(0)...Time.now).find_each.map(&:archive).compact.size
      puts "Archived #{n} ifeatures..."
    end

    task job_listings: :environment do |t|
      n = JobListing.where(end_at: Time.at(0)...Time.now).find_each.map(&:archive).compact.size
      puts "Archived #{n} job_listings..."
    end

    task mall_messages: :environment do |t|
      n = MallMessage.where(end_at: Time.at(0)...Time.now).find_each.map(&:archive).compact.size
      puts "Archived #{n} mall_messages..."
    end

    task products: :environment do |t|
      n = Product.expired.find_each.map(&:archive).compact.size
      puts "Archived #{n} products..."
    end
  end
end
