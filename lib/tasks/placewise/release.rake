namespace :placewise do
  desc 'Create a new release of the API'
  task release: [:environment] do |_, args|
    #####################################################################
    #  ensure we're on the right branch
    #####################################################################
    branch = `git rev-parse --abbrev-ref HEAD`.strip

    unless ['master', 'develop'].include?(branch)
      puts "Releases should be made from master or develop."
      next
    end

    #####################################################################
    #  determine version
    #####################################################################
    input = ARGV[1].try(:split, '=').try(:last).try(:downcase)
    old_version = YAML.load_file("#{Rails.root}/config/unsecure.yml")['defaults']['settings']['version']
    major, minor, patch = *old_version.split('.').map { |segment| segment[/\d+/].to_i }
    new_version = if input == 'major'
      puts 'Major Version!!'
      "v#{major + 1}.0.0"
    elsif input == 'minor'
      puts 'Minor Version'
      "v#{major}.#{minor + 1}.0"
    elsif input == 'patch'
      puts 'Patch'
      "v#{major}.#{minor}.#{patch + 1}"
    else
      puts "usage:  rake placewise:release type=[version_type] [message='Message for release tag']"
    end

    #####################################################################
    #  make the commits
    #####################################################################
    message = ARGV[2].try(:split, '=').try(:last) || "Release #{new_version}"

    if new_version
      puts "#{old_version} -> #{new_version}"

      unless input == 'patch'
        puts "Rebuilding docs..."
        docs = `bundle exec rake placewise:docs:all`
        unless docs[/0 failures/]
          puts "FAILED to build docs!"
          next
        end
        sh "git add #{Rails.root}/doc"
        sh "git commit -m 'regenerate docs' -n"
      end

      config = File.read("#{Rails.root}/config/unsecure.yml")
      File.open("#{Rails.root}/config/unsecure.yml", 'w') do |f|
        f.write(config.gsub(old_version, new_version))
      end

      sh "git add #{Rails.root}/config/unsecure.yml"
      sh "git commit -m 'bump version' -n"

      sh "git tag -a #{new_version} -m '#{message}'"
      sh "git push"
      sh "git push --tags"
    end
  end
end
