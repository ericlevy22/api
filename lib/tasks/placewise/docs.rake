namespace :placewise do
  namespace :docs do
    desc 'Run current rspec tests and generate the help docs'
    task :all do
      HelperFunctions.msg('Generating the docs')
      HelperFunctions.shell('bundle exec ./bin/rspec spec/features --format RspecApiDocumentation::ApiFormatter')
    end
  end
end
