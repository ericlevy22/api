namespace :placewise do
  namespace :db do
    desc 'Drop and create the current database, the argument [env] = environment.'
    task :recreate, [:env] do |_t, args|
      env = HelperFunctions.environments(args.env)
      HelperFunctions.msg("Dropping the #{env} database")
      HelperFunctions.shell("RAILS_ENV=#{env} rake db:drop", step: '1/3')
      HelperFunctions.msg("Creating the #{env} database")
      HelperFunctions.shell("RAILS_ENV=#{env} rake db:create", step: '2/3')
      HelperFunctions.msg("Running the #{env} database migrations")
      HelperFunctions.shell("RAILS_ENV=#{env} rake db:migrate", step: '3/3')
    end
  end
end
