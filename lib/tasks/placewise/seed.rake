namespace :placewise do
  namespace :seed do
    desc 'Seed the current database, the argument [env] = environment.'
    task :env, [:env] do |_t, args|
      HelperFunctions.msg("Time to seed the #{args.env} database")
      HelperFunctions.shell("rake db:seed:#{args.env}")
    end

    desc 'Seed the current active client accounts'
    task :client_accounts do
      HelperFunctions.msg('Time to add the active api users to the database')
      HelperFunctions.shell('rake db:seed:active_api_client_account')
    end

    desc 'Seed the current active placewise internal accounts'
    task :placewise_internal_accounts do
      HelperFunctions.msg('Time to add the active api users to the database')
      HelperFunctions.shell('rake db:seed:active_api_placewise_internal_account')
    end

    desc 'CRUD Active Static Site Accounts'
    task :static_site_accounts do
      HelperFunctions.msg('CRUD Active Static Site Accounts')
      HelperFunctions.shell('rake db:seed:static_sites_ajax_accounts')
    end
  end
end
