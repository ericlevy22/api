namespace :placewise do
  namespace :test do
    desc 'Run current rspec test, the optional [spec_path] arg'
    task :run, [:spec_path] do |_t, args|
      spec_path = args.spec_path || ''
      spec_msg = args.spec_path || 'all'
      Rake::Task['huedio:delete:all'].invoke
      HelperFunctions.msg("Running #{spec_msg} rspec tests")
      HelperFunctions.shell("RAILS_ENV=test bundle exec ./bin/rspec #{spec_path}")
    end

    desc 'Run current rspec tests including Simplecov tests'
    task :simplecov do
      HelperFunctions.msg('Running all rspec tests including Simplecov tests')
      Rake::Task['placewise:delete:all'].invoke
      HelperFunctions.shell('COVERAGE=true RAILS_ENV=test bundle exec ./bin/rspec')
      HelperFunctions.msg("Let's see how we are doing")
      if agree('Do you need me to launch your browser to view coverage details? ( yes or no )')
        HelperFunctions.shell('open tmp/coverage/index.html')
      else
        HelperFunctions.msg('OK FINE! I will not launch your browser for you...')
      end
    end

    desc 'Run rubocop which is a ruby static code analyzer'
    task :rubocop, [:file_path] do |_t, args|
      file_path = args.file_path || ''
      HelperFunctions.msg('Running rubocop to enforce many of the ruby guidelines')
      HelperFunctions.shell("bundle exec rubocop #{file_path} --config=.rubocop.yml")
    end
  end
end
